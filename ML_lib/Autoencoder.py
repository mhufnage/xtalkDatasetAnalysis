__all__ = ["Autoencoder"]
#
# Autoencoder with torch
#


import numpy as np
import torch
import torch.nn as nn


def getAutoencoderNeuronsList(inputSize, nNeuronsInput, nLayers):
    neuronsList = [inputSize, nNeuronsInput]

    if 2**nLayers > nNeuronsInput:
        raise ValueError("Using binary division for neurons in each layer, the number of layers (2**layers) is bigger than initial number of neurons.\nUse bigger nNeuronsInput or smaller nLayers!")

    for binNeuPow in range(1, nLayers+1):
        neuronsList.append(int(nNeuronsInput/2**binNeuPow))
    # print(neuronsList)
    #       Encoder,    Decoder
    return neuronsList, neuronsList[::-1]

class Autoencoder_Loop(torch.nn.Module):
    '''
    - Last layer can be ReLU (after linear) or Linear.
    - Loss functions: mse, rmse

    '''
    # https://medium.com/analytics-vidhya/creating-an-autoencoder-with-pytorch-a2b7e3851c2c
    # def __init__(self, learningRate=1e-3, encoderList=[10,6,2], decoderList=[2,6,10], bottleNeck=5, actFuncHidden=nn.ReLU(), lossfunction='mse', lastLayer='linear',inScaler=1, outScaler=1 ):
    def __init__(self, learningRate=1e-3, encoderList=[10,6,2], decoderList=[2,6,10], bottleNeck=5, actFuncHidden=nn.ReLU(), lossfunction='mse', lastLayer='linear'):
        super(Autoencoder_Loop, self).__init__() 

        self.learningRate   = learningRate
        # self.inScaler       = inScaler
        # self.outScaler      = outScaler
        self.bottleNeckSize = bottleNeck

        encModuleList = nn.ModuleList()
        for l in range(len(encoderList)-1):
            encModuleList.append(nn.Linear(encoderList[l], encoderList[l+1]))
            encModuleList.append(actFuncHidden)
        self.encoder = nn.Sequential(*encModuleList)

        # bottleNeck
        self.bottleNeck = torch.nn.Sequential(
            torch.nn.Linear(encoderList[-1], self.bottleNeckSize),
            torch.nn.ReLU(False),
        )

        # decoder
        decModuleList = nn.ModuleList()
        for l in range(len(decoderList)-1):
            if l==0:
                decModuleList.append(nn.Linear(self.bottleNeckSize, decoderList[l+1]))
                decModuleList.append(actFuncHidden)
            elif l>0 and not(l == (len(decoderList)-2)):
                decModuleList.append(nn.Linear(decoderList[l], decoderList[l+1]))
                decModuleList.append(actFuncHidden)
            elif (l == (len(decoderList)-2)) and (lastLayer=='linear'):
                decModuleList.append(nn.Linear(decoderList[l], decoderList[l+1]))
            elif (l == (len(decoderList)-2)) and (lastLayer=='relu'):
                decModuleList.append(nn.Linear(decoderList[l], decoderList[l+1]))
                decModuleList.append(actFuncHidden)

        self.decoder = nn.Sequential(*decModuleList)
        
        #
        # self.optimizer = torch.optim.Adam(self.parameters(), lr=self.learningRate, weight_decay=1e-5)
        # self.optimizer = torchOptimizer
        
        # if lossfunction=='mse':
        self.criterion = nn.MSELoss()
   

    def forward(self, inData):
        x = inData
        encoded  = self.encoder(x)
        coded    = self.bottleNeck(encoded)
        decoded  = self.decoder(coded)
        output   = decoded
        return output


# class Autoencoder_Loop(torch.nn.Module):
#     '''Last layer can be ReLU (after linear) or Linear.
#     '''
#     # https://medium.com/analytics-vidhya/creating-an-autoencoder-with-pytorch-a2b7e3851c2c
#     def __init__(self, learningRate=1e-3, encoderList=[10,6,2], decoderList=[2,6,10], bottleNeck=5, actFuncHidden=nn.ReLU(), lastLayer='linear',inScaler=1, outScaler=1 ):
#         super(Autoencoder_Loop, self).__init__() 
#         # self.batchSize      = batchSize
#         self.learningRate   = learningRate
#         self.inScaler       = inScaler
#         self.outScaler      = outScaler
#         self.bottleNeckSize = bottleNeck


#         # list of modules size test:
#         # if len(actFuncList) >= len(encoderList):
#         #     raise Warning("size of actFuncList >= encoder layers list.")
#         # if len(actFuncHidden) == 0:
#         #     raise ValueError("len(actFuncList) == 0 !")

#         encModuleList = nn.ModuleList()
#         for l in range(len(encoderList)-1):
#             encModuleList.append(nn.Linear(encoderList[l], encoderList[l+1]))
#             # if l != (len(encoderList)-2):
#             encModuleList.append(actFuncHidden)
#             # else:
#             #     self.encoder.append(nn.Linear())
#         self.encoder = nn.Sequential(*encModuleList)

#         # bottleNeck
#         self.bottleNeck = torch.nn.Sequential(
#             torch.nn.Linear(encoderList[-1], self.bottleNeckSize),
#             torch.nn.ReLU(False),
#         )

#         # decoder
#         decModuleList = nn.ModuleList()
#         for l in range(len(decoderList)-1):
#             if l==0:
#                 decModuleList.append(nn.Linear(self.bottleNeckSize, decoderList[l+1]))
#             else:
#                 decModuleList.append(nn.Linear(decoderList[l], decoderList[l+1]))
#             # if l != (len(decoderList)-2):
#             decModuleList.append(actFuncHidden)
#             # else:
#             #     self.decoder.append(nn.Linear())
#         self.decoder = nn.Sequential(*decModuleList)
        
#         #
#         # self.optimizer = torch.optim.Adam(self.parameters(), lr=self.learningRate, weight_decay=1e-5)
#         # self.optimizer = torchOptimizer
        
#         self.criterion = nn.MSELoss()
   

#     def forward(self, inData):
#         x = inData#*self.inScaler
#         encoded  = self.encoder(x)
#         coded    = self.bottleNeck(encoded)
#         decoded  = self.decoder(coded)
#         output      = decoded#*self.outScaler
#         return output


# class Autoencoder_Loop(torch.nn.Module):
#     # https://medium.com/analytics-vidhya/creating-an-autoencoder-with-pytorch-a2b7e3851c2c
#     def __init__(self, learningRate=1e-3, encoderList=[10,6,2], decoderList=[2,6,10], actFuncHidden=nn.ReLU(), inScaler=1, outScaler=1):
#         super(Autoencoder_Loop, self).__init__() 
#         # self.batchSize      = batchSize
#         self.learningRate   = learningRate
#         self.inScaler       = inScaler
#         self.outScaler      = outScaler


#         # list of modules size test:
#         # if len(actFuncList) >= len(encoderList):
#         #     raise Warning("size of actFuncList >= encoder layers list.")
#         # if len(actFuncHidden) == 0:
#         #     raise ValueError("len(actFuncList) == 0 !")

#         encModuleList = nn.ModuleList()
#         for l in range(len(encoderList)-1):
#             encModuleList.append(nn.Linear(encoderList[l], encoderList[l+1]))
#             if l != (len(encoderList)-2):
#                 encModuleList.append(actFuncHidden)
#             # else:
#             #     self.encoder.append(nn.Linear())
#         self.encoder = nn.Sequential(*encModuleList)

#         # decoder
#         decModuleList = nn.ModuleList()
#         for l in range(len(decoderList)-1):
#             decModuleList.append(nn.Linear(decoderList[l], decoderList[l+1]))
#             if l != (len(decoderList)-2):
#                 decModuleList.append(actFuncHidden)
#             # else:
#             #     self.decoder.append(nn.Linear())
#         self.decoder = nn.Sequential(*decModuleList)
        
#         #
#         self.optimizer = torch.optim.Adam(self.parameters(), lr=self.learningRate, weight_decay=1e-5)
#         self.criterion = nn.MSELoss()


#     def forward(self, inData):
#         x = inData*self.inScaler
#         encoded  = self.encoder(x)
#         decoded  = self.decoder(encoded)
#         output      = decoded*self.outScaler
#         return output


class Autoencoder(torch.nn.Module):
    # https://medium.com/analytics-vidhya/creating-an-autoencoder-with-pytorch-a2b7e3851c2c
    # def __init__(self, epochs=100, batchSize=128, learningRate=1e-3, nCellsEta=3, nCellsPhi=3, neu1=10):
    def __init__(self, learningRate=1e-3, nCellsEta=3, nCellsPhi=3, neu1=10):
        super(Autoencoder, self).__init__() 
        # self.epochs         = epochs
        # self.batchSize      = batchSize
        self.learningRate   = learningRate
        self.nCellsEta      = nCellsEta
        self.nCellsPhi      = nCellsPhi
        self.nSamples       = 4
        self.neu1           = neu1

        # encoder
        self.encoder = torch.nn.Sequential(
            torch.nn.Linear(self.nSamples * self.nCellsEta * self.nCellsPhi, self.neu1),
            torch.nn.ReLU(True),
            torch.nn.Linear(self.neu1, int(self.neu1/2)),
            torch.nn.ReLU(True)
        )

        # decoder
        self.decoder = torch.nn.Sequential(
            torch.nn.Linear(int(self.neu1/2), self.neu1 ),
            torch.nn.ReLU(True),
            torch.nn.Linear(self.neu1, self.nSamples * self.nCellsEta * self.nCellsPhi),
            torch.nn.ReLU(True)
        )
        
        #
        self.optimizer = torch.optim.Adam(self.parameters(), lr=self.learningRate, weight_decay=1e-5)
        self.criterion = nn.MSELoss()


    def forward(self, x):
        encoded  = self.encoder(x)
        decoded  = self.decoder(encoded)
        return decoded

    # def trainModel(self):

# #
# # ML auxiliary functions
# #

# def checkpoint(model, optimizer, filename, epoch, val_loss):
#     '''Save the model.'''
#     torch.save({
#         # 'optimizer'     : model.optimizer.state_dict(),
#         'optimizer'     : optimizer.state_dict(),
#         'model'         : model.state_dict(),
#         'bestEpoch'     : epoch,
#         'bestValLoss'   : val_loss,
#     }, filename)

# def resumeModel(model, optimizer, filename):
#     '''Load the model.'''
#     theCheckpoint   = torch.load(filename)
#     model.load_state_dict(theCheckpoint['model'])
#     optimizer.load_state_dict(theCheckpoint['optimizer'])
    
#     return theCheckpoint['bestValLoss']
# # def resumeModel(model, optimizer, filename):
# #     '''Load the model.'''
# #     theCheckpoint   = torch.load(filename)
# #     model.load_state_dict(theCheckpoint['model'])
# #     optimizer.load_state_dict(theCheckpoint['optimizer'])

# #
# #    Early stopper
# #

# class EarlyStopper:
#     def __init__(self, patience=1, min_delta=0):
#         self.patience = patience
#         self.min_delta = min_delta
#         self.counter = 0
#         self.min_validation_loss = np.inf
#         self.bestEpoch = -1
#         self.tmpModel = 0

#     def early_stop(self, validation_loss, epoch, model, optimizer, bestModelPath='bestModel.pth'):

#         # if validation loss is smaller than min val loss, reset.
#         if validation_loss < self.min_validation_loss:
#             self.min_validation_loss    = validation_loss
#             self.counter                = 0
#             self.bestEpoch              = epoch
#             # checkpoint(model, optimizer, bestModelPath, epoch, validation_loss)
#             self.tmpModel               = model

#         # else if, increase the patience counter.
#         elif validation_loss > (self.min_validation_loss + self.min_delta):
#             self.counter    += 1
#             if self.counter >= self.patience:
#                 # checkpoint(model, optimizer, bestModelPath, epoch, validation_loss)
#                 return True
#         return False
    
