__all__ = ["MLP"]

#
# Multilayer perceptron with torch
#

import numpy as np
import torch
import torch.nn as nn

from ML_lib.ML_custom_auxiliar import EarlyStopper, checkpoint, resumeModel


class MLP_FF(nn.Module):
    '''
    mlpType can be: ['sampleInSampleOut', 'sampleInEnergyOut']
    '''
    
    def __init__(self, neuronsList=[45, 4] , actFuncHidden=nn.ReLU(), learningRate=1e-3, mlpType='sampleInSampleOut'):
        super(MLP_FF, self).__init__() 
        
        self.lr         = learningRate
        self.mlpType    = mlpType
        
        # Create the input and hidden layers
        neuModuleList = nn.ModuleList()
        for l in range(len(neuronsList)-1):
            neuModuleList.append( nn.Linear(neuronsList[l], neuronsList[l+1]) )
            neuModuleList.append(actFuncHidden)
        
        # Link hidden layers with output
        if self.mlpType=='sampleInSampleOut':
            neuModuleList.append(nn.Linear(neuronsList[-1], neuronsList[0]))
            
        elif self.mlpType=='sampleInEnergyOut':
            raise AttributeError('mlp type sampleInEnergyOut not implemented yet!')
        else:
            raise AttributeError('mlp type {} not accepted!'.format(self.mlpType))
        
        self.mlp = nn.Sequential(*neuModuleList)
        
        self.criterion = nn.MSELoss()
        
        
    def forward(self, inData):
        output = self.mlp(inData)
        return output


def neuListToFileText(neuList):
    textcenter      = ''.join(['{}-'.format(n) for n in neuList[:]])
    textfinal       = textcenter+str(neuList[0])    
    return textfinal
    
class torch_OFCEnergy_LZT(nn.Module):
    '''
    its the torch model with an OFC coeff.
    
    '''
    
    def __init__(self, neta=3, nphi=3, nsamples=5):
        super(torch_OFCEnergy_LZT, self).__init__() 
        
        filter_weigths = []


        filter_weigths = [ -0.7679187573846364, 0.0766663041985699, 0.8840333207118379, 0.2575801038342427, -0.45036097136001446 ]
        
        # all_cells_filter_weights = []
        
        # for 9 cells, add the OFC coeff for each one of them
        # as for LZT they are all the same, just repeat the pattern of weights
        # neuModuleList = nn.ModuleList()
        
        self.OFC       = torch.tensor(filter_weigths, requires_grad=False, dtype=torch.float) # from lzt main
            
        self.filter1 = nn.Linear(nsamples, 1, bias=False)
        self.filter1.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter2 = nn.Linear(nsamples, 1, bias=False)
        self.filter2.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter3 = nn.Linear(nsamples, 1, bias=False)
        self.filter3.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter4 = nn.Linear(nsamples, 1, bias=False)
        self.filter4.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter5 = nn.Linear(nsamples, 1, bias=False)
        self.filter5.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter6 = nn.Linear(nsamples, 1, bias=False)
        self.filter6.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter7 = nn.Linear(nsamples, 1, bias=False)
        self.filter7.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter8 = nn.Linear(nsamples, 1, bias=False)
        self.filter8.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter9 = nn.Linear(nsamples, 1, bias=False)
        self.filter9.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        
        self.criterion = nn.MSELoss()
        
    def forward(self, inData):
        
        x1=self.filter1(inData[:,0:5])
        x2=self.filter2(inData[:,5:10])
        x3=self.filter3(inData[:,10:15])
        x4=self.filter4(inData[:,15:20])
        x5=self.filter5(inData[:,20:25])
        x6=self.filter6(inData[:,25:30])
        x7=self.filter7(inData[:,30:35])
        x8=self.filter8(inData[:,35:40])
        x9=self.filter9(inData[:,40:45])        
        
        return torch.transpose(torch.stack((x1,x2,x3,x4,x5,x6,x7,x8,x9)),dim0=0, dim1=1)
    
class torch_OFCTime_LZT(nn.Module):
    '''
    its the torch model with an OFC coeff.
    
    '''
    
    def __init__(self, scaler=torch.tensor(1.), neta=3, nphi=3, nsamples=5, samplingNoise=60):
        super(torch_OFCTime_LZT, self).__init__() 
        
        self.samplingNoise = samplingNoise
        self.scaler        = scaler
        
        filter_weigths = []

        filter_weigths = [ -0.22673589675790157, -18.897965812858967, 1.1980699915128548, 10.935880735903003, 5.0777868302019415 ]
        
        # all_cells_filter_weights = []
        
        # for 9 cells, add the OFC coeff for each one of them
        # as for LZT they are all the same, just repeat the pattern of weights
        # neuModuleList = nn.ModuleList()
        
        self.OFC       = torch.tensor(filter_weigths, requires_grad=False, dtype=torch.float) # from lzt main
            
        self.filter1 = nn.Linear(nsamples, 1, bias=False)
        self.filter1.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter2 = nn.Linear(nsamples, 1, bias=False)
        self.filter2.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter3 = nn.Linear(nsamples, 1, bias=False)
        self.filter3.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter4 = nn.Linear(nsamples, 1, bias=False)
        self.filter4.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter5 = nn.Linear(nsamples, 1, bias=False)
        self.filter5.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter6 = nn.Linear(nsamples, 1, bias=False)
        self.filter6.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter7 = nn.Linear(nsamples, 1, bias=False)
        self.filter7.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter8 = nn.Linear(nsamples, 1, bias=False)
        self.filter8.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        self.filter9 = nn.Linear(nsamples, 1, bias=False)
        self.filter9.weight = nn.Parameter(self.OFC, requires_grad=False)
        
        
        self.criterion = nn.MSELoss()
        
    def forward(self, inData, energies):
        # mask the energies which are below sampling noise criteria, as zero timing (like LAr DSP)
        # timing is get by dividing OFC(samples)/energy
        
        masked_energies = (energies >= 2*(self.samplingNoise*self.scaler)).int().float()
        
        x1 = (self.filter1(inData[:,0:5])*masked_energies[:,0]) / energies[:,0]
        x2 = (self.filter2(inData[:,5:10])*masked_energies[:,1]) / energies[:,1]
        x3 = (self.filter3(inData[:,10:15])*masked_energies[:,2]) / energies[:,2]
        x4 = (self.filter4(inData[:,15:20])*masked_energies[:,3]) / energies[:,3]
        x5 = (self.filter5(inData[:,20:25])*masked_energies[:,4]) / energies[:,4]
        x6 = (self.filter6(inData[:,25:30])*masked_energies[:,5]) / energies[:,5]
        x7 = (self.filter7(inData[:,30:35])*masked_energies[:,6]) / energies[:,6]
        x8 = (self.filter8(inData[:,35:40])*masked_energies[:,7]) / energies[:,7]
        x9 = (self.filter9(inData[:,40:45])*masked_energies[:,8]) / energies[:,8]
        
        return torch.transpose(torch.stack((x1,x2,x3,x4,x5,x6,x7,x8,x9)),dim0=0, dim1=1)

