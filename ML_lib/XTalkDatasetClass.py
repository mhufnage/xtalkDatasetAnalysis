try:
    import torch
    import torch.nn as nn
    from torch.utils.data import Dataset, DataLoader
    import numpy as np
    
    from helper_lib.calibrationFilesHelper import *
    from helper_lib.functionsHelper import stdVecToArray
    from lorenzetti_reader import read_Lorenzetti_entry, lorenzetti_file_filter_for_ML
    
except:
    pass


# *********************************************************************************************************
class XTalkDataset_LZT_FromROOT(Dataset):
    '''Build to be applied to Lorenzetti Showers data, with crosstalk module, in streaming mode.
    XXXXXXXXXXXXXXXXX
    It can work to create different Subsets, based on an array of indices.
    Those indices can be generated randomly in a K-Fold split.
    Each index is one event integer value.
    The total events (nEvents) are the lenght of the listxxxxxxxxx.
    So, for each fold, the dataset should be re-instantiated.
    
    !! The input is the output from a Lorenzetti AOD, converted to an NTuple, 
    using the 'lorenzetti_file_filter_for_ML(lzt_aod_tree, conditions, rootFileName='newFile.root')'!!
    
    conditions = {
        'etaSize':  3,
        'phiSize':  3,
        'nSamples': 5
    }
    
    The mode input accepts (strings):
        - supervised
        - supervisedOFC (added 11/11/2024 to loop over target energy and time, providing losses when using torch_ofc)
        - specialist
        '''
    def __init__(self, lzt_ml_tree, conditions: dict, mode='supervised', mevToGev=False, load_full_data=True):
        
        __acceptedModes = ['supervised', 'specialist', 'supervisedOFC']
        if mode not in __acceptedModes:
            raise AttributeError("Mode input is outside of acceptable types: {}".format(__acceptedModes))
        
        # Dataset configuration
        # self.shuffle        = shuffle
        self.OFCa           = [ -0.7679187573846364, 0.0766663041985699, 0.8840333207118379, 0.2575801038342427, -0.45036097136001446 ] # from lzt main
        self.OFCb           = [ -0.22673589675790157, -18.897965812858967, 1.1980699915128548, 10.935880735903003, 5.0777868302019415 ] # from lzt main
        self.neta           = conditions['etaSize']
        self.nphi           = conditions['phiSize']
        self.nsamples       = conditions['nSamples']
        
        # ROOT Tree control variables
        self.eventPtr       = 0   # points to input/output data (x and y)
        self.currentEvent   = 0  # global pointer to event ( events_fold[currentEvent], before reset)
        self.dataTree       = lzt_ml_tree
        # self.events_fold    = events_fold # list of event indexes related to current fold
        
        self.load_full_data = load_full_data

        # # Dataset itself
        # self.conditions        = conditions
        # self.NNmode            = mode
        
        self.pulse             = None
        self.xtpulse           = None
        self.e                 = None
        self.xte               = None
        self.t                 = None
        self.xtt               = None
        self.edep              = None
        self.tof               = None
        self.eta               = None
        self.phi               = None        
        self.hash              = None
        
        if mevToGev:
            self.normE = 1/1000
        else:
            self.normE = 1
        
        if conditions['nEvents']==-1:
            nEvents = lzt_ml_tree.GetEntries() # total events for this dataset/subset
        else:
            nEvents = conditions['nEvents']
        
        # Dataset itself
        self.conditions        = conditions
        self.NNmode            = mode
        
        self.pulse             = torch.empty(size=(nEvents, self.neta*self.nphi*self.nsamples), dtype=torch.float)
        self.xtpulse           = torch.empty(size=(nEvents, self.neta*self.nphi*self.nsamples), dtype=torch.float)
        if self.load_full_data:
            self.e                 = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
            self.xte               = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
            self.t                 = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
            self.xtt               = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
            self.edep              = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
            self.tof               = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
            self.eta               = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
            self.phi               = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
            self.hash              = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.long)
        
        # Sequential reading of TTree
        for event in range(0,nEvents):
            if event %10000 == 0:
                print('[XTalkDataset_LZT_FromROOT] Event idx {}/{}'.format(event, nEvents))
            
            if event >= nEvents: # end of file reached
                print('[XTalkDataset_LZT_FromROOT] event >= num of events.')
                return False

            # Get event from TTree
            self.dataTree.GetEntry(event)
            
            self.pulse   [event]  = torch.as_tensor(self.dataTree.cell_pulse)
            self.xtpulse [event]  = torch.as_tensor(self.dataTree.xtcell_pulse)
            if self.load_full_data:
                self.e   [event]   = torch.as_tensor(self.dataTree.cell_e)
                self.xte [event]   = torch.as_tensor(self.dataTree.xtcell_e)
                self.t   [event]   = torch.as_tensor(self.dataTree.cell_tau)
                self.xtt [event]   = torch.as_tensor(self.dataTree.xtcell_tau)
                self.edep[event]   = torch.as_tensor(self.dataTree.cell_edep)
                self.tof [event]   = torch.as_tensor(self.dataTree.cell_tof)
                self.eta [event]   = torch.as_tensor(self.dataTree.cell_eta)
                self.phi [event]   = torch.as_tensor(self.dataTree.cell_phi)
                self.hash[event]   = torch.as_tensor(self.dataTree.cell_hash)
        
        if self.NNmode=='supervised': # x and y will depend on the NN mode
            self.x  = self.xtpulse
            self.y  = self.pulse
        
        if self.NNmode=='supervisedOFC': # x and y will depend on the NN mode
            self.x  = self.xtpulse
            self.y  = self.pulse
            self.y1 = self.e
            self.y2 = self.t
            
        if self.NNmode=='specialist': #  !!under construction!!
            self.x      = self.pulse
            self.y      = self.pulse

        self.len    = len(self.x)
    
    def __getitem__(self, index):
        '''
        Loop over indexes of variables following the order:
        x:  xtpulse
        y:  pulse
        y1: energy (ofc, signal without XT)
        y2: time (ofc, signal without XT)
        '''
        if self.NNmode=='supervisedOFC':
            return self.x[index], self.y[index], self.y1[index], self.y2[index], index
        else:
            return self.x[index], self.y[index], index
    
    def __len__(self):
        return self.len
    
    def calibrate(self, batchIndexes, batchDigits ):
        ene = []
        tau = []
        
        for entry, idx in enumerate(batchIndexes):
            # if len(batchIndexes) == 1:
                # theSamples = batchDigits.detach().numpy().reshape(self.neta*self.nphi, self.nsamples)
            # else:
            theSamples = batchDigits[entry].detach().numpy().reshape(self.neta*self.nphi, self.nsamples)
            # theSamples = batchDigits[entry]
            for readout in range(0, len(theSamples)):
                reco_ene    = applyOFCEnergyToLorenzettiSamples(theSamples[readout], self.OFCa)
                reco_tau    = applyOFCTimeToLorenzettiSamples(theSamples[readout], self.OFCb, reco_ene)
                ene.append(reco_ene)
                tau.append(reco_tau)

        if len(batchIndexes) == 1:
            return torch.tensor(ene, dtype=float, requires_grad=True),  torch.tensor(tau, dtype=float, requires_grad=True)
        if len(batchIndexes) > 1:
            # print('[XTalkDataset_LZT_FromROOT] len(batchIndexes)', len(batchIndexes), 'len(EDSPWin))', len(EDSPWin))
            return torch.tensor(ene, dtype=float, requires_grad=True).reshape(len(batchIndexes), self.neta*self.nphi),  torch.tensor(tau, dtype=float, requires_grad=True).reshape(len(batchIndexes), self.neta*self.nphi)


# class XTalkDataset_LZT_FromROOT(Dataset):
#     '''Build to be applied to Lorenzetti Showers data, with crosstalk module, in streaming mode.
#     XXXXXXXXXXXXXXXXX
#     It can work to create different Subsets, based on an array of indices.
#     Those indices can be generated randomly in a K-Fold split.
#     Each index is one event integer value.
#     The total events (nEvents) are the lenght of the listxxxxxxxxx.
#     So, for each fold, the dataset should be re-instantiated.
    
#     !! The input is the output from a Lorenzetti AOD, converted to an NTuple, 
#     using the 'lorenzetti_file_filter_for_ML(lzt_aod_tree, conditions, rootFileName='newFile.root')'!!
    
#     conditions = {
#         'etaSize':  3,
#         'phiSize':  3,
#         'nSamples': 5
#     }
    
#     The mode input accepts (strings):
#         - supervised
#         - specialist
#         '''
#     def __init__(self, lzt_ml_tree, conditions: dict, mode='supervised', mevToGev=False, load_full_data=True):
        
#         __acceptedModes = ['supervised', 'specialist']
#         if mode not in __acceptedModes:
#             raise AttributeError("Mode input is outside of acceptable types: {}".format(__acceptedModes))
        
#         # Dataset configuration
#         # self.shuffle        = shuffle
#         self.OFCa           = [ -0.7679187573846364, 0.0766663041985699, 0.8840333207118379, 0.2575801038342427, -0.45036097136001446 ] # from lzt main
#         self.OFCb           = [ -0.22673589675790157, -18.897965812858967, 1.1980699915128548, 10.935880735903003, 5.0777868302019415 ] # from lzt main
#         self.neta           = conditions['etaSize']
#         self.nphi           = conditions['phiSize']
#         self.nsamples       = conditions['nSamples']
        
#         # ROOT Tree control variables
#         self.eventPtr       = 0   # points to input/output data (x and y)
#         self.currentEvent   = 0  # global pointer to event ( events_fold[currentEvent], before reset)
#         self.dataTree       = lzt_ml_tree
#         # self.events_fold    = events_fold # list of event indexes related to current fold
        
#         self.load_full_data = load_full_data

#         # # Dataset itself
#         # self.conditions        = conditions
#         # self.NNmode            = mode
        
#         self.pulse             = None
#         self.xtpulse           = None
#         self.e                 = None
#         self.xte               = None
#         self.t                 = None
#         self.xtt               = None
#         self.edep              = None
#         self.tof               = None
#         self.eta               = None
#         self.phi               = None        
#         self.hash              = None
        
#         if mevToGev:
#             self.normE = 1/1000
#         else:
#             self.normE = 1
        
#         if conditions['nEvents']==-1:
#             nEvents = lzt_ml_tree.GetEntries() # total events for this dataset/subset
#         else:
#             nEvents = conditions['nEvents']
        
#         # Dataset itself
#         self.conditions        = conditions
#         self.NNmode            = mode
        
#         self.pulse             = torch.empty(size=(nEvents, self.neta*self.nphi*self.nsamples), dtype=torch.float)
#         self.xtpulse           = torch.empty(size=(nEvents, self.neta*self.nphi*self.nsamples), dtype=torch.float)
#         if self.load_full_data:
#             self.e                 = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
#             self.xte               = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
#             self.t                 = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
#             self.xtt               = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
#             self.edep              = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
#             self.tof               = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
#             self.eta               = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
#             self.phi               = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.float)
#             self.hash              = torch.empty(size=(nEvents, self.neta*self.nphi), dtype=torch.long)
        
#         # Sequential reading of TTree
#         for event in range(0,nEvents):
#             if event %10000 == 0:
#                 print('[XTalkDataset_LZT_FromROOT] Event idx {}/{}'.format(event, nEvents))
            
#             if event >= nEvents: # end of file reached
#                 print('[XTalkDataset_LZT_FromROOT] event >= num of events.')
#                 return False

#             # Get event from TTree
#             self.dataTree.GetEntry(event)
            
#             self.pulse   [event]  = torch.as_tensor(self.dataTree.cell_pulse)
#             self.xtpulse [event]  = torch.as_tensor(self.dataTree.xtcell_pulse)
#             if self.load_full_data:
#                 self.e   [event]   = torch.as_tensor(self.dataTree.cell_e)
#                 self.xte [event]   = torch.as_tensor(self.dataTree.xtcell_e)
#                 self.t   [event]   = torch.as_tensor(self.dataTree.cell_tau)
#                 self.xtt [event]   = torch.as_tensor(self.dataTree.xtcell_tau)
#                 self.edep[event]   = torch.as_tensor(self.dataTree.cell_edep)
#                 self.tof [event]   = torch.as_tensor(self.dataTree.cell_tof)
#                 self.eta [event]   = torch.as_tensor(self.dataTree.cell_eta)
#                 self.phi [event]   = torch.as_tensor(self.dataTree.cell_phi)
#                 self.hash[event]   = torch.as_tensor(self.dataTree.cell_hash)
        
#         if self.NNmode=='supervised': # x and y will depend on the NN mode
#             self.x  = self.xtpulse
#             self.y  = self.pulse
        
#         if self.NNmode=='supervisedOFC': # x and y will depend on the NN mode
#             self.x  = self.xtpulse
#             self.y  = self.e
            
#         if self.NNmode=='specialist': #  !!under construction!!
#             self.x      = self.pulse
#             self.y      = self.pulse

#         self.len    = len(self.x)
    
#     def __getitem__(self, index):
#         return self.x[index], self.y[index], index
    
#     def __len__(self):
#         return self.len
    
#     def calibrate(self, batchIndexes, batchDigits ):
#         ene = []
#         tau = []
        
#         for entry, idx in enumerate(batchIndexes):
#             # if len(batchIndexes) == 1:
#                 # theSamples = batchDigits.detach().numpy().reshape(self.neta*self.nphi, self.nsamples)
#             # else:
#             theSamples = batchDigits[entry].detach().numpy().reshape(self.neta*self.nphi, self.nsamples)
#             # theSamples = batchDigits[entry]
#             for readout in range(0, len(theSamples)):
#                 reco_ene    = applyOFCEnergyToLorenzettiSamples(theSamples[readout], self.OFCa)
#                 reco_tau    = applyOFCTimeToLorenzettiSamples(theSamples[readout], self.OFCb, reco_ene)
#                 ene.append(reco_ene)
#                 tau.append(reco_tau)

#         if len(batchIndexes) == 1:
#             return torch.tensor(ene, dtype=float, requires_grad=True),  torch.tensor(tau, dtype=float, requires_grad=True)
#         if len(batchIndexes) > 1:
#             # print('[XTalkDataset_LZT_FromROOT] len(batchIndexes)', len(batchIndexes), 'len(EDSPWin))', len(EDSPWin))
#             return torch.tensor(ene, dtype=float, requires_grad=True).reshape(len(batchIndexes), self.neta*self.nphi),  torch.tensor(tau, dtype=float, requires_grad=True).reshape(len(batchIndexes), self.neta*self.nphi)



# ****************************************************************************************************


class XTalkStreamingDataLoader_LZT(Dataset):
    '''Build to be applied to Lorenzetti Showers data, with crosstalk module, in streaming mode.
    It works in single file mode, taking all the events available in a TTree or TChain.
    The mode input accepts (strings):
        - supervised
        - specialist
        '''
    def __init__(self, lzt_physics_tree, conditions, batch_size, buffer_batches, shuffle=False, mode='supervised', mevToGev=False):
        
        __acceptedModes = ['supervised', 'specialist']
        if mode not in __acceptedModes:
            raise AttributeError("Mode input is outside of acceptable types: {}".format(__acceptedModes))
        
        # Dataloader configuration
        self.batch_size     = batch_size
        self.buffer_batches = buffer_batches
        self.buffer_len     = self.buffer_batches * self.batch_size
        self.shuffle        = shuffle
        
        # print('buffer_len=',self.buffer_len)
        
        # ROOT Tree control variables
        self.eventPtr       = 0  # points to input/output data (x and y)
        self.currentEvent   = 0  # global pointer to event (from 0 to nEvents, before reset)
        self.dataTree       = lzt_physics_tree

        # Dataset itself
        self.conditions        = conditions
        self.NNmode            = mode
        
        self.pulse             = None
        self.xtpulse           = None
        self.e                 = None
        self.xte               = None
        self.t                 = None
        self.xtt               = None
        self.edep              = None
        self.tof               = None
        self.eta               = None
        self.phi               = None        
        self.hash              = None
        
        self.OFCa        = [ -0.7679187573846364, 0.0766663041985699, 0.8840333207118379, 0.2575801038342427, -0.45036097136001446 ] # from lzt main
        self.OFCb        = [ -0.22673589675790157, -18.897965812858967, 1.1980699915128548, 10.935880735903003, 5.0777868302019415 ] # from lzt main
        self.neta        = conditions['etaSize']
        self.nphi        = conditions['phiSize']
        self.nsamples    = conditions['nSamples']
        
        if mevToGev:
            self.normE = 1/1000
        else:
            self.normE = 1
        
        if conditions['nEvents']==-1:
            self.nEvents = self.dataTree.GetEntries()
        else:
            self.nEvents = conditions['nEvents']
                    
        self.reload_dataset_buffer()
    
    
    def reload_dataset_buffer(self):        
        # Load events into buffer
        
        buff_pulse      =[]
        buff_xtpulse    =[]
        buff_e          =[]
        buff_xte        =[]
        buff_t          =[]
        buff_xtt        =[]
        buff_edep       =[]
        buff_tof        =[]
        buff_eta        =[]
        buff_phi        =[]
        buff_hash       =[]
        
        buffer_event_ptr = 0 # event counter into buffer
        
        while buffer_event_ptr < self.buffer_len:
            # print('buffer_event_ptr=', buffer_event_ptr)
            # print('file currentEvent=', self.currentEvent)
            if self.currentEvent >= self.nEvents: # end of file reached
                return False
        
            dsDict = read_Lorenzetti_entry( tree                    = self.dataTree,
                                            entryNumber             = self.currentEvent, # <--- current event here.
                                            etBin                   = self.conditions['etBin'],
                                            etaBin                  = self.conditions['etaBin'],
                                            m_phaseSpaceDiv         = self.conditions['bPhaseSpaceDiv'],
                                            m_dumpCells             = self.conditions['dumpCells'],
                                            m_dumpOnlyWindow        = self.conditions['bDumpWindowOnly'],
                                            m_sampling_filter       = [self.conditions['caloSampling']], 
                                            m_eta_cut               = self.conditions['etaCut'], 
                                            m_eta_win               = self.conditions['etaSize'], 
                                            m_phi_win               = self.conditions['phiSize'], 
                                            m_sigma_cut             = self.conditions['SigmaCut'], 
                                            m_noEneCut              = self.conditions['bNoEneCut'],
                                            m_data_structure_mode   = '')

            buff_pulse      .append( torch.tensor( np.hstack(dsDict['cell_pulse'][0] * self.normE).flatten().astype(float) , dtype=torch.float, requires_grad=True)  )
            buff_xtpulse    .append( torch.tensor( np.hstack(dsDict['xtcell_pulse'][0] * self.normE).flatten().astype(float) , dtype=torch.float, requires_grad=True)  )
            buff_e          .append( dsDict['cell_e'][0] * self.normE  )
            buff_xte        .append( dsDict['xtcell_e'][0] * self.normE  )
            buff_t          .append( dsDict['cell_tau'][0] )
            buff_xtt        .append( dsDict['xtcell_tau'][0] )
            buff_edep       .append( dsDict['cell_edep'][0] )
            buff_tof        .append( dsDict['cell_tof'][0] )
            buff_eta        .append( dsDict['cell_eta'][0] )
            buff_phi        .append( dsDict['cell_phi'][0] )
            buff_hash       .append( dsDict['cell_hash'][0] )
            
            buffer_event_ptr+=1
            self.currentEvent+=1
            
        
        # Add buffer data into 'current' dataset        
        self.pulse     = buff_pulse
        self.xtpulse   = buff_xtpulse
        self.e         = buff_e
        self.xte       = buff_xte
        self.t         = buff_t
        self.xtt       = buff_xtt
        self.edep      = buff_edep
        self.tof       = buff_tof
        self.eta       = buff_eta
        self.phi       = buff_phi
        self.hash      = buff_hash
        
        if self.NNmode=='supervised': # x and y will depend on the NN mode
            self.x  = self.xtpulse
            # self.xe = self.xte
            # self.xt = self.xtt
            self.y  = self.pulse
            # self.ye = self.e
            # self.yt = self.t
            
        if self.NNmode=='specialist': #  under construction
            self.x      = self.pulse
            self.y      = self.pulse

        self.len    = len(self.x)
            
        return True # reload with success

    def __getitem__(self, index):
        return self.x[index], self.y[index], index
    
    def __iter__(self):
        return self
    
    def __len__(self):
        return self.len
    
    def __next__(self): # provide the next batch as a tuple (like Dataloader does)
        # shall reload?
        if ((self.eventPtr + self.batch_size) > self.buffer_len):
            self.eventPtr = 0
            result = self.reload_dataset_buffer()
            
            if result == False: # could not reload completely!
                # print('end of file (max events ({}/{}) reached!)'.format(self.currentEvent, self.nEvents))
                self.currentEvent = 0 # reset event counter
                raise StopIteration
            
        start   = self.eventPtr
        end     = self.eventPtr + self.batch_size
        x       = self.x[start:end]
        y       = self.y[start:end]
        
        indexes = np.arange(start,end)
        
        self.eventPtr += self.batch_size
        # print('self.eventPtr=',self.eventPtr)
        return (x, y, indexes)


    def calibrate(self, batchIndexes, batchDigits ):
        ene = []
        tau = []
        
        for entry, idx in enumerate(batchIndexes):
            theSamples = batchDigits[entry].detach().numpy().reshape(self.neta*self.nphi, self.nsamples)
            # theSamples = batchDigits[entry]
            for readout in range(0, len(theSamples)):
                reco_ene    = applyOFCEnergyToLorenzettiSamples(theSamples[readout], self.OFCa)
                reco_tau    = applyOFCTimeToLorenzettiSamples(theSamples[readout], self.OFCb, reco_ene)
                ene.append(reco_ene)
                tau.append(reco_tau)
            # ene.append( np.dot(, self.OFCa) )
            # tau.append( np.dot(batchDigits[entry].detach().numpy().reshape(self.neta*self.nphi,self.nsamples), self.OFCb) )

        
        if len(batchIndexes) == 1:
            return torch.tensor(ene, dtype=float, requires_grad=True),  torch.tensor(tau, dtype=float, requires_grad=True)
        if len(batchIndexes) > 1:
            # print('len(batchIndexes)', len(batchIndexes), 'len(EDSPWin))', len(EDSPWin))
            return torch.tensor(ene, dtype=float, requires_grad=True).reshape(len(batchIndexes), self.neta*self.nphi),  torch.tensor(tau, dtype=float, requires_grad=True).reshape(len(batchIndexes), self.neta*self.nphi)

# ****************************************************************************

class XTalkStreamingDataLoader_LZT_FoldSplit_Faster(Dataset):
    '''Build to be applied to Lorenzetti Showers data, with crosstalk module, in streaming mode.
    
    It can work to create different Subsets, based on an array of indices.
    Those indices can be generated randomly in a K-Fold split.
    Each index is one event integer value.
    The total events (nEvents) are the lenght of the list 'events_fold'.
    So, for each fold, the dataset should be re-instantiated.
    
    !! The input is the output from a Lorenzetti AOD, converted to an NTuple, 
    using the 'lorenzetti_file_filter_for_ML(lzt_aod_tree, conditions, rootFileName='newFile.root')'!!
    
    conditions = {
        'etaSize':  3,
        'phiSize':  3,
        'nSamples': 5
    }
    
    The mode input accepts (strings):
        - supervised
        - specialist
        '''
    def __init__(self, lzt_ml_tree, conditions: dict, events_fold: list, batch_size: int, buffer_batches: int, shuffle=False, mode='supervised', mevToGev=False, load_full_data=True):
        
        __acceptedModes = ['supervised', 'specialist']
        if mode not in __acceptedModes:
            raise AttributeError("Mode input is outside of acceptable types: {}".format(__acceptedModes))
        
        # Dataloader configuration
        self.batch_size     = batch_size
        self.buffer_batches = buffer_batches
        self.buffer_len     = self.buffer_batches * self.batch_size
        self.shuffle        = shuffle
        self.OFCa           = [ -0.7679187573846364, 0.0766663041985699, 0.8840333207118379, 0.2575801038342427, -0.45036097136001446 ] # from lzt main
        self.OFCb           = [ -0.22673589675790157, -18.897965812858967, 1.1980699915128548, 10.935880735903003, 5.0777868302019415 ] # from lzt main
        self.neta           = conditions['etaSize']
        self.nphi           = conditions['phiSize']
        self.nsamples       = conditions['nSamples']
        
        # ROOT Tree control variables
        self.eventPtr       = 0   # points to input/output data (x and y)
        self.currentEvent   = 0  # global pointer to event ( events_fold[currentEvent], before reset)
        self.dataTree       = lzt_ml_tree
        self.events_fold    = events_fold # list of event indexes related to current fold
        
        self.load_full_data = load_full_data
        
        # Clean the branches to speed up
        # lzt_ml_tree.SetBranchStatus("*",0)
        # branches = []
        # if self.load_full_data:
        #     branches = ['cell_pulse','xtcell_pulse','cell_e', 'xtcell_e', 'cell_tau', 'xtcell_tau','cell_edep','xtcell_edep','cell_tof', 'xtcell_tof', 'cell_eta', 'xtcell_eta', 'cell_phi', 'xtcell_phi','cell_hash' ]
        # else:
        #     branches = ['cell_pulse','xtcell_pulse' ]
        # for br in branches:
        #     lzt_ml_tree.SetBranchStatus(br,1)

        # Dataset itself
        self.conditions        = conditions
        self.NNmode            = mode
        
        self.pulse             = None
        self.xtpulse           = None
        self.e                 = None
        self.xte               = None
        self.t                 = None
        self.xtt               = None
        self.edep              = None
        self.tof               = None
        self.eta               = None
        self.phi               = None        
        self.hash              = None
        
        
        if mevToGev:
            self.normE = 1/1000
        else:
            self.normE = 1
        
        self.nEvents    = len(self.events_fold) # total events for this dataset/subset
        
        if self.buffer_len > self.nEvents:
            print('buffer lenght is grater than total number of events.\nbuffer_len=total_events ({}->{})'.format(self.buffer_len, self.nEvents))
            self.buffer_len = self.nEvents
        
        # if conditions['nEvents']==-1:
        #     self.nEvents = self.dataTree.GetEntries()
        # else:
        #     self.nEvents = conditions['nEvents']
                    
        self.reload_dataset_buffer()
    
    
    def reload_dataset_buffer(self):        
        # Load events into buffer
        
        buff_pulse      =[]#torch.empty(self.neta * self.nphi * self.nsamples, dtype=torch.float, requires_grad=True) #torch.tensor(nBatches, input/output size)
        buff_xtpulse    =[]#torch.empty(self.neta * self.nphi * self.nsamples, dtype=torch.float, requires_grad=True) #torch.tensor(nBatches, input/output size)
        buff_e          =[]
        buff_xte        =[]
        buff_t          =[]
        buff_xtt        =[]
        buff_edep       =[]
        buff_tof        =[]
        buff_eta        =[]
        buff_phi        =[]
        buff_hash       =[]
        
        buffer_event_ptr = 0 # event counter into buffer
        
        while buffer_event_ptr < self.buffer_len:
            
            if self.currentEvent >= self.nEvents: # end of file reached
                return False

            # Get event from TTree
            self.dataTree.GetEntry(self.events_fold[self.currentEvent])
            
            # buff_pulse      .append( torch.tensor(self.dataTree.cell_pulse, dtype=torch.float, requires_grad=True)*self.normE     )
            # buff_xtpulse    .append( torch.tensor(self.dataTree.xtcell_pulse, dtype=torch.float, requires_grad=True)*self.normE   )
            # tmp_tensor      = torch.tensor(self.dataTree.cell_pulse,   dtype=torch.float)   * self.normE
            # tmp_tensorxt    = torch.tensor(self.dataTree.xtcell_pulse, dtype=torch.float) * self.normE
            
            buff_pulse      .append(self.dataTree.cell_pulse)
            buff_xtpulse    .append(self.dataTree.xtcell_pulse)
            
            if self.load_full_data:
                buff_e          .append( self.dataTree.cell_e)
                buff_xte        .append( self.dataTree.xtcell_e)
                buff_t          .append( self.dataTree.cell_tau)
                buff_xtt        .append( self.dataTree.xtcell_tau)
                buff_edep       .append( self.dataTree.cell_edep)
                buff_tof        .append( self.dataTree.cell_tof)
                buff_eta        .append( self.dataTree.cell_eta)
                buff_phi        .append( self.dataTree.cell_phi)
                buff_hash       .append( self.dataTree.cell_hash)

            # print('buffer_event_ptr=', buffer_event_ptr)
            # print('file currentEvent[idx]=', self.events_fold[self.currentEvent])
            
            buffer_event_ptr+=1
            self.currentEvent+=1
        
        # Add buffer data into 'current' dataset
        # self.pulse     = (buff_pulse)
        # self.xtpulse   = (buff_xtpulse)
        # if self.load_full_data:
        #     self.e         = (buff_e)
        #     self.xte       = (buff_xte)
        #     self.t         = (buff_t)
        #     self.xtt       = (buff_xtt)
        #     self.edep      = (buff_edep)
        #     self.tof       = (buff_tof)
        #     self.eta       = (buff_eta)
        #     self.phi       = (buff_phi)
        #     self.hash      = (buff_hash)
        self.pulse     = torch.as_tensor(buff_pulse)*self.normE
        self.xtpulse   = torch.as_tensor(buff_xtpulse)*self.normE
        if self.load_full_data:
            self.e         = torch.as_tensor(buff_e)*self.normE
            self.xte       = torch.as_tensor(buff_xte)*self.normE
            self.t         = torch.as_tensor(buff_t)
            self.xtt       = torch.as_tensor(buff_xtt)
            self.edep      = torch.as_tensor(buff_edep)
            self.tof       = torch.as_tensor(buff_tof)
            self.eta       = torch.as_tensor(buff_eta)
            self.phi       = torch.as_tensor(buff_phi)
            self.hash      = torch.as_tensor(buff_hash)
        
        if self.NNmode=='supervised': # x and y will depend on the NN mode
            self.x  = self.xtpulse
            # self.xe = self.xte
            # self.xt = self.xtt
            self.y  = self.pulse
            # self.ye = self.e
            # self.yt = self.t
            
        if self.NNmode=='specialist': #  !!under construction!!
            self.x      = self.pulse
            self.y      = self.pulse

        self.len    = len(self.x)
            
        return True # reload with success

    def __getitem__(self, index):
        return self.x[index], self.y[index], index
    
    def __iter__(self):
        return self
    
    def __len__(self):
        return self.len
    
    def __next__(self): # provide the next batch as a tuple (like Dataloader does)
        # shall reload?
        if ((self.eventPtr + self.batch_size) > self.buffer_len):
            self.eventPtr = 0
            result = self.reload_dataset_buffer()
            
            if result == False: # could not reload completely!
                # print('end of file (max events ({}/{}) reached!)'.format(self.currentEvent, self.nEvents))
                self.currentEvent = 0 # reset event counter
                raise StopIteration
            
        start   = self.eventPtr
        end     = self.eventPtr + self.batch_size
        x       = self.x[start:end]
        y       = self.y[start:end]
        
        indexes = np.arange(start,end)
        
        self.eventPtr += self.batch_size
        # print('self.eventPtr=',self.eventPtr)
        return (x, y, indexes)


    def calibrate(self, batchIndexes, batchDigits ):
        ene = []
        tau = []
        
        for entry, idx in enumerate(batchIndexes):
            theSamples = batchDigits[entry].detach().numpy().reshape(self.neta*self.nphi, self.nsamples)
            # theSamples = batchDigits[entry]
            for readout in range(0, len(theSamples)):
                reco_ene    = applyOFCEnergyToLorenzettiSamples(theSamples[readout], self.OFCa)
                reco_tau    = applyOFCTimeToLorenzettiSamples(theSamples[readout], self.OFCb, reco_ene)
                ene.append(reco_ene)
                tau.append(reco_tau)
            # ene.append( np.dot(, self.OFCa) )
            # tau.append( np.dot(batchDigits[entry].detach().numpy().reshape(self.neta*self.nphi,self.nsamples), self.OFCb) )

        
        if len(batchIndexes) == 1:
            return torch.tensor(ene, dtype=float, requires_grad=True),  torch.tensor(tau, dtype=float, requires_grad=True)
        if len(batchIndexes) > 1:
            # print('len(batchIndexes)', len(batchIndexes), 'len(EDSPWin))', len(EDSPWin))
            return torch.tensor(ene, dtype=float, requires_grad=True).reshape(len(batchIndexes), self.neta*self.nphi),  torch.tensor(tau, dtype=float, requires_grad=True).reshape(len(batchIndexes), self.neta*self.nphi)

# ****************************************************************************

class XTalkStreamingDataLoader_LZT_FoldSplit(Dataset):
    '''Build to be applied to Lorenzetti Showers data, with crosstalk module, in streaming mode.
    
    It can work to create different Subsets, based on an array of indices.
    Those indices can be generated randomly in a K-Fold split.
    Each index is one event integer value.
    The total events (nEvents) are the lenght of the list 'events_fold'.
    So, for each fold, the dataset should be re-instantiated.
    
    !! The input is the output from a Lorenzetti AOD !!
    
    The mode input accepts (strings):
        - supervised
        - specialist
        '''
    def __init__(self, lzt_physics_tree, conditions, events_fold: list, batch_size, buffer_batches, shuffle=False, mode='supervised', mevToGev=False):
        
        __acceptedModes = ['supervised', 'specialist']
        if mode not in __acceptedModes:
            raise AttributeError("Mode input is outside of acceptable types: {}".format(__acceptedModes))
        
        # Dataloader configuration
        self.batch_size     = batch_size
        self.buffer_batches = buffer_batches
        self.buffer_len     = self.buffer_batches * self.batch_size
        self.shuffle        = shuffle
        
        # print('buffer_len=',self.buffer_len)
        
        # ROOT Tree control variables
        self.eventPtr       = 0   # points to input/output data (x and y)
        self.currentEvent   = 0  # global pointer to event ( events_fold[currentEvent], before reset)
        self.dataTree       = lzt_physics_tree
        self.events_fold    = events_fold # list of event indexes related to current fold

        # Dataset itself
        self.conditions        = conditions
        self.NNmode            = mode
        
        self.pulse             = None
        self.xtpulse           = None
        self.e                 = None
        self.xte               = None
        self.t                 = None
        self.xtt               = None
        self.edep              = None
        self.tof               = None
        self.eta               = None
        self.phi               = None        
        self.hash              = None
        
        self.OFCa        = [ -0.7679187573846364, 0.0766663041985699, 0.8840333207118379, 0.2575801038342427, -0.45036097136001446 ] # from lzt main
        self.OFCb        = [ -0.22673589675790157, -18.897965812858967, 1.1980699915128548, 10.935880735903003, 5.0777868302019415 ] # from lzt main
        self.neta        = conditions['etaSize']
        self.nphi        = conditions['phiSize']
        self.nsamples    = conditions['nSamples']
        
        if mevToGev:
            self.normE = 1/1000
        else:
            self.normE = 1
        
        self.nEvents    = len(self.events_fold) # total events for this dataset/subset
        
        # if conditions['nEvents']==-1:
        #     self.nEvents = self.dataTree.GetEntries()
        # else:
        #     self.nEvents = conditions['nEvents']
                    
        self.reload_dataset_buffer()
    
    
    def reload_dataset_buffer(self):        
        # Load events into buffer
        
        buff_pulse      =[]
        buff_xtpulse    =[]
        buff_e          =[]
        buff_xte        =[]
        buff_t          =[]
        buff_xtt        =[]
        buff_edep       =[]
        buff_tof        =[]
        buff_eta        =[]
        buff_phi        =[]
        buff_hash       =[]
        
        buffer_event_ptr = 0 # event counter into buffer
        
        while buffer_event_ptr < self.buffer_len:
            
            if self.currentEvent >= self.nEvents: # end of file reached
                return False
        
            dsDict = read_Lorenzetti_entry( tree                    = self.dataTree,
                                            entryNumber             = self.events_fold[self.currentEvent], # <--- current event here.
                                            etBin                   = self.conditions['etBin'],
                                            etaBin                  = self.conditions['etaBin'],
                                            m_phaseSpaceDiv         = self.conditions['bPhaseSpaceDiv'],
                                            m_dumpCells             = self.conditions['dumpCells'],
                                            m_dumpOnlyWindow        = self.conditions['bDumpWindowOnly'],
                                            m_sampling_filter       = [self.conditions['caloSampling']], 
                                            m_eta_cut               = self.conditions['etaCut'], 
                                            m_eta_win               = self.conditions['etaSize'], 
                                            m_phi_win               = self.conditions['phiSize'], 
                                            m_sigma_cut             = self.conditions['SigmaCut'], 
                                            m_noEneCut              = self.conditions['bNoEneCut'],
                                            m_data_structure_mode   = '')

            buff_pulse      .append( torch.tensor( np.hstack(dsDict['cell_pulse'][0] * self.normE).flatten().astype(float) , dtype=torch.float, requires_grad=True)  )
            buff_xtpulse    .append( torch.tensor( np.hstack(dsDict['xtcell_pulse'][0] * self.normE).flatten().astype(float) , dtype=torch.float, requires_grad=True)  )
            buff_e          .append( dsDict['cell_e'][0] * self.normE  )
            buff_xte        .append( dsDict['xtcell_e'][0] * self.normE  )
            buff_t          .append( dsDict['cell_tau'][0] )
            buff_xtt        .append( dsDict['xtcell_tau'][0] )
            buff_edep       .append( dsDict['cell_edep'][0] )
            buff_tof        .append( dsDict['cell_tof'][0] )
            buff_eta        .append( dsDict['cell_eta'][0] )
            buff_phi        .append( dsDict['cell_phi'][0] )
            buff_hash       .append( dsDict['cell_hash'][0] )
            
            # print('buffer_event_ptr=', buffer_event_ptr)
            # print('file currentEvent[idx]=', self.events_fold[self.currentEvent])
            
            buffer_event_ptr+=1
            self.currentEvent+=1
            
        
        # Add buffer data into 'current' dataset        
        self.pulse     = buff_pulse
        self.xtpulse   = buff_xtpulse
        self.e         = buff_e
        self.xte       = buff_xte
        self.t         = buff_t
        self.xtt       = buff_xtt
        self.edep      = buff_edep
        self.tof       = buff_tof
        self.eta       = buff_eta
        self.phi       = buff_phi
        self.hash      = buff_hash
        
        if self.NNmode=='supervised': # x and y will depend on the NN mode
            self.x  = self.xtpulse
            # self.xe = self.xte
            # self.xt = self.xtt
            self.y  = self.pulse
            # self.ye = self.e
            # self.yt = self.t
            
        if self.NNmode=='specialist': #  under construction
            self.x      = self.pulse
            self.y      = self.pulse

        self.len    = len(self.x)
            
        return True # reload with success

    def __getitem__(self, index):
        return self.x[index], self.y[index], index
    
    def __iter__(self):
        return self
    
    def __len__(self):
        return self.len
    
    def __next__(self): # provide the next batch as a tuple (like Dataloader does)
        # shall reload?
        if ((self.eventPtr + self.batch_size) > self.buffer_len):
            self.eventPtr = 0
            result = self.reload_dataset_buffer()
            
            if result == False: # could not reload completely!
                # print('end of file (max events ({}/{}) reached!)'.format(self.currentEvent, self.nEvents))
                self.currentEvent = 0 # reset event counter
                raise StopIteration
            
        start   = self.eventPtr
        end     = self.eventPtr + self.batch_size
        x       = self.x[start:end]
        y       = self.y[start:end]
        
        indexes = np.arange(start,end)
        
        self.eventPtr += self.batch_size
        # print('self.eventPtr=',self.eventPtr)
        return (x, y, indexes)


    def calibrate(self, batchIndexes, batchDigits ):
        ene = []
        tau = []
        
        for entry, idx in enumerate(batchIndexes):
            theSamples = batchDigits[entry].detach().numpy().reshape(self.neta*self.nphi, self.nsamples)
            # theSamples = batchDigits[entry]
            for readout in range(0, len(theSamples)):
                reco_ene    = applyOFCEnergyToLorenzettiSamples(theSamples[readout], self.OFCa)
                reco_tau    = applyOFCTimeToLorenzettiSamples(theSamples[readout], self.OFCb, reco_ene)
                ene.append(reco_ene)
                tau.append(reco_tau)
            # ene.append( np.dot(, self.OFCa) )
            # tau.append( np.dot(batchDigits[entry].detach().numpy().reshape(self.neta*self.nphi,self.nsamples), self.OFCb) )

        
        if len(batchIndexes) == 1:
            return torch.tensor(ene, dtype=float, requires_grad=True),  torch.tensor(tau, dtype=float, requires_grad=True)
        if len(batchIndexes) > 1:
            # print('len(batchIndexes)', len(batchIndexes), 'len(EDSPWin))', len(EDSPWin))
            return torch.tensor(ene, dtype=float, requires_grad=True).reshape(len(batchIndexes), self.neta*self.nphi),  torch.tensor(tau, dtype=float, requires_grad=True).reshape(len(batchIndexes), self.neta*self.nphi)

# ****************************************************************************

class XTalkDataset_LZT(Dataset):
    '''Build to be applied to Lorenzetti Showers data, with crosstalk module.
    This class is based on loading the full dataset from a dictionary, into memory at once.
    After that, It should be converted to Torch.DataLoader for batch usage.
    The mode input accepts (strings):
        - supervised
        - specialist
        '''
    def __init__(self, xtalkDict,  nEvents=-1, mode='supervised', neta=3, nphi=3, nSamples=5, mevToGev=False):
        
        __acceptedModes = ['supervised', 'specialist']
        if mode not in __acceptedModes:
            raise AttributeError("Mode input is outside of acceptable types: {}".format(__acceptedModes))

        self.pulse             = []
        self.xtpulse           = []
        self.e                 = []
        self.xte               = []
        self.t                 = []
        self.xtt               = []
        self.edep              = []
        self.tof               = []
        self.eta               = []
        self.phi               = []        
        self.hash              = []
        
        self.OFCa        = [ -0.7679187573846364, 0.0766663041985699, 0.8840333207118379, 0.2575801038342427, -0.45036097136001446 ] # from lzt main
        self.OFCb        = [ -0.22673589675790157, -18.897965812858967, 1.1980699915128548, 10.935880735903003, 5.0777868302019415 ] # from lzt main
        self.neta        = neta
        self.nphi        = nphi
        self.nsamples    = nSamples
        if mevToGev:
            self.normE = 1/1000
        else:
            self.normE = 1
        
        if nEvents==-1:
            nEvents=len(xtalkDict)

        for index, eventDict in enumerate(xtalkDict):
            if (len(eventDict['cell_e'][0]) != self.neta*self.nphi):
                continue

            self.pulse      .append( torch.tensor( np.hstack(eventDict['cell_pulse'][0] * self.normE).flatten().astype(float) , dtype=torch.float, requires_grad=True) )
            self.xtpulse    .append( torch.tensor( np.hstack(eventDict['xtcell_pulse'][0] * self.normE).flatten().astype(float) , dtype=torch.float, requires_grad=True) )
            self.e          .append( eventDict['cell_e'][0] * self.normE )
            self.xte        .append( eventDict['xtcell_e'][0] * self.normE )
            self.t          .append( eventDict['cell_tau'][0])
            self.xtt        .append( eventDict['xtcell_tau'][0])
            self.edep       .append( eventDict['cell_edep'][0])
            self.tof        .append( eventDict['cell_tof'][0])
            self.eta        .append( eventDict['cell_eta'][0])
            self.phi        .append( eventDict['cell_phi'][0])
            self.hash       .append( eventDict['cell_hash'][0])
            
        if mode=='supervised':
            self.x  = self.xtpulse
            # self.xe = self.xte
            # self.xt = self.xtt
            self.y  = self.pulse
            # self.ye = self.e
            # self.yt = self.t
            
        if mode=='specialist': #  under construction
            self.x      = self.pulse
            self.y      = self.pulse
            
        self.len    = len(self.x)

    def __getitem__(self, index):
        return self.x[index], self.y[index], index
    
    def __len__(self):
        return self.len

    def calibrate(self, batchIndexes, batchDigits ):
        ene = []
        tau = []
        
        for entry, idx in enumerate(batchIndexes):
            theSamples = batchDigits[entry].detach().numpy().reshape(self.neta*self.nphi, self.nsamples)
            # theSamples = batchDigits[entry]
            for readout in range(0, len(theSamples)):
                reco_ene    = applyOFCEnergyToLorenzettiSamples(theSamples[readout], self.OFCa)
                reco_tau    = applyOFCTimeToLorenzettiSamples(theSamples[readout], self.OFCb, reco_ene)
                ene.append(reco_ene)
                tau.append(reco_tau)
            # ene.append( np.dot(, self.OFCa) )
            # tau.append( np.dot(batchDigits[entry].detach().numpy().reshape(self.neta*self.nphi,self.nsamples), self.OFCb) )

        
        if len(batchIndexes) == 1:
            return torch.tensor(ene, dtype=float, requires_grad=True),  torch.tensor(tau, dtype=float, requires_grad=True)
        if len(batchIndexes) > 1:
            # print('len(batchIndexes)', len(batchIndexes), 'len(EDSPWin))', len(EDSPWin))
            return torch.tensor(ene, dtype=float, requires_grad=True).reshape(len(batchIndexes), self.neta*self.nphi),  torch.tensor(tau, dtype=float, requires_grad=True).reshape(len(batchIndexes), self.neta*self.nphi)


# ****************************************************************************

class XTalkDataset(Dataset):
    '''Built to be used on ATLAS data/MC.'''
    def __init__(self, xtalkDict, confDict, nEvents=-1, bRemovePed=False):
        self.isMC   = confDict['isNtupleFromMC']

        self.digits             = []
        self.windowIndexesCh    = []
        self.orderedIndexes     = []
        self.channelId          = []
        self.channelGain        = []
        self.channelLArPed      = []
        self.channelOFCa        = []
        self.channelOFCb        = []
        self.channelRamps0      = []
        self.channelRamps1      = []
        self.channelTimeOffset  = []
        self.channelDSPThr      = []

        self.bRemovePed         = bRemovePed

        self.confDict           = confDict
        # self.ene    = []
        # self.tau    = []

        if nEvents==-1:
            nEvents=len(xtalkDict)

        for cluster in xtalkDict[0:nEvents]:
            windowOrdered = cluster['windowIndexesCh'][cluster['orderedIndexes']].tolist()

            if bRemovePed:
                tmp_digits   = []
                for cellIdx in range(0, len(cluster['channelId'][windowOrdered])):
                    tmp_digits     .append( applyPEDremover( cluster['digits'][windowOrdered][cellIdx] , cluster['channelLArPed'][windowOrdered][cellIdx] ) )
                self.digits        .append( torch.tensor( np.hstack( tmp_digits ).flatten().astype(float) , dtype=torch.float, requires_grad=True) )
            else:
                self.digits             .append( torch.tensor( np.hstack(cluster['digits'][windowOrdered]).flatten().astype(float) , dtype=torch.float, requires_grad=True) )
            # self.windowIndexesCh    .append( cluster['windowIndexesCh'][windowOrdered])
            # self.orderedIndexes     .append( cluster['orderedIndexes'][windowOrdered])
            self.channelId          .append( cluster['channelId'][windowOrdered])
            self.channelGain        .append( cluster['channelGain'][windowOrdered])
            self.channelLArPed      .append( cluster['channelLArPed'][windowOrdered])
            self.channelOFCa        .append( cluster['channelOFCa'][windowOrdered])
            self.channelOFCb        .append( cluster['channelOFCb'][windowOrdered])
            self.channelRamps0      .append( cluster['channelRamps0'][windowOrdered])
            self.channelRamps1      .append( cluster['channelRamps1'][windowOrdered])
            self.channelTimeOffset  .append( cluster['channelTimeOffset'][windowOrdered])
            if self.isMC:
                self.channelDSPThr      .append( -9999 )
            else:
                self.channelDSPThr      .append( cluster['channelDSPThr'][windowOrdered])
                
        
        self.x      = self.digits
        self.y      = self.digits
        # self.gain   = self.channelGain
        self.len    = len(self.x)

    def __getitem__(self, index):
        # print(index)
        return self.x[index], self.y[index], index
    def __len__(self):
        return self.len

    def calibrate(self, batchIndexes, batchDigits ):
        ene = []
        tau = []
        

        for entry, idx in enumerate(batchIndexes):
            # print(batchDigits[entry].reshape(9,4))
            Awin, tauDSPWin, EDSPWin, _, _ = DSPCalibSamplesInCluster(
                                    self.channelId[idx],
                                    batchDigits[entry].detach().numpy().reshape(9,4),
                                    self.channelGain[idx], 
                                    self.channelLArPed[idx], 
                                    self.channelOFCa[idx], 
                                    self.channelOFCb[idx], 
                                    self.channelRamps0[idx], 
                                    self.channelRamps1[idx], 
                                    self.channelDSPThr[idx], 
                                    self.channelTimeOffset[idx], 
                                    thresholdMode=self.confDict['typeThreshold'], 
                                    bDoAbsE1=self.confDict['bAbsDSPE1'],
                                    bDigitsWithPed=not(self.bRemovePed),
                                    )

            # print('ene',EDSPWin)
            # print('tau',tauDSPWin)
            ene.append( EDSPWin )
            tau.append( tauDSPWin )
        
        if len(batchIndexes) == 1:
            return torch.tensor(ene, dtype=float, requires_grad=True),  torch.tensor(tau, dtype=float, requires_grad=True)
        if len(batchIndexes) > 1:
            # print('len(batchIndexes)', len(batchIndexes), 'len(EDSPWin))', len(EDSPWin))
            return torch.tensor(ene, dtype=float, requires_grad=True).reshape(len(batchIndexes), len(EDSPWin)),  torch.tensor(tau, dtype=float, requires_grad=True).reshape(len(batchIndexes), len(tauDSPWin))
        
