import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import Sampler, Dataset

#
# Samplers
#
class KFoldSampler(Sampler):
    def __init__(self, index_sequence):
        self.index_sequence = index_sequence

    def __iter__(self):
        return (i for i in self.index_sequence)

    def __len__(self):
        # return len(self.mask)
        return len(self.index_sequence)
    
#
# Subset
#
class Subset(Dataset):
    '''
    Subset of a dataset at specified indices. 
    (taken from https://stackoverflow.com/questions/50544730/how-do-i-split-a-custom-dataset-into-training-and-test-datasets )
    
    Arguments:
        dataset (Dataset): The whole dataset
        indices (sequence): indices in the whole set selected for subset.
    '''
    def __init__(self, dataset, indices):
        self.dataset = dataset
        self.indices = indices
        
    def __getitem__(self, index):
        return self.dataset[self.indices[index]]
    
    def __len__(self):
        return len(self.indices)

#
# Loss
#
class TauLossWeightedByEnergy(nn.Module):
    def __init__(self):
        super(TauLossWeightedByEnergy, self).__init__()
    
    def forward(self, tau_x, tau_y, ene_x):
        # ene_gev_abs = torch.abs(ene_x/1000)
        abs_ene     = torch.abs(ene_x)
        sigma_ene   = torch.sqrt(abs_ene)
        # sq_error    = torch.sigmoid(ene_gev)*(tau_x - tau_y)**2
        sq_error    = sigma_ene*(tau_x - tau_y)**2
        w_ene_mse   = torch.mean(sq_error)

        return w_ene_mse

    
#
# ML auxiliary functions
#
def checkpoint(model, optimizer, filename, epoch, val_loss):
    '''Save the model.'''
    torch.save({
        # 'optimizer'     : model.optimizer.state_dict(),
        'optimizer'     : optimizer.state_dict(),
        'model'         : model.state_dict(),
        'bestEpoch'     : epoch,
        'bestValLoss'   : val_loss,
    }, filename)

def resumeModel(model, optimizer, filename):
    '''Load the model.'''
    theCheckpoint   = torch.load(filename)
    model.load_state_dict(theCheckpoint['model'])
    optimizer.load_state_dict(theCheckpoint['optimizer'])
    
    return theCheckpoint['bestValLoss']

# def resumeModel(model, optimizer, filename):
#     '''Load the model.'''
#     theCheckpoint   = torch.load(filename)
#     model.load_state_dict(theCheckpoint['model'])
#     optimizer.load_state_dict(theCheckpoint['optimizer'])


#
#    Early stopper
#
class EarlyStopper:
    def __init__(self, patience=1, min_delta=0):
        self.patience = patience
        self.min_delta = min_delta
        self.counter = 0
        self.min_validation_loss = np.inf
        self.bestEpoch = -1
        self.tmpModel = 0

    def early_stop(self, validation_loss, epoch, model, optimizer, bestModelPath='bestModel.pth'):

        # if validation loss is smaller than min val loss, reset.
        if validation_loss < self.min_validation_loss:
            self.min_validation_loss    = validation_loss
            self.counter                = 0
            self.bestEpoch              = epoch
            # checkpoint(model, optimizer, bestModelPath, epoch, validation_loss)
            self.tmpModel               = model

        # else if, increase the patience counter.
        elif validation_loss > (self.min_validation_loss + self.min_delta):
            self.counter    += 1
            if self.counter >= self.patience:
                # checkpoint(model, optimizer, bestModelPath, epoch, validation_loss)
                return True
        return False