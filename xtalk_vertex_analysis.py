#!/usr/bin/env python
# coding: utf-8

# In[1]:


# try:
#     !pip install fxpmath
# except:
#     pass
# !source /eos/user/m/mhufnage/.venv/bin/activate
# from IPython.display import clear_output

import json
import os,sys
import time
import shelve

## Insert here the path to EventReader/share
eventReaderSharePath = '/eos/user/m/mhufnage/ALP_project/offline-ringer-dev/crosstalk/EventReader/share/'
sys.path.insert(1, eventReaderSharePath) 
# sys.path.insert(1, '/eos/user/m/mhufnage/SWAN_projects/XTalk/')

print("loading ROOT module...")
import ROOT
import numpy as np
import awkward as ak
import uproot

from glob import glob
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as mcolors
import matplotlib
from fxpmath import Fxp

# import mpl_scatter_density
import scipy.stats as scipy
import torch
import torch.nn as nn
from collections import OrderedDict

# import matplotlib.rcsetup as rcsetup
# print(rcsetup.all_backends)
# print(matplotlib.get_backend())
# matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS

## Custom Packages
from auxiliaryFunctions import *
from calibrationFilesHelper import *
from getXTDataAsPythonDict import *
# from calibrationFilesHelperROOT import *

# import tensorflow

## Load custom Help libraries
fDict       = open(eventReaderSharePath+'dictCaloByLayer.json')
caloDict    = json.load(fDict)


# # 3.0) ML Studies

# ## 3.1) Dict

# ### - Load TTree

# In[2]:


#---------------------------------------------
#----------   Read the ROOT trees   ----------
#---------------------------------------------
clusName            = 'topocluster'#,'7x11' #
# datasetPath         ='/eos/user/m/mhufnage/ALP_project/root/user.mhufnage.data17_13TeV_dumpedEventReader_TagAndProbe_v01_XYZ.root.tgz/*XYZ.root'
datasetPath       = '/eos/user/m/mhufnage/ALP_project/root/user.mhufnage.data17_13TeV_dumpedEventReader_v01_XYZ.root.tgz/*XYZ.root'

#---------------------------------------------

fileNames       = glob(datasetPath)

sTree   = ROOT.TChain("dumpedData",'')
sLBTree = ROOT.TChain("lumiblockData",'')
for file in fileNames:
    fileNameInputLog = "/".join((file+" added to the TChain...").split('/')[-2:])
    # print(fileNameInputLog) 
    sTree.Add(file+"/dumpedData") # main dumped data tree (event by event)
    sLBTree.Add(file+"/lumiblockData") # LB tree (lumiblock per lumiblock)

if clusName == 'topocluster':
    sTree.SetBranchStatus("cluster711_*",0)
    clusKey = "clusters"
if clusName == '7x11':
    sTree.SetBranchStatus("cluster_*",0)
    clusKey = '711_roi'


nEvents         = sTree.GetEntries()
nLumiBlocks     = sLBTree.GetEntries()

print("{} files were added with {} events.".format(len(fileNames),nEvents))


# ### Autoencoder test

# #### Classes

# In[43]:


def getCellsInWindowCustomSizeNEW(
                     cellEneList, 
                     cellIndexList, 
                     cellEtaList, 
                     cellPhiList, 
                     cellSamplingList, 
                     nCellsEta, 
                     nCellsPhi, 
                     DEtaList,
                     DPhiList,
                     layer='EMB2',
                     fullCluster=False):
    ''' 
    Create a nCellsEta x nCellsPhi cell ROI.
    
    # Return the indexes of the array of cells. 
    # This array is composed of a sub-array of the cluster, with only the cells of the selected layer.
    # window        : set of positional index inside sub-array, which belongs to the window
    # layerIndexes  : array of indexes of all cells from chosen layer.'''
   
    # get the hot cell pos and value (for the layer)
    layerIndexes      = getMatchedIndexes(cellSamplingList[0], layer)
    
    if fullCluster == True:
        return layerIndexes

    if np.size( layerIndexes ) == 0:
        return [], []
    
    # determine the window size in eta x phi
    if layer=='EMB2':
        deta = 0.025#00000037252903
        dphi = 0.024#54369328916073
    else:
        deta = np.min(np.array(DEtaList[0])[layerIndexes])
        dphi = np.min(np.array(DPhiList[0])[layerIndexes])

    etaSize = nCellsEta * deta
    phiSize = nCellsPhi * dphi  
    
    # print(deta,etaSize,dphi,phiSize)
    
    hotCellValue    = np.max(np.array(cellEneList[0])[layerIndexes])
    hotCellPos      = np.argmax(np.array(cellEneList[0])[layerIndexes])
    hotCellEta      = np.array(cellEtaList[0])[layerIndexes][hotCellPos]
    hotCellPhi      = np.array(cellPhiList[0])[layerIndexes][hotCellPos]
    # print('hotcell position: ',hotCellEta,hotCellPhi,', granularity: ',np.array(DEtaList[0])[layerIndexes][hotCellPos],np.array(DPhiList[0])[layerIndexes][hotCellPos])
    
    window          = [] # list of indexes of the cells inside the chosen window
    
    # find cells within limits of eta phi window
    # for sampling, ind, eta, phi in zip(cellSamplingList, cellIndexList[0], cellEtaList[0], cellPhiList[0]):
    for ind, eta, phi in zip(layerIndexes, np.array(cellEtaList[0])[layerIndexes], np.array(cellPhiList[0])[layerIndexes]):
        etaConditionL    = eta > (cellEtaList[0][hotCellPos] - etaSize/2)
        etaConditionR    = eta < (cellEtaList[0][hotCellPos] + etaSize/2)
#         phiConditionU    = phi >= correctPhi( (cellPhiList[0][hotCellPos] - phiSize/2) )
#         phiConditionD    = phi <= correctPhi( (cellPhiList[0][hotCellPos] + phiSize/2) )
        
        lowerPhiRange    = np.array(cellPhiList[0])[layerIndexes][hotCellPos] - phiSize/2
        upperPhiRange    = np.array(cellPhiList[0])[layerIndexes][hotCellPos] + phiSize/2
        
        bPhiOverFlow = ( (lowerPhiRange < (-1)*math.pi) or (lowerPhiRange > 1*math.pi) or (upperPhiRange < (-1)*math.pi) or (upperPhiRange > 1*math.pi) )
            
            # if overflows, need to include the discontinuity in the middle of the interval.
        if bPhiOverFlow:
            phiCondition = (((phi >= correctPhi(lowerPhiRange)) and (phi <= 1*math.pi)) or ((phi <= correctPhi(upperPhiRange)) and (phi >= (-1)*math.pi)))
        else:
            phiCondition = ((phi >= lowerPhiRange) and (phi <= upperPhiRange))
        
        if (etaConditionL and etaConditionR) and phiCondition:
            window.append( ind ) # add them to a list of index cells
            if len(window) >= ( nCellsEta * nCellsPhi ):
                break
                

    return np.asarray(window), np.asarray(layerIndexes), hotCellPos

def getSortedClusterIndexes(eta,phi):
    '''Return the indexes of an organized sequence of pairs of eta,phi, following a line from the smallest phi's, then the smallest eta's. '''
    ind = np.lexsort((eta,phi)) 
    return ind

def unpackCellsFromCluster(dataset_cluster, confDict, ped_dict={}):
    # get list indexes that match to current sampling
    channelLayerIndexes     = getMatchedIndexes(dataset_cluster['channels']['sampling'][0], confDict['sampling']) 
    # rawChannelLayerIndexes  = getMatchedIndexes(ev['electrons'][elec][clusName][clus]['rawChannels']['sampling'][0], layer)

    # is that layer empty for RAWCh or Ch? skip
    if (len(channelLayerIndexes) == 0 ): # or (len(rawChannelLayerIndexes)==0):
        return 0

    ###################     ###############
    ## Channel + Digits  +  ## RawChannel #
    ###################     ###############
    windowIndexesCh, layerIndexesCh, hotCellPos = getCellsInWindowCustomSizeNEW(
        dataset_cluster['channels']['energy'],
        dataset_cluster['channels']['index'],
        dataset_cluster['channels']['eta'],
        dataset_cluster['channels']['phi'],
        dataset_cluster['channels']['sampling'],
        confDict['nCellsWinEta'], 
        confDict['nCellsWinPhi'], 
        dataset_cluster['channels']['deta'],
        dataset_cluster['channels']['dphi'],
        layer=confDict['sampling'])

    # any of the windows aren't full??
    if confDict['bFullWindows']:
        if (len(windowIndexesCh) != (nCellsWinEta*nCellsWinPhi)):# or (len(windowIndexesRawCh) != (nCellsWinEta*nCellsWinPhi)):
            return 0

    # if they belong to the same calo object and have the same size, they MUST have the same cells, because the dumper rule is the same.
    digits              = np.array(dataset_cluster['channels']['digits'][0],dtype=object)
    digitsSize          = len (np.array(dataset_cluster['channels']['digits'][0],dtype=object))
    channelEnergy       = np.array(dataset_cluster['channels']['energy'][0],dtype=object)
    channelTime         = np.array(dataset_cluster['channels']['time'][0],dtype=object)
    # channelInfo         = listOfListsToArrayOfArrays(np.array(dataset_cluster['channels']['chInfo'][0],dtype=object))
    channelInfo         = np.array(dataset_cluster['channels']['chInfo'][0],dtype=object)
    channelId           = np.array(dataset_cluster['channels']['channelId'][0],dtype=object)
    channelSampling     = np.array(dataset_cluster['channels']['sampling'][0],dtype=object)
    channelEta          = np.array(dataset_cluster['channels']['eta'][0],dtype=object)
    channelPhi          = np.array(dataset_cluster['channels']['phi'][0],dtype=object)
    channelDEta         = np.array(dataset_cluster['channels']['deta'][0],dtype=object)
    channelDPhi         = np.array(dataset_cluster['channels']['dphi'][0],dtype=object)
    channelGain         = np.array(dataset_cluster['channels']['gain'][0],dtype=object)
    channelDSPThr       = np.array(dataset_cluster['channels']['DSPThreshold'][0],dtype=object)
    channelOFCa         = np.array(dataset_cluster['channels']['OFCa'][0],dtype=object)
    channelOFCb         = np.array(dataset_cluster['channels']['OFCb'][0],dtype=object)
    channelRamps0       = np.array(dataset_cluster['channels']['ADC2MeV0'][0],dtype=object)
    channelRamps1       = np.array(dataset_cluster['channels']['ADC2MeV1'][0],dtype=object)
    channelLArPed       = np.array(dataset_cluster['channels']['LArDB_Pedestal'][0],dtype=object)
    channelTimeOffset   = np.array(dataset_cluster['channels']['OFCTimeOffset'][0],dtype=object)
    channelEffSigma     = np.array(dataset_cluster['channels']['effSigma'][0],dtype=object)
    # channelNoise        = np.array(dataset_cluster['channels']['noise'][0],dtype=object)          [windowIndexesCh] .tolist()
    # channelShape        = np.array(ev['electrons'][elec][clusName][clus]['channels']['Shape'][0],dtype=object)          [windowIndexesCh] .tolist()
    
    # if bDoOfflineCalib:
    #     channelMinBiasAvg   = np.array(dataset_cluster['channels']['MinBiasAvg'][0],dtype=object)     [windowIndexesCh] .tolist()
    #     channelOfflHV       = np.array(dataset_cluster['channels']['OfflHVScale'][0],dtype=object)    [windowIndexesCh] .tolist()
    #     channelEneResc      = np.array(dataset_cluster['channels']['OfflEneRescaler'][0],dtype=object)[windowIndexesCh] .tolist()
    # print('sampling size:',len(channelSampling))
    # print('eta size:', len(channelEta))

    # Selection of Pedestal (DB or Script)
    if not(confDict['bPedFromDB']):
        peds = [] #testing
        gainsInt = getGainInteger(channelGain)
    if confDict['bPedFromDB']:
        peds = channelLArPed

    # The cells that came from Window algorithm are not the same when comparing RawCh and Digits.
    # For that, we will select the same Digits cells set, based on the HWID, inside the Raw Channel cluster cells set. 
    # mirrorIndexes = []
    for k, hwid in enumerate(channelId):
        # position        = np.where(np.array(ev['electrons'][elec][clusKey][clus]['channels']['channelId'][0],dtype=object)[layerIndexesCh] == hwid)[0][0]
        # mirrorIndexes.append(position) # its the window mirrored indexes for RawCh
        if not(confDict['bPedFromDB']):
            cell_dict_ped   = getLArChannelPED  (hwid, ped_dict  )
            peds.append(cell_dict_ped['ped'][0][gainsInt[k]]) # picking up the right gain weights


    orderedIndexes = getSortedClusterIndexes(channelEta[windowIndexesCh], channelPhi[windowIndexesCh])
    
    ml_dataset = {
        'orderedIndexes'    : orderedIndexes,
        'windowIndexesCh'   : windowIndexesCh,
        'layerIndexesCh'    : layerIndexesCh,
        'hotcellposition'   : hotCellPos,
        'digits'            : digits,
        'digitsSize'        : digitsSize,
        'channelEnergy'     : channelEnergy,
        'channelTime'       : channelTime,
        'channelInfo'       : channelInfo,
        'channelId'         : channelId,
        'channelSampling'   : channelSampling,
        'channelEta'        : channelEta,
        'channelPhi'        : channelPhi,
        'channelDEta'       : channelDEta,
        'channelDPhi'       : channelDPhi,
        'channelGain'       : channelGain,
        'channelDSPThr'     : channelDSPThr,
        'channelOFCa'       : channelOFCa,
        'channelOFCb'       : channelOFCb,
        'channelRamps0'     : channelRamps0,
        'channelRamps1'     : channelRamps1,
        'channelLArPed'     : peds,
        'channelTimeOffset' : channelTimeOffset,
        'channelEffSigma'   : channelEffSigma,
        # 'channelNoise'      : channelNoise,
    }
    

    # for key in ml_dataset.keys():
    #     if bReturnOrderedInWindow:
    #         ml_dataset[key] = np.array(ml_dataset[key],dtype=object)
    #     else:
    #         ml_dataset[key] = np.array(ml_dataset[key],dtype=object)
    

    return ml_dataset

# Creating a PyTorch class
# 28*28 ==> 9 ==> 28*28
class AE(torch.nn.Module):
    # https://www.geeksforgeeks.org/implementing-an-autoencoder-in-pytorch/
    def __init__(self):
        super().__init__()
         
        # Building an linear encoder with Linear
        # layer followed by Relu activation function
        # 784 ==> 9
        self.encoder = torch.nn.Sequential(
            torch.nn.Linear(28 * 28, 128),
            torch.nn.ReLU(),
            torch.nn.Linear(128, 64),
            torch.nn.ReLU(),
            torch.nn.Linear(64, 36),
            torch.nn.ReLU(),
            torch.nn.Linear(36, 18),
            torch.nn.ReLU(),
            torch.nn.Linear(18, 9)
        )
         
        # Building an linear decoder with Linear
        # layer followed by Relu activation function
        # The Sigmoid activation function
        # outputs the value between 0 and 1
        # 9 ==> 784
        self.decoder = torch.nn.Sequential(
            torch.nn.Linear(9, 18),
            torch.nn.ReLU(),
            torch.nn.Linear(18, 36),
            torch.nn.ReLU(),
            torch.nn.Linear(36, 64),
            torch.nn.ReLU(),
            torch.nn.Linear(64, 128),
            torch.nn.ReLU(),
            torch.nn.Linear(128, 28 * 28),
            torch.nn.Sigmoid()
        )
 
    def forward(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded


class Autoencoder(torch.nn.Module):
    # https://medium.com/analytics-vidhya/creating-an-autoencoder-with-pytorch-a2b7e3851c2c
    # def __init__(self, epochs=100, batchSize=128, learningRate=1e-3, nCellsEta=3, nCellsPhi=3, neu1=10):
    def __init__(self, learningRate=1e-3, nCellsEta=3, nCellsPhi=3, neu1=10):
        super(Autoencoder, self).__init__() 
        # self.epochs         = epochs
        # self.batchSize      = batchSize
        self.learningRate   = learningRate
        self.nCellsEta      = nCellsEta
        self.nCellsPhi      = nCellsPhi
        self.nSamples       = 4
        self.neu1           = neu1

        # encoder
        self.encoder = torch.nn.Sequential(
            torch.nn.Linear(self.nSamples * self.nCellsEta * self.nCellsPhi, self.neu1),
            torch.nn.ReLU(True),
            torch.nn.Linear(self.neu1, int(self.neu1/2)),
            torch.nn.ReLU(True)
        )

        # decoder
        self.decoder = torch.nn.Sequential(
            torch.nn.Linear(int(self.neu1/2), self.neu1 ),
            torch.nn.ReLU(True),
            torch.nn.Linear(self.neu1, self.nSamples * self.nCellsEta * self.nCellsPhi),
            torch.nn.ReLU(True)
        )
        
        #
        self.optimizer = torch.optim.Adam(self.parameters(), lr=self.learningRate, weight_decay=1e-5)
        self.criterion = nn.MSELoss()


    def forward(self, x):
        encoded  = self.encoder(x)
        decoded  = self.decoder(encoded)
        return decoded

    # def trainModel(self):


# ### - Loop data to Train

# In[107]:



#---------------------------------------------
#---------- ML Training Parameters -----------
#---------------------------------------------
epochs          = 30
clus_train      = 100 #nClusters to being user to train the model
clus_test       = 10
nNeuronsInput   = 10
nSamples        = 4


#---------------------------------------------
#--------- Data Analysis conditions ----------
#---------------------------------------------
evt_lim             = 10000#nEvents
bPlotClusters       = False
bPedFromDB          = True # Ped from DB (TRUE) or from Script (FALSE)
bFullWindows        = True # use only full windows from clusters
bDoOfflineCalib     = False
el_thr              = 10 # GeV
nCellsWinEta        = 3
nCellsWinPhi        = 3
calibType           = 'dumper' # name of the type of calibration, to differentiate from offline or database (analysis/debug) #'database'#'database' #'typical' # or 
layer               = 'EMB2'
loopModeType        = 'cluster' # or 'event'
#---------------------------------------------
#------------- DSP conditions #------------- < Not getting nice results from quantization. Ignore those *Quant* boolean options. >
#---------------------------------------------
typeThreshold       = 'EneThr'#'fixedValue'#'EneThr'#'NoiseSigma'#
bAbsDSPE1           = False # ~X~ use abs(E) to get E1, and compare to E_thr
bPerfomQuant        = False # ~X~ apply the quantized form of energy and time estimation (like DSP)
applyCteQuant       = False # ~X~ True # apply quantization to the alpha, beta, Pa, Pb. If False, use likeDSP function, but full precision constants.
reorderSamp         = False # ~X~ reorder samples like-dsp

confDict = {
    'el_thr'            : el_thr,
    'sampling'          : layer,
    'nCellsWinEta'      : nCellsWinEta,
    'nCellsWinPhi'      : nCellsWinPhi,
    'datasetPath'       : datasetPath,
    'calibType'         : calibType,
    'bFullWindows'      : bFullWindows,
    'bPedFromDB'        : bPedFromDB,
    'bDoOfflineCalib'   : bDoOfflineCalib,
    'typeThreshold'     : typeThreshold,
    'bAbsDSPE1'         : bAbsDSPE1,
    'bPerfomQuant'      : bPerfomQuant,
    'applyCteQuant'     : applyCteQuant,
    'reorderSamp'       : reorderSamp,
    'fileNames'         : fileNames
}

#---------------------------------------------
#------- Online Calibration constants --------
#---------------------------------------------

ped_dict = {}
if calibType=='database':
    ofc_dict = np.load('calibration/OFC_dict.npz',allow_pickle=True )['OFC'].tolist()
    ped_dict = np.load('calibration/PED_dict.npz',allow_pickle=True )['PED'].tolist()
    mpmc_dict = np.load('calibration/MPMC_dict.npz',allow_pickle=True )['MPMC'].tolist()
    uamev_dict = np.load('calibration/UAMEV_dict.npz',allow_pickle=True )['UAMEV'].tolist()
    ramps_dict = np.load('calibration/RAMPS_dict.npz',allow_pickle=True )['RAMPS'].tolist()

#---------------------------------------------
#------------ ML Model Construct -------------
#---------------------------------------------

model       = Autoencoder(nCellsEta=nCellsWinEta, nCellsPhi=nCellsWinPhi, neu1=nNeuronsInput)

train_logging_dict = {
    'loss'      : [],
    'loss_tau'  : [],
    'sig_tau'   : [],
}


# loss_fn = nn.MSELoss()
# optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

#---------------------------------------------
#--------- Event and Data Reading ------------
#---------------------------------------------
print('Calibration type: {}'.format(calibType))
print('Electron pt cut: {} GeV'.format(el_thr))
print('Processing cluster: {}'.format(clusName))

for epoch in range(epochs):
    print('Epoch: {}/{}...'.format(epoch,epochs))

    elec_counter    = 0 # counter of electrons in dataset
    clus_counter    = 0
    bMaxClusters    = False

    #---------------------------------------------
    #------ TTree Loop and data extraction -------
    #---------------------------------------------
    meanTimePerEvent = 0.0
    startTime = time()

    for evIndex in range(0, nEvents): ## Loop events into TTree
        if bMaxClusters:
            break
        
        if loopModeType=='event':
            if evIndex > evt_lim:
                break
            if (evIndex%int(evt_lim/5)) == 0:
                print("Event {}/{}...".format(evIndex,evt_lim))
    
        # dataset = getXTDataAsPythonDict(sTree, evIndex, eventReaderSharePath, dataHasBadCh=True, isMC=False)
        dataset = getXTDataAsPythonDictFaster(sTree, evIndex, eventReaderSharePath, dataHasBadCh=True, isMC=False, clusterType=clusName) #<- apply sTree.GetEntry(evIndex) here!

        if dataset==0: # Data has no electrons in this event!
            continue

        for evtn, ev in enumerate(dataset['dataset']): # for each event...
            for elec in ev['electrons'].keys(): # for each electron in this event...
                if bMaxClusters:
                    break
                # print(ev['electrons'][elec]['pt'])
                if ev['electrons'][elec]['pt'][0]/1000 < el_thr:
                    continue
                if np.abs(ev['electrons'][elec]['eta']) > 1.4:
                    continue
                # datasetML = dataset ## Test

                clusloop        = ev['electrons'][elec][clusKey]

                elecPt          = (ev['electrons'][elec]['pt'])
                elecEta         = (ev['electrons'][elec]['eta'])
                elecPhi         = (ev['electrons'][elec]['phi'])
                elecVtxZ        = (ev['electrons'][elec]['vertexZ'])
                elecNClus       = len(ev['electrons'][elec][clusKey])
                
                for clus in clusloop.keys(): # for each cluster associated to that electron...
                    if loopModeType=='cluster':
                        if clus_counter > clus_train:
                            bMaxClusters = True
                            break
                        # if(clus_counter%2):
                        #     print('{}/{} clusters were used to train the model so far...'.format(clus_counter,clus_train))
                    
                    #---------------------------------------------
                    #------- Cluster Dataset Processing ----------
                    #---------------------------------------------
                    dataset_cluster = ev['electrons'][elec][clusKey][clus]

                    datasetML       = unpackCellsFromCluster(dataset_cluster, confDict, ped_dict=ped_dict)
                    
                    if datasetML == 0: # Selected sampling has no cells!
                        # print(' Selected sampling has no cells!')
                        continue
                    if(clus_counter%int(clus_train/5)==0):
                        print('\t{}/{} clusters were used to train the model so far...'.format(clus_counter,clus_train))

                    clus_counter+=1
                    
                    # if loopModeType=='cluster':
                    #     if clus_counter > clus_train:
                    #         bMaxClusters = True
                    #         break

                        # if(clus_counter%2):
                        #     print('{}/{} clusters were used to train the model so far...'.format(clus_counter,clus_train))

                    if bPlotClusters:
                        X = np.array(datasetML['channelEta'])
                        Y = np.array(datasetML['channelPhi'])
                        Z = np.array(datasetML['channelEnergy'])

                        window  = np.array(datasetML['windowIndexesCh'])
                        layer   = np.array(datasetML['layerIndexesCh'])
                        deta    = np.array(datasetML['channelDEta'])
                        dphi    = np.array(datasetML['channelDPhi'])

                        fig, ax = plt.subplots(figsize=(10,10))                
                        plt.scatter(X[layer],Y[layer],c=Z[layer], s=200,marker='s',cmap='summer')
                        plt.scatter(X[window],Y[window],fc='none',ec='red',s=400, marker='o')
                        plt.scatter(X[layer][datasetML['hotcellposition']], Y[layer][datasetML['hotcellposition']], fc='none',ec='black',marker='x')
                        plt.colorbar()
                        plt.show()

                    #---------------------------------------------
                    #-------------- ML Training ------------------
                    #---------------------------------------------
                    
                    # do the software DSP estimation
                    windowOrdered = datasetML['windowIndexesCh'][datasetML['orderedIndexes']].tolist() 

                    Awin, tauDSPWinInput, EDSPWin, _ = DSPCalibSamplesInCluster(
                        datasetML['channelId'][windowOrdered],
                        datasetML['digits'][windowOrdered], 
                        datasetML['channelGain'][windowOrdered], 
                        datasetML['channelLArPed'][windowOrdered], 
                        datasetML['channelOFCa'][windowOrdered], 
                        datasetML['channelOFCb'][windowOrdered], 
                        datasetML['channelRamps0'][windowOrdered], 
                        datasetML['channelRamps1'][windowOrdered], 
                        datasetML['channelDSPThr'][windowOrdered], 
                        datasetML['channelTimeOffset'][windowOrdered], 
                        thresholdMode=confDict['typeThreshold'], 
                        bDoAbsE1=confDict['bAbsDSPE1'])
                        
                    data_input = torch.tensor(np.hstack(datasetML['digits'][windowOrdered]).flatten().astype(float)).type(torch.float)
                    
                    # test data
                    output      = model(data_input)
                    output_conv = output.detach().numpy().reshape(len(datasetML['channelId'][windowOrdered]),nSamples)

                    Awin, tauDSPWinOutput, EDSPWin, _ = DSPCalibSamplesInCluster(
                        datasetML['channelId'][windowOrdered],
                        output_conv, 
                        datasetML['channelGain'][windowOrdered], 
                        datasetML['channelLArPed'][windowOrdered], 
                        datasetML['channelOFCa'][windowOrdered], 
                        datasetML['channelOFCb'][windowOrdered], 
                        datasetML['channelRamps0'][windowOrdered], 
                        datasetML['channelRamps1'][windowOrdered], 
                        datasetML['channelDSPThr'][windowOrdered], 
                        datasetML['channelTimeOffset'][windowOrdered], 
                        thresholdMode=confDict['typeThreshold'], 
                        bDoAbsE1=confDict['bAbsDSPE1'])

                    # loss
                    loss        = model.criterion(output, data_input)
                    loss_tau    = model.criterion(torch.tensor(tauDSPWinOutput).type(torch.float),torch.tensor(tauDSPWinInput).type(torch.float))

                    # backpropagation (optimize weights)
                    model.optimizer.zero_grad()
                    loss.backward()
                    model.optimizer.step()
                    

                    # add here the offline calibration (not working yet)
                    if confDict['bDoOfflineCalib']:
                        offl_pileupOffset   = 0 # getOfflinePileUpOffset(evt_lumiblock, lumiPerBCIDVector, evt_bcid, minBiasAvgs, ofcas, shapes)
                        offl_tauOffset      = 0
                        offl_ene, offl_tau  = offlineCalibInCluster(EDSPWin, tauDSPWin, channelOfflHV, channelEneResc, offl_pileupOffset, offl_tauOffset)

                    
                    #---------------------------------------------
                    #---------------------------------------------
                    #---------------------------------------------

                    if bMaxClusters:
                        break
                elec_counter+=1

                if bMaxClusters:
                    break
            if bMaxClusters:
                break
        if bMaxClusters:
            break
    #
    # End of the current Epoch
    #
    train_logging_dict['loss'].append(loss.detach().numpy())
    train_logging_dict['loss_tau'].append(loss_tau.detach().numpy())

confDict['elec_counter'] = elec_counter
print('Total Electrons: ',elec_counter)
print('Total clusters: ', clus_counter)
# print(confDict)
endTime = time()

print("Total running time: {:.2f} minutes. ({:.2f} us/event)".format((endTime - startTime)/60, (endTime - startTime)/3600*1000000/evt_lim))

#
# Save the results
#
resultsName = 'training_dict_test_01.npz'
np.savez(resultsName,train_log=train_logging_dict)
print(resultsName,' saved.')

# plt.figure(figsize=(10,4))
# plt.style.use('fivethirtyeight')

# plt.subplot(1,2,1)
# plt.plot(train_logging_dict['loss'])
# plt.xlabel('Iterations')
# plt.ylabel('Loss')

# plt.subplot(1,2,2)
# plt.plot(train_logging_dict['loss_tau'])
# plt.xlabel('Iterations')
# plt.ylabel('Loss')


# In[109]:


plt.figure(figsize=(10,4))
plt.style.use('fivethirtyeight')

plt.subplot(1,2,1)
plt.plot(train_logging_dict['loss'])
plt.xlabel('Iterations')
plt.ylabel('Loss')

plt.subplot(1,2,2)
plt.plot(train_logging_dict['loss_tau'])
plt.xlabel('Iterations')
plt.ylabel('Loss in tau')


# In[106]:


plt.figure(figsize=(10,4))
plt.style.use('fivethirtyeight')

plt.subplot(1,2,1)
plt.plot(train_logging_dict['loss'])
plt.xlabel('Iterations')
plt.ylabel('Loss')

plt.subplot(1,2,2)
plt.plot(train_logging_dict['loss_tau'])
plt.xlabel('Iterations')
plt.ylabel('Loss')


# In[95]:


# torch.tensor(tauDSPWinOutput)
# torch.tensor(tauDSPWinInput)
loss


# In[93]:


# np.hstack()
# np.hstack(datasetML['digits'][windowOrdered])tauDSPWin
# tauDSPWin
loss_tau    = model.criterion(torch.tensor(tauDSPWinOutput),torch.tensor(tauDSPWinInput))


# In[98]:


# np.shape(np.asarray(datasetML['digits']))
# np.hstack(digits)
windowOrdered


# ### - Dev. Helpers

# In[57]:


# def getCellsInWindowCustomSizeNEW(
#                      cellEneList, 
#                      cellIndexList, 
#                      cellEtaList, 
#                      cellPhiList, 
#                      cellSamplingList, 
#                      nCellsEta, 
#                      nCellsPhi, 
#                      DEtaList,
#                      DPhiList,
#                      layer='EMB2',
#                      fullCluster=False):
#     ''' 
#     Create a nCellsEta x nCellsPhi cell ROI.
    
#     # Return the indexes of the array of cells. 
#     # This array is composed of a sub-array of the cluster, with only the cells of the selected layer.
#     # window        : set of positional index inside sub-array, which belongs to the window
#     # layerIndexes  : array of indexes of all cells from chosen layer.'''
   
#     # get the hot cell pos and value (for the layer)
#     layerIndexes      = getMatchedIndexes(cellSamplingList[0], layer)
    
#     if fullCluster == True:
#         return layerIndexes

#     if np.size( layerIndexes ) == 0:
#         return [], []
    
#     # determine the window size in eta x phi
#     if layer=='EMB2':
#         deta = 0.025#00000037252903
#         dphi = 0.024#54369328916073
#     else:
#         deta = np.min(np.array(DEtaList[0])[layerIndexes])
#         dphi = np.min(np.array(DPhiList[0])[layerIndexes])

#     etaSize = nCellsEta * deta
#     phiSize = nCellsPhi * dphi  
    
#     # print(deta,etaSize,dphi,phiSize)
    
#     hotCellValue    = np.max(np.array(cellEneList[0])[layerIndexes])
#     hotCellPos      = np.argmax(np.array(cellEneList[0])[layerIndexes])
#     hotCellEta      = np.array(cellEtaList[0])[layerIndexes][hotCellPos]
#     hotCellPhi      = np.array(cellPhiList[0])[layerIndexes][hotCellPos]
#     # print('hotcell position: ',hotCellEta,hotCellPhi,', granularity: ',np.array(DEtaList[0])[layerIndexes][hotCellPos],np.array(DPhiList[0])[layerIndexes][hotCellPos])
    
#     window          = [] # list of indexes of the cells inside the chosen window
    
#     # find cells within limits of eta phi window
#     # for sampling, ind, eta, phi in zip(cellSamplingList, cellIndexList[0], cellEtaList[0], cellPhiList[0]):
#     for ind, eta, phi in zip(layerIndexes, np.array(cellEtaList[0])[layerIndexes], np.array(cellPhiList[0])[layerIndexes]):
#         etaConditionL    = eta > (cellEtaList[0][hotCellPos] - etaSize/2)
#         etaConditionR    = eta < (cellEtaList[0][hotCellPos] + etaSize/2)
# #         phiConditionU    = phi >= correctPhi( (cellPhiList[0][hotCellPos] - phiSize/2) )
# #         phiConditionD    = phi <= correctPhi( (cellPhiList[0][hotCellPos] + phiSize/2) )
        
#         lowerPhiRange    = np.array(cellPhiList[0])[layerIndexes][hotCellPos] - phiSize/2
#         upperPhiRange    = np.array(cellPhiList[0])[layerIndexes][hotCellPos] + phiSize/2
        
#         bPhiOverFlow = ( (lowerPhiRange < (-1)*math.pi) or (lowerPhiRange > 1*math.pi) or (upperPhiRange < (-1)*math.pi) or (upperPhiRange > 1*math.pi) )
            
#             # if overflows, need to include the discontinuity in the middle of the interval.
#         if bPhiOverFlow:
#             phiCondition = (((phi >= correctPhi(lowerPhiRange)) and (phi <= 1*math.pi)) or ((phi <= correctPhi(upperPhiRange)) and (phi >= (-1)*math.pi)))
#         else:
#             phiCondition = ((phi >= lowerPhiRange) and (phi <= upperPhiRange))
        
#         if (etaConditionL and etaConditionR) and phiCondition:
#             window.append( ind ) # add them to a list of index cells
#             if len(window) >= ( nCellsEta * nCellsPhi ):
#                 break
                

#     return np.asarray(window), np.asarray(layerIndexes), hotCellPos

# def getSortedClusterIndexes(eta,phi):
#     '''Return the indexes of an organized sequence of pairs of eta,phi, following a line from the smallest phi's, then the smallest eta's. '''
#     ind = np.lexsort((eta,phi)) 
#     return ind

# def unpackCellsFromCluster(dataset_cluster, confDict, ped_dict={}):
#     # get list indexes that match to current sampling
#     channelLayerIndexes     = getMatchedIndexes(dataset_cluster['channels']['sampling'][0], confDict['sampling']) 
#     # rawChannelLayerIndexes  = getMatchedIndexes(ev['electrons'][elec][clusName][clus]['rawChannels']['sampling'][0], layer)

#     # is that layer empty for RAWCh or Ch? skip
#     if (len(channelLayerIndexes) == 0 ): # or (len(rawChannelLayerIndexes)==0):
#         return 0

#     ###################     ###############
#     ## Channel + Digits  +  ## RawChannel #
#     ###################     ###############
#     windowIndexesCh, layerIndexesCh, hotCellPos = getCellsInWindowCustomSizeNEW(
#         dataset_cluster['channels']['energy'],
#         dataset_cluster['channels']['index'],
#         dataset_cluster['channels']['eta'],
#         dataset_cluster['channels']['phi'],
#         dataset_cluster['channels']['sampling'],
#         confDict['nCellsWinEta'], 
#         confDict['nCellsWinPhi'], 
#         dataset_cluster['channels']['deta'],
#         dataset_cluster['channels']['dphi'],
#         layer=confDict['sampling'])

#     # any of the windows aren't full??
#     if confDict['bFullWindows']:
#         if (len(windowIndexesCh) != (nCellsWinEta*nCellsWinPhi)):# or (len(windowIndexesRawCh) != (nCellsWinEta*nCellsWinPhi)):
#             return 0

#     # if they belong to the same calo object and have the same size, they MUST have the same cells, because the dumper rule is the same.
#     digits              = np.array(dataset_cluster['channels']['digits'][0],dtype=object)
#     digitsSize          = len (np.array(dataset_cluster['channels']['digits'][0],dtype=object))
#     channelEnergy       = np.array(dataset_cluster['channels']['energy'][0],dtype=object)
#     channelTime         = np.array(dataset_cluster['channels']['time'][0],dtype=object)
#     # channelInfo         = listOfListsToArrayOfArrays(np.array(dataset_cluster['channels']['chInfo'][0],dtype=object))
#     channelInfo         = np.array(dataset_cluster['channels']['chInfo'][0],dtype=object)
#     channelId           = np.array(dataset_cluster['channels']['channelId'][0],dtype=object)
#     channelSampling     = np.array(dataset_cluster['channels']['sampling'][0],dtype=object)
#     channelEta          = np.array(dataset_cluster['channels']['eta'][0],dtype=object)
#     channelPhi          = np.array(dataset_cluster['channels']['phi'][0],dtype=object)
#     channelDEta         = np.array(dataset_cluster['channels']['deta'][0],dtype=object)
#     channelDPhi         = np.array(dataset_cluster['channels']['dphi'][0],dtype=object)
#     channelGain         = np.array(dataset_cluster['channels']['gain'][0],dtype=object)
#     channelDSPThr       = np.array(dataset_cluster['channels']['DSPThreshold'][0],dtype=object)
#     channelOFCa         = np.array(dataset_cluster['channels']['OFCa'][0],dtype=object)
#     channelOFCb         = np.array(dataset_cluster['channels']['OFCb'][0],dtype=object)
#     channelRamps0       = np.array(dataset_cluster['channels']['ADC2MeV0'][0],dtype=object)
#     channelRamps1       = np.array(dataset_cluster['channels']['ADC2MeV1'][0],dtype=object)
#     channelLArPed       = np.array(dataset_cluster['channels']['LArDB_Pedestal'][0],dtype=object)
#     channelTimeOffset   = np.array(dataset_cluster['channels']['OFCTimeOffset'][0],dtype=object)
#     channelEffSigma     = np.array(dataset_cluster['channels']['effSigma'][0],dtype=object)
#     # channelNoise        = np.array(dataset_cluster['channels']['noise'][0],dtype=object)          [windowIndexesCh] .tolist()
#     # channelShape        = np.array(ev['electrons'][elec][clusName][clus]['channels']['Shape'][0],dtype=object)          [windowIndexesCh] .tolist()
    
#     # if bDoOfflineCalib:
#     #     channelMinBiasAvg   = np.array(dataset_cluster['channels']['MinBiasAvg'][0],dtype=object)     [windowIndexesCh] .tolist()
#     #     channelOfflHV       = np.array(dataset_cluster['channels']['OfflHVScale'][0],dtype=object)    [windowIndexesCh] .tolist()
#     #     channelEneResc      = np.array(dataset_cluster['channels']['OfflEneRescaler'][0],dtype=object)[windowIndexesCh] .tolist()
#     # print('sampling size:',len(channelSampling))
#     # print('eta size:', len(channelEta))

#     # Selection of Pedestal (DB or Script)
#     if not(confDict['bPedFromDB']):
#         peds = [] #testing
#         gainsInt = getGainInteger(channelGain)
#     if confDict['bPedFromDB']:
#         peds = channelLArPed

#     # The cells that came from Window algorithm are not the same when comparing RawCh and Digits.
#     # For that, we will select the same Digits cells set, based on the HWID, inside the Raw Channel cluster cells set. 
#     # mirrorIndexes = []
#     for k, hwid in enumerate(channelId):
#         # position        = np.where(np.array(ev['electrons'][elec][clusKey][clus]['channels']['channelId'][0],dtype=object)[layerIndexesCh] == hwid)[0][0]
#         # mirrorIndexes.append(position) # its the window mirrored indexes for RawCh
#         if not(confDict['bPedFromDB']):
#             cell_dict_ped   = getLArChannelPED  (hwid, ped_dict  )
#             peds.append(cell_dict_ped['ped'][0][gainsInt[k]]) # picking up the right gain weights


#     orderedIndexes = getSortedClusterIndexes(channelEta[windowIndexesCh], channelPhi[windowIndexesCh])
    
#     ml_dataset = {
#         'orderedIndexes'    : orderedIndexes,
#         'windowIndexesCh'   : windowIndexesCh,
#         'layerIndexesCh'    : layerIndexesCh,
#         'hotcellposition'   : hotCellPos,
#         'digits'            : digits,
#         'digitsSize'        : digitsSize,
#         'channelEnergy'     : channelEnergy,
#         'channelTime'       : channelTime,
#         'channelInfo'       : channelInfo,
#         'channelId'         : channelId,
#         'channelSampling'   : channelSampling,
#         'channelEta'        : channelEta,
#         'channelPhi'        : channelPhi,
#         'channelDEta'       : channelDEta,
#         'channelDPhi'       : channelDPhi,
#         'channelGain'       : channelGain,
#         'channelDSPThr'     : channelDSPThr,
#         'channelOFCa'       : channelOFCa,
#         'channelOFCb'       : channelOFCb,
#         'channelRamps0'     : channelRamps0,
#         'channelRamps1'     : channelRamps1,
#         'channelLArPed'     : peds,
#         'channelTimeOffset' : channelTimeOffset,
#         'channelEffSigma'   : channelEffSigma,
#         # 'channelNoise'      : channelNoise,
#     }
    

#     # for key in ml_dataset.keys():
#     #     if bReturnOrderedInWindow:
#     #         ml_dataset[key] = np.array(ml_dataset[key],dtype=object)
#     #     else:
#     #         ml_dataset[key] = np.array(ml_dataset[key],dtype=object)
    

#     return ml_dataset


# ## 3.2) Test Uproot
# 
