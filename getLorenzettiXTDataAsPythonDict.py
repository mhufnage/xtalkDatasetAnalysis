# Lorenzetti Showers Collaboration 2020 - 2023 - https://doi.org/10.1016/j.cpc.2023.108671

import sys
sys.path.insert(0,'/home/mhufnage/lorenzetti/Applications') #lorenzetti utils path

# import subprocess
# subprocess.run(['. /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.24.08/x86_64-centos7-gcc48-opt/bin/thisroot.sh'])

# from ROOT import TFile, gROOT
from pandas import DataFrame
# from lorenzetti_utils import dataframe_h
from time import time
from pprint import pprint
import array
import numpy as np
import os 
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from auxiliaryFunctions import *
from lorenzetti_reader import *


## Reading AOD data ##
evtMax          = 1000000
SigmaCut        = 1
dumpCells       = True
dumpWindowOnly  = True
caloSampling    = CaloSampling.EMB2
winSize         = 3
DS_Lim          = 9
bDoNoNoise      = False
DS_List         = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]#[7, 8, 9 ]#[1, 2, 3, 4, 5, 6, 7, 8, 9 ]
if bDoNoNoise:
    DS_PathAlias     = '/data/atlas/mhufnage/lzt_single_e/elec_singleCell_10kEvts_centroid_e50_DS{}/AOD_noNoise/electron.AOD.root'
else:
    #DS_PathAlias     = '/data/atlas/mhufnage/lzt_single_e/elec_singleCell_10kEvts_centroid_e50_DS{}/AOD/electron.AOD.root'
    DS_PathAlias     = '/data/atlas/mhufnage/lzt_single_e/elec_singleCell_10kEvts_volume_e50_DS{}/AOD/electron.AOD.root'

#phase space regions
etBin  = [[15 , 20] ,[20, 30]  ,[30, 40]  ,[40, 50],[50,100],[100,10000000]]
etaBin = [[0.00, 0.80], [0.80, 1.37], [1.37,1.54],[1.54,2.50],[2.50,3.2]]
## **************** ##

sampText  = ['PSB', 'PSE', 'EM1', 'EM2', 'EM3']
fileNames = []
dataset   = []

startTime = time()

for dsindex in DS_List:#range(1,DS_Lim):
    path_signal   = DS_PathAlias.format(dsindex)
    path          = '/'.join(path_signal.split('/')[:-1])+'/'
    
    print('DS {}: '.format(dsindex),path)

    # Saving NPZ data 
    sig_dict      = read_events(path_signal,
                                evtMax,1,
                                etBin,
                                etaBin,
                                m_phaseSpaceDiv=False,
                                m_dumpCells=dumpCells, 
                                m_dumpOnlyWindow=dumpWindowOnly, 
                                m_sampling_filter=[caloSampling], 
                                m_eta_cut=1.4, 
                                m_eta_win=winSize, 
                                m_phi_win=winSize, 
                                m_sigma_cut=SigmaCut,
                                m_data_structure_mode='event') # <--- Pay attention here in case of using DS for analysis only!

    # Saving complete NPZ data
    
    if (dumpCells):   addName='.WithCells'
    else:             addName=''
    
    if bDoNoNoise:
        fileName = 'user.mhufnage.lzt.e50.xtalk.DS{}.win{}.nopileup{}_{}.event.noNoise.npz'.format(dsindex,winSize,addName,sampText[caloSampling])
    else:
        fileName = 'user.mhufnage.lzt.e50.xtalk.DS{}.win{}.nopileup{}_{}.event.npz'.format(dsindex,winSize,addName,sampText[caloSampling])
    fileNames.append(fileName)
    
    # dataset.append(sig_dict)
    # np.savez(path+fileName,**sig_dict)
    # np.savez(path+fileName,dataset=dataset)
    np.savez(path+fileName,dataset=sig_dict)
    print(' Dumped {} successfully'.format(fileName))
endTime = time()
print('Files saved: ',fileNames)
print('Total time: {:.2f} min ({:.2f} s)'.format((endTime-startTime)/60 , endTime-startTime))

