import sys
import ROOT
from ROOT import TFile, gROOT, TTree, TChain
from lorenzetti_utils import dataframe_h
try:
    gROOT.ProcessLine(dataframe_h)
except:
    pass
# if 'numpy' not in sys.modules:
import numpy as np
import math
from time import time

from helper_lib.auxiliaryFunctions import getSortedClusterIndexes

MeVToGeV        = 1/1000

class Detector():
  LAR     = 0
  TILE    = 1
  TTEM    = 2
  TTHEC   = 3
  FCALEM  = 5
  FCALHAD = 6

class CaloSampling():
    PSB       = 0
    PSE       = 1
    EMB1      = 2
    EMB2      = 3
    EMB3      = 4
    TileCal1  = 5
    TileCal2  = 6
    TileCal3  = 7
    TileExt1  = 8
    TileExt2  = 9
    TileExt3  = 10
    EMEC1     = 11
    EMEC2     = 12
    EMEC3     = 13
    HEC1      = 14
    HEC2      = 15
    HEC3      = 16
    
def lorenzetti_data_branches(tree, nCells=25, nSamples=25, nParticlesEvent=1, nClusterEvent=1):
    
    # Create branches
    vars_clusters_int       = ['roi']
    vars_clusters_double    = ['e','et','eta','phi','reta','rphi','rhad','eratio','weta2','f1','f3']
    vars_cells_int          = ['index','sampling','detector','hash']
    vars_cells_double       = ['e','eta','phi','et','tau','edep','tof','deta', 'dphi']#, 'winIndex', 'winOrdIndex', 'hotCellIndex']
    vars_pulse_n_cells      = ['pulse']
    
    d = { 'cluster_'+key:ROOT.std.vector('int')(nClusterEvent*[0]) for key in vars_clusters_int }
    d.update({'xtcluster_'+key:ROOT.std.vector('int')(nClusterEvent*[0]) for key in vars_clusters_int })    
    d.update({  'cluster_'+key:ROOT.std.vector('double')(nClusterEvent*[0.]) for key in vars_clusters_double })
    d.update({'xtcluster_'+key:ROOT.std.vector('double')(nClusterEvent*[0.]) for key in vars_clusters_double })
    
    d.update({'cell_'+key:ROOT.std.vector('int')(nCells*[0]) for key in vars_cells_int})
    d.update({'cell_'+key:ROOT.std.vector('double')(nCells*[0.]) for key in vars_cells_double})
    d.update({'cell_'+key:ROOT.std.vector('double')(nCells*nSamples*[0.]) for key in vars_pulse_n_cells})
    d.update({'xtcell_'+key:ROOT.std.vector('int')(nCells*[0]) for key in vars_cells_int})
    d.update({'xtcell_'+key:ROOT.std.vector('double')(nCells*[0.]) for key in vars_cells_double})
    d.update({'xtcell_'+key:ROOT.std.vector('double')(nCells*nSamples*[0.]) for key in vars_pulse_n_cells})
    
    
    
    # d['eventNumber']=ROOT.std.vector('int')(nCells*[0.])
    # d['rings']=ROOT.std.vector('double')([0.])
    # d['xtrings']=ROOT.std.vector('double')([0.])
    d['truth_e']=ROOT.std.vector('double')(nParticlesEvent*[0.])
    d['truth_et']=ROOT.std.vector('double')(nParticlesEvent*[0.])
    d['truth_eta']=ROOT.std.vector('double')(nParticlesEvent*[0.])
    d['truth_phi']=ROOT.std.vector('double')(nParticlesEvent*[0.])
    d['truth_px']=ROOT.std.vector('double')(nParticlesEvent*[0.])
    d['truth_py']=ROOT.std.vector('double')(nParticlesEvent*[0.])
    d['truth_pz']=ROOT.std.vector('double')(nParticlesEvent*[0.])
    d['truth_vx']=ROOT.std.vector('double')(nParticlesEvent*[0.])
    d['truth_vy']=ROOT.std.vector('double')(nParticlesEvent*[0.])
    d['truth_vz']=ROOT.std.vector('double')(nParticlesEvent*[0.])
    
    # Initialize branches
    for key in d.keys():
        # print(key)
        tree.Branch(key, d[key] )
    
    return d

def lorenzetti_file_filter_for_ML(sTree, conditions, rootFileName='newFile.root'):
    '''
    Gets the output of AOD in Lorenzetti and conditions as inputs.
    Transforms it into cuts for noise, energy, sampling and window size (eta x phi).
    Save into new root file, with the same labels from dict.
    
    '''
    
    # Create TTree and output File
    output      = TFile(rootFileName,'recreate')
    tree        = TTree('ML_tree','ML_tree')
    
    nCells          = conditions['etaSize'] * conditions['phiSize']
    nSamples        = conditions['nSamples']
    nParticlesEvent = 1
    # nClusterEvent   = 1
    
    # Create and Link variables to branches    
    newData = lorenzetti_data_branches(tree, nCells=nCells, nSamples=nSamples)
    
    if conditions['nEvents']==-1:
        maxEvents=sTree.GetEntries()
    else:
        maxEvents=conditions['nEvents']
    
    # Loop over events, to filter window
    for entry in range(maxEvents):
        if entry % 1000 == 0:
            print('{} entries processed so far.'.format(entry))
            
        dsDict = read_Lorenzetti_entry( tree                = sTree,
                                    entryNumber             = entry, # <--- current event here.
                                    etBin                   = conditions['etBin'],
                                    etaBin                  = conditions['etaBin'],
                                    m_phaseSpaceDiv         = conditions['bPhaseSpaceDiv'],
                                    m_dumpCells             = conditions['dumpCells'],
                                    m_dumpOnlyWindow        = conditions['bDumpWindowOnly'],
                                    m_sampling_filter       = [conditions['caloSampling']], 
                                    m_eta_cut               = conditions['etaCut'], 
                                    m_eta_win               = conditions['etaSize'], 
                                    m_phi_win               = conditions['phiSize'], 
                                    m_sigma_cut             = conditions['SigmaCut'], 
                                    m_noEneCut              = conditions['bNoEneCut'],
                                    m_data_structure_mode   = '')

        # Fill branches: CELLS
        for idx in range(0,nCells):
            for key in newData.keys():
                if (not 'cluster' in key) and (not 'truth' in key):
                    if 'pulse' in key:
                        continue
                    else:
                        newData[key][idx] = dsDict[key][0][idx]
        
        # Fill CLUSTERS and TRUTH
        for idx in range(0,nParticlesEvent):
            for key in newData.keys():
                if ('cluster' in key) or ('truth' in key):
                    newData[key][0] = dsDict[key][0]
        
        # Fill CELL_PULSES
        for idx in range(0, nCells*nSamples):
            for key in ['cell_pulse', 'xtcell_pulse']:
                newData[key][idx] = dsDict[key][0].flatten()[idx]
                        
    
        tree.Fill()
    
    output.Write()
    output.Close()


def windowCutAndReorderCells_lztData(cellEneList, cellEtaList, cellPhiList, nCellsEta, nCellsPhi):
    
    if np.size( cellEneList ) == 0:
        return [], [], []
    
    testEneLen = len(cellEneList)
    if testEneLen != len(cellEtaList):
        print('Energies list has different size from cellEtaList')
        return [],[], []
    if testEneLen != len(cellPhiList):
        print('Energies list has different size from cellPhiList')
        return [],[], []
    
    # determine the window size in eta x phi (EMB2)
    deta = 0.025
    dphi = 0.025

    etaSize = nCellsEta * deta
    phiSize = nCellsPhi * dphi
    hotCellPos      = np.argmax(np.array(cellEneList))
    
    window      = [] # list of indexes of the cells inside the chosen window
    window_eta  = []
    window_phi  = []
    
    # find cells within limits of eta phi window
    for ind, (eta, phi) in enumerate(zip(np.array(cellEtaList), np.array(cellPhiList))):
        etaConditionL    = eta >= (np.array(cellEtaList)[hotCellPos] - etaSize/2)
        etaConditionR    = eta <= (np.array(cellEtaList)[hotCellPos] + etaSize/2)
        
        lowerPhiRange    = np.array(cellPhiList)[hotCellPos] - phiSize/2
        upperPhiRange    = np.array(cellPhiList)[hotCellPos] + phiSize/2
        
        bPhiOverFlow = ( (lowerPhiRange < (-1)*math.pi) or (lowerPhiRange > 1*math.pi) or (upperPhiRange < (-1)*math.pi) or (upperPhiRange > 1*math.pi) )
            
        # if overflows, need to include the discontinuity in the middle of the interval.
        if bPhiOverFlow:
            phiCondition = (((phi >= correctPhi(lowerPhiRange)) and (phi <= 1*math.pi)) or ((phi <= correctPhi(upperPhiRange)) and (phi >= (-1)*math.pi)))
        else:
            phiCondition = ((phi >= lowerPhiRange) and (phi <= upperPhiRange))
        
        if (etaConditionL and etaConditionR) and phiCondition:
            window.append( ind ) # add them to a list of index cells
            window_eta.append( eta )
            window_phi.append( phi )
            if len(window) >= ( nCellsEta * nCellsPhi ):
                break
                
    windowOrdered = getSortedClusterIndexes(window_eta, window_phi, neta=nCellsEta, nphi=nCellsPhi)
    # windowOrdered = getSortedClusterIndexes(cellEtaList, cellPhiList, neta=nCellsEta, nphi=nCellsPhi)

    return np.fromiter(window, dtype=np.int32, count=nCellsEta*nCellsPhi), windowOrdered, hotCellPos, nCellsEta*nCellsPhi # window indexes and hottestCell Index


def stdvector_to_list(vec, size=None):
  if size:
    l=size*[0]
  else:
    l = vec.size()*[0]
  for i in range(vec.size()):
    l[i] = vec[i]
  return l

def lorenzetti_data_dict():
    vars_clusters   = ['roi','cell_links','e','et','eta','phi','reta','rphi','rhad','eratio','weta2','f1','f3']
    vars_cells      = ['index','sampling','detector','pulse','e','eta','phi','et','tau','edep','tof','deta', 'dphi', 'hash', 'winIndex', 'winOrdIndex', 'hotCellIndex']

    d = { 'cluster_'+key:[] for key in vars_clusters }
    d.update({'xtcluster_'+key:[] for key in vars_clusters })
    d.update({'cell_'+key:[] for key in vars_cells})
    d.update({'xtcell_'+key:[] for key in vars_cells})
    d['eventNumber']=[]
    d['target']=[]
    d['rings']=[]
    d['xtrings']=[]
    d['truth_e']=[]
    d['truth_et']=[]
    d['truth_eta']=[]
    d['truth_phi']=[]
    d['truth_px']=[]
    d['truth_py']=[]
    d['truth_pz']=[]
    d['truth_vx']=[]
    d['truth_vy']=[]
    d['truth_vz']=[]
    
    return d

def read_Lorenzetti_entry ( tree,
                            entryNumber,
                            etBin,
                            etaBin,
                            m_phaseSpaceDiv=False,
                            m_dumpCells=False,
                            m_dumpOnlyWindow=False,
                            m_sampling_filter=[], 
                            m_eta_cut=6.0,
                            m_eta_win=3,
                            m_phi_win=3,
                            m_sigma_cut=2.0,
                            m_noEneCut=True,
                            m_data_structure_mode=''):
    '''
    m_data_structure_mode : ['event', 'anything or empty'] -> in event mode, the dictionary will be stored into a list of dicts, event by event.
    '''
    
    if entryNumber >= tree.GetEntries():
        print('eof: entryNumber >= tree entries')
        return 0
    
    tree.GetEntry(entryNumber)
        
    sigma = m_sigma_cut
    sampNoise = [90, 26, 26, 60, 40] # PSB, PSE, EM1, EM2, EM3
    
    if m_data_structure_mode!='event':
        d = lorenzetti_data_dict()
    else:
        eventList = []

    # for eventNumber,event in enumerate(tree):
        
    if m_data_structure_mode=='event':
        d = lorenzetti_data_dict()
    
    # if (eventNumber % int(tree.GetEntries()/5)) == 0:
    #     print('\tEvent {}/{}...'.format(eventNumber, tree.GetEntries()))

    truth_container = tree.TruthParticleContainer_Particles
    for roi, truth in enumerate(truth_container):
        d['truth_e'].append(truth.e)
        d['truth_et'].append(truth.et)
        d['truth_eta'].append(truth.eta)
        d['truth_phi'].append(truth.phi)
        d['truth_px'].append(truth.px)
        d['truth_py'].append(truth.py)
        d['truth_pz'].append(truth.pz)
        d['truth_vx'].append(truth.vx)
        d['truth_vy'].append(truth.vy)
        d['truth_vz'].append(truth.vz)

    for xtSimu in ['','XT']:

        if tree.GetName()=='physics': ## AOD

            caloRings_container = getattr(tree, 'CaloRingsContainer_{}Rings'.format(xtSimu))
            for roi,caloRings in enumerate(caloRings_container):
                d['{}rings'.format(xtSimu.lower())].append(stdvector_to_list(caloRings.rings))

            cluster_container = getattr(tree, 'CaloClusterContainer_{}Clusters'.format(xtSimu))
            for roi,clus in enumerate(cluster_container):
                
                if abs(clus.eta) < m_eta_cut:
                                
                    if(m_phaseSpaceDiv):
                        if not(etBin[0]  < clus.et/1e3  < etBin[1] ): continue
                        if not(etaBin[0] < np.abs(clus.eta) < etaBin[1]): continue
                    
                    d['{}cluster_roi'.format(xtSimu.lower())].append( roi )
                    d['{}cluster_e'.format(xtSimu.lower())].append( clus.e*MeVToGeV )
                    d['{}cluster_et'.format(xtSimu.lower())].append( clus.et*MeVToGeV )
                    d['{}cluster_eta'.format(xtSimu.lower())].append( clus.eta )
                    d['{}cluster_phi'.format(xtSimu.lower())].append( clus.phi )
                    d['{}cluster_reta'.format(xtSimu.lower())].append( clus.reta )
                    d['{}cluster_rphi'.format(xtSimu.lower())].append( clus.rphi )
                    d['{}cluster_rhad'.format(xtSimu.lower())].append( clus.rhad )
                    d['{}cluster_eratio'.format(xtSimu.lower())].append( clus.eratio )
                    d['{}cluster_weta2'.format(xtSimu.lower())].append( clus.weta2 )
                    d['{}cluster_f1'.format(xtSimu.lower())].append( clus.f1 )
                    d['{}cluster_f3'.format(xtSimu.lower())].append( clus.f3 )
                    d['eventNumber'].append(entryNumber)
                    # d['target'].append(particle)
                    links = stdvector_to_list(clus.cell_links)
                    d['cluster_cell_links'].append( links )#stdvector_to_list(clus.cell_links) )
                    
                    if (m_dumpCells):
                        __cellEList=[]
                        __cellEtaList=[]
                        __cellPhiList=[]
                        
                        cellIndexList=[]
                        cellHashList=[]
                        cellEList=[]
                        cellEtaList=[]
                        cellDEtaList=[]
                        cellPhiList=[]
                        cellDPhiList=[]
                        cellEtList=[]
                        cellTauList=[]
                        cellEdepList=[]
                        cellTofList=[]
                        cellSamplingList=[]
                        cellDetectorList=[]
                        cellPulseList=[]

                        # detdescriptor_container = event.CaloDetDescriptorContainer_Cells
                        detdescriptor_container = getattr(tree, 'CaloDetDescriptorContainer_{}Cells'.format(xtSimu))

                        # cellTimeInit = time()
                        
                        for cell_index,detdesc in enumerate(detdescriptor_container):
                            if (cell_index in links) and (detdesc.sampling in m_sampling_filter) and ( (detdesc.e > sigma*sampNoise[detdesc.sampling]) or m_noEneCut  ):
                                cellIndexList.append( cell_index )
                                cellPulseList.append( stdvector_to_list( detdesc.pulse ) )
                                cellEdepList.append( detdesc.edep )
                                cellTofList.append( detdesc.tof )
                                cellSamplingList.append( detdesc.sampling )
                                cellDetectorList.append( detdesc.detector )
                                cellDEtaList.append( detdesc.deta )
                                cellDPhiList.append( detdesc.dphi )
                                cellHashList.append( detdesc.hash )
                            else: continue
                        # cellTimeFinal = time()
                        # print('CaloDetDescriptorContainer_{}Cells time is: {:.6f}s'.format(xtSimu, (cellTimeFinal-cellTimeInit)))
                        
                        # cellTimeInit = time()
                        
                        # cell_container = event.CaloCellContainer_Cells
                        cell_container = getattr(tree, 'CaloCellContainer_{}Cells'.format(xtSimu))
                        if xtSimu=='XT':
                            __cell_container = getattr(tree, 'CaloCellContainer_Cells')
                            for cell_index,cell in enumerate(__cell_container):
                                if cell_index in cellIndexList:
                                    __cellEList.append(cell.e)
                                    __cellEtaList.append(cell.eta)
                                    __cellPhiList.append(cell.phi)
                                else: continue
                            
                        for cell_index,cell in enumerate(cell_container):
                            if cell_index in cellIndexList:
                                cellEList.append(cell.e)
                                cellEtaList.append(cell.eta)
                                cellPhiList.append(cell.phi)
                                cellEtList.append(cell.et)
                                cellTauList.append(cell.tau)
                            else: continue
                        
                        # cellTimeFinal = time()
                        # print('CaloCellContainer_{}Cells time is: {:.6f}s'.format(xtSimu, (cellTimeFinal-cellTimeInit)))
                        
                        if (xtSimu!='XT'):
                            win, winOrd, hotC, winSize = windowCutAndReorderCells_lztData(cellEList, cellEtaList, cellPhiList, m_eta_win, m_phi_win)
                        else:
                            win, winOrd, hotC, winSize = windowCutAndReorderCells_lztData(__cellEList, __cellEtaList, __cellPhiList, m_eta_win, m_phi_win)
                        
                        if m_dumpOnlyWindow:

                            # d['{}cell_winIndex'.format(xtSimu.lower())].append( win )
                            # d['{}cell_winOrdIndex'.format(xtSimu.lower())].append( winOrd )
                            # d['{}cell_hotCellIndex'.format(xtSimu.lower())].append( hotC )
                            
                            d['{}cell_index'.format(xtSimu.lower())]    .append( np.fromiter(   cellIndexList, dtype=np.int32  ) [win[winOrd]] )
                            d['{}cell_e'.format(xtSimu.lower())]        .append( np.fromiter(       cellEList, dtype=np.float32) [win[winOrd]] )
                            d['{}cell_eta'.format(xtSimu.lower())]      .append( np.fromiter(     cellEtaList, dtype=np.float32) [win[winOrd]] )
                            d['{}cell_deta'.format(xtSimu.lower())]     .append( np.fromiter(    cellDEtaList, dtype=np.float32) [win[winOrd]] )
                            d['{}cell_phi'.format(xtSimu.lower())]      .append( np.fromiter(     cellPhiList, dtype=np.float32) [win[winOrd]] )
                            d['{}cell_dphi'.format(xtSimu.lower())]     .append( np.fromiter(    cellDPhiList, dtype=np.float32) [win[winOrd]] )
                            d['{}cell_et'.format(xtSimu.lower())]       .append( np.fromiter(      cellEtList, dtype=np.float32) [win[winOrd]] )
                            d['{}cell_tau'.format(xtSimu.lower())]      .append( np.fromiter(     cellTauList, dtype=np.float32) [win[winOrd]] )                           
                            d['{}cell_pulse'.format(xtSimu.lower())]    .append( np.array   (   cellPulseList, dtype=object    ) [win[winOrd]] )
                            d['{}cell_edep'.format(xtSimu.lower())]     .append( np.fromiter(    cellEdepList, dtype=np.float32) [win[winOrd]] )
                            d['{}cell_tof'.format(xtSimu.lower())]      .append( np.fromiter(     cellTofList, dtype=np.float32) [win[winOrd]] )
                            d['{}cell_sampling'.format(xtSimu.lower())] .append( np.fromiter(cellSamplingList, dtype=np.int32  ) [win[winOrd]] )
                            d['{}cell_detector'.format(xtSimu.lower())] .append( np.fromiter(cellDetectorList, dtype=np.int32  ) [win[winOrd]] )
                            d['{}cell_hash'.format(xtSimu.lower())]     .append( np.fromiter(    cellHashList, dtype=np.int64  ) [win[winOrd]] )

                            # d['{}cell_index'.format(xtSimu.lower())]    .append( np.array(   cellIndexList,dtype=object)[np.array(win)[winOrd]] )
                            # d['{}cell_e'.format(xtSimu.lower())]        .append( np.array(       cellEList,dtype=object)[np.array(win)[winOrd]] )
                            # d['{}cell_eta'.format(xtSimu.lower())]      .append( np.array(     cellEtaList,dtype=object)[np.array(win)[winOrd]] )
                            # d['{}cell_deta'.format(xtSimu.lower())]     .append( np.array(    cellDEtaList,dtype=object)[np.array(win)[winOrd]] )
                            # d['{}cell_phi'.format(xtSimu.lower())]      .append( np.array(     cellPhiList,dtype=object)[np.array(win)[winOrd]] )
                            # d['{}cell_dphi'.format(xtSimu.lower())]     .append( np.array(    cellDPhiList,dtype=object)[np.array(win)[winOrd]] )
                            # d['{}cell_et'.format(xtSimu.lower())]       .append( np.array(      cellEtList,dtype=object)[np.array(win)[winOrd]] )
                            # d['{}cell_tau'.format(xtSimu.lower())]      .append( np.array(     cellTauList,dtype=object)[np.array(win)[winOrd]] )
                            # d['{}cell_pulse'.format(xtSimu.lower())]    .append( np.array(   cellPulseList,dtype=object)[np.array(win)[winOrd]] )
                            # d['{}cell_edep'.format(xtSimu.lower())]     .append( np.array(    cellEdepList,dtype=object)[np.array(win)[winOrd]] )
                            # d['{}cell_tof'.format(xtSimu.lower())]      .append( np.array(     cellTofList,dtype=object)[np.array(win)[winOrd]] )
                            # d['{}cell_sampling'.format(xtSimu.lower())] .append( np.array(cellSamplingList,dtype=object)[np.array(win)[winOrd]] )
                            # d['{}cell_detector'.format(xtSimu.lower())] .append( np.array(cellDetectorList,dtype=object)[np.array(win)[winOrd]] )
                            # d['{}cell_hash'.format(xtSimu.lower())]     .append( np.array(    cellHashList,dtype=object)[np.array(win)[winOrd]] )
                            
                        else:                                
                            d['{}cell_winIndex'.format(xtSimu.lower())].append( win )
                            d['{}cell_winOrdIndex'.format(xtSimu.lower())].append( winOrd )
                            d['{}cell_hotCellIndex'.format(xtSimu.lower())].append( hotC )

                            d['{}cell_index'.format(xtSimu.lower())].append( cellIndexList )
                            d['{}cell_e'.format(xtSimu.lower())].append( cellEList )
                            d['{}cell_eta'.format(xtSimu.lower())].append( cellEtaList )
                            d['{}cell_deta'.format(xtSimu.lower())].append( cellDEtaList )
                            d['{}cell_phi'.format(xtSimu.lower())].append( cellPhiList )
                            d['{}cell_dphi'.format(xtSimu.lower())].append( cellDPhiList )
                            d['{}cell_et'.format(xtSimu.lower())].append( cellEtList )
                            d['{}cell_tau'.format(xtSimu.lower())].append( cellTauList )
                            d['{}cell_pulse'.format(xtSimu.lower())].append( cellPulseList )
                            d['{}cell_edep'.format(xtSimu.lower())].append( cellEdepList )
                            d['{}cell_tof'.format(xtSimu.lower())].append( cellTofList )
                            d['{}cell_sampling'.format(xtSimu.lower())].append( cellSamplingList )
                            d['{}cell_detector'.format(xtSimu.lower())].append( cellDetectorList )
                            d['{}cell_hash'.format(xtSimu.lower())].append( cellHashList )

        if m_data_structure_mode=='event':
            eventList.append( d )
        # if ((eventNumber >= evtMax )and (not (evtMax == -1))): break
        
    if m_data_structure_mode=='event':
        return eventList
    else:
        return d



def read_events (path_signal,
                 evtMax,
                 particle,
                 etBin,
                 etaBin,
                 m_phaseSpaceDiv=False,
                 m_dumpCells=False,
                 m_dumpOnlyWindow=False,
                 m_sampling_filter=[], 
                 m_eta_cut=6.0,
                 m_eta_win=3,
                 m_phi_win=3,
                 m_sigma_cut=2.0,
                 m_noEneCut=True,
                 m_data_structure_mode=''):
    '''
    m_data_structure_mode : ['event', 'anything or empty'] -> in event mode, the dictionary will be stored into a list of dicts, event by event.
    '''

    # path            = '/'.join(path_signal.split('/')[:-1])+'/'
    # dumpCells       = True

    # gROOT.ProcessLine(dataframe_h)
    
    file_sig        = TFile(path_signal)
    
    if path_signal.split('/')[-1].split('.')[1] == 'AOD':
        treeName = 'physics'
    if path_signal.split('/')[-1].split('.')[1] == 'ESD':
        treeName = 'CollectionTree'

    tree        = file_sig.Get(treeName)
    
    sigma = m_sigma_cut
    sampNoise = [90, 26, 26, 60, 40] # PSB, PSE, EM1, EM2, EM3
    
    if m_data_structure_mode!='event':
        d = lorenzetti_data_dict()
    else:
        eventList = []

    for eventNumber,event in enumerate(tree):
        
        if m_data_structure_mode=='event':
            d = lorenzetti_data_dict()
        
        if (eventNumber % int(tree.GetEntries()/5)) == 0:
            print('\tEvent {}/{}...'.format(eventNumber, tree.GetEntries()))

        truth_container = event.TruthParticleContainer_Particles
        for roi, truth in enumerate(truth_container):
            d['truth_e'].append(truth.e)
            d['truth_et'].append(truth.et)
            d['truth_eta'].append(truth.eta)
            d['truth_phi'].append(truth.phi)
            d['truth_px'].append(truth.px)
            d['truth_py'].append(truth.py)
            d['truth_pz'].append(truth.pz)
            d['truth_vx'].append(truth.vx)
            d['truth_vy'].append(truth.vy)
            d['truth_vz'].append(truth.vz)

        for xtSimu in ['','XT']:

            if tree.GetName()=='physics': ## AOD

                caloRings_container = getattr(event, 'CaloRingsContainer_{}Rings'.format(xtSimu))
                for roi,caloRings in enumerate(caloRings_container):
                    d['{}rings'.format(xtSimu.lower())].append(stdvector_to_list(caloRings.rings))

                cluster_container = getattr(event, 'CaloClusterContainer_{}Clusters'.format(xtSimu))
                for roi,clus in enumerate(cluster_container):
                    
                    if abs(clus.eta) < m_eta_cut:
                                    
                        if(m_phaseSpaceDiv):
                            if not(etBin[0]  < clus.et/1e3  < etBin[1] ): continue
                            if not(etaBin[0] < np.abs(clus.eta) < etaBin[1]): continue
                        
                        d['{}cluster_roi'.format(xtSimu.lower())].append( roi )
                        d['{}cluster_e'.format(xtSimu.lower())].append( clus.e*MeVToGeV )
                        d['{}cluster_et'.format(xtSimu.lower())].append( clus.et*MeVToGeV )
                        d['{}cluster_eta'.format(xtSimu.lower())].append( clus.eta )
                        d['{}cluster_phi'.format(xtSimu.lower())].append( clus.phi )
                        d['{}cluster_reta'.format(xtSimu.lower())].append( clus.reta )
                        d['{}cluster_rphi'.format(xtSimu.lower())].append( clus.rphi )
                        d['{}cluster_rhad'.format(xtSimu.lower())].append( clus.rhad )
                        d['{}cluster_eratio'.format(xtSimu.lower())].append( clus.eratio )
                        d['{}cluster_weta2'.format(xtSimu.lower())].append( clus.weta2 )
                        d['{}cluster_f1'.format(xtSimu.lower())].append( clus.f1 )
                        d['{}cluster_f3'.format(xtSimu.lower())].append( clus.f3 )
                        d['eventNumber'].append(eventNumber)
                        d['target'].append(particle)
                        d['cluster_cell_links'].append( stdvector_to_list(clus.cell_links) )
                        links = stdvector_to_list(clus.cell_links)
                        
                        if (m_dumpCells):
                            __cellEList=[]
                            __cellEtaList=[]
                            __cellPhiList=[]
                            
                            cellIndexList=[]
                            cellHashList=[]
                            cellEList=[]
                            cellEtaList=[]
                            cellDEtaList=[]
                            cellPhiList=[]
                            cellDPhiList=[]
                            cellEtList=[]
                            cellTauList=[]
                            cellEdepList=[]
                            cellTofList=[]
                            cellSamplingList=[]
                            cellDetectorList=[]
                            cellPulseList=[]

                            # detdescriptor_container = event.CaloDetDescriptorContainer_Cells
                            detdescriptor_container = getattr(event, 'CaloDetDescriptorContainer_{}Cells'.format(xtSimu))

                            for cell_index,detdesc in enumerate(detdescriptor_container):
                                if (cell_index in links) and (detdesc.sampling in m_sampling_filter) and ( (detdesc.e > sigma*sampNoise[detdesc.sampling]) or m_noEneCut  ):
                                    cellIndexList.append( cell_index )
                                    cellPulseList.append( stdvector_to_list( detdesc.pulse ) )
                                    cellEdepList.append( detdesc.edep )
                                    cellTofList.append( detdesc.tof )
                                    cellSamplingList.append( detdesc.sampling )
                                    cellDetectorList.append( detdesc.detector )
                                    cellDEtaList.append( detdesc.deta )
                                    cellDPhiList.append( detdesc.dphi )
                                    cellHashList.append( detdesc.hash )
                                else: continue

                            # cell_container = event.CaloCellContainer_Cells
                            cell_container = getattr(event, 'CaloCellContainer_{}Cells'.format(xtSimu))
                            if xtSimu=='XT':
                                __cell_container = getattr(event, 'CaloCellContainer_Cells')
                                for cell_index,cell in enumerate(__cell_container):
                                    if cell_index in cellIndexList:
                                        __cellEList.append(cell.e)
                                        __cellEtaList.append(cell.eta)
                                        __cellPhiList.append(cell.phi)
                                    else: continue
                                
                            for cell_index,cell in enumerate(cell_container):
                                if cell_index in cellIndexList:
                                    cellEList.append(cell.e)
                                    cellEtaList.append(cell.eta)
                                    cellPhiList.append(cell.phi)
                                    cellEtList.append(cell.et)
                                    cellTauList.append(cell.tau)
                                else: continue
                            
                            if (xtSimu!='XT'):
                                win, winOrd, hotC, winSize = windowCutAndReorderCells_lztData(cellEList, cellEtaList, cellPhiList, m_eta_win, m_phi_win)
                            else:
                                win, winOrd, hotC, winSize = windowCutAndReorderCells_lztData(__cellEList, __cellEtaList, __cellPhiList, m_eta_win, m_phi_win)
                            
                            if m_dumpOnlyWindow:

                                # d['{}cell_winIndex'.format(xtSimu.lower())].append( win )
                                # d['{}cell_winOrdIndex'.format(xtSimu.lower())].append( winOrd )
                                # d['{}cell_hotCellIndex'.format(xtSimu.lower())].append( hotC )

                                d['{}cell_index'.format(xtSimu.lower())]    .append( np.array(   cellIndexList,dtype=object)[np.array(win)[winOrd]] )
                                d['{}cell_e'.format(xtSimu.lower())]        .append( np.array(       cellEList,dtype=object)[np.array(win)[winOrd]] )
                                d['{}cell_eta'.format(xtSimu.lower())]      .append( np.array(     cellEtaList,dtype=object)[np.array(win)[winOrd]] )
                                d['{}cell_deta'.format(xtSimu.lower())]     .append( np.array(    cellDEtaList,dtype=object)[np.array(win)[winOrd]] )
                                d['{}cell_phi'.format(xtSimu.lower())]      .append( np.array(     cellPhiList,dtype=object)[np.array(win)[winOrd]] )
                                d['{}cell_dphi'.format(xtSimu.lower())]     .append( np.array(    cellDPhiList,dtype=object)[np.array(win)[winOrd]] )
                                d['{}cell_et'.format(xtSimu.lower())]       .append( np.array(      cellEtList,dtype=object)[np.array(win)[winOrd]] )
                                d['{}cell_tau'.format(xtSimu.lower())]      .append( np.array(     cellTauList,dtype=object)[np.array(win)[winOrd]] )
                                d['{}cell_pulse'.format(xtSimu.lower())]    .append( np.array(   cellPulseList,dtype=object)[np.array(win)[winOrd]] )
                                d['{}cell_edep'.format(xtSimu.lower())]     .append( np.array(    cellEdepList,dtype=object)[np.array(win)[winOrd]] )
                                d['{}cell_tof'.format(xtSimu.lower())]      .append( np.array(     cellTofList,dtype=object)[np.array(win)[winOrd]] )
                                d['{}cell_sampling'.format(xtSimu.lower())] .append( np.array(cellSamplingList,dtype=object)[np.array(win)[winOrd]] )
                                d['{}cell_detector'.format(xtSimu.lower())] .append( np.array(cellDetectorList,dtype=object)[np.array(win)[winOrd]] )
                                d['{}cell_hash'.format(xtSimu.lower())]     .append( np.array(    cellHashList,dtype=object)[np.array(win)[winOrd]] )
                                
                            else:                                
                                d['{}cell_winIndex'.format(xtSimu.lower())].append( win )
                                d['{}cell_winOrdIndex'.format(xtSimu.lower())].append( winOrd )
                                d['{}cell_hotCellIndex'.format(xtSimu.lower())].append( hotC )

                                d['{}cell_index'.format(xtSimu.lower())].append( cellIndexList )
                                d['{}cell_e'.format(xtSimu.lower())].append( cellEList )
                                d['{}cell_eta'.format(xtSimu.lower())].append( cellEtaList )
                                d['{}cell_deta'.format(xtSimu.lower())].append( cellDEtaList )
                                d['{}cell_phi'.format(xtSimu.lower())].append( cellPhiList )
                                d['{}cell_dphi'.format(xtSimu.lower())].append( cellDPhiList )
                                d['{}cell_et'.format(xtSimu.lower())].append( cellEtList )
                                d['{}cell_tau'.format(xtSimu.lower())].append( cellTauList )
                                d['{}cell_pulse'.format(xtSimu.lower())].append( cellPulseList )
                                d['{}cell_edep'.format(xtSimu.lower())].append( cellEdepList )
                                d['{}cell_tof'.format(xtSimu.lower())].append( cellTofList )
                                d['{}cell_sampling'.format(xtSimu.lower())].append( cellSamplingList )
                                d['{}cell_detector'.format(xtSimu.lower())].append( cellDetectorList )
                                d['{}cell_hash'.format(xtSimu.lower())].append( cellHashList )

            ### OUTDATED!
            else: ## ESD
                cellIndexList=[]
                cellHashList=[]
                cellEList=[]
                cellEtaList=[]
                cellDEtaList=[]
                cellPhiList=[]
                cellDPhiList=[]
                cellEtList=[]
                cellTauList=[]
                cellEdepList=[]
                cellTofList=[]
                cellSamplingList=[]
                cellDetectorList=[]
                cellPulseList=[]

                # detdescriptor_container = event.CaloDetDescriptorContainer_Cells
                detdescriptor_container = getattr(event, 'CaloDetDescriptorContainer_{}Cells'.format(xtSimu))

                for cell_index,detdesc in enumerate(detdescriptor_container):
                    if (detdesc.sampling in m_sampling_filter) and (detdesc.e > sigma*sampNoise[detdesc.sampling]):
                        cellIndexList.append( cell_index )
                        cellPulseList.append( stdvector_to_list( detdesc.pulse ) )
                        cellEdepList.append( detdesc.edep )
                        cellTofList.append( detdesc.tof )
                        cellSamplingList.append( detdesc.sampling )
                        cellDetectorList.append( detdesc.detector )
                        cellDEtaList.append( detdesc.deta )
                        cellDPhiList.append( detdesc.dphi )
                        cellHashList.append( detdesc.hash )
                    else: continue

                # cell_container = event.CaloCellContainer_Cells
                cell_container = getattr(event, 'CaloCellContainer_{}Cells'.format(xtSimu))
                for cell_index,cell in enumerate(cell_container):
                    if cell_index in cellIndexList:
                        # cellIndexList.append(cell_index)
                        cellEList.append(cell.e)
                        cellEtaList.append(cell.eta)
                        cellPhiList.append(cell.phi)
                        cellEtList.append(cell.et)
                        cellTauList.append(cell.tau)

                win, winOrd, hotC, winSize = windowCutAndReorderCells_lztData(cellEList, cellIndexList, cellEtaList, cellPhiList, m_eta_win, m_phi_win)

                d['{}cell_winIndex'.format(xtSimu.lower())].append( win )
                d['{}cell_winOrdIndex'.format(xtSimu.lower())].append( winOrd )
                d['{}cell_hotCellIndex'.format(xtSimu.lower())].append( hotC )

                d['{}cell_index'.format(xtSimu.lower())].append( cellIndexList )
                d['{}cell_e'.format(xtSimu.lower())].append( cellEList )
                d['{}cell_eta'.format(xtSimu.lower())].append( cellEtaList )
                d['{}cell_deta'.format(xtSimu.lower())].append( cellDEtaList )
                d['{}cell_phi'.format(xtSimu.lower())].append( cellPhiList )
                d['{}cell_dphi'.format(xtSimu.lower())].append( cellDPhiList )
                d['{}cell_et'.format(xtSimu.lower())].append( cellEtList )
                d['{}cell_tau'.format(xtSimu.lower())].append( cellTauList )
                d['{}cell_pulse'.format(xtSimu.lower())].append( cellPulseList )
                d['{}cell_edep'.format(xtSimu.lower())].append( cellEdepList )
                d['{}cell_tof'.format(xtSimu.lower())].append( cellTofList )
                d['{}cell_sampling'.format(xtSimu.lower())].append( cellSamplingList )
                d['{}cell_detector'.format(xtSimu.lower())].append( cellDetectorList )
                d['{}cell_hash'.format(xtSimu.lower())].append( cellHashList )

        if m_data_structure_mode=='event':
            eventList.append( d )
        if ((eventNumber >= evtMax )and (not (evtMax == -1))): break
        
    if m_data_structure_mode=='event':
        return eventList
    else:
        return d