
import ROOT
import numpy as np
# import pandas as pd
# from ROOT import TH1F, TTree, TChain, TFile, gROOT
from glob import glob
from time import time
# from histHelper import histLayerRegionFixBin, histLayerDynBin, histTH1F, histTH1D, histTH2F, plotTH1F, plotTH2F  #custom
# from functionsHelper import getRegionString, getSamplingString, getCaloGainString ,stdVecToArray, stdVecOfStdVecToArrayOfList, indexConversion, isInsideLArCrackRegion, loadJsonFile, saveAsJsonFile, save_pickle, load_pickle #custom
from helper_lib.functionsHelper import getRegionString, getSamplingString, getCaloGainString ,stdVecToArray, stdVecOfStdVecToArrayOfList, indexConversion, isInsideLArCrackRegion, loadJsonFile, saveAsJsonFile, save_pickle, load_pickle #custom
import math
import argparse
import logging
import os, sys

## Custom Packages
# from auxiliaryFunctions import getMatchedIndexes

ROOT.gROOT.SetBatch(True)

def getDictForAnalysis(layersDict, type='full'):
    '''This function is a mask to get ta dictionary that suits better to analysis.
    Types of dict:
    1) 'full'
    2) 'channel'
    3) 'rawChannel'
    4) 'particle'
    '''
    # layers              = caloDict["Layer"][0:4]
    analysis   = [
        # Particle / event
        'datasetElectronIndex',

        # Channel
        'digitsWindow',
        'digitsWindowSize',
        'channelEnergyWindow',
        'channelTimeWindow', 
        # 'channelInfoWindow',
        'channelBarrel_ec',
        'channelPos_neg',
        'channelFeedthrough',
        'channelSlot', 
        'channelNumber',
        'channelIdWindow',
        'channelGainWindow',
        'channelEtaWindow',
        'channelPhiWindow',
        'channelEffSigmaWindow',

        # Online Calib
        'channelDSPThrWindow',
        'channelRamps0Window',
        'channelRamps1Window',
        'channelPedWindow',
        'channelPedDictWindow',
        
        # Offline calib
        # 'channelShapeWindow',
        # 'channelMinBiasAvgWindow',
        # 'channelOfflHVWindow',
        # 'channelEneRescWindow',

        # Estimated values (using calib. constants)
        'AWindow',
        'tauDSPWindow',  # online (DSP) calibration
        'EDSPCalibWindow',   # online (DSP) calibration
        'tauDSPThrDiffWindow',
        # 'eneDSPerrorCalib',
        # 'tauDSPerrorCalib',
        # 'eneOfflineWindow',
        # 'tauOfflineWindow',

        'rawChEnergyWindow',
        'rawChTimeWindow',                  
        'rawChInfoWindow',
        'rawChIdWindow',
        'rawChGainWindow',
        'rawChDSPThrWindow',
        'rawChEtaWindow',
        'rawChPhiWindow',
        'rawChProvWindow'
        ]

    results                 = {key:{key1:[] for key1 in analysis} for key in layersDict} #mount
    # Particle: Electron data
    results['electron']     = {'datasetElectronIndex': [],'electronPt': [],'electronEta': [],'electronPhi': [],'electronVertexZ': [],'electronNClus': [],'electronClusSizes': []} 
    
    return results


# ----------------------- (*) LumiBlock -------------------------------------
def getLBInfoAsPythonDict(sLBTree, evLB):

    lumiblockDict = {
    "LumiBlock":        [],
    "LumiPerBCID":      []
    }

    sLBTree.GetEntry(evLB)

    lumiBlockFile           = getattr(sLBTree, "lb_lumiblock")
    lumiPerBCIDperLB        = getattr(sLBTree, "lb_bcidLuminosity")

    lumiPerBCIDperLBArray   = stdVecOfStdVecToArrayOfList(lumiPerBCIDperLB)
    lumiBlockFileArray      = stdVecToArray(lumiBlockFile)

    lumiblockDict["LumiBlock"].append(lumiBlockFileArray)
    lumiblockDict["LumiPerBCID"].append(lumiPerBCIDperLBArray)

    # datasetDict.update(lumiblockDict) # update dataset dictionary with new LB info
    # print(sLBTree, theEvent)
    
    return lumiblockDict
# ----------------------------------------------------------------------------


def getXTDataAsPythonDict(sTree, evN, eventReaderSharePath, dataHasBadCh=True, isMC=False, verbose=0):
    '''Return 0 case there were NO electrons in this event.'''

    ############################
    ####### Configuration ######
    ############################
    # jetROI  = -1

    # dataHasBadCh    = True # if there isnt any bad channel, force it to false.
    try:
        dataBadCh   = getattr(sTree,'cluster_channel_bad')
        # jetROI  = np.float32(getattr(sTree, 'jet_roi_r'))
    except:
        if (verbose==1):
            print("Data does not have bad_channel info. dataHasBadCh=False")
        dataHasBadCh  = False # if the data dumped has the flag 'noBadCells=True', this flag has to be False. Otherwise, its True.
        pass
    # evLim               = nEvts

    # ****** DICTIONARIES ******
    fDictLayerName      = eventReaderSharePath+'dictCaloByLayer.json'
    dictCaloLayer       = loadJsonFile(fDictLayerName) # Load Calo Geometry Dictionary
    transitionEtaWidthL = dictCaloLayer["transitionRegionEta"] #0.2 # eta value around the transition regions to consider (or discard) cells which could be tagged as belonging to another region in the same layer.
    transitionEtaWidthH = dictCaloLayer["transitionRegionEta"] #0.2 # (eta + transitionEtaWidthL) < Eta < (eta - transitionEtaWidthH). A value for each layer.
    maxOfLayers         = len(dictCaloLayer['Layer']) 

    # Number of regions in each sampling layer (eta x phi region)
    regionsPerLayer = []
    for reg in dictCaloLayer['granularityEtaLayer']:
        regionsPerLayer.append(len(reg))

    datasetDict = {"dataset": []}

    sTree.GetEntry(evN)
    # ----------------------------------------
    # Verify if the event has any electron: 
    # ----------------------------------------
    electronIndex          = getattr(sTree,'el_index')
    electronIndexArray     = stdVecToArray(electronIndex)

    # print(len(electronIndexArray)) # debug
    if len(electronIndexArray) == 0: # skip events without electrons
        return 0

    # -------------------------------------------
    
    ei_runNumber    = int(getattr(sTree, "event_RunNumber"))
    ei_eventNumber  = int(getattr(sTree, "event_EventNumber"))
    ei_bcid         = int(getattr(sTree, "event_BCID"))
    ei_lumibNumber  = int(getattr(sTree, "event_Lumiblock"))
    ei_avgMu        = getattr(sTree, "event_avg_mu_inTimePU")
    ei_avgMuOOT     = getattr(sTree, "event_avg_mu_OOTimePU")
    

    ### Particle Level
    electronPt                  = getattr(sTree,'el_Pt')
    # electronIndex               = getattr(sTree,'el_index')
    electronIndex_clusterLvl    = getattr(sTree,'c_electronIndex_clusterLvl')
    electronEta                 = getattr(sTree,'el_Eta')
    electronPhi                 = getattr(sTree,'el_Phi')
    electronEoverP              = getattr(sTree,'el_eoverp')
    electronSSf1                = getattr(sTree,'el_f1')
    electronSSf3                = getattr(sTree,'el_f3')
    electronSSeratio            = getattr(sTree,'el_eratio')
    electronSSweta1             = getattr(sTree,'el_weta1')
    electronSSweta2             = getattr(sTree,'el_weta2')
    electronSSfracs1            = getattr(sTree,'el_fracs1')
    electronSSwtots1            = getattr(sTree,'el_wtots1')
    electronSSe277              = getattr(sTree,'el_e277')
    electronSSreta              = getattr(sTree,'el_reta')
    electronSSrphi              = getattr(sTree,'el_rphi')
    electronSSdeltae            = getattr(sTree,'el_deltae')
    electronSSrhad              = getattr(sTree,'el_rhad')
    electronSSrhad1             = getattr(sTree,'el_rhad1')
    vertexZ                     = getattr(sTree,'vtx_z')

    ### TopoCluster or SuperCluster
    clusterIndex                 = getattr(sTree,'cluster_index')
    clusterPt                    = getattr(sTree,'cluster_pt')
    clusterEta                   = getattr(sTree,'cluster_eta')
    clusterPhi                   = getattr(sTree,'cluster_phi')
    # clusterEtaCalc               = getattr(sTree,'cluster_eta_calc')
    # clusterPhiCalc               = getattr(sTree,'cluster_phi_calc')
    # Cluster Cells 
    clusterCellIndex             = getattr(sTree,'cluster_cell_index') 
    clusterIndexCellLvl          = getattr(sTree,'cluster_index_cellLvl')
    clusterCellCaloGain          = getattr(sTree,'cluster_cell_caloGain')
    clusterCellLayer             = getattr(sTree,'cluster_cell_layer')
    clusterCellEnergy            = getattr(sTree,'cluster_cell_energy')
    clusterCellTime              = getattr(sTree,'cluster_cell_time')
    clusterCellRegion            = getattr(sTree,'cluster_cell_region')
    clusterCellEta               = getattr(sTree,'cluster_cell_eta')
    clusterCellPhi               = getattr(sTree,'cluster_cell_phi') 
    clusterCellDEta              = getattr(sTree,'cluster_cell_deta')
    clusterCellDPhi              = getattr(sTree,'cluster_cell_dphi')
    clusterCellsDistDEta         = getattr(sTree,'cluster_cellsDist_deta')
    clusterCellsDistDPhi         = getattr(sTree,'cluster_cellsDist_dphi')
    # Cluster Channel 
    clusterIndexChLvl            = getattr(sTree,'cluster_index_chLvl')
    clusterChannelIndex          = getattr(sTree,'cluster_channel_index')
    clusterChannelHash           = getattr(sTree,'cluster_channel_hash')
    clusterChannelId             = getattr(sTree,'cluster_channel_id')
    clusterChannelDigits         = getattr(sTree,'cluster_channel_digits')
    clusterChannelEnergy         = getattr(sTree,'cluster_channel_energy')
    clusterChannelTime           = getattr(sTree,'cluster_channel_time')
    clusterChannelLayer          = getattr(sTree,'cluster_channel_layer')
    if dataHasBadCh: clusterChannelBad            = getattr(sTree,'cluster_channel_bad')
    clusterChannelChInfo         = getattr(sTree,'cluster_channel_chInfo')
    clusterChannelEffSigma       = getattr(sTree,'cluster_channel_effSigma')
    clusterChannelNoise          = getattr(sTree,'cluster_channel_noise')
    clusterChannelDSPThr         = getattr(sTree,'cluster_channel_DSPThreshold')
    clusterChannelOFCTimeOffset  = getattr(sTree,'cluster_channel_OFCTimeOffset')
    clusterChannelADC2MeV0       = getattr(sTree,'cluster_channel_ADC2MeV0')
    clusterChannelADC2MeV1       = getattr(sTree,'cluster_channel_ADC2MeV1')
    clusterChannelPed            = getattr(sTree,'cluster_channel_pedestal')
    clusterChannelOFCa           = getattr(sTree,'cluster_channel_OFCa')
    clusterChannelOFCb           = getattr(sTree,'cluster_channel_OFCb')
    clusterChannelShape          = getattr(sTree,'cluster_channel_shape')
    clusterChannelShapeDer       = getattr(sTree,'cluster_channel_shapeDer')
    clusterChannelMinBiasAvg     = getattr(sTree,'cluster_channel_MinBiasAvg')
    clusterChannelOfflHVScale    = getattr(sTree,'cluster_channel_OfflineHVScale')
    clusterChannelOfflEneResc    = getattr(sTree,'cluster_channel_OfflineEnergyRescaler')
    # Cluster raw channel 
    clusterIndexRawChLvl         = getattr(sTree,'cluster_index_rawChLvl')
    clusterRawChannelIndex       = getattr(sTree,'cluster_rawChannel_index')
    clusterRawChannelAmplitude   = getattr(sTree,'cluster_rawChannel_amplitude')
    clusterRawChannelTime        = getattr(sTree,'cluster_rawChannel_time')
    clusterRawChannelLayer       = getattr(sTree,'cluster_rawChannel_layer')
    clusterRawChannelPed         = getattr(sTree,'cluster_rawChannel_Ped')
    clusterRawChannelProv        = getattr(sTree,'cluster_rawChannel_Prov')
    clusterRawChannelQual        = getattr(sTree,'cluster_rawChannel_qual')
    clusterRawChannelChInfo      = getattr(sTree,'cluster_rawChannel_chInfo')
    clusterRawChannelId          = getattr(sTree,'cluster_rawChannel_id')
    clusterRawChannelDSPThr      = getattr(sTree,'cluster_rawChannel_DSPThreshold')

    # if isMC:


    ### Fixed Window Clusters (7x11 EMB2)
    cluster711Index                 = getattr(sTree,'cluster711_index')
    cluster711Pt                    = getattr(sTree,'cluster711_pt')
    cluster711Eta                   = getattr(sTree,'cluster711_eta')
    cluster711Phi                   = getattr(sTree,'cluster711_phi')
    # Cluster Cells 
    cluster711CellIndex             = getattr(sTree,'cluster711_cell_index') 
    cluster711IndexCellLvl          = getattr(sTree,'cluster711_index_cellLvl')
    cluster711CellCaloGain          = getattr(sTree,'cluster711_cell_caloGain')
    cluster711CellLayer             = getattr(sTree,'cluster711_cell_layer')
    cluster711CellRegion            = getattr(sTree,'cluster711_cell_region')
    cluster711CellEta               = getattr(sTree,'cluster711_cell_eta')
    cluster711CellPhi               = getattr(sTree,'cluster711_cell_phi') 
    cluster711CellDEta              = getattr(sTree,'cluster711_cell_deta')
    cluster711CellDPhi              = getattr(sTree,'cluster711_cell_dphi')
    cluster711CellsDistDEta         = getattr(sTree,'cluster711_cellsDist_deta')
    cluster711CellsDistDPhi         = getattr(sTree,'cluster711_cellsDist_dphi')
    # Cluster Channel 
    cluster711IndexChLvl            = getattr(sTree,'cluster711_index_chLvl')
    cluster711ChannelIndex          = getattr(sTree,'cluster711_channel_index')
    cluster711ChannelHash           = getattr(sTree,'cluster711_channel_hash')
    cluster711ChannelId             = getattr(sTree,'cluster711_channel_id')
    cluster711ChannelDigits         = getattr(sTree,'cluster711_channel_digits')
    cluster711ChannelEnergy         = getattr(sTree,'cluster711_channel_energy')
    cluster711ChannelTime           = getattr(sTree,'cluster711_channel_time')
    # if dataHasBadCh: cluster711ChannelBad            = getattr(sTree,'cluster711_channel_bad')
    cluster711ChannelChInfo         = getattr(sTree,'cluster711_channel_chInfo')
    cluster711ChannelEffSigma       = getattr(sTree,'cluster711_channel_effSigma')
    cluster711ChannelNoise          = getattr(sTree,'cluster711_channel_noise')
    cluster711ChannelDSPThr         = getattr(sTree,'cluster711_channel_DSPThreshold')
    cluster711ChannelOFCTimeOffset  = getattr(sTree,'cluster711_channel_OFCTimeOffset')
    cluster711ChannelADC2MeV0       = getattr(sTree,'cluster711_channel_ADC2MeV0')
    cluster711ChannelADC2MeV1       = getattr(sTree,'cluster711_channel_ADC2MeV1')
    cluster711ChannelPed            = getattr(sTree,'cluster711_channel_pedestal')
    cluster711ChannelOFCa           = getattr(sTree,'cluster711_channel_OFCa')
    cluster711ChannelOFCb           = getattr(sTree,'cluster711_channel_OFCb')
    cluster711ChannelShape          = getattr(sTree,'cluster711_channel_shape')
    cluster711ChannelShapeDer       = getattr(sTree,'cluster711_channel_shapeDer')
    cluster711ChannelMinBiasAvg     = getattr(sTree,'cluster711_channel_MinBiasAvg')
    cluster711ChannelOfflHVScale    = getattr(sTree,'cluster711_channel_OfflineHVScale')
    cluster711ChannelOfflEneResc    = getattr(sTree,'cluster711_channel_OfflineEnergyRescaler')

    # Cluster raw channel 
    cluster711IndexRawChLvl         = getattr(sTree,'cluster711_index_rawChLvl')
    cluster711RawChannelIndex       = getattr(sTree,'cluster711_rawChannel_index')
    cluster711RawChannelAmplitude   = getattr(sTree,'cluster711_rawChannel_amplitude')
    cluster711RawChannelTime        = getattr(sTree,'cluster711_rawChannel_time')
    cluster711RawChannelPed         = getattr(sTree,'cluster711_rawChannel_Ped')
    cluster711RawChannelProv        = getattr(sTree,'cluster711_rawChannel_Prov')
    cluster711RawChannelQual        = getattr(sTree,'cluster711_rawChannel_qual')
    cluster711RawChannelChInfo      = getattr(sTree,'cluster711_rawChannel_chInfo')
    cluster711RawChannelId          = getattr(sTree,'cluster711_rawChannel_id')
    cluster711RawChannelDSPThr      = getattr(sTree,'cluster711_rawChannel_DSPThreshold')

    # if isMC:
    #     mcTruthEnergy  = getattr(sTree,'mc_energy')
    #     mcTruthPt      = getattr(sTree,'mc_pt')
    #     mcTruthM       = getattr(sTree,'mc_m')
    #     mcTruthEta     = getattr(sTree,'mc_eta')
    #     mcTruthPhi     = getattr(sTree,'mc_phi')
    #     mcTruthPdgId   = getattr(sTree,'mc_pdgId')

    # photonEnergy  = getattr(sTree,'ph_energy')
    # photonPt      = getattr(sTree,'ph_pt')
    # photonEta     = getattr(sTree,'ph_eta')
    # photonPhi     = getattr(sTree,'ph_phi')
    # photonM       = getattr(sTree,'ph_m')

    #**********************************************
    # Format into python list/np array
    #**********************************************
    electronPtArray                 = stdVecToArray(electronPt)
    # electronIndexArray              = stdVecToArray(electronIndex)
    electronIndex_clusterLvlArray   = stdVecToArray(electronIndex_clusterLvl)
    electronEtaArray                = stdVecToArray(electronEta)
    electronPhiArray                = stdVecToArray(electronPhi)
    electronEoverPArray             = stdVecToArray(electronEoverP)
    electronSSf1Array               = stdVecToArray(electronSSf1)
    electronSSf3Array               = stdVecToArray(electronSSf3)
    electronSSeratioArray           = stdVecToArray(electronSSeratio)
    electronSSweta1Array            = stdVecToArray(electronSSweta1)
    electronSSweta2Array            = stdVecToArray(electronSSweta2)
    electronSSfracs1Array           = stdVecToArray(electronSSfracs1)
    electronSSwtots1Array           = stdVecToArray(electronSSwtots1)
    electronSSe277Array             = stdVecToArray(electronSSe277)
    electronSSretaArray             = stdVecToArray(electronSSreta)
    electronSSrphiArray             = stdVecToArray(electronSSrphi)
    electronSSdeltaeArray           = stdVecToArray(electronSSdeltae)
    electronSSrhadArray             = stdVecToArray(electronSSrhad)
    electronSSrhad1Array            = stdVecToArray(electronSSrhad1)
    vertexZArray                    = stdVecToArray(vertexZ)

    clusterIndexArray               = stdVecToArray(clusterIndex)
    clusterPtArray                  = stdVecToArray(clusterPt)
    clusterEtaArray                 = stdVecToArray(clusterEta)
    clusterPhiArray                 = stdVecToArray(clusterPhi)

    clusterCellIndexArray           = stdVecToArray(clusterCellIndex)
    clusterIndexCellLvlArray        = stdVecToArray(clusterIndexCellLvl)
    clusterCellCaloGainArray        = stdVecToArray(clusterCellCaloGain)
    clusterCellLayerArray           = stdVecToArray(clusterCellLayer)
    clusterCellRegionArray          = stdVecToArray(clusterCellRegion)
    clusterCellEtaArray             = stdVecToArray(clusterCellEta)
    clusterCellPhiArray             = stdVecToArray(clusterCellPhi)
    clusterCellDEtaArray            = stdVecToArray(clusterCellDEta)
    clusterCellDPhiArray            = stdVecToArray(clusterCellDPhi)
    clusterCellsDistDEtaArray       = stdVecToArray(clusterCellsDistDEta)
    clusterCellsDistDPhiArray       = stdVecToArray(clusterCellsDistDPhi)
    
    clusterIndexChLvlArray          = stdVecToArray(clusterIndexChLvl)
    clusterChannelIndexArray        = stdVecToArray(clusterChannelIndex)
    clusterChannelHashArray         = stdVecToArray(clusterChannelHash)
    clusterChannelIdArray           = stdVecToArray(clusterChannelId)
    clusterChannelDigitsArray       = stdVecOfStdVecToArrayOfList(clusterChannelDigits, convertToInt=True)
    clusterChannelEnergyArray       = stdVecToArray(clusterChannelEnergy)
    clusterChannelTimeArray         = stdVecToArray(clusterChannelTime)
    clusterChannelChInfoArray       = stdVecOfStdVecToArrayOfList(clusterChannelChInfo)
    if dataHasBadCh: clusterChannelBadArray      = stdVecToArray(clusterChannelBad)
    clusterChannelEffSigmaArray         = stdVecToArray(clusterChannelEffSigma)
    clusterChannelNoiseArray            = stdVecToArray(clusterChannelNoise)
    clusterChannelDSPThrArray           = stdVecToArray(clusterChannelDSPThr)
    clusterChannelOFCTimeOffsetArray    = stdVecToArray(clusterChannelOFCTimeOffset)
    clusterChannelADC2MeV0Array         = stdVecToArray(clusterChannelADC2MeV0)
    clusterChannelADC2MeV1Array         = stdVecToArray(clusterChannelADC2MeV1)
    clusterChannelPedArray              = stdVecToArray(clusterChannelPed)
    clusterChannelOFCaArray             = stdVecOfStdVecToArrayOfList(clusterChannelOFCa)
    clusterChannelOFCbArray             = stdVecOfStdVecToArrayOfList(clusterChannelOFCb)
    clusterChannelShapeArray            = stdVecOfStdVecToArrayOfList(clusterChannelShape)
    clusterChannelShapeDerArray         = stdVecOfStdVecToArrayOfList(clusterChannelShapeDer)
    clusterChannelMinBiasAvgArray       = stdVecToArray(clusterChannelMinBiasAvg)
    clusterChannelOfflHVScaleArray      = stdVecToArray(clusterChannelOfflHVScale)
    clusterChannelOfflEneRescArray      = stdVecToArray(clusterChannelOfflEneResc)

    clusterIndexRawChLvlArray       = stdVecToArray(clusterIndexRawChLvl)
    clusterRawChannelIndexArray     = stdVecToArray(clusterRawChannelIndex)
    clusterRawChannelAmplitudeArray = stdVecToArray(clusterRawChannelAmplitude)
    clusterRawChannelTimeArray      = stdVecToArray(clusterRawChannelTime)
    clusterRawChannelPedArray       = stdVecToArray(clusterRawChannelPed)
    clusterRawChannelProvArray      = stdVecToArray(clusterRawChannelProv)
    clusterRawChannelQualArray      = stdVecToArray(clusterRawChannelQual)
    clusterRawChannelChInfoArray    = stdVecOfStdVecToArrayOfList(clusterRawChannelChInfo)
    clusterRawChannelIdArray        = stdVecToArray(clusterRawChannelId)
    clusterRawChannelDSPThrArray    = stdVecToArray(clusterRawChannelDSPThr)

    cluster711IndexArray               = stdVecToArray(cluster711Index)
    cluster711PtArray                  = stdVecToArray(cluster711Pt)
    cluster711EtaArray                 = stdVecToArray(cluster711Eta)
    cluster711PhiArray                 = stdVecToArray(cluster711Phi)

    cluster711CellIndexArray           = stdVecToArray(cluster711CellIndex)
    cluster711IndexCellLvlArray        = stdVecToArray(cluster711IndexCellLvl)
    cluster711CellCaloGainArray        = stdVecToArray(cluster711CellCaloGain)
    cluster711CellLayerArray           = stdVecToArray(cluster711CellLayer)
    cluster711CellRegionArray          = stdVecToArray(cluster711CellRegion)
    cluster711CellEtaArray             = stdVecToArray(cluster711CellEta)
    cluster711CellPhiArray             = stdVecToArray(cluster711CellPhi)
    cluster711CellDEtaArray            = stdVecToArray(cluster711CellDEta)
    cluster711CellDPhiArray            = stdVecToArray(cluster711CellDPhi)
    cluster711CellsDistDEtaArray       = stdVecToArray(cluster711CellsDistDEta)
    cluster711CellsDistDPhiArray       = stdVecToArray(cluster711CellsDistDPhi)
    
    cluster711IndexChLvlArray          = stdVecToArray(cluster711IndexChLvl)
    cluster711ChannelIndexArray        = stdVecToArray(cluster711ChannelIndex)
    cluster711ChannelHashArray         = stdVecToArray(cluster711ChannelHash)
    cluster711ChannelIdArray           = stdVecToArray(cluster711ChannelId)
    cluster711ChannelDigitsArray       = stdVecOfStdVecToArrayOfList(cluster711ChannelDigits, convertToInt=True)
    cluster711ChannelEnergyArray       = stdVecToArray(cluster711ChannelEnergy)
    cluster711ChannelTimeArray         = stdVecToArray(cluster711ChannelTime)
    cluster711ChannelChInfoArray       = stdVecOfStdVecToArrayOfList(cluster711ChannelChInfo)
    # if dataHasBadCh: cluster711ChannelBadArray      = stdVecToArray(cluster711ChannelBad)
    cluster711ChannelEffSigmaArray     = stdVecToArray(cluster711ChannelEffSigma)
    cluster711ChannelNoiseArray        = stdVecToArray(cluster711ChannelNoise)
    cluster711ChannelDSPThrArray       = stdVecToArray(cluster711ChannelDSPThr)
    cluster711ChannelOFCTimeOffsetArray    = stdVecToArray(cluster711ChannelOFCTimeOffset)
    cluster711ChannelADC2MeV0Array         = stdVecToArray(cluster711ChannelADC2MeV0)
    cluster711ChannelADC2MeV1Array         = stdVecToArray(cluster711ChannelADC2MeV1)
    cluster711ChannelPedArray              = stdVecToArray(cluster711ChannelPed)
    cluster711ChannelOFCaArray             = stdVecOfStdVecToArrayOfList(cluster711ChannelOFCa)
    cluster711ChannelOFCbArray             = stdVecOfStdVecToArrayOfList(cluster711ChannelOFCb)
    cluster711ChannelShapeArray            = stdVecOfStdVecToArrayOfList(cluster711ChannelShape)
    cluster711ChannelShapeDerArray         = stdVecOfStdVecToArrayOfList(cluster711ChannelShapeDer)
    cluster711ChannelMinBiasAvgArray       = stdVecToArray(cluster711ChannelMinBiasAvg)
    cluster711ChannelOfflHVScaleArray      = stdVecToArray(cluster711ChannelOfflHVScale)
    cluster711ChannelOfflEneRescArray      = stdVecToArray(cluster711ChannelOfflEneResc)

    cluster711IndexRawChLvlArray       = stdVecToArray(cluster711IndexRawChLvl)
    cluster711RawChannelIndexArray     = stdVecToArray(cluster711RawChannelIndex)
    cluster711RawChannelAmplitudeArray = stdVecToArray(cluster711RawChannelAmplitude)
    cluster711RawChannelTimeArray      = stdVecToArray(cluster711RawChannelTime)
    cluster711RawChannelPedArray        = stdVecToArray(cluster711RawChannelPed)
    cluster711RawChannelProvArray       = stdVecToArray(cluster711RawChannelProv)
    cluster711RawChannelQualArray      = stdVecToArray(cluster711RawChannelQual)
    cluster711RawChannelChInfoArray    = stdVecOfStdVecToArrayOfList(cluster711RawChannelChInfo)
    cluster711RawChannelIdArray        = stdVecToArray(cluster711RawChannelId)
    cluster711RawChannelDSPThrArray    = stdVecToArray(cluster711RawChannelDSPThr)

    # timePerEvRead = time()

    #**********************************************
    #  XTALKS STUDIES: DATASET DICT
    #**********************************************

    # ----------------------- 0 EventInfo -------------------------------------
    eventInfoDict = {
        "runNumber":        [],
        "eventNumber":      [],
        "BCID":             [],
        "Lumiblock":        [],
        "avgMu":            [],
        "avgMuOOT":         [],
        "electrons":        {}
    }
    eventInfoDict["runNumber"].append(ei_runNumber)
    eventInfoDict["eventNumber"].append(ei_eventNumber)
    eventInfoDict["BCID"].append(ei_bcid)
    eventInfoDict["Lumiblock"].append(ei_lumibNumber)
    eventInfoDict["avgMu"].append(ei_avgMu)
    eventInfoDict["avgMuOOT"].append(ei_avgMuOOT)

    # ----------------------- I Electron -------------------------------------
    
    # print("Found an electron!")
    for elec in range(0, len(electronIndexArray)):
        electronDict    = {
        "el_{}".format(elec):{
            "index":      [],
            "pt":         [],
            "eta":        [],
            "phi":        [],
            "eoverp":     [],
            #shower shapes
            "f1":         [],
            "f3":         [],
            "eratio":     [],
            "weta1" :     [],
            "weta2" :     [],
            "fracs1":     [],
            "wtots1":     [],
            "e277"  :     [],
            "reta"  :     [],
            "rphi"  :     [],
            "deltae":     [],
            "rhad"  :     [],
            "rhad1" :     [],
            #vertex
            "vertexZ":    [],

            "clusters":{},
            "711_roi": {} 
            }
        } # electron-end

        electronDict["el_{}".format(elec)]["index"].append(electronIndexArray[elec])
        electronDict["el_{}".format(elec)]["pt"].append(electronPtArray[elec])
        electronDict["el_{}".format(elec)]["eta"].append(electronEtaArray[elec])
        electronDict["el_{}".format(elec)]["phi"].append(electronPhiArray[elec])
        electronDict["el_{}".format(elec)]["eoverp"].append(electronEoverPArray[elec])
        electronDict["el_{}".format(elec)]["f1"].append(electronSSf1Array[elec])
        electronDict["el_{}".format(elec)]["f3"].append(electronSSf3Array[elec])
        electronDict["el_{}".format(elec)]["eratio"].append(electronSSeratioArray[elec])
        electronDict["el_{}".format(elec)]["weta1"].append(electronSSweta1Array[elec])
        electronDict["el_{}".format(elec)]["weta2"].append(electronSSweta2Array[elec])
        electronDict["el_{}".format(elec)]["fracs1"].append(electronSSfracs1Array[elec])
        electronDict["el_{}".format(elec)]["wtots1"].append(electronSSwtots1Array[elec])
        electronDict["el_{}".format(elec)]["e277"].append(electronSSe277Array[elec])
        electronDict["el_{}".format(elec)]["reta"].append(electronSSretaArray[elec])
        electronDict["el_{}".format(elec)]["rphi"].append(electronSSrphiArray[elec])
        electronDict["el_{}".format(elec)]["deltae"].append(electronSSdeltaeArray[elec])
        electronDict["el_{}".format(elec)]["rhad"].append(electronSSrhadArray[elec])
        electronDict["el_{}".format(elec)]["rhad1"].append(electronSSrhad1Array[elec])
        electronDict["el_{}".format(elec)]["vertexZ"].append(vertexZArray[elec])

    # --------------------------------------------------------------------------
    #                       TopoCluster or SuperCluster
    # --------------------------------------------------------------------------

    # ----------------------- II Calo Objects (TopoCluster or SuperCluster) ----------------------
        # find array indexes for the cluster collection, from each electron.
        eIndexClus = np.where( electronIndex_clusterLvlArray == electronIndexArray[elec] )[0]

        # with the array indexes, find the the cluster indexes, linked to the selected electron
        # ( the cluster index is used to reach the cell level indexes. )
        # for cIndex in clusterIndexArray[eIndexClus]: # for each cluster
        for cl in eIndexClus:
    
            # layerIndex = np.where(clusterCellLayerArray[cIndexCell] == 2)[0] # get EMB2 cells
            clusterDict = {
                "cl_{}".format(clusterIndexArray[cl]):{
                    "index":        [],
                    "pt":           [],
                    "eta":          [],
                    "phi":          [],
                    # cells
                    "cells":         {}, # structure
                    "channels":      {}, # structure
                    "rawChannels":   {} # structure
                }
            }
            # fill clusters
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["index"].append(clusterIndexArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["pt"].append(clusterPtArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["eta"].append(clusterEtaArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["phi"].append(clusterPhiArray[cl])

    # ----------------------- III CaloCell and Channels ------------------
            # get cells, channels and rawChannels linked to each cluster
            clusIndexCell   = np.where( clusterIndexCellLvlArray    == clusterIndexArray[cl] )[0]
            clusIndexCh     = np.where( clusterIndexChLvlArray      == clusterIndexArray[cl] )[0]
            clusIndexRawCh  = np.where( clusterIndexRawChLvlArray   == clusterIndexArray[cl] )[0]
            # print("clusterCellIndexArray:", clusterCellIndexArray)
            # print("clusterRawChannelIndexArray",clusterRawChannelIndexArray)

            # -- Cells --
            # OBS.: it was chosen to remove most of the information in this part of dict (cell level), because
            # it is more convenient to work with the xtalk and cell data in the same level, in this case, in channel.
            # 28 Sept, it was added again to perform some tests.
            cellDict    = {
                "index":        [],
                "caloRegion":   [],
                "sampling":     [],
                "eta":          [],
                "phi":          [],
                "deta":         [],
                "dphi":         [],
                "gain":         []
            }
            cellRegionString    = getRegionString( clusterCellRegionArray[clusIndexCell] , dictCaloLayer["Region"])
            cellSamplingString  = getSamplingString( clusterCellLayerArray[clusIndexCell] , dictCaloLayer["Layer"] )
            cellCaloGainString  = getCaloGainString( clusterCellCaloGainArray[clusIndexCell] , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

            cellDict["index"].append( clusterCellIndexArray[clusIndexCell].tolist() )
            cellDict["sampling"].append( cellSamplingString )
            cellDict["caloRegion"].append( cellRegionString )
            cellDict["eta"].append( clusterCellEtaArray[clusIndexCell].tolist() )
            cellDict["phi"].append( clusterCellPhiArray[clusIndexCell].tolist() )
            cellDict["deta"].append( clusterCellDEtaArray[clusIndexCell].tolist() )
            cellDict["dphi"].append( clusterCellDPhiArray[clusIndexCell].tolist() )
            # cellDict["gain"].append( clusterCellCaloGainArray[clusIndexCell] )
            cellDict["gain"].append( cellCaloGainString )

            # print(clusterCellCaloGainArray[clusIndexCell], cellDict["gain"] )
            # print("debug el_{}, cl_{} clusterCellIndexArray[clusIndexCell]: ".format(elec,cl),clusterCellIndexArray[clusIndexCell])
            # print("debug el_{}, cl_{} clusterCellCaloGainArray[clusIndexCell]: ".format(elec,cl), clusterCellCaloGainArray[clusIndexCell] )
            # print("debug el_{} cl_{} clusterCellDEtaArray[clusIndexCell]: ".format(elec,cl),clusterCellDEtaArray[clusIndexCell])

            # -- Channels --
            channelDict     = {
                "index":            [],
                "caloRegion":       [], # from cell level
                "sampling":         [], # from cell level
                "energy":           [],
                "time":             [],
                "digits":           [],
                "eta":              [], # from cell level
                "phi":              [], # from cell level
                "gain":             [], # from cell level
                "deta":             [], # from cell level
                "dphi":             [], # from cell level
                "chInfo":           [], # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                "hashId":           [],
                "channelId":        [],
                "effSigma":         [],
                "noise":            [],
                "DSPThreshold":     [],
                "OFCTimeOffset":    [],
                "ADC2MeV0":         [],
                "ADC2MeV1":         [],
                "LArDB_Pedestal":   [],
                "OFCa":             [],
                "OFCb":             [],
                "Shape":            [],
                "ShapeDer":         [],
                "MinBiasAvg":       [],
                "OfflHVScale":      [],
                "OfflEneRescaler":  []
            }
            if dataHasBadCh:
                channelDict.update({"badCh": []})
                channelDict["badCh"].append(clusterChannelBadArray[clusIndexCh].tolist())

            roundedChIndex  = [round(x,1) for x in clusterChannelIndexArray[clusIndexCh]]
        
            channelDict["index"].append( roundedChIndex )            
            channelDict["energy"].append(clusterChannelEnergyArray[clusIndexCh].tolist())
            channelDict["time"].append(clusterChannelTimeArray[clusIndexCh].tolist())
            channelDict["digits"].append(clusterChannelDigitsArray[clusIndexCh].tolist())
            channelDict["chInfo"].append(clusterChannelChInfoArray[clusIndexCh].tolist())
            channelDict["hashId"].append(clusterChannelHashArray[clusIndexCh].tolist())
            channelDict["channelId"].append(clusterChannelIdArray[clusIndexCh].tolist())
            channelDict["effSigma"].append(clusterChannelEffSigmaArray[clusIndexCh].tolist())
            channelDict["noise"].append(clusterChannelNoiseArray[clusIndexCh].tolist())
            channelDict["DSPThreshold"].append(clusterChannelDSPThrArray[clusIndexCh].tolist())
            channelDict["OFCTimeOffset"].append(clusterChannelOFCTimeOffsetArray[clusIndexCh].tolist())
            channelDict["ADC2MeV0"].append(clusterChannelADC2MeV0Array[clusIndexCh].tolist())
            channelDict["ADC2MeV1"].append(clusterChannelADC2MeV1Array[clusIndexCh].tolist())
            channelDict["LArDB_Pedestal"].append(clusterChannelPedArray[clusIndexCh].tolist())
            channelDict["OFCa"].append(clusterChannelOFCaArray[clusIndexCh].tolist())
            channelDict["OFCb"].append(clusterChannelOFCbArray[clusIndexCh].tolist())
            channelDict["Shape"].append(clusterChannelShapeArray[clusIndexCh].tolist())
            channelDict["ShapeDer"].append(clusterChannelShapeDerArray[clusIndexCh].tolist())
            channelDict["MinBiasAvg"].append(clusterChannelMinBiasAvgArray[clusIndexCh].tolist())
            channelDict["OfflHVScale"].append(clusterChannelOfflHVScaleArray[clusIndexCh].tolist())
            channelDict["OfflEneRescaler"].append(clusterChannelOfflEneRescArray[clusIndexCh].tolist())

            # Data decompression
            channelRegionLink       = indexConversion( channelDict["index"] , cellDict["index"], clusterCellRegionArray[clusIndexCell])
            channelSamplingLink     = indexConversion( channelDict["index"] , cellDict["index"], clusterCellLayerArray[clusIndexCell] )
            channelGainLink         = indexConversion( channelDict["index"] , cellDict["index"], clusterCellCaloGainArray[clusIndexCell] )
            channelEtaLink          = indexConversion( channelDict["index"] , cellDict["index"], clusterCellEtaArray[clusIndexCell] )
            channelPhiLink          = indexConversion( channelDict["index"] , cellDict["index"], clusterCellPhiArray[clusIndexCell] )
            channelDEtaLink         = indexConversion( channelDict["index"] , cellDict["index"], clusterCellDEtaArray[clusIndexCell] )
            channelDPhiLink         = indexConversion( channelDict["index"] , cellDict["index"], clusterCellDPhiArray[clusIndexCell] )

            channelRegionString     = getRegionString( channelRegionLink , dictCaloLayer["Region"] )
            channelSamplingString   = getSamplingString( channelSamplingLink , dictCaloLayer["Layer"])
            channelCaloGainString   = getCaloGainString( channelGainLink , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

            channelDict["caloRegion"].append( channelRegionString )
            channelDict["sampling"].append( channelSamplingString )
            channelDict["gain"].append( channelCaloGainString )
            channelDict["eta"].append (channelEtaLink.tolist() )
            channelDict["phi"].append (channelPhiLink.tolist() )
            channelDict["deta"].append(channelDEtaLink.tolist() )
            channelDict["dphi"].append(channelDPhiLink.tolist() )
        
            # -- RawChannels --
            rawChannelDict  = {
                "index":            [],
                "eta":              [],
                "phi":              [],
                "deta":             [],
                "dphi":             [],
                "caloRegion":       [],
                "sampling":         [],
                "amplitude":        [],
                "time":             [],
                "Tile_Pedestal":    [],
                "LAr_Provenance":   [],
                "quality":          [],
                "chInfo":           [], # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                "channelId":        [],
                "DSPThreshold":     []
            }
            # must be treated because these indexes has floating point to represent tile PMT1 (+0.1) and PMT2 (+0.2), but when loading them, it is not precise (small bug to be fixed)
            roundedRawChIndex = [round(x,1) for x in clusterRawChannelIndexArray[clusIndexRawCh]]

            rawChannelDict["index"].append( roundedRawChIndex )
            rawChannelDict["amplitude"].append(clusterRawChannelAmplitudeArray[clusIndexRawCh].tolist())
            rawChannelDict["time"].append(clusterRawChannelTimeArray[clusIndexRawCh].tolist())
            rawChannelDict["Tile_Pedestal"].append(clusterRawChannelPedArray[clusIndexRawCh].tolist())
            rawChannelDict["LAr_Provenance"].append(clusterRawChannelProvArray[clusIndexRawCh].tolist())
            rawChannelDict["quality"].append(clusterRawChannelQualArray[clusIndexRawCh].tolist())
            rawChannelDict["chInfo"].append(clusterRawChannelChInfoArray[clusIndexRawCh].tolist())
            rawChannelDict["channelId"].append(clusterRawChannelIdArray[clusIndexRawCh].tolist())
            rawChannelDict["DSPThreshold"].append(clusterRawChannelDSPThrArray[clusIndexRawCh].tolist())
            
            #  Index conversion: it works like data decompression from root files, that were indexed to use less
            # disk when dumped from ESD format. Now, information that were saved for only one data level (cell, channel, etc),
            # can be propagated to the others to built a dataset which is easier to handle, independently of each data level.
            rawChannelRegionLink        = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellRegionArray[clusIndexCell])
            rawChannelSamplingLink      = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellLayerArray[clusIndexCell] )
            rawChannelEtaLink           = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellEtaArray[clusIndexCell])
            rawChannelDEtaLink          = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellDEtaArray[clusIndexCell])
            rawChannelPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellPhiArray[clusIndexCell])
            rawChannelDPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellDPhiArray[clusIndexCell])
            rawChannelRegionString      = getRegionString( rawChannelRegionLink , dictCaloLayer["Region"] )
            rawChannelSamplingString    = getSamplingString( rawChannelSamplingLink , dictCaloLayer["Layer"])            

            rawChannelDict["caloRegion"].append( rawChannelRegionString )
            rawChannelDict["sampling"].append( rawChannelSamplingString )
            rawChannelDict["eta"].append( rawChannelEtaLink.tolist() )
            rawChannelDict["phi"].append( rawChannelPhiLink.tolist() )
            rawChannelDict["deta"].append( rawChannelDEtaLink.tolist() )
            rawChannelDict["dphi"].append( rawChannelDPhiLink.tolist() )

        # ----------------------- III-end ------------------------------------
            # fill clusters with cells, channels and raw channels
            clusName    = str("cl_{}".format(clusterIndexArray[cl]))
            clusterDict[clusName]["cells"].update( cellDict )
            clusterDict[clusName]["channels"].update( channelDict )
            clusterDict[clusName]["rawChannels"].update( rawChannelDict )

            # fill electron with cluster(s)
            electronDict["el_{}".format(elec)]["clusters"].update( clusterDict ) 

        # --------------------------------------------------------------------------
        #                       ROI 7_11 EMB2 Reference
        # --------------------------------------------------------------------------

        # ----------------------- II-1 Calo Objects (711 ROI) ----------------------
        # find array indexes for the cluster collection, from each electron.
        # eIndexClus = np.where( electronIndex_clusterLvlArray == electronIndexArray[elec] )[0]

        # with the array indexes, find the the cluster indexes, linked to the selected electron
        # ( the cluster index is used to reach the cell level indexes. )
        # for cIndex in clusterIndexArray[eIndexClus]: # for each cluster
        for cl in eIndexClus:
    
            # layerIndex = np.where(clusterCellLayerArray[cIndexCell] == 2)[0] # get EMB2 cells
            clusterDict = {
                "cl_{}".format(cluster711IndexArray[cl]):{
                    "index":        [],
                    "pt":           [],
                    "eta":          [],
                    "phi":          [],
                    # cells
                    "cells":         {}, # structure
                    "channels":      {}, # structure
                    "rawChannels":   {} # structure
                }
            }
            # fill clusters
            clusterDict["cl_{}".format(cluster711IndexArray[cl])]["index"].append(cluster711IndexArray[cl])
            clusterDict["cl_{}".format(cluster711IndexArray[cl])]["pt"].append (cluster711PtArray[cl])
            clusterDict["cl_{}".format(cluster711IndexArray[cl])]["eta"].append(cluster711EtaArray[cl])
            clusterDict["cl_{}".format(cluster711IndexArray[cl])]["phi"].append(cluster711PhiArray[cl])

            # ----------------------- III-1 CaloCell and Channels (711) ------------------
            # get cells, channels and rawChannels linked to each cluster
            clusIndexCell   = np.where( cluster711IndexCellLvlArray    == cluster711IndexArray[cl] )[0]
            clusIndexCh     = np.where( cluster711IndexChLvlArray      == cluster711IndexArray[cl] )[0]
            clusIndexRawCh  = np.where( cluster711IndexRawChLvlArray   == cluster711IndexArray[cl] )[0]
            # print("clusterCellIndexArray:", clusterCellIndexArray)
            # print("clusterRawChannelIndexArray",clusterRawChannelIndexArray)

            # -- Cells --
            # OBS.: it was chosen to remove most of the information in this part of dict (cell level), because
            # it is more convenient to work with the xtalk and cell data in the same level, in this case, in channel.
            # 28 Sept, it was added again to perform some tests.
            cellDict    = {
                "index":        [],
                "caloRegion":   [],
                "sampling":     [],
                "eta":          [],
                "phi":          [],
                "deta":         [],
                "dphi":         [],
                "gain":         []
            }
            cellRegionString    = getRegionString   ( cluster711CellRegionArray[clusIndexCell] , dictCaloLayer["Region"])
            cellSamplingString  = getSamplingString ( cluster711CellLayerArray[clusIndexCell] , dictCaloLayer["Layer"] )
            cellCaloGainString  = getCaloGainString ( cluster711CellCaloGainArray[clusIndexCell] , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

            cellDict["index"].append        ( cluster711CellIndexArray[clusIndexCell].tolist() )
            cellDict["sampling"].append     ( cellSamplingString )
            cellDict["caloRegion"].append   ( cellRegionString )
            cellDict["eta"].append          ( cluster711CellEtaArray[clusIndexCell].tolist() )
            cellDict["phi"].append          ( cluster711CellPhiArray[clusIndexCell].tolist() )
            cellDict["deta"].append         ( cluster711CellDEtaArray[clusIndexCell].tolist() )
            cellDict["dphi"].append         ( cluster711CellDPhiArray[clusIndexCell].tolist() )
            # cellDict["gain"].append( clusterCellCaloGainArray[clusIndexCell] )
            cellDict["gain"].append         ( cellCaloGainString )

            # print(clusterCellCaloGainArray[clusIndexCell], cellDict["gain"] )

            # -- Channels --
            channelDict     = {
                "index":            [],
                "caloRegion":       [], # from cell level
                "sampling":         [], # from cell level
                "energy":           [],
                "time":             [],
                "digits":           [],
                "eta":              [], # from cell level
                "phi":              [], # from cell level
                "gain":             [], # from cell level
                "deta":             [], # from cell level
                "dphi":             [], # from cell level
                "chInfo":           [],  # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                "hashId":           [],
                "channelId":        [],
                "effSigma":         [],
                "noise":            [],
                "DSPThreshold":     [],
                "OFCTimeOffset":    [],
                "ADC2MeV0":         [],
                "ADC2MeV1":         [],
                "LArDB_Pedestal":   [],
                "OFCa":             [],
                "OFCb":             [],
                "Shape":            [],
                "ShapeDer":         [],
                "MinBiasAvg":       [],
                "OfflHVScale":      [],
                "OfflEneRescaler":  []
            }
            if dataHasBadCh:
                channelDict.update({"badCh": []})
                # channelDict["badCh"].append(cluster711ChannelBadArray[clusIndexCh].tolist())
            roundedChIndex  = [round(x,1) for x in cluster711ChannelIndexArray[clusIndexCh]]

            # print(cluster711ChannelEnergyArray,cluster711ChannelChInfoArray)
        
            channelDict["index"].append ( roundedChIndex )            
            channelDict["energy"].append(cluster711ChannelEnergyArray[clusIndexCh].tolist())
            channelDict["time"].append  (cluster711ChannelTimeArray[clusIndexCh].tolist())
            channelDict["digits"].append(cluster711ChannelDigitsArray[clusIndexCh].tolist())
            channelDict["chInfo"].append(cluster711ChannelChInfoArray[clusIndexCh].tolist())
            channelDict["hashId"].append(cluster711ChannelHashArray[clusIndexCh].tolist())
            channelDict["channelId"].append(cluster711ChannelIdArray[clusIndexCh].tolist())
            channelDict["effSigma"].append(cluster711ChannelEffSigmaArray[clusIndexCh].tolist())
            channelDict["noise"].append(cluster711ChannelNoiseArray[clusIndexCh].tolist())
            channelDict["DSPThreshold"].append(cluster711ChannelDSPThrArray[clusIndexCh].tolist())
            channelDict["OFCTimeOffset"].append(cluster711ChannelOFCTimeOffsetArray[clusIndexCh].tolist())
            channelDict["ADC2MeV0"].append(cluster711ChannelADC2MeV0Array[clusIndexCh].tolist())
            channelDict["ADC2MeV1"].append(cluster711ChannelADC2MeV1Array[clusIndexCh].tolist())
            channelDict["LArDB_Pedestal"].append(cluster711ChannelPedArray[clusIndexCh].tolist())
            channelDict["OFCa"].append(cluster711ChannelOFCaArray[clusIndexCh].tolist())
            channelDict["OFCb"].append(cluster711ChannelOFCbArray[clusIndexCh].tolist())
            channelDict["Shape"].append(cluster711ChannelShapeArray[clusIndexCh].tolist())
            channelDict["ShapeDer"].append(cluster711ChannelShapeDerArray[clusIndexCh].tolist())
            channelDict["MinBiasAvg"].append(cluster711ChannelMinBiasAvgArray[clusIndexCh].tolist())
            channelDict["OfflHVScale"].append(cluster711ChannelOfflHVScaleArray[clusIndexCh].tolist())
            channelDict["OfflEneRescaler"].append(cluster711ChannelOfflEneRescArray[clusIndexCh].tolist)

            # Data decompression
            channelRegionLink       = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellRegionArray[clusIndexCell])
            channelSamplingLink     = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellLayerArray[clusIndexCell] )
            channelGainLink         = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellCaloGainArray[clusIndexCell] )
            channelEtaLink          = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellEtaArray[clusIndexCell] )
            channelPhiLink          = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellPhiArray[clusIndexCell] )
            channelDEtaLink         = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellDEtaArray[clusIndexCell] )
            channelDPhiLink         = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellDPhiArray[clusIndexCell] )

            channelRegionString     = getRegionString( channelRegionLink , dictCaloLayer["Region"] )
            channelSamplingString   = getSamplingString( channelSamplingLink , dictCaloLayer["Layer"])
            channelCaloGainString   = getCaloGainString( channelGainLink , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

            channelDict["caloRegion"].append(channelRegionString )
            channelDict["sampling"].append  (channelSamplingString )
            channelDict["gain"].append      (channelCaloGainString)  #( cellCaloGainString ) ## is that organized with indexes sequences for channel?
            channelDict["eta"].append       (channelEtaLink.tolist() )
            channelDict["phi"].append       (channelPhiLink.tolist() )
            channelDict["deta"].append      (channelDEtaLink.tolist() )
            channelDict["dphi"].append      (channelDPhiLink.tolist() )
            
            # # cellDict["gain"].append( clusterCellCaloGainArray[clusIndexCell] )
        
            # -- RawChannels --
            rawChannelDict  = {
                "index":            [],
                "eta":              [],
                "phi":              [],
                "deta":             [],
                "dphi":             [],
                "caloRegion":       [],
                "sampling":         [],
                "amplitude":        [],
                "time":             [],
                "Tile_Pedestal":    [],
                "LAr_Provenance":   [],
                "quality":          [],
                "chInfo":           [], # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                "channelId":        [],
                "DSPThreshold":     []
            }
            # must be treated because these indexes has floating point to represent tile PMT1 (+0.1) and PMT2 (+0.2), but when loading them, it is not precise (small bug to be fixed)
            roundedRawChIndex = [round(x,1) for x in cluster711RawChannelIndexArray[clusIndexRawCh]]

            rawChannelDict["index"].append          ( roundedRawChIndex )
            rawChannelDict["amplitude"].append      (cluster711RawChannelAmplitudeArray[clusIndexRawCh].tolist())
            rawChannelDict["time"].append           (cluster711RawChannelTimeArray[clusIndexRawCh].tolist())
            rawChannelDict["Tile_Pedestal"].append  (cluster711RawChannelPedArray[clusIndexRawCh].tolist())
            rawChannelDict["LAr_Provenance"].append (cluster711RawChannelProvArray[clusIndexRawCh].tolist())
            rawChannelDict["quality"].append        (cluster711RawChannelQualArray[clusIndexRawCh].tolist())
            rawChannelDict["chInfo"].append         (cluster711RawChannelChInfoArray[clusIndexRawCh].tolist())
            rawChannelDict["channelId"].append      (cluster711RawChannelIdArray[clusIndexRawCh].tolist())
            rawChannelDict["DSPThreshold"].append   (cluster711RawChannelDSPThrArray[clusIndexRawCh].tolist())


            #  Index conversion: it works like data decompression from root files, that were indexed to use less
            # disk when dumped from ESD format. Now, information that were saved for only one data level (cell, channel, etc),
            # can be propagated to the others to built a dataset which is easier to handle, independently of each data level.
            # Here, the cluster711Cell variables in cell level, will be converted to their respective cells in the rawChannel level.
            rawChannelRegionLink        = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellRegionArray[clusIndexCell])
            rawChannelSamplingLink      = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellLayerArray[clusIndexCell] )
            rawChannelEtaLink           = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellEtaArray[clusIndexCell])
            rawChannelDEtaLink          = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellDEtaArray[clusIndexCell])
            rawChannelPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellPhiArray[clusIndexCell])
            rawChannelDPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"] , cluster711CellDPhiArray[clusIndexCell])

            rawChannelRegionString      = getRegionString( rawChannelRegionLink , dictCaloLayer["Region"] )
            rawChannelSamplingString    = getSamplingString( rawChannelSamplingLink , dictCaloLayer["Layer"])            

            rawChannelDict["caloRegion"].append( rawChannelRegionString )
            rawChannelDict["sampling"].append( rawChannelSamplingString )
            rawChannelDict["eta"].append( rawChannelEtaLink.tolist() )
            rawChannelDict["phi"].append( rawChannelPhiLink.tolist() )
            rawChannelDict["deta"].append( rawChannelDEtaLink.tolist() )
            rawChannelDict["dphi"].append( rawChannelDPhiLink.tolist() )

            # print(np.shape(rawChannelDict["phi"]), rawChannelDict["phi"])
            # print(np.shape(cellDict["eta"]), cellDict["eta"] )

            # df = pd.concat({k: pd.DataFrame.from_dict(v, orient='index') for k, v in rawChannelDict.items()}, axis=1)
            # print(df)
            # print(pd.concat({k: pd.DataFrame.from_dict(v) for k, v in rawChannelDict.items()}))

            # ----------------------- III-end ------------------------------------
            # fill clusters with cells, channels and raw channels
            clusName    = str("cl_{}".format(cluster711IndexArray[cl]))
            clusterDict[clusName]["cells"].update( cellDict )
            clusterDict[clusName]["channels"].update( channelDict )
            clusterDict[clusName]["rawChannels"].update( rawChannelDict )

            # saveAsJsonFile(clusterDict, 'file001.json')
            # lDatasetDict = loadJsonFile('file001.json')
            # print(lDatasetDict)

            # fill electron with cluster(s)
            electronDict["el_{}".format(elec)]["711_roi"].update( clusterDict ) 

        # fill event with particles
        eventInfoDict["electrons"].update( electronDict )

    # fill dataset with events
    datasetDict["dataset"].append(eventInfoDict)
    
    return datasetDict


def getXTDataAsPythonDictFaster(sTree, evN, eventReaderSharePath, dataHasBadCh=True, isMC=False, verbose=0, clusterType='topocluster'): # or '7x11'
    '''Return 0 case there were NO electrons in this event.'''

    ############################
    ####### Configuration ######
    ############################
    # jetROI  = -1

    # dataHasBadCh    = True # if there isnt any bad channel, force it to false.
    try:
        dataBadCh   = getattr(sTree,'cluster_channel_bad')
        # jetROI  = np.float32(getattr(sTree, 'jet_roi_r'))
    except:
        if (verbose==1):
            print("Data does not have bad_channel info. dataHasBadCh=False")
        dataHasBadCh  = False # if the data dumped has the flag 'noBadCells=True', this flag has to be False. Otherwise, its True.
        pass
    # evLim               = nEvts

    # ****** DICTIONARIES ******
    fDictLayerName      = eventReaderSharePath+'dictCaloByLayer.json'
    dictCaloLayer       = loadJsonFile(fDictLayerName) # Load Calo Geometry Dictionary
    transitionEtaWidthL = dictCaloLayer["transitionRegionEta"] #0.2 # eta value around the transition regions to consider (or discard) cells which could be tagged as belonging to another region in the same layer.
    transitionEtaWidthH = dictCaloLayer["transitionRegionEta"] #0.2 # (eta + transitionEtaWidthL) < Eta < (eta - transitionEtaWidthH). A value for each layer.
    maxOfLayers         = len(dictCaloLayer['Layer']) 

    # Number of regions in each sampling layer (eta x phi region)
    regionsPerLayer = []
    for reg in dictCaloLayer['granularityEtaLayer']:
        regionsPerLayer.append(len(reg))

    datasetDict = {"dataset": []}

    sTree.GetEntry(evN)
    # ----------------------------------------
    # Verify if the event has any electron: 
    # ----------------------------------------
    electronIndex          = getattr(sTree,'el_index')
    electronIndexArray     = stdVecToArray(electronIndex)

    # print(len(electronIndexArray)) # debug
    if len(electronIndexArray) == 0: # skip events without electrons
        return 0

    # -------------------------------------------
    
    ei_runNumber    = int(getattr(sTree, "event_RunNumber"))
    ei_eventNumber  = int(getattr(sTree, "event_EventNumber"))
    ei_bcid         = int(getattr(sTree, "event_BCID"))
    ei_lumibNumber  = int(getattr(sTree, "event_Lumiblock"))
    ei_avgMu        = getattr(sTree, "event_avg_mu_inTimePU")
    ei_avgMuOOT     = getattr(sTree, "event_avg_mu_OOTimePU")
    
    ### Particle Level
    electronPt                  = getattr(sTree,'el_Pt')
    # electronIndex               = getattr(sTree,'el_index')
    electronIndex_clusterLvl    = getattr(sTree,'c_electronIndex_clusterLvl')
    electronEta                 = getattr(sTree,'el_Eta')
    electronPhi                 = getattr(sTree,'el_Phi')
    electronEoverP              = getattr(sTree,'el_eoverp')
    electronSSf1                = getattr(sTree,'el_f1')
    electronSSf3                = getattr(sTree,'el_f3')
    electronSSeratio            = getattr(sTree,'el_eratio')
    electronSSweta1             = getattr(sTree,'el_weta1')
    electronSSweta2             = getattr(sTree,'el_weta2')
    electronSSfracs1            = getattr(sTree,'el_fracs1')
    electronSSwtots1            = getattr(sTree,'el_wtots1')
    electronSSe277              = getattr(sTree,'el_e277')
    electronSSreta              = getattr(sTree,'el_reta')
    electronSSrphi              = getattr(sTree,'el_rphi')
    electronSSdeltae            = getattr(sTree,'el_deltae')
    electronSSrhad              = getattr(sTree,'el_rhad')
    electronSSrhad1             = getattr(sTree,'el_rhad1')
    vertexX                     = getattr(sTree,'vtx_x')
    vertexY                     = getattr(sTree,'vtx_y')
    vertexZ                     = getattr(sTree,'vtx_z')

    ### TopoCluster or SuperCluster
    if clusterType=='topocluster':
        clusterIndex                 = getattr(sTree,'cluster_index')
        clusterPt                    = getattr(sTree,'cluster_pt')
        clusterEta                   = getattr(sTree,'cluster_eta')
        clusterPhi                   = getattr(sTree,'cluster_phi')
        clusterTime                  = getattr(sTree,'cluster_time')
        # clusterEtaCalc               = getattr(sTree,'cluster_eta_calc')
        # clusterPhiCalc               = getattr(sTree,'cluster_phi_calc')
        # Cluster Cells 
        clusterCellIndex             = getattr(sTree,'cluster_cell_index') 
        clusterIndexCellLvl          = getattr(sTree,'cluster_index_cellLvl')
        clusterCellCaloGain          = getattr(sTree,'cluster_cell_caloGain')
        clusterCellLayer             = getattr(sTree,'cluster_cell_layer')
        clusterCellEnergy            = getattr(sTree,'cluster_cell_energy')
        clusterCellTime              = getattr(sTree,'cluster_cell_time')
        clusterCellRegion            = getattr(sTree,'cluster_cell_region')
        clusterCellEta               = getattr(sTree,'cluster_cell_eta')
        clusterCellPhi               = getattr(sTree,'cluster_cell_phi') 
        clusterCellDEta              = getattr(sTree,'cluster_cell_deta')
        clusterCellDPhi              = getattr(sTree,'cluster_cell_dphi')
        clusterCellsDistDEta         = getattr(sTree,'cluster_cellsDist_deta')
        clusterCellsDistDPhi         = getattr(sTree,'cluster_cellsDist_dphi')
        # Cluster Channel 
        clusterIndexChLvl            = getattr(sTree,'cluster_index_chLvl')
        clusterChannelIndex          = getattr(sTree,'cluster_channel_index')
        clusterChannelHash           = getattr(sTree,'cluster_channel_hash')
        clusterChannelId             = getattr(sTree,'cluster_channel_id')
        clusterChannelDigits         = getattr(sTree,'cluster_channel_digits')
        clusterChannelEnergy         = getattr(sTree,'cluster_channel_energy')
        clusterChannelTime           = getattr(sTree,'cluster_channel_time')
        clusterChannelLayer          = getattr(sTree,'cluster_channel_layer')
        if dataHasBadCh: clusterChannelBad            = getattr(sTree,'cluster_channel_bad')
        clusterChannelChInfo         = getattr(sTree,'cluster_channel_chInfo')
        clusterChannelEffSigma       = getattr(sTree,'cluster_channel_effSigma')
        clusterChannelNoise          = getattr(sTree,'cluster_channel_noise')
        if not(isMC): # calibration for data only
            clusterChannelDSPThr         = getattr(sTree,'cluster_channel_DSPThreshold')
            clusterChannelShape          = getattr(sTree,'cluster_channel_shape')
            clusterChannelShapeDer       = getattr(sTree,'cluster_channel_shapeDer')
            clusterChannelOfflHVScale    = getattr(sTree,'cluster_channel_OfflineHVScale')
            clusterChannelOfflEneResc    = getattr(sTree,'cluster_channel_OfflineEnergyRescaler')
        clusterChannelOFCTimeOffset  = getattr(sTree,'cluster_channel_OFCTimeOffset')
        clusterChannelADC2MeV0       = getattr(sTree,'cluster_channel_ADC2MeV0')
        clusterChannelADC2MeV1       = getattr(sTree,'cluster_channel_ADC2MeV1')
        clusterChannelPed            = getattr(sTree,'cluster_channel_pedestal')
        clusterChannelOFCa           = getattr(sTree,'cluster_channel_OFCa')
        clusterChannelOFCb           = getattr(sTree,'cluster_channel_OFCb')
        clusterChannelMinBiasAvg     = getattr(sTree,'cluster_channel_MinBiasAvg')
            
            
        # Cluster raw channel 
        clusterIndexRawChLvl         = getattr(sTree,'cluster_index_rawChLvl')
        clusterRawChannelIndex       = getattr(sTree,'cluster_rawChannel_index')
        clusterRawChannelAmplitude   = getattr(sTree,'cluster_rawChannel_amplitude')
        clusterRawChannelTime        = getattr(sTree,'cluster_rawChannel_time')
        clusterRawChannelLayer       = getattr(sTree,'cluster_rawChannel_layer')
        clusterRawChannelPed         = getattr(sTree,'cluster_rawChannel_Ped')
        clusterRawChannelProv        = getattr(sTree,'cluster_rawChannel_Prov')
        clusterRawChannelQual        = getattr(sTree,'cluster_rawChannel_qual')
        clusterRawChannelChInfo      = getattr(sTree,'cluster_rawChannel_chInfo')
        clusterRawChannelId          = getattr(sTree,'cluster_rawChannel_id')
        if not(isMC):
            clusterRawChannelDSPThr      = getattr(sTree,'cluster_rawChannel_DSPThreshold')

    if isMC:
        # Hits
        hits_layer                   = getattr(sTree,'hits_sampling')
        hits_clusterIndexChLvl       = getattr(sTree,'hits_clusterIndex_chLvl')
        hits_hash                    = getattr(sTree,'hits_hash')
        hits_clusterChannelIndex     = getattr(sTree,'hits_clusterChannelIndex')
        # hits_energy       = getattr(sTree,'hits_energy')
        hits_energy                  = getattr(sTree,'hits_energyConv')
        hits_time                    = getattr(sTree,'hits_time')
        hits_cellEta                 = getattr(sTree,'hits_cellEta')
        hits_cellPhi                 = getattr(sTree,'hits_cellPhi')
        # Truth Particle
        mc_partEnergy               = getattr(sTree,'mc_part_energy')
        mc_partPt                   = getattr(sTree,'mc_part_pt')
        mc_partMass                 = getattr(sTree,'mc_part_m')
        mc_partEta                  = getattr(sTree,'mc_part_eta')
        mc_partPhi                  = getattr(sTree,'mc_part_phi')
        mc_partPdgId                = getattr(sTree,'mc_part_pdgId')
        # Truth Vertex
        mc_vertX                    = getattr(sTree,'mc_vert_x')
        mc_vertY                    = getattr(sTree,'mc_vert_y')
        mc_vertZ                    = getattr(sTree,'mc_vert_z')
        mc_vertTime                 = getattr(sTree,'mc_vert_time')
        mc_vertEta                  = getattr(sTree,'mc_vert_eta')
        mc_vertPhi                  = getattr(sTree,'mc_vert_phi')

    ### Fixed Window Clusters (7x11 EMB2)
    if clusterType=='7x11':
        cluster711Index                 = getattr(sTree,'cluster711_index')
        cluster711Pt                    = getattr(sTree,'cluster711_pt')
        cluster711Eta                   = getattr(sTree,'cluster711_eta')
        cluster711Phi                   = getattr(sTree,'cluster711_phi')
        # Cluster Cells 
        cluster711CellIndex             = getattr(sTree,'cluster711_cell_index') 
        cluster711IndexCellLvl          = getattr(sTree,'cluster711_index_cellLvl')
        cluster711CellCaloGain          = getattr(sTree,'cluster711_cell_caloGain')
        cluster711CellLayer             = getattr(sTree,'cluster711_cell_layer')
        cluster711CellRegion            = getattr(sTree,'cluster711_cell_region')
        cluster711CellEta               = getattr(sTree,'cluster711_cell_eta')
        cluster711CellPhi               = getattr(sTree,'cluster711_cell_phi') 
        cluster711CellDEta              = getattr(sTree,'cluster711_cell_deta')
        cluster711CellDPhi              = getattr(sTree,'cluster711_cell_dphi')
        cluster711CellsDistDEta         = getattr(sTree,'cluster711_cellsDist_deta')
        cluster711CellsDistDPhi         = getattr(sTree,'cluster711_cellsDist_dphi')
        # Cluster Channel 
        cluster711IndexChLvl            = getattr(sTree,'cluster711_index_chLvl')
        cluster711ChannelIndex          = getattr(sTree,'cluster711_channel_index')
        cluster711ChannelHash           = getattr(sTree,'cluster711_channel_hash')
        cluster711ChannelId             = getattr(sTree,'cluster711_channel_id')
        cluster711ChannelDigits         = getattr(sTree,'cluster711_channel_digits')
        cluster711ChannelEnergy         = getattr(sTree,'cluster711_channel_energy')
        cluster711ChannelTime           = getattr(sTree,'cluster711_channel_time')
        # if dataHasBadCh: cluster711ChannelBad            = getattr(sTree,'cluster711_channel_bad')
        cluster711ChannelChInfo         = getattr(sTree,'cluster711_channel_chInfo')
        cluster711ChannelEffSigma       = getattr(sTree,'cluster711_channel_effSigma')
        cluster711ChannelNoise          = getattr(sTree,'cluster711_channel_noise')
        cluster711ChannelDSPThr         = getattr(sTree,'cluster711_channel_DSPThreshold')
        cluster711ChannelOFCTimeOffset  = getattr(sTree,'cluster711_channel_OFCTimeOffset')
        cluster711ChannelADC2MeV0       = getattr(sTree,'cluster711_channel_ADC2MeV0')
        cluster711ChannelADC2MeV1       = getattr(sTree,'cluster711_channel_ADC2MeV1')
        cluster711ChannelPed            = getattr(sTree,'cluster711_channel_pedestal')
        cluster711ChannelOFCa           = getattr(sTree,'cluster711_channel_OFCa')
        cluster711ChannelOFCb           = getattr(sTree,'cluster711_channel_OFCb')
        cluster711ChannelShape          = getattr(sTree,'cluster711_channel_shape')
        cluster711ChannelShapeDer       = getattr(sTree,'cluster711_channel_shapeDer')
        cluster711ChannelMinBiasAvg     = getattr(sTree,'cluster711_channel_MinBiasAvg')
        cluster711ChannelOfflHVScale    = getattr(sTree,'cluster711_channel_OfflineHVScale')
        cluster711ChannelOfflEneResc    = getattr(sTree,'cluster711_channel_OfflineEnergyRescaler')

        # Cluster raw channel 
        cluster711IndexRawChLvl         = getattr(sTree,'cluster711_index_rawChLvl')
        cluster711RawChannelIndex       = getattr(sTree,'cluster711_rawChannel_index')
        cluster711RawChannelAmplitude   = getattr(sTree,'cluster711_rawChannel_amplitude')
        cluster711RawChannelTime        = getattr(sTree,'cluster711_rawChannel_time')
        cluster711RawChannelPed         = getattr(sTree,'cluster711_rawChannel_Ped')
        cluster711RawChannelProv        = getattr(sTree,'cluster711_rawChannel_Prov')
        cluster711RawChannelQual        = getattr(sTree,'cluster711_rawChannel_qual')
        cluster711RawChannelChInfo      = getattr(sTree,'cluster711_rawChannel_chInfo')
        cluster711RawChannelId          = getattr(sTree,'cluster711_rawChannel_id')
        cluster711RawChannelDSPThr      = getattr(sTree,'cluster711_rawChannel_DSPThreshold')

    #**********************************************
    # Format into python list/np array
    #**********************************************
    electronPtArray                 = stdVecToArray(electronPt)
    # electronIndexArray              = stdVecToArray(electronIndex)
    electronIndex_clusterLvlArray   = stdVecToArray(electronIndex_clusterLvl)
    electronEtaArray                = stdVecToArray(electronEta)
    electronPhiArray                = stdVecToArray(electronPhi)
    electronEoverPArray             = stdVecToArray(electronEoverP)
    electronSSf1Array               = stdVecToArray(electronSSf1)
    electronSSf3Array               = stdVecToArray(electronSSf3)
    electronSSeratioArray           = stdVecToArray(electronSSeratio)
    electronSSweta1Array            = stdVecToArray(electronSSweta1)
    electronSSweta2Array            = stdVecToArray(electronSSweta2)
    electronSSfracs1Array           = stdVecToArray(electronSSfracs1)
    electronSSwtots1Array           = stdVecToArray(electronSSwtots1)
    electronSSe277Array             = stdVecToArray(electronSSe277)
    electronSSretaArray             = stdVecToArray(electronSSreta)
    electronSSrphiArray             = stdVecToArray(electronSSrphi)
    electronSSdeltaeArray           = stdVecToArray(electronSSdeltae)
    electronSSrhadArray             = stdVecToArray(electronSSrhad)
    electronSSrhad1Array            = stdVecToArray(electronSSrhad1)
    vertexZArray                    = stdVecToArray(vertexZ)
    vertexYArray                    = stdVecToArray(vertexY)
    vertexXArray                    = stdVecToArray(vertexX)

    if clusterType=='topocluster':
        clusterIndexArray               = stdVecToArray(clusterIndex)
        clusterPtArray                  = stdVecToArray(clusterPt)
        clusterEtaArray                 = stdVecToArray(clusterEta)
        clusterPhiArray                 = stdVecToArray(clusterPhi)
        clusterTimeArray                = stdVecToArray(clusterTime)

        clusterCellIndexArray           = stdVecToArray(clusterCellIndex)
        clusterIndexCellLvlArray        = stdVecToArray(clusterIndexCellLvl)
        clusterCellCaloGainArray        = stdVecToArray(clusterCellCaloGain)
        clusterCellLayerArray           = stdVecToArray(clusterCellLayer)
        clusterCellEnergyArray          = stdVecToArray(clusterCellEnergy)
        clusterCellTimeArray            = stdVecToArray(clusterCellTime)
        clusterCellRegionArray          = stdVecToArray(clusterCellRegion)
        clusterCellEtaArray             = stdVecToArray(clusterCellEta)
        clusterCellPhiArray             = stdVecToArray(clusterCellPhi)
        clusterCellDEtaArray            = stdVecToArray(clusterCellDEta)
        clusterCellDPhiArray            = stdVecToArray(clusterCellDPhi)
        clusterCellsDistDEtaArray       = stdVecToArray(clusterCellsDistDEta)
        clusterCellsDistDPhiArray       = stdVecToArray(clusterCellsDistDPhi)
        
        clusterIndexChLvlArray          = stdVecToArray(clusterIndexChLvl)
        clusterChannelIndexArray        = stdVecToArray(clusterChannelIndex)
        clusterChannelHashArray         = stdVecToArray(clusterChannelHash)
        clusterChannelIdArray           = stdVecToArray(clusterChannelId)
        clusterChannelDigitsArray       = stdVecOfStdVecToArrayOfList(clusterChannelDigits, convertToInt=True)
        clusterChannelEnergyArray       = stdVecToArray(clusterChannelEnergy)
        clusterChannelTimeArray         = stdVecToArray(clusterChannelTime)
        clusterChannelLayerArray        = stdVecToArray(clusterChannelLayer)
        clusterChannelChInfoArray       = stdVecOfStdVecToArrayOfList(clusterChannelChInfo)
        if dataHasBadCh: clusterChannelBadArray      = stdVecToArray(clusterChannelBad)
        clusterChannelEffSigmaArray         = stdVecToArray(clusterChannelEffSigma)
        clusterChannelNoiseArray            = stdVecToArray(clusterChannelNoise)
        if not(isMC):
            clusterChannelDSPThrArray           = stdVecToArray(clusterChannelDSPThr)
            clusterChannelShapeArray            = stdVecOfStdVecToArrayOfList(clusterChannelShape)
            clusterChannelShapeDerArray         = stdVecOfStdVecToArrayOfList(clusterChannelShapeDer)
            clusterChannelOfflHVScaleArray      = stdVecToArray(clusterChannelOfflHVScale)
            clusterChannelOfflEneRescArray      = stdVecToArray(clusterChannelOfflEneResc)
        clusterChannelOFCTimeOffsetArray    = stdVecToArray(clusterChannelOFCTimeOffset)
        clusterChannelADC2MeV0Array         = stdVecToArray(clusterChannelADC2MeV0)
        clusterChannelADC2MeV1Array         = stdVecToArray(clusterChannelADC2MeV1)
        clusterChannelPedArray              = stdVecToArray(clusterChannelPed)
        clusterChannelOFCaArray             = stdVecOfStdVecToArrayOfList(clusterChannelOFCa)
        clusterChannelOFCbArray             = stdVecOfStdVecToArrayOfList(clusterChannelOFCb)
        clusterChannelMinBiasAvgArray       = stdVecToArray(clusterChannelMinBiasAvg)

        clusterIndexRawChLvlArray       = stdVecToArray(clusterIndexRawChLvl)
        clusterRawChannelIndexArray     = stdVecToArray(clusterRawChannelIndex)
        clusterRawChannelAmplitudeArray = stdVecToArray(clusterRawChannelAmplitude)
        clusterRawChannelTimeArray      = stdVecToArray(clusterRawChannelTime)
        clusterRawChannelLayerArray     = stdVecToArray(clusterRawChannelLayer)
        clusterRawChannelPedArray       = stdVecToArray(clusterRawChannelPed)
        clusterRawChannelProvArray      = stdVecToArray(clusterRawChannelProv)
        clusterRawChannelQualArray      = stdVecToArray(clusterRawChannelQual)
        clusterRawChannelChInfoArray    = stdVecOfStdVecToArrayOfList(clusterRawChannelChInfo)
        clusterRawChannelIdArray        = stdVecToArray(clusterRawChannelId)
        if not(isMC):
            clusterRawChannelDSPThrArray    = stdVecToArray(clusterRawChannelDSPThr)

    if isMC:        
        hits_layerArray                   = stdVecToArray(hits_layer)
        hits_clusterIndexChLvlArray       = stdVecToArray(hits_clusterIndexChLvl)
        hits_hashArray                    = stdVecToArray(hits_hash)
        hits_clusterChannelIndexArray     = stdVecToArray(hits_clusterChannelIndex)
        hits_energyArray                  = stdVecToArray(hits_energy)
        hits_timeArray                    = stdVecToArray(hits_time)
        hits_cellEtaArray                 = stdVecToArray(hits_cellEta)
        hits_cellPhiArray                 = stdVecToArray(hits_cellPhi)

        mc_partEnergyArray               = stdVecToArray(mc_partEnergy)
        mc_partPtArray                   = stdVecToArray(mc_partPt)
        mc_partMassArray                 = stdVecToArray(mc_partMass)
        mc_partEtaArray                  = stdVecToArray(mc_partEta)
        mc_partPhiArray                  = stdVecToArray(mc_partPhi)
        mc_partPdgIdArray                = stdVecToArray(mc_partPdgId)

        mc_vertXArray                    = stdVecToArray(mc_vertX)
        mc_vertYArray                    = stdVecToArray(mc_vertY)
        mc_vertZArray                    = stdVecToArray(mc_vertZ)
        mc_vertTimeArray                 = stdVecToArray(mc_vertTime)
        mc_vertEtaArray                  = stdVecToArray(mc_vertEta)
        mc_vertPhiArray                  = stdVecToArray(mc_vertPhi)

    if clusterType=='7x11':
        cluster711IndexArray               = stdVecToArray(cluster711Index)
        cluster711PtArray                  = stdVecToArray(cluster711Pt)
        cluster711EtaArray                 = stdVecToArray(cluster711Eta)
        cluster711PhiArray                 = stdVecToArray(cluster711Phi)

        cluster711CellIndexArray           = stdVecToArray(cluster711CellIndex)
        cluster711IndexCellLvlArray        = stdVecToArray(cluster711IndexCellLvl)
        cluster711CellCaloGainArray        = stdVecToArray(cluster711CellCaloGain)
        cluster711CellLayerArray           = stdVecToArray(cluster711CellLayer)
        cluster711CellRegionArray          = stdVecToArray(cluster711CellRegion)
        cluster711CellEtaArray             = stdVecToArray(cluster711CellEta)
        cluster711CellPhiArray             = stdVecToArray(cluster711CellPhi)
        cluster711CellDEtaArray            = stdVecToArray(cluster711CellDEta)
        cluster711CellDPhiArray            = stdVecToArray(cluster711CellDPhi)
        cluster711CellsDistDEtaArray       = stdVecToArray(cluster711CellsDistDEta)
        cluster711CellsDistDPhiArray       = stdVecToArray(cluster711CellsDistDPhi)
        
        cluster711IndexChLvlArray          = stdVecToArray(cluster711IndexChLvl)
        cluster711ChannelIndexArray        = stdVecToArray(cluster711ChannelIndex)
        cluster711ChannelHashArray         = stdVecToArray(cluster711ChannelHash)
        cluster711ChannelIdArray           = stdVecToArray(cluster711ChannelId)
        cluster711ChannelDigitsArray       = stdVecOfStdVecToArrayOfList(cluster711ChannelDigits, convertToInt=True)
        cluster711ChannelEnergyArray       = stdVecToArray(cluster711ChannelEnergy)
        cluster711ChannelTimeArray         = stdVecToArray(cluster711ChannelTime)
        cluster711ChannelChInfoArray       = stdVecOfStdVecToArrayOfList(cluster711ChannelChInfo)
        # if dataHasBadCh: cluster711ChannelBadArray      = stdVecToArray(cluster711ChannelBad)
        cluster711ChannelEffSigmaArray     = stdVecToArray(cluster711ChannelEffSigma)
        cluster711ChannelNoiseArray        = stdVecToArray(cluster711ChannelNoise)
        cluster711ChannelDSPThrArray       = stdVecToArray(cluster711ChannelDSPThr)
        cluster711ChannelOFCTimeOffsetArray    = stdVecToArray(cluster711ChannelOFCTimeOffset)
        cluster711ChannelADC2MeV0Array         = stdVecToArray(cluster711ChannelADC2MeV0)
        cluster711ChannelADC2MeV1Array         = stdVecToArray(cluster711ChannelADC2MeV1)
        cluster711ChannelPedArray              = stdVecToArray(cluster711ChannelPed)
        cluster711ChannelOFCaArray             = stdVecOfStdVecToArrayOfList(cluster711ChannelOFCa)
        cluster711ChannelOFCbArray             = stdVecOfStdVecToArrayOfList(cluster711ChannelOFCb)
        cluster711ChannelShapeArray            = stdVecOfStdVecToArrayOfList(cluster711ChannelShape)
        cluster711ChannelShapeDerArray         = stdVecOfStdVecToArrayOfList(cluster711ChannelShapeDer)
        cluster711ChannelMinBiasAvgArray       = stdVecToArray(cluster711ChannelMinBiasAvg)
        cluster711ChannelOfflHVScaleArray      = stdVecToArray(cluster711ChannelOfflHVScale)
        cluster711ChannelOfflEneRescArray      = stdVecToArray(cluster711ChannelOfflEneResc)

        cluster711IndexRawChLvlArray       = stdVecToArray(cluster711IndexRawChLvl)
        cluster711RawChannelIndexArray     = stdVecToArray(cluster711RawChannelIndex)
        cluster711RawChannelAmplitudeArray = stdVecToArray(cluster711RawChannelAmplitude)
        cluster711RawChannelTimeArray      = stdVecToArray(cluster711RawChannelTime)
        cluster711RawChannelPedArray        = stdVecToArray(cluster711RawChannelPed)
        cluster711RawChannelProvArray       = stdVecToArray(cluster711RawChannelProv)
        cluster711RawChannelQualArray      = stdVecToArray(cluster711RawChannelQual)
        cluster711RawChannelChInfoArray    = stdVecOfStdVecToArrayOfList(cluster711RawChannelChInfo)
        cluster711RawChannelIdArray        = stdVecToArray(cluster711RawChannelId)
        cluster711RawChannelDSPThrArray    = stdVecToArray(cluster711RawChannelDSPThr)

    # timePerEvRead = time()

    #**********************************************
    #  XTALKS STUDIES: DATASET DICT
    #**********************************************
    if clusterType=='topocluster':
        clusnamedict = "clusters"
    if clusterType=='7x11':
        clusnamedict = "711_roi"

    # ----------------------- 0 EventInfo -------------------------------------
    eventInfoDict = {
        "runNumber":        [],
        "eventNumber":      [],
        "BCID":             [],
        "Lumiblock":        [],
        "avgMu":            [],
        "avgMuOOT":         [],
        "electrons":        {}
    }
    eventInfoDict["runNumber"].append(ei_runNumber)
    eventInfoDict["eventNumber"].append(ei_eventNumber)
    eventInfoDict["BCID"].append(ei_bcid)
    eventInfoDict["Lumiblock"].append(ei_lumibNumber)
    eventInfoDict["avgMu"].append(ei_avgMu)
    eventInfoDict["avgMuOOT"].append(ei_avgMuOOT)      

    # ------------------- MC Truth Particle and Vertex -----------------------
    if isMC:
        eventInfoDict.update({"MC_truth": {}})

        mcTruthDict = {
            # particle
            "energy":       [],
            "pt":           [],
            "mass":         [],
            "eta":          [],
            "phi":          [],
            "pdgId":        [],
            # vertex
            "vertex_x":     [],
            "vertex_y":     [],
            "vertex_z":     [],
            "vertex_time":  [],
            "vertex_eta":   [],
            "vertex_phi":   [],
        }

        mcTruthDict["energy"].append( mc_partEnergyArray )
        mcTruthDict["pt"].append( mc_partPtArray )
        mcTruthDict["mass"].append( mc_partMassArray )
        mcTruthDict["eta"].append( mc_partEtaArray )
        mcTruthDict["phi"].append( mc_partPhiArray )
        mcTruthDict["pdgId"].append( mc_partPdgIdArray )
        mcTruthDict["vertex_x"].append( mc_vertXArray )
        mcTruthDict["vertex_y"].append( mc_vertYArray )
        mcTruthDict["vertex_z"].append( mc_vertZArray )
        mcTruthDict["vertex_time"].append( mc_vertTimeArray )
        mcTruthDict["vertex_eta"].append( mc_vertEtaArray )
        mcTruthDict["vertex_phi"].append( mc_vertPhiArray )

    # ----------------------- I Electron -------------------------------------
    
    # print("Found an electron!")
    for elec in range(0, len(electronIndexArray)):
        electronDict    = {
        "el_{}".format(elec):{
            "index":      [],
            "pt":         [],
            "eta":        [],
            "phi":        [],
            "eoverp":     [],
            #shower shapes
            "f1":         [],
            "f3":         [],
            "eratio":     [],
            "weta1" :     [],
            "weta2" :     [],
            "fracs1":     [],
            "wtots1":     [],
            "e277"  :     [],
            "reta"  :     [],
            "rphi"  :     [],
            "deltae":     [],
            "rhad"  :     [],
            "rhad1" :     [],
            #vertex
            "vertexZ":    [],
            "vertexY":    [],
            "vertexX":    [],
            "{}".format(clusnamedict): {},
            }
        } # electron-end

        electronDict["el_{}".format(elec)]["index"].append(electronIndexArray[elec])
        electronDict["el_{}".format(elec)]["pt"].append(electronPtArray[elec])
        electronDict["el_{}".format(elec)]["eta"].append(electronEtaArray[elec])
        electronDict["el_{}".format(elec)]["phi"].append(electronPhiArray[elec])
        electronDict["el_{}".format(elec)]["eoverp"].append(electronEoverPArray[elec])
        electronDict["el_{}".format(elec)]["f1"].append(electronSSf1Array[elec])
        electronDict["el_{}".format(elec)]["f3"].append(electronSSf3Array[elec])
        electronDict["el_{}".format(elec)]["eratio"].append(electronSSeratioArray[elec])
        electronDict["el_{}".format(elec)]["weta1"].append(electronSSweta1Array[elec])
        electronDict["el_{}".format(elec)]["weta2"].append(electronSSweta2Array[elec])
        electronDict["el_{}".format(elec)]["fracs1"].append(electronSSfracs1Array[elec])
        electronDict["el_{}".format(elec)]["wtots1"].append(electronSSwtots1Array[elec])
        electronDict["el_{}".format(elec)]["e277"].append(electronSSe277Array[elec])
        electronDict["el_{}".format(elec)]["reta"].append(electronSSretaArray[elec])
        electronDict["el_{}".format(elec)]["rphi"].append(electronSSrphiArray[elec])
        electronDict["el_{}".format(elec)]["deltae"].append(electronSSdeltaeArray[elec])
        electronDict["el_{}".format(elec)]["rhad"].append(electronSSrhadArray[elec])
        electronDict["el_{}".format(elec)]["rhad1"].append(electronSSrhad1Array[elec])
        electronDict["el_{}".format(elec)]["vertexZ"].append(vertexZArray[elec])
        electronDict["el_{}".format(elec)]["vertexY"].append(vertexYArray[elec])
        electronDict["el_{}".format(elec)]["vertexX"].append(vertexXArray[elec])

    # --------------------------------------------------------------------------
    #                       TopoCluster or SuperCluster
    # --------------------------------------------------------------------------
    if clusterType == 'topocluster':

        # ----------------------- II Calo Objects (TopoCluster or SuperCluster) ----------------------
        # find array indexes for the cluster collection, from each electron.
        eIndexClus = np.where( electronIndex_clusterLvlArray == electronIndexArray[elec] )[0]

        # with the array indexes, find the the cluster indexes, linked to the selected electron
        # ( the cluster index is used to reach the cell level indexes. )
        # for cIndex in clusterIndexArray[eIndexClus]: # for each cluster
        for cl in eIndexClus:
    
            # layerIndex = np.where(clusterCellLayerArray[cIndexCell] == 2)[0] # get EMB2 cells
            clusterDict = {
                "cl_{}".format(clusterIndexArray[cl]):{
                    "index":        [],
                    "pt":           [],
                    "eta":          [],
                    "phi":          [],
                    "time":         [],
                    # cells
                    "cells":         {}, # structure
                    "channels":      {}, # structure
                    "rawChannels":   {} # structure
                }
            }
            if isMC:
                clusterDict["cl_{}".format(clusterIndexArray[cl])].update({"hits": {}})

            # fill clusters
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["index"].append(clusterIndexArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["pt"].append(clusterPtArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["eta"].append(clusterEtaArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["phi"].append(clusterPhiArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["time"].append(clusterTimeArray[cl])

            # ----------------------- III CaloCell and Channels ------------------
            # get cells, channels and rawChannels linked to each cluster
            clusIndexCell   = np.where( clusterIndexCellLvlArray    == clusterIndexArray[cl] )[0]
            clusIndexCh     = np.where( clusterIndexChLvlArray      == clusterIndexArray[cl] )[0]
            clusIndexRawCh  = np.where( clusterIndexRawChLvlArray   == clusterIndexArray[cl] )[0]
            if isMC:
                clusIndexHits   = np.where( hits_clusterIndexChLvlArray == clusterIndexArray[cl] )[0]
            # print("clusterCellIndexArray:", clusterCellIndexArray)
            # print("clusterRawChannelIndexArray",clusterRawChannelIndexArray)

            # -- Cells --
            # OBS.: it was chosen to remove most of the information in this part of dict (cell level), because
            # it is more convenient to work with the xtalk and cell data in the same level, in this case, in channel.
            # 28 Sept, it was added again to perform some tests.
            cellDict    = {
                "index":        [],
                "caloRegion":   [],
                "sampling":     [],
                "energy":       [],
                "time":         [],
                "eta":          [],
                "phi":          [],
                "deta":         [],
                "dphi":         [],
                "gain":         []
            }
            cellRegionString    = getRegionString( clusterCellRegionArray[clusIndexCell] , dictCaloLayer["Region"])
            cellSamplingString  = getSamplingString( clusterCellLayerArray[clusIndexCell] , dictCaloLayer["Layer"] )
            cellCaloGainString  = getCaloGainString( clusterCellCaloGainArray[clusIndexCell] , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

            cellDict["index"].append( clusterCellIndexArray[clusIndexCell].tolist() )
            cellDict["sampling"].append( cellSamplingString )
            cellDict["caloRegion"].append( cellRegionString )
            cellDict["energy"].append( clusterCellEnergyArray[clusIndexCell].tolist() )
            cellDict["time"].append( clusterCellTimeArray[clusIndexCell].tolist() )
            cellDict["eta"].append( clusterCellEtaArray[clusIndexCell].tolist() )
            cellDict["phi"].append( clusterCellPhiArray[clusIndexCell].tolist() )
            cellDict["deta"].append( clusterCellDEtaArray[clusIndexCell].tolist() )
            cellDict["dphi"].append( clusterCellDPhiArray[clusIndexCell].tolist() )
            # cellDict["gain"].append( clusterCellCaloGainArray[clusIndexCell] )
            cellDict["gain"].append( cellCaloGainString )

            # print(clusterCellCaloGainArray[clusIndexCell], cellDict["gain"] )
            # print("debug el_{}, cl_{} clusterCellIndexArray[clusIndexCell]: ".format(elec,cl),clusterCellIndexArray[clusIndexCell])
            # print("debug el_{}, cl_{} clusterCellCaloGainArray[clusIndexCell]: ".format(elec,cl), clusterCellCaloGainArray[clusIndexCell] )
            # print("debug el_{} cl_{} clusterCellDEtaArray[clusIndexCell]: ".format(elec,cl),clusterCellDEtaArray[clusIndexCell])

            # -- Channels --
            channelDict     = {
                "index":            [],
                "caloRegion":       [], # from cell level
                "sampling":         [], # from cell level
                "energy":           [],
                "time":             [],
                "digits":           [],
                "eta":              [], # from cell level
                "phi":              [], # from cell level
                "gain":             [], # from cell level
                "deta":             [], # from cell level
                "dphi":             [], # from cell level
                "chInfo":           [], # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                "hashId":           [],
                "channelId":        [],
                "effSigma":         [],
                "noise":            [],
                # "DSPThreshold":     [],
                # "OfflHVScale":      [],
                # "OfflEneRescaler":  []
                "OFCTimeOffset":    [],
                # "Shape":            [],
                # "ShapeDer":         [],
                "ADC2MeV0":         [],
                "ADC2MeV1":         [],
                "LArDB_Pedestal":   [],
                "OFCa":             [],
                "OFCb":             [],                
                "MinBiasAvg":       [],                
            }
            if not(isMC):
                channelDict.update({"DSPThreshold":     []})
                channelDict.update({"OfflHVScale":      []})
                channelDict.update({"OfflEneRescaler":  []})
                channelDict.update({"Shape":            []})
                channelDict.update({"ShapeDer":         []})
                
                channelDict["DSPThreshold"].append(clusterChannelDSPThrArray[clusIndexCh].tolist())
                channelDict["Shape"].append(clusterChannelShapeArray[clusIndexCh].tolist())
                channelDict["ShapeDer"].append(clusterChannelShapeDerArray[clusIndexCh].tolist())
                channelDict["OfflHVScale"].append(clusterChannelOfflHVScaleArray[clusIndexCh].tolist())
                channelDict["OfflEneRescaler"].append(clusterChannelOfflEneRescArray[clusIndexCh].tolist())

            if dataHasBadCh:
                channelDict.update({"badCh": []})
                channelDict["badCh"].append(clusterChannelBadArray[clusIndexCh].tolist())

            roundedChIndex  = [round(x,1) for x in clusterChannelIndexArray[clusIndexCh]]
        
            channelDict["index"].append( roundedChIndex )            
            channelDict["energy"].append(clusterChannelEnergyArray[clusIndexCh].tolist())
            channelDict["time"].append(clusterChannelTimeArray[clusIndexCh].tolist())
            channelDict["digits"].append(clusterChannelDigitsArray[clusIndexCh].tolist())
            channelDict["chInfo"].append(clusterChannelChInfoArray[clusIndexCh].tolist())
            channelDict["hashId"].append(clusterChannelHashArray[clusIndexCh].tolist())
            channelDict["channelId"].append(clusterChannelIdArray[clusIndexCh].tolist())
            channelDict["effSigma"].append(clusterChannelEffSigmaArray[clusIndexCh].tolist())
            channelDict["noise"].append(clusterChannelNoiseArray[clusIndexCh].tolist())
            channelDict["OFCTimeOffset"].append(clusterChannelOFCTimeOffsetArray[clusIndexCh].tolist())
            channelDict["ADC2MeV0"].append(clusterChannelADC2MeV0Array[clusIndexCh].tolist())
            channelDict["ADC2MeV1"].append(clusterChannelADC2MeV1Array[clusIndexCh].tolist())
            channelDict["LArDB_Pedestal"].append(clusterChannelPedArray[clusIndexCh].tolist())
            channelDict["OFCa"].append(clusterChannelOFCaArray[clusIndexCh].tolist())
            channelDict["OFCb"].append(clusterChannelOFCbArray[clusIndexCh].tolist())            
            channelDict["MinBiasAvg"].append(clusterChannelMinBiasAvgArray[clusIndexCh].tolist())
            

            # Data decompression
            channelRegionLink       = indexConversion( channelDict["index"] , cellDict["index"], clusterCellRegionArray[clusIndexCell])
            # channelSamplingLink     = indexConversion( channelDict["index"] , cellDict["index"], clusterCellLayerArray[clusIndexCell] )
            channelSamplingLink     = clusterChannelLayerArray[clusIndexCh].tolist()
            channelGainLink         = indexConversion( channelDict["index"] , cellDict["index"], clusterCellCaloGainArray[clusIndexCell] )
            channelEtaLink          = indexConversion( channelDict["index"] , cellDict["index"], clusterCellEtaArray[clusIndexCell] )
            channelPhiLink          = indexConversion( channelDict["index"] , cellDict["index"], clusterCellPhiArray[clusIndexCell] )
            channelDEtaLink         = indexConversion( channelDict["index"] , cellDict["index"], clusterCellDEtaArray[clusIndexCell] )
            channelDPhiLink         = indexConversion( channelDict["index"] , cellDict["index"], clusterCellDPhiArray[clusIndexCell] )

            channelRegionString     = getRegionString( channelRegionLink , dictCaloLayer["Region"] )
            channelSamplingString   = getSamplingString( channelSamplingLink , dictCaloLayer["Layer"])
            channelCaloGainString   = getCaloGainString( channelGainLink , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

            channelDict["caloRegion"].append( channelRegionString )
            channelDict["sampling"].append( channelSamplingString )
            channelDict["gain"].append( channelCaloGainString )
            channelDict["eta"].append (channelEtaLink.tolist() )
            channelDict["phi"].append (channelPhiLink.tolist() )
            channelDict["deta"].append(channelDEtaLink.tolist() )
            channelDict["dphi"].append(channelDPhiLink.tolist() )
        
            # -- RawChannels --
            rawChannelDict  = {
                "index":            [],
                "eta":              [],
                "phi":              [],
                "deta":             [],
                "dphi":             [],
                "layer":            [],
                "caloRegion":       [],
                "sampling":         [],
                "amplitude":        [],
                "time":             [],
                "Tile_Pedestal":    [],
                "LAr_Provenance":   [],
                "quality":          [],
                "chInfo":           [], # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                "channelId":        [],
                "DSPThreshold":     []
            }
            # must be treated because these indexes has floating point to represent tile PMT1 (+0.1) and PMT2 (+0.2), but when loading them, it is not precise (small bug to be fixed)
            roundedRawChIndex = [round(x,1) for x in clusterRawChannelIndexArray[clusIndexRawCh]]

            rawChannelDict["index"].append( roundedRawChIndex )
            rawChannelDict["amplitude"].append(clusterRawChannelAmplitudeArray[clusIndexRawCh].tolist())
            rawChannelDict["time"].append(clusterRawChannelTimeArray[clusIndexRawCh].tolist())
            rawChannelDict["Tile_Pedestal"].append(clusterRawChannelPedArray[clusIndexRawCh].tolist())
            rawChannelDict["LAr_Provenance"].append(clusterRawChannelProvArray[clusIndexRawCh].tolist())
            rawChannelDict["quality"].append(clusterRawChannelQualArray[clusIndexRawCh].tolist())
            rawChannelDict["chInfo"].append(clusterRawChannelChInfoArray[clusIndexRawCh].tolist())
            rawChannelDict["channelId"].append(clusterRawChannelIdArray[clusIndexRawCh].tolist())
            if not(isMC):
                rawChannelDict["DSPThreshold"].append(clusterRawChannelDSPThrArray[clusIndexRawCh].tolist())
            
            #  Index conversion: it works like data decompression from root files, that were indexed to use less
            # disk when dumped from ESD format. Now, information that were saved for only one data level (cell, channel, etc),
            # can be propagated to the others to built a dataset which is easier to handle, independently of each data level.
            rawChannelRegionLink        = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellRegionArray[clusIndexCell])
            # rawChannelSamplingLink      = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellLayerArray[clusIndexCell] )
            rawChannelSamplingLink      = clusterRawChannelLayerArray[clusIndexRawCh].tolist()
            rawChannelEtaLink           = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellEtaArray[clusIndexCell])
            rawChannelDEtaLink          = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellDEtaArray[clusIndexCell])
            rawChannelPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellPhiArray[clusIndexCell])
            rawChannelDPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellDPhiArray[clusIndexCell])
            rawChannelRegionString      = getRegionString( rawChannelRegionLink , dictCaloLayer["Region"] )
            rawChannelSamplingString    = getSamplingString( rawChannelSamplingLink , dictCaloLayer["Layer"])            

            rawChannelDict["caloRegion"].append( rawChannelRegionString )
            rawChannelDict["sampling"].append( rawChannelSamplingString )
            rawChannelDict["eta"].append( rawChannelEtaLink.tolist() )
            rawChannelDict["phi"].append( rawChannelPhiLink.tolist() )
            rawChannelDict["deta"].append( rawChannelDEtaLink.tolist() )
            rawChannelDict["dphi"].append( rawChannelDPhiLink.tolist() )

            # -- Hits (MC "Truth") --
            if isMC:
                hitsDict    = {
                    "index":        [],
                    "sampling":     [],
                    "energy":       [],
                    "time":         [],
                    "eta":          [],
                    "phi":          [],
                }

                roundedHitsChIndex  = [round(x,1) for x in hits_clusterChannelIndexArray[clusIndexHits]]

                hitsDict["index"].append( roundedHitsChIndex )
                hitsDict["sampling"].append( hits_layerArray[clusIndexHits] )
                hitsDict["energy"].append( hits_energyArray[clusIndexHits] )
                hitsDict["time"].append( hits_timeArray[clusIndexHits] )
                hitsDict["eta"].append( hits_cellEtaArray[clusIndexHits] )
                hitsDict["phi"].append( hits_cellPhiArray[clusIndexHits] )

            # ----------------------- III-end ------------------------------------
            # fill clusters with cells, channels and raw channels
            clusName    = str("cl_{}".format(clusterIndexArray[cl]))
            clusterDict[clusName]["cells"].update( cellDict )
            clusterDict[clusName]["channels"].update( channelDict )
            clusterDict[clusName]["rawChannels"].update( rawChannelDict )
            if isMC:
                clusterDict[clusName]["hits"].update( hitsDict )

            # fill electron with cluster(s)
            electronDict["el_{}".format(elec)]["{}".format(clusnamedict)].update( clusterDict )

        # --------------------------------------------------------------------------
        #                       ROI 7_11 EMB2 Reference
        # --------------------------------------------------------------------------
        if clusterType=='7x11':
            
            # ----------------------- II-1 Calo Objects (711 ROI) ----------------------
            # find array indexes for the cluster collection, from each electron.
            # eIndexClus = np.where( electronIndex_clusterLvlArray == electronIndexArray[elec] )[0]

            # with the array indexes, find the the cluster indexes, linked to the selected electron
            # ( the cluster index is used to reach the cell level indexes. )
            # for cIndex in clusterIndexArray[eIndexClus]: # for each cluster
            for cl in eIndexClus:
        
                # layerIndex = np.where(clusterCellLayerArray[cIndexCell] == 2)[0] # get EMB2 cells
                clusterDict = {
                    "cl_{}".format(cluster711IndexArray[cl]):{
                        "index":        [],
                        "pt":           [],
                        "eta":          [],
                        "phi":          [],
                        # cells
                        "cells":         {}, # structure
                        "channels":      {}, # structure
                        "rawChannels":   {} # structure
                    }
                }
                # fill clusters
                clusterDict["cl_{}".format(cluster711IndexArray[cl])]["index"].append(cluster711IndexArray[cl])
                clusterDict["cl_{}".format(cluster711IndexArray[cl])]["pt"].append (cluster711PtArray[cl])
                clusterDict["cl_{}".format(cluster711IndexArray[cl])]["eta"].append(cluster711EtaArray[cl])
                clusterDict["cl_{}".format(cluster711IndexArray[cl])]["phi"].append(cluster711PhiArray[cl])

                # ----------------------- III-1 CaloCell and Channels (711) ------------------
                # get cells, channels and rawChannels linked to each cluster
                clusIndexCell   = np.where( cluster711IndexCellLvlArray    == cluster711IndexArray[cl] )[0]
                clusIndexCh     = np.where( cluster711IndexChLvlArray      == cluster711IndexArray[cl] )[0]
                clusIndexRawCh  = np.where( cluster711IndexRawChLvlArray   == cluster711IndexArray[cl] )[0]
                # print("clusterCellIndexArray:", clusterCellIndexArray)
                # print("clusterRawChannelIndexArray",clusterRawChannelIndexArray)

                # -- Cells --
                # OBS.: it was chosen to remove most of the information in this part of dict (cell level), because
                # it is more convenient to work with the xtalk and cell data in the same level, in this case, in channel.
                # 28 Sept, it was added again to perform some tests.
                cellDict    = {
                    "index":        [],
                    "caloRegion":   [],
                    "sampling":     [],
                    "eta":          [],
                    "phi":          [],
                    "deta":         [],
                    "dphi":         [],
                    "gain":         []
                }
                cellRegionString    = getRegionString   ( cluster711CellRegionArray[clusIndexCell] , dictCaloLayer["Region"])
                cellSamplingString  = getSamplingString ( cluster711CellLayerArray[clusIndexCell] , dictCaloLayer["Layer"] )
                cellCaloGainString  = getCaloGainString ( cluster711CellCaloGainArray[clusIndexCell] , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

                cellDict["index"].append        ( cluster711CellIndexArray[clusIndexCell].tolist() )
                cellDict["sampling"].append     ( cellSamplingString )
                cellDict["caloRegion"].append   ( cellRegionString )
                cellDict["eta"].append          ( cluster711CellEtaArray[clusIndexCell].tolist() )
                cellDict["phi"].append          ( cluster711CellPhiArray[clusIndexCell].tolist() )
                cellDict["deta"].append         ( cluster711CellDEtaArray[clusIndexCell].tolist() )
                cellDict["dphi"].append         ( cluster711CellDPhiArray[clusIndexCell].tolist() )
                # cellDict["gain"].append( clusterCellCaloGainArray[clusIndexCell] )
                cellDict["gain"].append         ( cellCaloGainString )

                # print(clusterCellCaloGainArray[clusIndexCell], cellDict["gain"] )

                # -- Channels --
                channelDict     = {
                    "index":            [],
                    "caloRegion":       [], # from cell level
                    "sampling":         [], # from cell level
                    "energy":           [],
                    "time":             [],
                    "digits":           [],
                    "eta":              [], # from cell level
                    "phi":              [], # from cell level
                    "gain":             [], # from cell level
                    "deta":             [], # from cell level
                    "dphi":             [], # from cell level
                    "chInfo":           [],  # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                    "hashId":           [],
                    "channelId":        [],
                    "effSigma":         [],
                    "noise":            [],
                    "DSPThreshold":     [],
                    "OFCTimeOffset":    [],
                    "ADC2MeV0":         [],
                    "ADC2MeV1":         [],
                    "LArDB_Pedestal":   [],
                    "OFCa":             [],
                    "OFCb":             [],
                    "Shape":            [],
                    "ShapeDer":         [],
                    "MinBiasAvg":       [],
                    "OfflHVScale":      [],
                    "OfflEneRescaler":  []
                }
                if dataHasBadCh:
                    channelDict.update({"badCh": []})
                    # channelDict["badCh"].append(cluster711ChannelBadArray[clusIndexCh].tolist())
                roundedChIndex  = [round(x,1) for x in cluster711ChannelIndexArray[clusIndexCh]]

                # print(cluster711ChannelEnergyArray,cluster711ChannelChInfoArray)
            
                channelDict["index"].append ( roundedChIndex )            
                channelDict["energy"].append(cluster711ChannelEnergyArray[clusIndexCh].tolist())
                channelDict["time"].append  (cluster711ChannelTimeArray[clusIndexCh].tolist())
                channelDict["digits"].append(cluster711ChannelDigitsArray[clusIndexCh].tolist())
                channelDict["chInfo"].append(cluster711ChannelChInfoArray[clusIndexCh].tolist())
                channelDict["hashId"].append(cluster711ChannelHashArray[clusIndexCh].tolist())
                channelDict["channelId"].append(cluster711ChannelIdArray[clusIndexCh].tolist())
                channelDict["effSigma"].append(cluster711ChannelEffSigmaArray[clusIndexCh].tolist())
                channelDict["noise"].append(cluster711ChannelNoiseArray[clusIndexCh].tolist())
                channelDict["DSPThreshold"].append(cluster711ChannelDSPThrArray[clusIndexCh].tolist())
                channelDict["OFCTimeOffset"].append(cluster711ChannelOFCTimeOffsetArray[clusIndexCh].tolist())
                channelDict["ADC2MeV0"].append(cluster711ChannelADC2MeV0Array[clusIndexCh].tolist())
                channelDict["ADC2MeV1"].append(cluster711ChannelADC2MeV1Array[clusIndexCh].tolist())
                channelDict["LArDB_Pedestal"].append(cluster711ChannelPedArray[clusIndexCh].tolist())
                channelDict["OFCa"].append(cluster711ChannelOFCaArray[clusIndexCh].tolist())
                channelDict["OFCb"].append(cluster711ChannelOFCbArray[clusIndexCh].tolist())
                channelDict["Shape"].append(cluster711ChannelShapeArray[clusIndexCh].tolist())
                channelDict["ShapeDer"].append(cluster711ChannelShapeDerArray[clusIndexCh].tolist())
                channelDict["MinBiasAvg"].append(cluster711ChannelMinBiasAvgArray[clusIndexCh].tolist())
                channelDict["OfflHVScale"].append(cluster711ChannelOfflHVScaleArray[clusIndexCh].tolist())
                channelDict["OfflEneRescaler"].append(cluster711ChannelOfflEneRescArray[clusIndexCh].tolist)

                # Data decompression
                channelRegionLink       = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellRegionArray[clusIndexCell])
                channelSamplingLink     = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellLayerArray[clusIndexCell] )
                channelGainLink         = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellCaloGainArray[clusIndexCell] )
                channelEtaLink          = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellEtaArray[clusIndexCell] )
                channelPhiLink          = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellPhiArray[clusIndexCell] )
                channelDEtaLink         = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellDEtaArray[clusIndexCell] )
                channelDPhiLink         = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellDPhiArray[clusIndexCell] )

                channelRegionString     = getRegionString( channelRegionLink , dictCaloLayer["Region"] )
                channelSamplingString   = getSamplingString( channelSamplingLink , dictCaloLayer["Layer"])
                channelCaloGainString   = getCaloGainString( channelGainLink , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

                channelDict["caloRegion"].append(channelRegionString )
                channelDict["sampling"].append  (channelSamplingString )
                channelDict["gain"].append      (channelCaloGainString)  #( cellCaloGainString ) ## is that organized with indexes sequences for channel?
                channelDict["eta"].append       (channelEtaLink.tolist() )
                channelDict["phi"].append       (channelPhiLink.tolist() )
                channelDict["deta"].append      (channelDEtaLink.tolist() )
                channelDict["dphi"].append      (channelDPhiLink.tolist() )
                
                # # cellDict["gain"].append( clusterCellCaloGainArray[clusIndexCell] )
            
                # -- RawChannels --
                rawChannelDict  = {
                    "index":            [],
                    "eta":              [],
                    "phi":              [],
                    "deta":             [],
                    "dphi":             [],
                    "caloRegion":       [],
                    "sampling":         [],
                    "amplitude":        [],
                    "time":             [],
                    "Tile_Pedestal":    [],
                    "LAr_Provenance":   [],
                    "quality":          [],
                    "chInfo":           [], # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                    "channelId":        [],
                    "DSPThreshold":     []
                }
                # must be treated because these indexes has floating point to represent tile PMT1 (+0.1) and PMT2 (+0.2), but when loading them, it is not precise (small bug to be fixed)
                roundedRawChIndex = [round(x,1) for x in cluster711RawChannelIndexArray[clusIndexRawCh]]

                rawChannelDict["index"].append          ( roundedRawChIndex )
                rawChannelDict["amplitude"].append      (cluster711RawChannelAmplitudeArray[clusIndexRawCh].tolist())
                rawChannelDict["time"].append           (cluster711RawChannelTimeArray[clusIndexRawCh].tolist())
                rawChannelDict["Tile_Pedestal"].append  (cluster711RawChannelPedArray[clusIndexRawCh].tolist())
                rawChannelDict["LAr_Provenance"].append (cluster711RawChannelProvArray[clusIndexRawCh].tolist())
                rawChannelDict["quality"].append        (cluster711RawChannelQualArray[clusIndexRawCh].tolist())
                rawChannelDict["chInfo"].append         (cluster711RawChannelChInfoArray[clusIndexRawCh].tolist())
                rawChannelDict["channelId"].append      (cluster711RawChannelIdArray[clusIndexRawCh].tolist())
                rawChannelDict["DSPThreshold"].append   (cluster711RawChannelDSPThrArray[clusIndexRawCh].tolist())


                #  Index conversion: it works like data decompression from root files, that were indexed to use less
                # disk when dumped from ESD format. Now, information that were saved for only one data level (cell, channel, etc),
                # can be propagated to the others to built a dataset which is easier to handle, independently of each data level.
                # Here, the cluster711Cell variables in cell level, will be converted to their respective cells in the rawChannel level.
                rawChannelRegionLink        = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellRegionArray[clusIndexCell])
                rawChannelSamplingLink      = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellLayerArray[clusIndexCell] )
                rawChannelEtaLink           = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellEtaArray[clusIndexCell])
                rawChannelDEtaLink          = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellDEtaArray[clusIndexCell])
                rawChannelPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellPhiArray[clusIndexCell])
                rawChannelDPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"] , cluster711CellDPhiArray[clusIndexCell])

                rawChannelRegionString      = getRegionString( rawChannelRegionLink , dictCaloLayer["Region"] )
                rawChannelSamplingString    = getSamplingString( rawChannelSamplingLink , dictCaloLayer["Layer"])            

                rawChannelDict["caloRegion"].append( rawChannelRegionString )
                rawChannelDict["sampling"].append( rawChannelSamplingString )
                rawChannelDict["eta"].append( rawChannelEtaLink.tolist() )
                rawChannelDict["phi"].append( rawChannelPhiLink.tolist() )
                rawChannelDict["deta"].append( rawChannelDEtaLink.tolist() )
                rawChannelDict["dphi"].append( rawChannelDPhiLink.tolist() )

                # print(np.shape(rawChannelDict["phi"]), rawChannelDict["phi"])
                # print(np.shape(cellDict["eta"]), cellDict["eta"] )

                # df = pd.concat({k: pd.DataFrame.from_dict(v, orient='index') for k, v in rawChannelDict.items()}, axis=1)
                # print(df)
                # print(pd.concat({k: pd.DataFrame.from_dict(v) for k, v in rawChannelDict.items()}))

                # ----------------------- III-end ------------------------------------
                # fill clusters with cells, channels and raw channels
                clusName    = str("cl_{}".format(cluster711IndexArray[cl]))
                clusterDict[clusName]["cells"].update( cellDict )
                clusterDict[clusName]["channels"].update( channelDict )
                clusterDict[clusName]["rawChannels"].update( rawChannelDict )

                # saveAsJsonFile(clusterDict, 'file001.json')
                # lDatasetDict = loadJsonFile('file001.json')
                # print(lDatasetDict)

                # fill electron with cluster(s)
                electronDict["el_{}".format(elec)]["{}".format(clusnamedict)].update( clusterDict ) 

        # fill event with particles
        eventInfoDict["electrons"].update( electronDict )
        if isMC:
            eventInfoDict["MC_truth"].update( mcTruthDict )

    # fill dataset with events
    datasetDict["dataset"].append(eventInfoDict)
    
    return datasetDict


def getXTDataAsPythonDictFasterClean(theEvent, isMC=False, verbose=0, clusterType='topocluster', isTagAndProbe=False):
    '''Return 0 case there were NO electrons in this event.'''

    ############################
    ####### Configuration ######
    ############################
    # jetROI  = -1

    dataHasBadCh    = True # if there isnt any bad channel, force it to false.
    try:
        dataBadCh   = getattr(theEvent,'cluster_channel_bad')
        # jetROI  = np.float32(getattr(sTree, 'jet_roi_r'))
    except:
        if (verbose==1):
            print("Data does not have bad_channel info. dataHasBadCh=False")
        dataHasBadCh  = False # if the data dumped has the flag 'noBadCells=True', this flag has to be False. Otherwise, its True.
        pass
    # evLim               = nEvts

    # ****** DICTIONARIES ******
    fDictLayerName      = 'dictCaloByLayer.json'
    dictCaloLayer       = loadJsonFile(fDictLayerName) # Load Calo Geometry Dictionary
    # transitionEtaWidthL = dictCaloLayer["transitionRegionEta"] #0.2 # eta value around the transition regions to consider (or discard) cells which could be tagged as belonging to another region in the same layer.
    # transitionEtaWidthH = dictCaloLayer["transitionRegionEta"] #0.2 # (eta + transitionEtaWidthL) < Eta < (eta - transitionEtaWidthH). A value for each layer.
    # maxOfLayers         = len(dictCaloLayer['Layer']) 

    # Number of regions in each sampling layer (eta x phi region)
    regionsPerLayer = []
    for reg in dictCaloLayer['granularityEtaLayer']:
        regionsPerLayer.append(len(reg))

    datasetDict = {"dataset": {}}

    # sTree.GetEntry(evN)
    # ----------------------------------------
    # Verify if the event has any electron: 
    # ----------------------------------------
    electronIndex          = getattr(theEvent,'el_index')
    electronIndexArray     = stdVecToArray(electronIndex)

    # print(len(electronIndexArray)) # debug
    if len(electronIndexArray) == 0: # skip events without electrons
        return 0

    # -------------------------------------------
    
    ei_runNumber    = int(getattr(theEvent, "event_RunNumber"))
    ei_eventNumber  = int(getattr(theEvent, "event_EventNumber"))
    ei_bcid         = int(getattr(theEvent, "event_BCID"))
    ei_lumibNumber  = int(getattr(theEvent, "event_Lumiblock"))
    ei_avgMu        = getattr(theEvent, "event_avg_mu_inTimePU")
    ei_avgMuOOT     = getattr(theEvent, "event_avg_mu_OOTimePU")
    
    ### Particle Level
    electronPt                  = getattr(theEvent,'el_Pt')
    # electronIndex               = getattr(theEvent,'el_index')
    electronIndex_clusterLvl    = getattr(theEvent,'c_electronIndex_clusterLvl')
    electronEta                 = getattr(theEvent,'el_Eta')
    electronPhi                 = getattr(theEvent,'el_Phi')
    electronEoverP              = getattr(theEvent,'el_eoverp')
    electronSSf1                = getattr(theEvent,'el_f1')
    electronSSf3                = getattr(theEvent,'el_f3')
    electronSSeratio            = getattr(theEvent,'el_eratio')
    electronSSweta1             = getattr(theEvent,'el_weta1')
    electronSSweta2             = getattr(theEvent,'el_weta2')
    electronSSfracs1            = getattr(theEvent,'el_fracs1')
    electronSSwtots1            = getattr(theEvent,'el_wtots1')
    electronSSe277              = getattr(theEvent,'el_e277')
    electronSSreta              = getattr(theEvent,'el_reta')
    electronSSrphi              = getattr(theEvent,'el_rphi')
    electronSSdeltae            = getattr(theEvent,'el_deltae')
    electronSSrhad              = getattr(theEvent,'el_rhad')
    electronSSrhad1             = getattr(theEvent,'el_rhad1')
    vertexX                     = getattr(theEvent,'vtx_x')
    vertexY                     = getattr(theEvent,'vtx_y')
    vertexZ                     = getattr(theEvent,'vtx_z')
    
    if isTagAndProbe:
        zeeMass     = getattr(theEvent,'zee_M')
        zeeEnergy   = getattr(theEvent,'zee_E')
        zeePt       = getattr(theEvent,'zee_pt')
        zeePx       = getattr(theEvent,'zee_px')
        zeePy       = getattr(theEvent,'zee_py')
        zeePz       = getattr(theEvent,'zee_pz')
        zeeT        = getattr(theEvent,'zee_T')
        zeeDeltaR   = getattr(theEvent,'zee_deltaR')

    ### TopoCluster or SuperCluster
    if clusterType=='topocluster':
        clusterIndex                 = getattr(theEvent,'cluster_index')
        clusterPt                    = getattr(theEvent,'cluster_pt')
        clusterEta                   = getattr(theEvent,'cluster_eta')
        clusterPhi                   = getattr(theEvent,'cluster_phi')
        clusterTime                  = getattr(theEvent,'cluster_time')
        # clusterEtaCalc               = getattr(theEvent,'cluster_eta_calc')
        # clusterPhiCalc               = getattr(theEvent,'cluster_phi_calc')
        # Cluster Cells 
        clusterCellIndex             = getattr(theEvent,'cluster_cell_index') 
        clusterIndexCellLvl          = getattr(theEvent,'cluster_index_cellLvl')
        clusterCellCaloGain          = getattr(theEvent,'cluster_cell_caloGain')
        clusterCellLayer             = getattr(theEvent,'cluster_cell_layer')
        clusterCellEnergy            = getattr(theEvent,'cluster_cell_energy')
        clusterCellTime              = getattr(theEvent,'cluster_cell_time')
        clusterCellRegion            = getattr(theEvent,'cluster_cell_region')
        clusterCellEta               = getattr(theEvent,'cluster_cell_eta')
        clusterCellPhi               = getattr(theEvent,'cluster_cell_phi') 
        clusterCellDEta              = getattr(theEvent,'cluster_cell_deta')
        clusterCellDPhi              = getattr(theEvent,'cluster_cell_dphi')
        clusterCellsDistDEta         = getattr(theEvent,'cluster_cellsDist_deta')
        clusterCellsDistDPhi         = getattr(theEvent,'cluster_cellsDist_dphi')
        # Cluster Channel 
        clusterIndexChLvl            = getattr(theEvent,'cluster_index_chLvl')
        clusterChannelIndex          = getattr(theEvent,'cluster_channel_index')
        clusterChannelHash           = getattr(theEvent,'cluster_channel_hash')
        clusterChannelId             = getattr(theEvent,'cluster_channel_id')
        clusterChannelDigits         = getattr(theEvent,'cluster_channel_digits')
        clusterChannelEnergy         = getattr(theEvent,'cluster_channel_energy')
        clusterChannelTime           = getattr(theEvent,'cluster_channel_time')
        clusterChannelLayer          = getattr(theEvent,'cluster_channel_layer')
        if dataHasBadCh: clusterChannelBad            = getattr(theEvent,'cluster_channel_bad')
        clusterChannelChInfo         = getattr(theEvent,'cluster_channel_chInfo')
        clusterChannelEffSigma       = getattr(theEvent,'cluster_channel_effSigma')
        clusterChannelNoise          = getattr(theEvent,'cluster_channel_noise')
        if not(isMC): # calibration for data only
            clusterChannelDSPThr         = getattr(theEvent,'cluster_channel_DSPThreshold')
            clusterChannelShape          = getattr(theEvent,'cluster_channel_shape')
            clusterChannelShapeDer       = getattr(theEvent,'cluster_channel_shapeDer')
            clusterChannelOfflHVScale    = getattr(theEvent,'cluster_channel_OfflineHVScale')
            clusterChannelOfflEneResc    = getattr(theEvent,'cluster_channel_OfflineEnergyRescaler')
        clusterChannelOFCTimeOffset  = getattr(theEvent,'cluster_channel_OFCTimeOffset')
        clusterChannelADC2MeV0       = getattr(theEvent,'cluster_channel_ADC2MeV0')
        clusterChannelADC2MeV1       = getattr(theEvent,'cluster_channel_ADC2MeV1')
        clusterChannelPed            = getattr(theEvent,'cluster_channel_pedestal')
        clusterChannelOFCa           = getattr(theEvent,'cluster_channel_OFCa')
        clusterChannelOFCb           = getattr(theEvent,'cluster_channel_OFCb')
        clusterChannelMinBiasAvg     = getattr(theEvent,'cluster_channel_MinBiasAvg')
            
            
        # Cluster raw channel 
        clusterIndexRawChLvl         = getattr(theEvent,'cluster_index_rawChLvl')
        clusterRawChannelIndex       = getattr(theEvent,'cluster_rawChannel_index')
        clusterRawChannelAmplitude   = getattr(theEvent,'cluster_rawChannel_amplitude')
        clusterRawChannelTime        = getattr(theEvent,'cluster_rawChannel_time')
        clusterRawChannelLayer       = getattr(theEvent,'cluster_rawChannel_layer')
        clusterRawChannelPed         = getattr(theEvent,'cluster_rawChannel_Ped')
        clusterRawChannelProv        = getattr(theEvent,'cluster_rawChannel_Prov')
        clusterRawChannelQual        = getattr(theEvent,'cluster_rawChannel_qual')
        clusterRawChannelChInfo      = getattr(theEvent,'cluster_rawChannel_chInfo')
        clusterRawChannelId          = getattr(theEvent,'cluster_rawChannel_id')
        if not(isMC):
            clusterRawChannelDSPThr      = getattr(theEvent,'cluster_rawChannel_DSPThreshold')

    if isMC:
        # Hits
        hits_layer                   = getattr(theEvent,'hits_sampling')
        hits_clusterIndexChLvl       = getattr(theEvent,'hits_clusterIndex_chLvl')
        hits_hash                    = getattr(theEvent,'hits_hash')
        hits_clusterChannelIndex     = getattr(theEvent,'hits_clusterChannelIndex')
        # hits_energy       = getattr(theEvent,'hits_energy')
        hits_energy                  = getattr(theEvent,'hits_energyConv')
        hits_time                    = getattr(theEvent,'hits_time')
        hits_cellEta                 = getattr(theEvent,'hits_cellEta')
        hits_cellPhi                 = getattr(theEvent,'hits_cellPhi')
        # Truth Particle
        mc_partEnergy               = getattr(theEvent,'mc_part_energy')
        mc_partPt                   = getattr(theEvent,'mc_part_pt')
        mc_partMass                 = getattr(theEvent,'mc_part_m')
        mc_partEta                  = getattr(theEvent,'mc_part_eta')
        mc_partPhi                  = getattr(theEvent,'mc_part_phi')
        mc_partPdgId                = getattr(theEvent,'mc_part_pdgId')
        # Truth Vertex
        mc_vertX                    = getattr(theEvent,'mc_vert_x')
        mc_vertY                    = getattr(theEvent,'mc_vert_y')
        mc_vertZ                    = getattr(theEvent,'mc_vert_z')
        mc_vertTime                 = getattr(theEvent,'mc_vert_time')
        mc_vertEta                  = getattr(theEvent,'mc_vert_eta')
        mc_vertPhi                  = getattr(theEvent,'mc_vert_phi')

    ### Fixed Window Clusters (7x11 EMB2)
    if clusterType=='7x11':
        cluster711Index                 = getattr(theEvent,'cluster711_index')
        cluster711Pt                    = getattr(theEvent,'cluster711_pt')
        cluster711Eta                   = getattr(theEvent,'cluster711_eta')
        cluster711Phi                   = getattr(theEvent,'cluster711_phi')
        # Cluster Cells 
        cluster711CellIndex             = getattr(theEvent,'cluster711_cell_index') 
        cluster711IndexCellLvl          = getattr(theEvent,'cluster711_index_cellLvl')
        cluster711CellCaloGain          = getattr(theEvent,'cluster711_cell_caloGain')
        cluster711CellLayer             = getattr(theEvent,'cluster711_cell_layer')
        cluster711CellRegion            = getattr(theEvent,'cluster711_cell_region')
        cluster711CellEta               = getattr(theEvent,'cluster711_cell_eta')
        cluster711CellPhi               = getattr(theEvent,'cluster711_cell_phi') 
        cluster711CellDEta              = getattr(theEvent,'cluster711_cell_deta')
        cluster711CellDPhi              = getattr(theEvent,'cluster711_cell_dphi')
        cluster711CellsDistDEta         = getattr(theEvent,'cluster711_cellsDist_deta')
        cluster711CellsDistDPhi         = getattr(theEvent,'cluster711_cellsDist_dphi')
        # Cluster Channel 
        cluster711IndexChLvl            = getattr(theEvent,'cluster711_index_chLvl')
        cluster711ChannelIndex          = getattr(theEvent,'cluster711_channel_index')
        cluster711ChannelHash           = getattr(theEvent,'cluster711_channel_hash')
        cluster711ChannelId             = getattr(theEvent,'cluster711_channel_id')
        cluster711ChannelDigits         = getattr(theEvent,'cluster711_channel_digits')
        cluster711ChannelEnergy         = getattr(theEvent,'cluster711_channel_energy')
        cluster711ChannelTime           = getattr(theEvent,'cluster711_channel_time')
        # if dataHasBadCh: cluster711ChannelBad            = getattr(theEvent,'cluster711_channel_bad')
        cluster711ChannelChInfo         = getattr(theEvent,'cluster711_channel_chInfo')
        cluster711ChannelEffSigma       = getattr(theEvent,'cluster711_channel_effSigma')
        cluster711ChannelNoise          = getattr(theEvent,'cluster711_channel_noise')
        cluster711ChannelDSPThr         = getattr(theEvent,'cluster711_channel_DSPThreshold')
        cluster711ChannelOFCTimeOffset  = getattr(theEvent,'cluster711_channel_OFCTimeOffset')
        cluster711ChannelADC2MeV0       = getattr(theEvent,'cluster711_channel_ADC2MeV0')
        cluster711ChannelADC2MeV1       = getattr(theEvent,'cluster711_channel_ADC2MeV1')
        cluster711ChannelPed            = getattr(theEvent,'cluster711_channel_pedestal')
        cluster711ChannelOFCa           = getattr(theEvent,'cluster711_channel_OFCa')
        cluster711ChannelOFCb           = getattr(theEvent,'cluster711_channel_OFCb')
        cluster711ChannelShape          = getattr(theEvent,'cluster711_channel_shape')
        cluster711ChannelShapeDer       = getattr(theEvent,'cluster711_channel_shapeDer')
        cluster711ChannelMinBiasAvg     = getattr(theEvent,'cluster711_channel_MinBiasAvg')
        cluster711ChannelOfflHVScale    = getattr(theEvent,'cluster711_channel_OfflineHVScale')
        cluster711ChannelOfflEneResc    = getattr(theEvent,'cluster711_channel_OfflineEnergyRescaler')

        # Cluster raw channel 
        cluster711IndexRawChLvl         = getattr(theEvent,'cluster711_index_rawChLvl')
        cluster711RawChannelIndex       = getattr(theEvent,'cluster711_rawChannel_index')
        cluster711RawChannelAmplitude   = getattr(theEvent,'cluster711_rawChannel_amplitude')
        cluster711RawChannelTime        = getattr(theEvent,'cluster711_rawChannel_time')
        cluster711RawChannelPed         = getattr(theEvent,'cluster711_rawChannel_Ped')
        cluster711RawChannelProv        = getattr(theEvent,'cluster711_rawChannel_Prov')
        cluster711RawChannelQual        = getattr(theEvent,'cluster711_rawChannel_qual')
        cluster711RawChannelChInfo      = getattr(theEvent,'cluster711_rawChannel_chInfo')
        cluster711RawChannelId          = getattr(theEvent,'cluster711_rawChannel_id')
        cluster711RawChannelDSPThr      = getattr(theEvent,'cluster711_rawChannel_DSPThreshold')

    #**********************************************
    # Format into python list/np array
    #**********************************************
    electronPtArray                 = stdVecToArray(electronPt)
    # electronIndexArray              = stdVecToArray(electronIndex)
    electronIndex_clusterLvlArray   = stdVecToArray(electronIndex_clusterLvl)
    electronEtaArray                = stdVecToArray(electronEta)
    electronPhiArray                = stdVecToArray(electronPhi)
    electronEoverPArray             = stdVecToArray(electronEoverP)
    electronSSf1Array               = stdVecToArray(electronSSf1)
    electronSSf3Array               = stdVecToArray(electronSSf3)
    electronSSeratioArray           = stdVecToArray(electronSSeratio)
    electronSSweta1Array            = stdVecToArray(electronSSweta1)
    electronSSweta2Array            = stdVecToArray(electronSSweta2)
    electronSSfracs1Array           = stdVecToArray(electronSSfracs1)
    electronSSwtots1Array           = stdVecToArray(electronSSwtots1)
    electronSSe277Array             = stdVecToArray(electronSSe277)
    electronSSretaArray             = stdVecToArray(electronSSreta)
    electronSSrphiArray             = stdVecToArray(electronSSrphi)
    electronSSdeltaeArray           = stdVecToArray(electronSSdeltae)
    electronSSrhadArray             = stdVecToArray(electronSSrhad)
    electronSSrhad1Array            = stdVecToArray(electronSSrhad1)
    vertexZArray                    = stdVecToArray(vertexZ)
    vertexYArray                    = stdVecToArray(vertexY)
    vertexXArray                    = stdVecToArray(vertexX)
    
    if isTagAndProbe:
        zeeMassArray     = stdVecToArray(zeeMass)
        zeeEnergyArray   = stdVecToArray(zeeEnergy)
        zeePtArray       = stdVecToArray(zeePt)
        zeePxArray       = stdVecToArray(zeePx)
        zeePyArray       = stdVecToArray(zeePy)
        zeePzArray       = stdVecToArray(zeePz)
        zeeTArray        = stdVecToArray(zeeT)
        zeeDeltaRArray   = stdVecToArray(zeeDeltaR)

    if clusterType=='topocluster':
        clusterIndexArray               = stdVecToArray(clusterIndex)
        clusterPtArray                  = stdVecToArray(clusterPt)
        clusterEtaArray                 = stdVecToArray(clusterEta)
        clusterPhiArray                 = stdVecToArray(clusterPhi)
        clusterTimeArray                = stdVecToArray(clusterTime)

        clusterCellIndexArray           = stdVecToArray(clusterCellIndex)
        clusterIndexCellLvlArray        = stdVecToArray(clusterIndexCellLvl)
        clusterCellCaloGainArray        = stdVecToArray(clusterCellCaloGain)
        clusterCellLayerArray           = stdVecToArray(clusterCellLayer)
        clusterCellEnergyArray          = stdVecToArray(clusterCellEnergy)
        clusterCellTimeArray            = stdVecToArray(clusterCellTime)
        clusterCellRegionArray          = stdVecToArray(clusterCellRegion)
        clusterCellEtaArray             = stdVecToArray(clusterCellEta)
        clusterCellPhiArray             = stdVecToArray(clusterCellPhi)
        clusterCellDEtaArray            = stdVecToArray(clusterCellDEta)
        clusterCellDPhiArray            = stdVecToArray(clusterCellDPhi)
        clusterCellsDistDEtaArray       = stdVecToArray(clusterCellsDistDEta)
        clusterCellsDistDPhiArray       = stdVecToArray(clusterCellsDistDPhi)
        
        clusterIndexChLvlArray          = stdVecToArray(clusterIndexChLvl)
        clusterChannelIndexArray        = stdVecToArray(clusterChannelIndex)
        clusterChannelHashArray         = stdVecToArray(clusterChannelHash)
        clusterChannelIdArray           = stdVecToArray(clusterChannelId)
        clusterChannelDigitsArray       = stdVecOfStdVecToArrayOfList(clusterChannelDigits, convertToInt=True)
        clusterChannelEnergyArray       = stdVecToArray(clusterChannelEnergy)
        clusterChannelTimeArray         = stdVecToArray(clusterChannelTime)
        clusterChannelLayerArray        = stdVecToArray(clusterChannelLayer)
        clusterChannelChInfoArray       = stdVecOfStdVecToArrayOfList(clusterChannelChInfo)
        if dataHasBadCh: clusterChannelBadArray      = stdVecToArray(clusterChannelBad)
        clusterChannelEffSigmaArray         = stdVecToArray(clusterChannelEffSigma)
        clusterChannelNoiseArray            = stdVecToArray(clusterChannelNoise)
        if not(isMC):
            clusterChannelDSPThrArray           = stdVecToArray(clusterChannelDSPThr)
            clusterChannelShapeArray            = stdVecOfStdVecToArrayOfList(clusterChannelShape)
            clusterChannelShapeDerArray         = stdVecOfStdVecToArrayOfList(clusterChannelShapeDer)
            clusterChannelOfflHVScaleArray      = stdVecToArray(clusterChannelOfflHVScale)
            clusterChannelOfflEneRescArray      = stdVecToArray(clusterChannelOfflEneResc)
        clusterChannelOFCTimeOffsetArray    = stdVecToArray(clusterChannelOFCTimeOffset)
        clusterChannelADC2MeV0Array         = stdVecToArray(clusterChannelADC2MeV0)
        clusterChannelADC2MeV1Array         = stdVecToArray(clusterChannelADC2MeV1)
        clusterChannelPedArray              = stdVecToArray(clusterChannelPed)
        clusterChannelOFCaArray             = stdVecOfStdVecToArrayOfList(clusterChannelOFCa)
        clusterChannelOFCbArray             = stdVecOfStdVecToArrayOfList(clusterChannelOFCb)
        clusterChannelMinBiasAvgArray       = stdVecToArray(clusterChannelMinBiasAvg)

        clusterIndexRawChLvlArray       = stdVecToArray(clusterIndexRawChLvl)
        clusterRawChannelIndexArray     = stdVecToArray(clusterRawChannelIndex)
        clusterRawChannelAmplitudeArray = stdVecToArray(clusterRawChannelAmplitude)
        clusterRawChannelTimeArray      = stdVecToArray(clusterRawChannelTime)
        clusterRawChannelLayerArray     = stdVecToArray(clusterRawChannelLayer)
        clusterRawChannelPedArray       = stdVecToArray(clusterRawChannelPed)
        clusterRawChannelProvArray      = stdVecToArray(clusterRawChannelProv)
        clusterRawChannelQualArray      = stdVecToArray(clusterRawChannelQual)
        clusterRawChannelChInfoArray    = stdVecOfStdVecToArrayOfList(clusterRawChannelChInfo)
        clusterRawChannelIdArray        = stdVecToArray(clusterRawChannelId)
        if not(isMC):
            clusterRawChannelDSPThrArray    = stdVecToArray(clusterRawChannelDSPThr)

    if isMC:        
        hits_layerArray                   = stdVecToArray(hits_layer)
        hits_clusterIndexChLvlArray       = stdVecToArray(hits_clusterIndexChLvl)
        hits_hashArray                    = stdVecToArray(hits_hash)
        hits_clusterChannelIndexArray     = stdVecToArray(hits_clusterChannelIndex)
        hits_energyArray                  = stdVecToArray(hits_energy)
        hits_timeArray                    = stdVecToArray(hits_time)
        hits_cellEtaArray                 = stdVecToArray(hits_cellEta)
        hits_cellPhiArray                 = stdVecToArray(hits_cellPhi)

        mc_partEnergyArray               = stdVecToArray(mc_partEnergy)
        mc_partPtArray                   = stdVecToArray(mc_partPt)
        mc_partMassArray                 = stdVecToArray(mc_partMass)
        mc_partEtaArray                  = stdVecToArray(mc_partEta)
        mc_partPhiArray                  = stdVecToArray(mc_partPhi)
        mc_partPdgIdArray                = stdVecToArray(mc_partPdgId)

        mc_vertXArray                    = stdVecToArray(mc_vertX)
        mc_vertYArray                    = stdVecToArray(mc_vertY)
        mc_vertZArray                    = stdVecToArray(mc_vertZ)
        mc_vertTimeArray                 = stdVecToArray(mc_vertTime)
        mc_vertEtaArray                  = stdVecToArray(mc_vertEta)
        mc_vertPhiArray                  = stdVecToArray(mc_vertPhi)

    if clusterType=='7x11':
        cluster711IndexArray               = stdVecToArray(cluster711Index)
        cluster711PtArray                  = stdVecToArray(cluster711Pt)
        cluster711EtaArray                 = stdVecToArray(cluster711Eta)
        cluster711PhiArray                 = stdVecToArray(cluster711Phi)

        cluster711CellIndexArray           = stdVecToArray(cluster711CellIndex)
        cluster711IndexCellLvlArray        = stdVecToArray(cluster711IndexCellLvl)
        cluster711CellCaloGainArray        = stdVecToArray(cluster711CellCaloGain)
        cluster711CellLayerArray           = stdVecToArray(cluster711CellLayer)
        cluster711CellRegionArray          = stdVecToArray(cluster711CellRegion)
        cluster711CellEtaArray             = stdVecToArray(cluster711CellEta)
        cluster711CellPhiArray             = stdVecToArray(cluster711CellPhi)
        cluster711CellDEtaArray            = stdVecToArray(cluster711CellDEta)
        cluster711CellDPhiArray            = stdVecToArray(cluster711CellDPhi)
        cluster711CellsDistDEtaArray       = stdVecToArray(cluster711CellsDistDEta)
        cluster711CellsDistDPhiArray       = stdVecToArray(cluster711CellsDistDPhi)
        
        cluster711IndexChLvlArray          = stdVecToArray(cluster711IndexChLvl)
        cluster711ChannelIndexArray        = stdVecToArray(cluster711ChannelIndex)
        cluster711ChannelHashArray         = stdVecToArray(cluster711ChannelHash)
        cluster711ChannelIdArray           = stdVecToArray(cluster711ChannelId)
        cluster711ChannelDigitsArray       = stdVecOfStdVecToArrayOfList(cluster711ChannelDigits, convertToInt=True)
        cluster711ChannelEnergyArray       = stdVecToArray(cluster711ChannelEnergy)
        cluster711ChannelTimeArray         = stdVecToArray(cluster711ChannelTime)
        cluster711ChannelChInfoArray       = stdVecOfStdVecToArrayOfList(cluster711ChannelChInfo)
        # if dataHasBadCh: cluster711ChannelBadArray      = stdVecToArray(cluster711ChannelBad)
        cluster711ChannelEffSigmaArray     = stdVecToArray(cluster711ChannelEffSigma)
        cluster711ChannelNoiseArray        = stdVecToArray(cluster711ChannelNoise)
        cluster711ChannelDSPThrArray       = stdVecToArray(cluster711ChannelDSPThr)
        cluster711ChannelOFCTimeOffsetArray    = stdVecToArray(cluster711ChannelOFCTimeOffset)
        cluster711ChannelADC2MeV0Array         = stdVecToArray(cluster711ChannelADC2MeV0)
        cluster711ChannelADC2MeV1Array         = stdVecToArray(cluster711ChannelADC2MeV1)
        cluster711ChannelPedArray              = stdVecToArray(cluster711ChannelPed)
        cluster711ChannelOFCaArray             = stdVecOfStdVecToArrayOfList(cluster711ChannelOFCa)
        cluster711ChannelOFCbArray             = stdVecOfStdVecToArrayOfList(cluster711ChannelOFCb)
        cluster711ChannelShapeArray            = stdVecOfStdVecToArrayOfList(cluster711ChannelShape)
        cluster711ChannelShapeDerArray         = stdVecOfStdVecToArrayOfList(cluster711ChannelShapeDer)
        cluster711ChannelMinBiasAvgArray       = stdVecToArray(cluster711ChannelMinBiasAvg)
        cluster711ChannelOfflHVScaleArray      = stdVecToArray(cluster711ChannelOfflHVScale)
        cluster711ChannelOfflEneRescArray      = stdVecToArray(cluster711ChannelOfflEneResc)

        cluster711IndexRawChLvlArray       = stdVecToArray(cluster711IndexRawChLvl)
        cluster711RawChannelIndexArray     = stdVecToArray(cluster711RawChannelIndex)
        cluster711RawChannelAmplitudeArray = stdVecToArray(cluster711RawChannelAmplitude)
        cluster711RawChannelTimeArray      = stdVecToArray(cluster711RawChannelTime)
        cluster711RawChannelPedArray        = stdVecToArray(cluster711RawChannelPed)
        cluster711RawChannelProvArray       = stdVecToArray(cluster711RawChannelProv)
        cluster711RawChannelQualArray      = stdVecToArray(cluster711RawChannelQual)
        cluster711RawChannelChInfoArray    = stdVecOfStdVecToArrayOfList(cluster711RawChannelChInfo)
        cluster711RawChannelIdArray        = stdVecToArray(cluster711RawChannelId)
        cluster711RawChannelDSPThrArray    = stdVecToArray(cluster711RawChannelDSPThr)

    # timePerEvRead = time()

    #**********************************************
    #  XTALKS STUDIES: DATASET DICT
    #**********************************************
    if clusterType=='topocluster':
        clusnamedict = "clusters"
    if clusterType=='7x11':
        clusnamedict = "711_roi"

    if len(clusterCellIndexArray) == 0:
        if verbose==1:
            print("Event {}: Cluster Cells are empty! Skipping this event.".format(theEvent.event_EventNumber))
        return 0
    # ----------------------- 0 EventInfo -------------------------------------
    eventInfoDict = {
        "runNumber":        [],
        "eventNumber":      [],
        "BCID":             [],
        "Lumiblock":        [],
        "avgMu":            [],
        "avgMuOOT":         [],
        "electrons":        {}
    }
    eventInfoDict["runNumber"].append(ei_runNumber)
    eventInfoDict["eventNumber"].append(ei_eventNumber)
    eventInfoDict["BCID"].append(ei_bcid)
    eventInfoDict["Lumiblock"].append(ei_lumibNumber)
    eventInfoDict["avgMu"].append(ei_avgMu)
    eventInfoDict["avgMuOOT"].append(ei_avgMuOOT)      
    # ------------------- Zee Tag And Probe selection ------------------------
    if isTagAndProbe:
        eventInfoDict.update({"ZeeTP": {}})
        
        zeeTPDict = {
            "mass"      : [],
            "energy"    : [],
            "pt"        : [],
            "px"        : [],
            "py"        : [],
            "pz"        : [],
            "T"         : [],
            "deltaR"    : [],
        }
        
        zeeTPDict["mass"].append( zeeMassArray )
        zeeTPDict["energy"].append( zeeEnergyArray )
        zeeTPDict["pt"].append( zeePtArray )
        zeeTPDict["px"].append( zeePxArray )
        zeeTPDict["py"].append( zeePyArray )
        zeeTPDict["pz"].append( zeePzArray )
        zeeTPDict["T"].append( zeeTArray )
        zeeTPDict["deltaR"].append( zeeDeltaRArray )

    # ------------------- MC Truth Particle and Vertex -----------------------
    if isMC:
        eventInfoDict.update({"MC_truth": {}})

        mcTruthDict = {
            # particle
            "energy":       [],
            "pt":           [],
            "mass":         [],
            "eta":          [],
            "phi":          [],
            "pdgId":        [],
            # vertex
            "vertex_x":     [],
            "vertex_y":     [],
            "vertex_z":     [],
            "vertex_time":  [],
            "vertex_eta":   [],
            "vertex_phi":   [],
        }

        mcTruthDict["energy"].append( mc_partEnergyArray )
        mcTruthDict["pt"].append( mc_partPtArray )
        mcTruthDict["mass"].append( mc_partMassArray )
        mcTruthDict["eta"].append( mc_partEtaArray )
        mcTruthDict["phi"].append( mc_partPhiArray )
        mcTruthDict["pdgId"].append( mc_partPdgIdArray )
        mcTruthDict["vertex_x"].append( mc_vertXArray )
        mcTruthDict["vertex_y"].append( mc_vertYArray )
        mcTruthDict["vertex_z"].append( mc_vertZArray )
        mcTruthDict["vertex_time"].append( mc_vertTimeArray )
        mcTruthDict["vertex_eta"].append( mc_vertEtaArray )
        mcTruthDict["vertex_phi"].append( mc_vertPhiArray )

    # ----------------------- I Electron -------------------------------------
    
    # print("Found an electron!")
    for elec in range(0, len(electronIndexArray)):
        electronDict    = {
        "el_{}".format(elec):{
            "index":      [],
            "pt":         [],
            "eta":        [],
            "phi":        [],
            "eoverp":     [],
            #shower shapes
            "f1":         [],
            "f3":         [],
            "eratio":     [],
            "weta1" :     [],
            "weta2" :     [],
            "fracs1":     [],
            "wtots1":     [],
            "e277"  :     [],
            "reta"  :     [],
            "rphi"  :     [],
            "deltae":     [],
            "rhad"  :     [],
            "rhad1" :     [],
            #vertex
            "vertexZ":    [],
            "vertexY":    [],
            "vertexX":    [],
            "{}".format(clusnamedict): {},
            }
        } # electron-end

        electronDict["el_{}".format(elec)]["index"].append(electronIndexArray[elec])
        electronDict["el_{}".format(elec)]["pt"].append(electronPtArray[elec])
        electronDict["el_{}".format(elec)]["eta"].append(electronEtaArray[elec])
        electronDict["el_{}".format(elec)]["phi"].append(electronPhiArray[elec])
        electronDict["el_{}".format(elec)]["eoverp"].append(electronEoverPArray[elec])
        electronDict["el_{}".format(elec)]["f1"].append(electronSSf1Array[elec])
        electronDict["el_{}".format(elec)]["f3"].append(electronSSf3Array[elec])
        electronDict["el_{}".format(elec)]["eratio"].append(electronSSeratioArray[elec])
        electronDict["el_{}".format(elec)]["weta1"].append(electronSSweta1Array[elec])
        electronDict["el_{}".format(elec)]["weta2"].append(electronSSweta2Array[elec])
        electronDict["el_{}".format(elec)]["fracs1"].append(electronSSfracs1Array[elec])
        electronDict["el_{}".format(elec)]["wtots1"].append(electronSSwtots1Array[elec])
        electronDict["el_{}".format(elec)]["e277"].append(electronSSe277Array[elec])
        electronDict["el_{}".format(elec)]["reta"].append(electronSSretaArray[elec])
        electronDict["el_{}".format(elec)]["rphi"].append(electronSSrphiArray[elec])
        electronDict["el_{}".format(elec)]["deltae"].append(electronSSdeltaeArray[elec])
        electronDict["el_{}".format(elec)]["rhad"].append(electronSSrhadArray[elec])
        electronDict["el_{}".format(elec)]["rhad1"].append(electronSSrhad1Array[elec])
        electronDict["el_{}".format(elec)]["vertexZ"].append(vertexZArray[elec])
        electronDict["el_{}".format(elec)]["vertexY"].append(vertexYArray[elec])
        electronDict["el_{}".format(elec)]["vertexX"].append(vertexXArray[elec])
        
    # --------------------------------------------------------------------------
    #                       TopoCluster or SuperCluster
    # --------------------------------------------------------------------------
    if clusterType == 'topocluster':

        # ----------------------- II Calo Objects (TopoCluster or SuperCluster) ----------------------
        # find array indexes for the cluster collection, from each electron.
        eIndexClus = np.where( electronIndex_clusterLvlArray == electronIndexArray[elec] )[0]

        # with the array indexes, find the the cluster indexes, linked to the selected electron
        # ( the cluster index is used to reach the cell level indexes. )
        # for cIndex in clusterIndexArray[eIndexClus]: # for each cluster
        for cl in eIndexClus:
    
            # layerIndex = np.where(clusterCellLayerArray[cIndexCell] == 2)[0] # get EMB2 cells
            clusterDict = {
                "cl_{}".format(clusterIndexArray[cl]):{
                    "index":        [],
                    "pt":           [],
                    "eta":          [],
                    "phi":          [],
                    "time":         [],
                    # cells
                    "cells":         {}, # structure
                    "channels":      {}, # structure
                    "rawChannels":   {} # structure
                }
            }
            if isMC:
                clusterDict["cl_{}".format(clusterIndexArray[cl])].update({"hits": {}})

            # fill clusters
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["index"].append(clusterIndexArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["pt"].append(clusterPtArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["eta"].append(clusterEtaArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["phi"].append(clusterPhiArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["time"].append(clusterTimeArray[cl])

            # ----------------------- III CaloCell and Channels ------------------
            # get cells, channels and rawChannels linked to each cluster
            clusIndexCell   = np.where( clusterIndexCellLvlArray    == clusterIndexArray[cl] )[0]
            clusIndexCh     = np.where( clusterIndexChLvlArray      == clusterIndexArray[cl] )[0]
            clusIndexRawCh  = np.where( clusterIndexRawChLvlArray   == clusterIndexArray[cl] )[0]
            if isMC:
                clusIndexHits   = np.where( hits_clusterIndexChLvlArray == clusterIndexArray[cl] )[0]
            # print("clusterCellIndexArray:", clusterCellIndexArray)
            # print("clusterRawChannelIndexArray",clusterRawChannelIndexArray)

            # -- Cells --
            # OBS.: it was chosen to remove most of the information in this part of dict (cell level), because
            # it is more convenient to work with the xtalk and cell data in the same level, in this case, in channel.
            # 28 Sept, it was added again to perform some tests.
            cellDict    = {
                "index":        [],
                "caloRegion":   [],
                "sampling":     [],
                "energy":       [],
                "time":         [],
                "eta":          [],
                "phi":          [],
                "deta":         [],
                "dphi":         [],
                "gain":         []
            }
            cellRegionString    = getRegionString( clusterCellRegionArray[clusIndexCell] , dictCaloLayer["Region"])
            cellSamplingString  = getSamplingString( clusterCellLayerArray[clusIndexCell] , dictCaloLayer["Layer"] )
            cellCaloGainString  = getCaloGainString( clusterCellCaloGainArray[clusIndexCell] , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

            cellDict["index"] =  clusterCellIndexArray[clusIndexCell].tolist()
            cellDict["sampling"]   =  cellSamplingString
            cellDict["caloRegion"]   =  cellRegionString
            cellDict["energy"]   =  clusterCellEnergyArray[clusIndexCell].tolist()
            cellDict["time"]   =  clusterCellTimeArray[clusIndexCell].tolist()
            cellDict["eta"]   =  clusterCellEtaArray[clusIndexCell].tolist()
            cellDict["phi"]   =  clusterCellPhiArray[clusIndexCell].tolist()
            cellDict["deta"]   =  clusterCellDEtaArray[clusIndexCell].tolist()
            cellDict["dphi"]   =  clusterCellDPhiArray[clusIndexCell].tolist()
            cellDict["gain"]   =  cellCaloGainString

            # print(clusterCellCaloGainArray[clusIndexCell], cellDict["gain"] )
            # print("debug el_{}, cl_{} clusterCellIndexArray[clusIndexCell]: ".format(elec,cl),clusterCellIndexArray[clusIndexCell])
            # print("debug el_{}, cl_{} clusterCellCaloGainArray[clusIndexCell]: ".format(elec,cl), clusterCellCaloGainArray[clusIndexCell] )
            # print("debug el_{} cl_{} clusterCellDEtaArray[clusIndexCell]: ".format(elec,cl),clusterCellDEtaArray[clusIndexCell])

            # -- Channels --
            channelDict     = {
                "index":            [],
                "caloRegion":       [], # from cell level
                "sampling":         [], # from cell level
                "energy":           [],
                "time":             [],
                "digits":           [],
                "eta":              [], # from cell level
                "phi":              [], # from cell level
                "gain":             [], # from cell level
                "deta":             [], # from cell level
                "dphi":             [], # from cell level
                "chInfo":           [], # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                "hashId":           [],
                "channelId":        [],
                "effSigma":         [],
                "noise":            [],
                # "DSPThreshold":     [],
                # "OfflHVScale":      [],
                # "OfflEneRescaler":  []
                "OFCTimeOffset":    [],
                # "Shape":            [],
                # "ShapeDer":         [],
                "ADC2MeV0":         [],
                "ADC2MeV1":         [],
                "LArDB_Pedestal":   [],
                "OFCa":             [],
                "OFCb":             [],                
                "MinBiasAvg":       [],                
            }
            if not(isMC):
                channelDict.update({"DSPThreshold":     []})
                channelDict.update({"OfflHVScale":      []})
                channelDict.update({"OfflEneRescaler":  []})
                channelDict.update({"Shape":            []})
                channelDict.update({"ShapeDer":         []})
                
                channelDict["DSPThreshold"]   = clusterChannelDSPThrArray[clusIndexCh].tolist()
                channelDict["Shape"]   = clusterChannelShapeArray[clusIndexCh].tolist()
                channelDict["ShapeDer"]   = clusterChannelShapeDerArray[clusIndexCh].tolist()
                channelDict["OfflHVScale"]   = clusterChannelOfflHVScaleArray[clusIndexCh].tolist()
                channelDict["OfflEneRescaler"]   = clusterChannelOfflEneRescArray[clusIndexCh].tolist()

            if dataHasBadCh:
                channelDict.update({"badCh": []})
                channelDict["badCh"]   = clusterChannelBadArray[clusIndexCh].tolist()

            roundedChIndex  = [round(x,1) for x in clusterChannelIndexArray[clusIndexCh]]
        
            channelDict["index"]    =  roundedChIndex
            channelDict["energy"]   = clusterChannelEnergyArray[clusIndexCh].tolist()
            channelDict["time"]     = clusterChannelTimeArray[clusIndexCh].tolist()
            channelDict["digits"]    = clusterChannelDigitsArray[clusIndexCh].tolist()
            channelDict["chInfo"]   = clusterChannelChInfoArray[clusIndexCh].tolist()
            channelDict["hashId"]   = clusterChannelHashArray[clusIndexCh].tolist()
            channelDict["channelId"]   = clusterChannelIdArray[clusIndexCh].tolist()
            channelDict["effSigma"]   = clusterChannelEffSigmaArray[clusIndexCh].tolist()
            channelDict["noise"]   = clusterChannelNoiseArray[clusIndexCh].tolist()
            channelDict["OFCTimeOffset"]   = clusterChannelOFCTimeOffsetArray[clusIndexCh].tolist()
            channelDict["ADC2MeV0"]   = clusterChannelADC2MeV0Array[clusIndexCh].tolist()
            channelDict["ADC2MeV1"]   = clusterChannelADC2MeV1Array[clusIndexCh].tolist()
            channelDict["LArDB_Pedestal"]   = clusterChannelPedArray[clusIndexCh].tolist()
            channelDict["OFCa"]   = clusterChannelOFCaArray[clusIndexCh].tolist()
            channelDict["OFCb"]   = clusterChannelOFCbArray[clusIndexCh].tolist()
            channelDict["MinBiasAvg"]   = clusterChannelMinBiasAvgArray[clusIndexCh].tolist()
            

            # Data decompression
            channelRegionLink       = indexConversion( channelDict["index"] , cellDict["index"], clusterCellRegionArray[clusIndexCell])
            # channelSamplingLink     = indexConversion( channelDict["index"] , cellDict["index"], clusterCellLayerArray[clusIndexCell] )
            channelSamplingLink     = clusterChannelLayerArray[clusIndexCh].tolist()
            channelGainLink         = indexConversion( channelDict["index"] , cellDict["index"], clusterCellCaloGainArray[clusIndexCell] )
            channelEtaLink          = indexConversion( channelDict["index"] , cellDict["index"], clusterCellEtaArray[clusIndexCell] )
            channelPhiLink          = indexConversion( channelDict["index"] , cellDict["index"], clusterCellPhiArray[clusIndexCell] )
            channelDEtaLink         = indexConversion( channelDict["index"] , cellDict["index"], clusterCellDEtaArray[clusIndexCell] )
            channelDPhiLink         = indexConversion( channelDict["index"] , cellDict["index"], clusterCellDPhiArray[clusIndexCell] )

            channelRegionString     = getRegionString( channelRegionLink , dictCaloLayer["Region"] )
            channelSamplingString   = getSamplingString( channelSamplingLink , dictCaloLayer["Layer"])
            channelCaloGainString   = getCaloGainString( channelGainLink , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

            channelDict["caloRegion"]   =  channelRegionString
            channelDict["sampling"]   =  channelSamplingString
            channelDict["gain"]   =  channelCaloGainString
            channelDict["eta"] = channelEtaLink.tolist()
            channelDict["phi"] = channelPhiLink.tolist()
            channelDict["deta"]   = channelDEtaLink.tolist()
            channelDict["dphi"]   = channelDPhiLink.tolist()
        
            # -- RawChannels --
            rawChannelDict  = {
                "index":            [],
                "eta":              [],
                "phi":              [],
                "deta":             [],
                "dphi":             [],
                "layer":            [],
                "caloRegion":       [],
                "sampling":         [],
                "amplitude":        [],
                "time":             [],
                "Tile_Pedestal":    [],
                "LAr_Provenance":   [],
                "quality":          [],
                "chInfo":           [], # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                "channelId":        [],
                "DSPThreshold":     []
            }
            # must be treated because these indexes has floating point to represent tile PMT1 (+0.1) and PMT2 (+0.2), but when loading them, it is not precise (small bug to be fixed)
            roundedRawChIndex = [round(x,1) for x in clusterRawChannelIndexArray[clusIndexRawCh]]

            rawChannelDict["index"]   =  roundedRawChIndex
            rawChannelDict["amplitude"]   = clusterRawChannelAmplitudeArray[clusIndexRawCh].tolist()
            rawChannelDict["time"]   = clusterRawChannelTimeArray[clusIndexRawCh].tolist()
            rawChannelDict["Tile_Pedestal"]   = clusterRawChannelPedArray[clusIndexRawCh].tolist()
            rawChannelDict["LAr_Provenance"]   = clusterRawChannelProvArray[clusIndexRawCh].tolist()
            rawChannelDict["quality"]   = clusterRawChannelQualArray[clusIndexRawCh].tolist()
            rawChannelDict["chInfo"]   = clusterRawChannelChInfoArray[clusIndexRawCh].tolist()
            rawChannelDict["channelId"]   = clusterRawChannelIdArray[clusIndexRawCh].tolist()
            if not(isMC):
                rawChannelDict["DSPThreshold"]   = clusterRawChannelDSPThrArray[clusIndexRawCh].tolist()
            
            #  Index conversion: it works like data decompression from root files, that were indexed to use less
            # disk when dumped from ESD format. Now, information that were saved for only one data level (cell, channel, etc),
            # can be propagated to the others to built a dataset which is easier to handle, independently of each data level.
            rawChannelRegionLink        = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellRegionArray[clusIndexCell])
            # rawChannelSamplingLink      = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellLayerArray[clusIndexCell] )
            rawChannelSamplingLink      = clusterRawChannelLayerArray[clusIndexRawCh].tolist()
            rawChannelEtaLink           = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellEtaArray[clusIndexCell])
            rawChannelDEtaLink          = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellDEtaArray[clusIndexCell])
            rawChannelPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellPhiArray[clusIndexCell])
            rawChannelDPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellDPhiArray[clusIndexCell])
            rawChannelRegionString      = getRegionString( rawChannelRegionLink , dictCaloLayer["Region"] )
            rawChannelSamplingString    = getSamplingString( rawChannelSamplingLink , dictCaloLayer["Layer"])            

            rawChannelDict["caloRegion"]   =  rawChannelRegionString
            rawChannelDict["sampling"]   =  rawChannelSamplingString
            rawChannelDict["eta"]   =  rawChannelEtaLink.tolist()
            rawChannelDict["phi"]   =  rawChannelPhiLink.tolist()
            rawChannelDict["deta"]   =  rawChannelDEtaLink.tolist()
            rawChannelDict["dphi"]   =  rawChannelDPhiLink.tolist()

            # -- Hits (MC "Truth") --
            if isMC:
                hitsDict    = {
                    "index":        [],
                    "sampling":     [],
                    "energy":       [],
                    "time":         [],
                    "eta":          [],
                    "phi":          [],
                }

                roundedHitsChIndex  = [round(x,1) for x in hits_clusterChannelIndexArray[clusIndexHits]]

                hitsDict["index"]   =  roundedHitsChIndex
                hitsDict["sampling"]   =  hits_layerArray[clusIndexHits]
                hitsDict["energy"]   =  hits_energyArray[clusIndexHits]
                hitsDict["time"]   =  hits_timeArray[clusIndexHits]
                hitsDict["eta"]   =  hits_cellEtaArray[clusIndexHits]
                hitsDict["phi"]   =  hits_cellPhiArray[clusIndexHits]

            # ----------------------- III-end ------------------------------------
            # fill clusters with cells, channels and raw channels
            clusName    = str("cl_{}".format(clusterIndexArray[cl]))
            clusterDict[clusName]["cells"].update( cellDict )
            clusterDict[clusName]["channels"].update( channelDict )
            clusterDict[clusName]["rawChannels"].update( rawChannelDict )
            if isMC:
                clusterDict[clusName]["hits"].update( hitsDict )

            # fill electron with cluster(s)
            electronDict["el_{}".format(elec)]["{}".format(clusnamedict)].update( clusterDict )

        # --------------------------------------------------------------------------
        #                       ROI 7_11 EMB2 Reference
        # --------------------------------------------------------------------------
        if clusterType=='7x11':
            
            # ----------------------- II-1 Calo Objects (711 ROI) ----------------------
            # find array indexes for the cluster collection, from each electron.
            # eIndexClus = np.where( electronIndex_clusterLvlArray == electronIndexArray[elec] )[0]

            # with the array indexes, find the the cluster indexes, linked to the selected electron
            # ( the cluster index is used to reach the cell level indexes. )
            # for cIndex in clusterIndexArray[eIndexClus]: # for each cluster
            for cl in eIndexClus:
        
                # layerIndex = np.where(clusterCellLayerArray[cIndexCell] == 2)[0] # get EMB2 cells
                clusterDict = {
                    "cl_{}".format(cluster711IndexArray[cl]):{
                        "index":        [],
                        "pt":           [],
                        "eta":          [],
                        "phi":          [],
                        # cells
                        "cells":         {}, # structure
                        "channels":      {}, # structure
                        "rawChannels":   {} # structure
                    }
                }
                # fill clusters
                clusterDict["cl_{}".format(cluster711IndexArray[cl])]["index"].append(cluster711IndexArray[cl])
                clusterDict["cl_{}".format(cluster711IndexArray[cl])]["pt"].append (cluster711PtArray[cl])
                clusterDict["cl_{}".format(cluster711IndexArray[cl])]["eta"].append(cluster711EtaArray[cl])
                clusterDict["cl_{}".format(cluster711IndexArray[cl])]["phi"].append(cluster711PhiArray[cl])

                # ----------------------- III-1 CaloCell and Channels (711) ------------------
                # get cells, channels and rawChannels linked to each cluster
                clusIndexCell   = np.where( cluster711IndexCellLvlArray    == cluster711IndexArray[cl] )[0]
                clusIndexCh     = np.where( cluster711IndexChLvlArray      == cluster711IndexArray[cl] )[0]
                clusIndexRawCh  = np.where( cluster711IndexRawChLvlArray   == cluster711IndexArray[cl] )[0]
                # print("clusterCellIndexArray:", clusterCellIndexArray)
                # print("clusterRawChannelIndexArray",clusterRawChannelIndexArray)

                # -- Cells --
                # OBS.: it was chosen to remove most of the information in this part of dict (cell level), because
                # it is more convenient to work with the xtalk and cell data in the same level, in this case, in channel.
                # 28 Sept, it was added again to perform some tests.
                cellDict    = {
                    "index":        [],
                    "caloRegion":   [],
                    "sampling":     [],
                    "eta":          [],
                    "phi":          [],
                    "deta":         [],
                    "dphi":         [],
                    "gain":         []
                }
                cellRegionString    = getRegionString   ( cluster711CellRegionArray[clusIndexCell] , dictCaloLayer["Region"])
                cellSamplingString  = getSamplingString ( cluster711CellLayerArray[clusIndexCell] , dictCaloLayer["Layer"] )
                cellCaloGainString  = getCaloGainString ( cluster711CellCaloGainArray[clusIndexCell] , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

                cellDict["index"]   =  cluster711CellIndexArray[clusIndexCell].tolist() 
                cellDict["sampling"]    =  cellSamplingString 
                cellDict["caloRegion"]  =  cellRegionString 
                cellDict["eta"] =  cluster711CellEtaArray[clusIndexCell].tolist() 
                cellDict["phi"] =  cluster711CellPhiArray[clusIndexCell].tolist() 
                cellDict["deta"]    =  cluster711CellDEtaArray[clusIndexCell].tolist() 
                cellDict["dphi"]    =  cluster711CellDPhiArray[clusIndexCell].tolist() 
                # cellDict["gai =  clusterCellCaloGainArray[clusIndexCell] 
                cellDict["gain"]    =  cellCaloGainString 

                # print(clusterCellCaloGainArray[clusIndexCell], cellDict["gain"] )

                # -- Channels --
                channelDict     = {
                    "index":            [],
                    "caloRegion":       [], # from cell level
                    "sampling":         [], # from cell level
                    "energy":           [],
                    "time":             [],
                    "digits":           [],
                    "eta":              [], # from cell level
                    "phi":              [], # from cell level
                    "gain":             [], # from cell level
                    "deta":             [], # from cell level
                    "dphi":             [], # from cell level
                    "chInfo":           [],  # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                    "hashId":           [],
                    "channelId":        [],
                    "effSigma":         [],
                    "noise":            [],
                    "DSPThreshold":     [],
                    "OFCTimeOffset":    [],
                    "ADC2MeV0":         [],
                    "ADC2MeV1":         [],
                    "LArDB_Pedestal":   [],
                    "OFCa":             [],
                    "OFCb":             [],
                    "Shape":            [],
                    "ShapeDer":         [],
                    "MinBiasAvg":       [],
                    "OfflHVScale":      [],
                    "OfflEneRescaler":  []
                }
                if dataHasBadCh:
                    channelDict.update({"badCh": []})
                    # channelDict["badCh"].append(cluster711ChannelBadArray[clusIndexCh].tolist())
                roundedChIndex  = [round(x,1) for x in cluster711ChannelIndexArray[clusIndexCh]]

                # print(cluster711ChannelEnergyArray,cluster711ChannelChInfoArray)
            
                channelDict["index"] =  roundedChIndex
                channelDict["energy"] = cluster711ChannelEnergyArray[clusIndexCh].tolist()
                channelDict["time"] = cluster711ChannelTimeArray[clusIndexCh].tolist()
                channelDict["digits"] = cluster711ChannelDigitsArray[clusIndexCh].tolist()
                channelDict["chInfo"] = cluster711ChannelChInfoArray[clusIndexCh].tolist()
                channelDict["hashId"] = cluster711ChannelHashArray[clusIndexCh].tolist()
                channelDict["channelId"] = cluster711ChannelIdArray[clusIndexCh].tolist()
                channelDict["effSigma"] = cluster711ChannelEffSigmaArray[clusIndexCh].tolist()
                channelDict["noise"] = cluster711ChannelNoiseArray[clusIndexCh].tolist()
                channelDict["DSPThreshold"] = cluster711ChannelDSPThrArray[clusIndexCh].tolist()
                channelDict["OFCTimeOffset"] = cluster711ChannelOFCTimeOffsetArray[clusIndexCh].tolist()
                channelDict["ADC2MeV0"] = cluster711ChannelADC2MeV0Array[clusIndexCh].tolist()
                channelDict["ADC2MeV1"] = cluster711ChannelADC2MeV1Array[clusIndexCh].tolist()
                channelDict["LArDB_Pedestal"] = cluster711ChannelPedArray[clusIndexCh].tolist()
                channelDict["OFCa"] = cluster711ChannelOFCaArray[clusIndexCh].tolist()
                channelDict["OFCb"] = cluster711ChannelOFCbArray[clusIndexCh].tolist()
                channelDict["Shape"] = cluster711ChannelShapeArray[clusIndexCh].tolist()
                channelDict["ShapeDer"] = cluster711ChannelShapeDerArray[clusIndexCh].tolist()
                channelDict["MinBiasAvg"] = cluster711ChannelMinBiasAvgArray[clusIndexCh].tolist()
                channelDict["OfflHVScale"] = cluster711ChannelOfflHVScaleArray[clusIndexCh].tolist()
                channelDict["OfflEneRescaler"] = cluster711ChannelOfflEneRescArray[clusIndexCh].tolist()

                # Data decompression
                channelRegionLink       = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellRegionArray[clusIndexCell])
                channelSamplingLink     = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellLayerArray[clusIndexCell] )
                channelGainLink         = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellCaloGainArray[clusIndexCell] )
                channelEtaLink          = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellEtaArray[clusIndexCell] )
                channelPhiLink          = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellPhiArray[clusIndexCell] )
                channelDEtaLink         = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellDEtaArray[clusIndexCell] )
                channelDPhiLink         = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellDPhiArray[clusIndexCell] )

                channelRegionString     = getRegionString( channelRegionLink , dictCaloLayer["Region"] )
                channelSamplingString   = getSamplingString( channelSamplingLink , dictCaloLayer["Layer"])
                channelCaloGainString   = getCaloGainString( channelGainLink , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

                channelDict["caloRegion"]   =channelRegionString
                channelDict["sampling"] =  channelSamplingString
                channelDict["gain"] =      channelCaloGainString  #( cellCaloGainString ) ## is that organized with indexes sequences for channel?
                channelDict["eta"]  =      channelEtaLink.tolist()
                channelDict["phi"]  =      channelPhiLink.tolist()
                channelDict["deta"] =      channelDEtaLink.tolist()
                channelDict["dphi"] =      channelDPhiLink.tolist()
                
                # # cellDict["gain"].append( clusterCellCaloGainArray[clusIndexCell] )
            
                # -- RawChannels --
                rawChannelDict  = {
                    "index":            [],
                    "eta":              [],
                    "phi":              [],
                    "deta":             [],
                    "dphi":             [],
                    "caloRegion":       [],
                    "sampling":         [],
                    "amplitude":        [],
                    "time":             [],
                    "Tile_Pedestal":    [],
                    "LAr_Provenance":   [],
                    "quality":          [],
                    "chInfo":           [], # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                    "channelId":        [],
                    "DSPThreshold":     []
                }
                # must be treated because these indexes has floating point to represent tile PMT1 (+0.1) and PMT2 (+0.2), but when loading them, it is not precise (small bug to be fixed)
                roundedRawChIndex = [round(x,1) for x in cluster711RawChannelIndexArray[clusIndexRawCh]]

                rawChannelDict["index"] =          roundedRawChIndex 
                rawChannelDict["amplitude"] =      cluster711RawChannelAmplitudeArray[clusIndexRawCh].tolist()
                rawChannelDict["time"] =           cluster711RawChannelTimeArray[clusIndexRawCh].tolist()
                rawChannelDict["Tile_Pedestal"] =  cluster711RawChannelPedArray[clusIndexRawCh].tolist()
                rawChannelDict["LAr_Provenance"] = cluster711RawChannelProvArray[clusIndexRawCh].tolist()
                rawChannelDict["quality"] =        cluster711RawChannelQualArray[clusIndexRawCh].tolist()
                rawChannelDict["chInfo"] =         cluster711RawChannelChInfoArray[clusIndexRawCh].tolist()
                rawChannelDict["channelId"] =      cluster711RawChannelIdArray[clusIndexRawCh].tolist()
                rawChannelDict["DSPThreshold"] =   cluster711RawChannelDSPThrArray[clusIndexRawCh].tolist()


                #  Index conversion: it works like data decompression from root files, that were indexed to use less
                # disk when dumped from ESD format. Now, information that were saved for only one data level (cell, channel, etc),
                # can be propagated to the others to built a dataset which is easier to handle, independently of each data level.
                # Here, the cluster711Cell variables in cell level, will be converted to their respective cells in the rawChannel level.
                rawChannelRegionLink        = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellRegionArray[clusIndexCell])
                rawChannelSamplingLink      = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellLayerArray[clusIndexCell] )
                rawChannelEtaLink           = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellEtaArray[clusIndexCell])
                rawChannelDEtaLink          = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellDEtaArray[clusIndexCell])
                rawChannelPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellPhiArray[clusIndexCell])
                rawChannelDPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"] , cluster711CellDPhiArray[clusIndexCell])

                rawChannelRegionString      = getRegionString( rawChannelRegionLink , dictCaloLayer["Region"] )
                rawChannelSamplingString    = getSamplingString( rawChannelSamplingLink , dictCaloLayer["Layer"])            

                rawChannelDict["caloRegion"] =  rawChannelRegionString 
                rawChannelDict["sampling"] =  rawChannelSamplingString 
                rawChannelDict["eta"] =  rawChannelEtaLink.tolist() 
                rawChannelDict["phi"] =  rawChannelPhiLink.tolist() 
                rawChannelDict["deta"] =  rawChannelDEtaLink.tolist() 
                rawChannelDict["dphi"] =  rawChannelDPhiLink.tolist() 

                # print(np.shape(rawChannelDict["phi"]), rawChannelDict["phi"])
                # print(np.shape(cellDict["eta"]), cellDict["eta"] )

                # df = pd.concat({k: pd.DataFrame.from_dict(v, orient='index') for k, v in rawChannelDict.items()}, axis=1)
                # print(df)
                # print(pd.concat({k: pd.DataFrame.from_dict(v) for k, v in rawChannelDict.items()}))

                # ----------------------- III-end ------------------------------------
                # fill clusters with cells, channels and raw channels
                clusName    = str("cl_{}".format(cluster711IndexArray[cl]))
                clusterDict[clusName]["cells"].update( cellDict )
                clusterDict[clusName]["channels"].update( channelDict )
                clusterDict[clusName]["rawChannels"].update( rawChannelDict )

                # saveAsJsonFile(clusterDict, 'file001.json')
                # lDatasetDict = loadJsonFile('file001.json')
                # print(lDatasetDict)

                # fill electron with cluster(s)
                electronDict["el_{}".format(elec)]["{}".format(clusnamedict)].update( clusterDict ) 

        # fill event with particles
        eventInfoDict["electrons"].update( electronDict )
        if isTagAndProbe:
            eventInfoDict["ZeeTP"].update( zeeTPDict)
        if isMC:
            eventInfoDict["MC_truth"].update( mcTruthDict )

    # fill dataset with events
    # datasetDict["dataset"].append(eventInfoDict)
    datasetDict["dataset"].update(eventInfoDict)
    
    return datasetDict
