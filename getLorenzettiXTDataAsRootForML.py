# Lorenzetti Showers Collaboration 2020 - 2023 - https://doi.org/10.1016/j.cpc.2023.108671

import sys
sys.path.insert(0,'/home/mhufnage/lorenzetti/Applications') #lorenzetti utils path

# import subprocess
# subprocess.run(['. /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.24.08/x86_64-centos7-gcc48-opt/bin/thisroot.sh'])

# from ROOT import TFile, gROOT
from pandas import DataFrame
# from lorenzetti_utils import dataframe_h
from time import time
from glob import glob
from pprint import pprint
import array
import numpy as np
import os 
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

from helper_lib.auxiliaryFunctions import *
from lorenzetti_reader import *


## Reading AOD data ##
bDoNoNoise      = False
dumpCells       = True
#AOD_Cap0.0_Ind0.4  AOD_Cap0.0_Ind1.1  AOD_Cap4.2_Ind2.3  AOD_noNoise_Cap0.0_Ind0.4  AOD_noNoise_Cap0.0_Ind1.1  AOD_noNoise_Cap4.2_Ind2.3
# xtInd           = 2.3
# xtCap           = 4.2
xtInds           = [0.4, 1.1]
winSizes        = [3, 5, 7]

for winSize in winSizes:
    print('Window size: {}'.format(winSize))
    for xtInd in xtInds:
        xtCap           = 0.0
        
        conditions = {
            'nEvents':         -1,
            'SigmaCut':        1,
            'etaCut':          1.4,
            'caloSampling':    CaloSampling.EMB2,
            'etaSize':         winSize,
            'phiSize':         winSize,
            'nSamples':        5,
            'dumpCells':       True,
            'bDumpWindowOnly': True,
            'bNoEneCut':       True,
            'XTalkLevel':      'L = {:.1f}%, C = {:.1f}%'.format(xtInd, xtCap),
            
            # Not implemented yet!
            'etBin':           [[15 , 20] ,[20, 30]  ,[30, 40]  ,[40, 50],[50,100],[100,10000000]],
            'etaBin':          [[0.00, 0.80], [0.80, 1.37], [1.37,1.54],[1.54,2.50],[2.50,3.2]],
            'bPhaseSpaceDiv':  False,
        }

        print(conditions)

        datasetPath         = '/data/atlas/mhufnage/lzt_single_e/'
        datasets_indexes    = np.arange(1,31)

        datasets = []
        if bDoNoNoise:
            for ds in datasets_indexes:
                datasets.append(datasetPath+'elec_singleCell_10kEvts_centroid_e50_DS{}/AOD_noNoise_Cap{}_Ind{}/electron.AOD.root'.format(ds, xtCap, xtInd))
        else:
            for ds in datasets_indexes:
                datasets.append(datasetPath+'elec_singleCell_10kEvts_centroid_e50_DS{}/AOD_Cap{}_Ind{}/electron.AOD.root'.format(ds, xtCap, xtInd))
                

        #phase space regions
        etBin  = [[15 , 20] ,[20, 30]  ,[30, 40]  ,[40, 50],[50,100],[100,10000000]]
        etaBin = [[0.00, 0.80], [0.80, 1.37], [1.37,1.54],[1.54,2.50],[2.50,3.2]]
        ## **************** ##

        sampText  = ['PSB', 'PSE', 'EM1', 'EM2', 'EM3']
        fileNames = []

        startTime = time()

        for dsindex in datasets_indexes:#range(1,DS_Lim):
            
            # Mount the ML dataset naming scheme
            file    = datasets[dsindex-1]
            path    = '/'.join(file.split('/')[:-1])+'/'
            
            if (dumpCells):   addName='.WithCells'
            else:             addName=''    
            if bDoNoNoise:
                fileName = path+'user.mhufnage.lzt.e50.xtalk.DS{}.win{}{}.nopileup{}_{}.noNoise.root'.format(dsindex,conditions['etaSize'],conditions['phiSize'],addName,sampText[conditions['caloSampling']])
            else:
                fileName = path+'user.mhufnage.lzt.e50.xtalk.DS{}.win{}{}.nopileup{}_{}.root'.format(dsindex,conditions['etaSize'],conditions['phiSize'],addName,sampText[conditions['caloSampling']])
                
            
            print('DS {}: '.format(dsindex),path)
            
            # Read TTree
            sTree   = TChain("physics",'')
            sTree.Add(file+"/physics") # main dumped data tree (event by event)
            print('Total input entries: {}'.format(sTree.GetEntries()))
            
            # Pre-processing, get window and saving into new (compact) root file.
            lorenzetti_file_filter_for_ML(sTree, conditions, rootFileName=fileName)    

            
            fileNames.append(fileName)
            print(' Dumped {} successfully'.format(fileName))
            
        endTime = time()
        print('Files saved: ',fileNames)
        print('Total time: {:.2f} min ({:.2f} s)'.format((endTime-startTime)/60 , endTime-startTime))

        # print(datasets)

