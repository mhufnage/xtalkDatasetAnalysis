import sys
import os

## Insert here the path to xtalkDatasetAnalysis git package
sys.path.insert(1, '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/')

import time
import numpy as np
import torch
import torch.nn as nn

from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score

import ML_lib.MLP as MLP

from helper_lib.test_helper import loadTestDataset_LorenzettiSupervised, getTestResutlsDict, loadTorchModelForTest

#   Script to apply trained models to unseen lorenzetti data.
#   Its organized to perform it in two loops: model type (hyperparameter1) and number of events (hyperparameter2)
#   The results are stored in a python dict, organized by hyperparameter2 and per cell.
#   There are 1 dict per hyperparameter1, saved with its name.
#
#       -> pending: generalize script for all hyperparameters
#   Must modify: 
#   - pathLoss
#   - trainNeuLists
#   - range_clusSize
#   - dictKeyName
#   Optional:
#   - modelSignalType
#   - optimConfig
#


if __name__ == '__main__':
    conditions = {
    'nEvents':         50000, # <--- it is the total events to feed test dataset.
    'SigmaCut':        1,
    'etaCut':          1.4,
    'caloSampling':    'EM2',#CaloSampling.EMB2,
    'etaSize':         3,
    'phiSize':         3,
    'nSamples':        5,
    'dumpCells':       True,
    'bDumpWindowOnly': True,
    'bNoEneCut':       True,
    'XTalk_ind':       1.1,
    'XTalk_cap':       0.0,
    'XTalkLevel':      'L = 1.1%, C = 0.0%',
}

    # Loop 1
    # trainNeuLists   = [[45,15,15], [45,100,100]]
    # range_clusSize  = [5000, 10000, 20000, 30000, 50000, 70000, 100000]#, 45000]# batch size in clusters
    
    # Loop 2
    # trainNeuLists   = [[45,10], [45,20],[45,45],[45,60],[45,75],[45,100],[45,120],[45,150],[45,200],[45,250]]
    # theSeries       = [10,20,45,60,75,100,120,150,200,250] # hyperparameter2
    # total_clusters  = [50000]# number of events fed into training+valid process
        
    # Loop 3
    trainNeuLists   = [[45,10,10], [45,20,20],[45,45,45],[45,60,60],[45,75,75],[45,100,100],[45,120,120],[45,150,150],[45,200,200],[45,250,250]]
    theSeries       = [10,20,45,60,75,100,120,150,200,250] # hyperparameter2
    list_total_clusters  = [50000]# number of events fed into training+valid process
    
    dictKeyName     = 'LayerLoop' # 'nEvents'
    max_iter        = 10
    kFolds          = 10

    # Load test dataset
    dataloaderTest, datasetTest = loadTestDataset_LorenzettiSupervised(conditions)

    for netIdx, trainNeuList in enumerate(trainNeuLists):
        range_neuInput  = trainNeuList#[45,20,20]
        range_neuText   = MLP.neuListToFileText(range_neuInput)

        # pathLoss        = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/_testBench/'
        # pathLoss        = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/loop_nEvents_33_new_10folds/model{}/'.format(range_neuText)
        # pathLoss        = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/loop_model_1Layer_33_10folds/{}/'.format(range_neuText)
        pathLoss        = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/loop_model_2layer_33_10folds/{}/'.format(range_neuText)
        
        modelSignalType = 'nnp'
        optimConfig     = 'adam'
        range_neuText   = MLP.neuListToFileText(range_neuInput)
        print(range_neuText)

        print('\tTest events: {}'.format(len(datasetTest)))
        for idx, total_clusters in enumerate(list_total_clusters):
            print('\t\tTrained model events: {}'.format( total_clusters))
            
            eneTestDict             = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            eneRMSETestDict         = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            eneMAETestDict          = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            eneR2TestDict           = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            eneTruthRMSETestDict    = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            eneTruthMAETestDict     = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            eneR2TruthTestDict      = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            timeTestDict            = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            timeRMSETestDict        = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            timeMAETestDict         = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            timeR2TestDict          = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            timeTruthRMSETestDict   = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            timeTruthMAETestDict    = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            timeR2TruthTestDict     = getTestResutlsDict(dictKeyName, max_iter, kFolds, bSmallerDict=True)
            # samplesTestDict = getTestResutlsDict('nEvents', max_iter, kFolds, bSmallerDict=True)

            # config_train = np.load(pathLoss+'config_train_nEvts{}_{}_{}_neu{}.npz'.format(total_clusters, modelSignalType, optimConfig, range_neuText), allow_pickle=True)['config'].tolist()
            config_train_filename = pathLoss+'config_train_nEvts{}_{}_{}_neu{}.npz'.format(total_clusters, modelSignalType, optimConfig, range_neuText)

            with np.load(config_train_filename, allow_pickle=True) as conf_file:

                config_train = conf_file['config'].tolist()

                trainRatio   = config_train['train_ratio']
                batch_size   = config_train['train_args']['batch_size']
                max_epochs   = config_train['train_args']['nEpochs']
                patience     = config_train['train_args']['patience']
                # max_iter     = 1#config_train['train_args']['nIter']
                # k            = 1#config_train['train_args']['kFolds']
                num_proc     = config_train['train_args']['num_processes']
                scalerInput  = config_train['train_args']['scalerInput']
                scalerOutput = config_train['train_args']['scalerOutput']
                
                clusters_per_fold_train = round((trainRatio*total_clusters)/kFolds)
                clusters_per_fold_val   = round(((1-trainRatio)*total_clusters)/kFolds)

                # for rep in range(max_rep):
                for it in range(max_iter):
                    for fold in range(kFolds):
                        # print('\t\t\t It {}, Fold {}'.format(it, fold))
                        
                        # Load the models
                        bestModelPath   = pathLoss+'bestModel_nEvts{}_{}_fold{}_iter{}_neu{}_batch{}.pth'.format(total_clusters, modelSignalType, fold, it, range_neuText, batch_size)
                        model           = loadTorchModelForTest(trainNeuList, bestModelPath, config_train['train_args'], modelType='mlp' )
                        
                        # for name, param in model.named_parameters():
                        #     if param.requires_grad:
                        #         print(name, param.data)
                        
                        for idx, (data_input, target, batch_idx) in enumerate(dataloaderTest):
                            # pass input through the model and generate the output
                            output                  = model(data_input*scalerInput) #normalized input
                            outputScaled            = output*scalerOutput
                            ene_target, tau_target  = datasetTest.calibrate(batch_idx, target) # unormlized input
                            ene_output, tau_output  = datasetTest.calibrate(batch_idx, output*scalerOutput) # denormalized output
                            
                            # Fill dict with ene, time and samples
                            for cell in range(conditions['etaSize']*conditions['phiSize']):                                
                                eneTestDict[dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)].append(ene_output.detach().tolist()[cell])
                                timeTestDict[dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)].append(tau_output.detach().tolist()[cell])

                        for cell in range(conditions['etaSize']*conditions['phiSize']):
                            eneReco     = eneTestDict[dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)]
                            eneOFC      = datasetTest.e.detach().numpy()[:,cell].tolist()
                            eneTruth    = datasetTest.edep.detach().numpy()[:,cell].tolist()
                            
                            timeReco    = timeTestDict[dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)]
                            timeOFC     = datasetTest.t.detach().numpy()[:,cell].tolist()
                            timeTruth   = datasetTest.tof.detach().numpy()[:,cell].tolist()
                            
                            eneRMSETestDict     [dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)] = mean_squared_error(eneOFC, eneReco, squared=False)
                            eneMAETestDict      [dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)] = mean_absolute_error(eneOFC, eneReco)
                            eneR2TestDict       [dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)] = r2_score(eneOFC, eneReco)
                            eneTruthRMSETestDict[dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)] = mean_squared_error(eneTruth, eneReco, squared=False)
                            eneTruthMAETestDict [dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)] = mean_absolute_error(eneTruth, eneReco)
                            eneR2TruthTestDict  [dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)] = r2_score(eneTruth, eneReco)
                            
                            timeRMSETestDict     [dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)] = mean_squared_error(timeOFC, timeReco, squared=False)
                            timeMAETestDict      [dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)] = mean_absolute_error(timeOFC, timeReco)
                            timeR2TestDict       [dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)] = r2_score(timeOFC, timeReco)
                            timeTruthRMSETestDict[dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)] = mean_squared_error(timeTruth, timeReco, squared=False)
                            timeTruthMAETestDict [dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)] = mean_absolute_error(timeTruth, timeReco)
                            timeR2TruthTestDict  [dictKeyName]['it_{}'.format(it)]['fold_{}'.format(fold)]['cell_{}'.format(cell)] = r2_score(timeTruth, timeReco)
            
            outputPathAlias = '/'.join(pathLoss.split('/')[:-2])+'/metrics/'
            
            if not(os.path.isdir(outputPathAlias)):
                os.mkdir(outputPathAlias)

            np.savez(outputPathAlias+'{}_nEvents{}_eneTestDict.npz'.format(range_neuText, total_clusters) ,ene=eneTestDict)
            np.savez(outputPathAlias+'{}_nEvents{}_timeTestDict.npz'.format(range_neuText, total_clusters) ,time=timeTestDict)
            
            np.savez(outputPathAlias+'{}_nEvents{}_eneRMSE.npz'.format(range_neuText, total_clusters) ,ene=eneRMSETestDict)
            np.savez(outputPathAlias+'{}_nEvents{}_eneMAE.npz'.format(range_neuText, total_clusters) ,ene=eneMAETestDict)
            np.savez(outputPathAlias+'{}_nEvents{}_eneR2.npz'.format(range_neuText, total_clusters) ,ene=eneR2TestDict)
            np.savez(outputPathAlias+'{}_nEvents{}_eneRMSETruth.npz'.format(range_neuText, total_clusters) ,ene=eneTruthRMSETestDict)
            np.savez(outputPathAlias+'{}_nEvents{}_eneMAETruth.npz'.format(range_neuText, total_clusters) ,ene=eneTruthMAETestDict)
            np.savez(outputPathAlias+'{}_nEvents{}_eneR2Truth.npz'.format(range_neuText, total_clusters) ,ene=eneR2TruthTestDict)
            
            np.savez(outputPathAlias+'{}_nEvents{}_timeRMSE.npz'.format(range_neuText, total_clusters) ,time=timeRMSETestDict)
            np.savez(outputPathAlias+'{}_nEvents{}_timeMAE.npz'.format(range_neuText, total_clusters) ,time=timeMAETestDict)
            np.savez(outputPathAlias+'{}_nEvents{}_timeR2.npz'.format(range_neuText, total_clusters) ,time=timeR2TestDict)
            np.savez(outputPathAlias+'{}_nEvents{}_timeRMSETruth.npz'.format(range_neuText, total_clusters) ,time=timeTruthRMSETestDict)
            np.savez(outputPathAlias+'{}_nEvents{}_timeMAETruth.npz'.format(range_neuText, total_clusters) ,time=timeTruthMAETestDict)
            np.savez(outputPathAlias+'{}_nEvents{}_timeR2Truth.npz'.format(range_neuText, total_clusters) ,time=timeR2TruthTestDict)
            
            
            # np.savez('/'.join(pathLoss.split('/')[:-2])+'/'+'{}_samplesTestDict.npz'.format(range_neuText) ,samples=samplesTestDict)
            
            ## RMSE of OutputEnergy, OFC Energy , Truth Energy
            ## RMSE of OutputTime  , OFC Time   , Truth Time
            ## JS Divergence
