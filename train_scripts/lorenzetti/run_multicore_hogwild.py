# Lorenzetti Showers Collaboration 2020 - 2023 - https://doi.org/10.1016/j.cpc.2023.108671
print('Starting script for neuron/layers space search...')

import argparse
# parser = argparse.ArgumentParser()

# args = parser.parse_args()

import sys
sys.path.insert(0,'/home/mhufnage/lorenzetti/Applications') #lorenzetti utils path

## Insert here the path to xtalkDatasetAnalysis git package
sys.path.insert(1, '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/')

uprootLibrary = 'np'#'np' #'ak'

import json
import os
import time
import shelve

import numpy as np
from glob import glob
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as mcolors
import matplotlib
# matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS

import scipy.stats as scipy
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader, DistributedSampler, Subset
import torch.multiprocessing as mp

torch.multiprocessing.set_sharing_strategy('file_system')

from collections import OrderedDict
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score

import mplhep as hep

plt.style.use([hep.style.ATLAS])

import uproot

# from calibrationFilesHelper import *
# from Autoencoder import *
# from XTalkDatasetClass import XTalkDataset_LZT, Subset
# from auxiliaryFunctions import confInterval, getBins

# Custom lib
from helper_lib.calibrationFilesHelper import *
from helper_lib.auxiliaryFunctions import confInterval, getBins
from helper_lib.functionsHelper import stdVecToArray, stdVecToPythonArray
import ML_lib.MLP as MLP
from ML_lib.ML_custom_auxiliar import KFoldSampler
from ML_lib.Autoencoder import *
from ML_lib.XTalkDatasetClass import XTalkDataset_LZT_FromROOT#, XTalkDataset_LZT, XTalkStreamingDataLoader_LZT, XTalkStreamingDataLoader_LZT_FoldSplit, XTalkStreamingDataLoader_LZT_FoldSplit_Faster
from lorenzetti_reader import *

import ROOT
from ROOT import TFile, gROOT, TChain, TTree

from train_multicore import trainMLP, trainMLP_benchmark, trainMLP_ROOTdataset_hogwild#trainMLP_multicore, trainMLP_ROOTdataset_multicore



# Training settings
# pyenv;setupROOT;python3 /home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/train_scripts/lorenzetti/run_multicore_hogwild.py --mp --nIter 10 --nEpochs 500 --nEvents 5000 --num-processes 1 --kFolds 10 --path "/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/loop_nEvents_33_new_10folds/model45-100-100-45/" --neuronsList "45-100-100"
# pyenv;setupROOT;python3 /home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/train_scripts/lorenzetti/run_multicore_hogwild.py --mp --nIter 10 --nEpochs 500 --nEvents 10000 --num-processes 1 --kFolds 10 --path "/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/loop_nEvents_33_new_10folds/model45-100-100-45/" --neuronsList "45-100-100"
# pyenv;setupROOT;python3 /home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/train_scripts/lorenzetti/run_multicore_hogwild.py --mp --nIter 10 --nEpochs 500 --nEvents 20000 --num-processes 1 --kFolds 10 --path "/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/loop_nEvents_33_new_10folds/model45-100-100-45/" --neuronsList "45-100-100"
# pyenv;setupROOT;python3 /home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/train_scripts/lorenzetti/run_multicore_hogwild.py --mp --nIter 10 --nEpochs 500 --nEvents 30000 --num-processes 1 --kFolds 10 --path "/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/loop_nEvents_33_new_10folds/model45-100-100-45/" --neuronsList "45-100-100"
# pyenv;setupROOT;python3 /home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/train_scripts/lorenzetti/run_multicore_hogwild.py --mp --nIter 10 --nEpochs 500 --nEvents 500000 --num-processes 1 --kFolds 10 --path "/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/loop_nEvents_33_new_10folds/model45-100-100-45/" --neuronsList "45-100-100"
# pyenv;setupROOT;python3 /home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/train_scripts/lorenzetti/run_multicore_hogwild.py --mp --nIter 10 --nEpochs 500 --nEvents 70000 --num-processes 1 --kFolds 10 --path "/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/loop_nEvents_33_new_10folds/model45-100-100-45/" --neuronsList "45-100-100"
# pyenv;setupROOT;python3 /home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/train_scripts/lorenzetti/run_multicore_hogwild.py --mp --nIter 10 --nEpochs 500 --nEvents 100000 --num-processes 1 --kFolds 10 --path "/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/loop_nEvents_33_new_10folds/model45-100-100-45/" --neuronsList "45-100-100"

parser = argparse.ArgumentParser(description='PyTorch MLP pulseXT-pulse, pulseXT-pulse-OFCa,b')

parser.add_argument("--mp", help="Run training on multiprocess", action="store_true")
# parser.add_argument("--path", help="Path to store training data.", required=True, )
parser.add_argument("--path", help="optimization algorithm.", nargs='?', default='/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/_testBench/', const='alg')
parser.add_argument('--electronEnergy', type=float, default=50.0, metavar='ELENE',help='electron energy (default: 50.0 GeV)')
## optimization algorithm
parser.add_argument("--optimAlg", help="optimization algorithm.", nargs='?', default='adam', const='alg')
parser.add_argument('--lr', type=float, default=1e-3, metavar='LR',help='learning rate (default: 0.001)')
parser.add_argument('--weightDecay', type=float, default=1e-5, metavar='WDA',help='weight decay adam (default: 1e-5)')
# parser.add_argument('--momentum', type=float, default=0.5, metavar='M',help='SGD momentum (default: 0.5)')

## training in general
parser.add_argument('--neuronsList', nargs='?', default='45-15-15', const='alg', help='List of neurons of MLP per layer. Ex: 45-15-15.. use dash-separated values.')
parser.add_argument('--patience', type=int, default=25)
parser.add_argument('--min_delta_loss', type=int, default=1e-5)
parser.add_argument('--kFolds', type=int, default=10)
parser.add_argument('--nIter', type=int, default=10)
parser.add_argument('--useFullCellData', action='store_true', default=False,help='Load ROOT dataset with all available branches. (default: False)')
parser.add_argument('--batch-size', type=int, default=64, metavar='N', help='input batch size for training (default: 64)')
parser.add_argument('--nEpochs', type=int, default=10, metavar='N',help='number of epochs to train (default: 10)')
parser.add_argument('--nEvents', type=int, default=1000, metavar='N',help='number of events to train (default: 10)')
parser.add_argument('--scalerInput', type=float, default=1e-3, metavar='SI',help='MeV to GeV converter(default: 1/1000)')
parser.add_argument('--scalerOutput', type=float, default=1e3, metavar='SO',help='GeV to MeV converter(default: 1000)')

parser.add_argument('--seed', type=int, default=1, metavar='S',help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',help='how many batches to wait before logging training status')
parser.add_argument('--num-processes', type=int, default=1, metavar='N',help='how many training processes to use (default: 2)')
parser.add_argument('--num-threads', type=int, default=1, metavar='N',help='how many training processes to use (default: 2)')
parser.add_argument('--cuda', action='store_true', default=False,help='enables CUDA training')

# not implemented
parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',help='input batch size for testing (default: 1000)')
parser.add_argument('--mps', action='store_true', default=False,help='enables macOS GPU training')
parser.add_argument('--save_model', action='store_true', default=False,help='save the trained model to state_dict')
parser.add_argument('--dry-run', action='store_true', default=False,help='quickly check a single pass')


if __name__ == '__main__':
    
    args = parser.parse_args()
    
    args_dict = vars(args)

    #
    # Device management
    #
    use_cuda = args.cuda and torch.cuda.is_available()
    use_mps = args.mps and torch.backends.mps.is_available()
    if use_cuda:
        device = torch.device("cuda")        
        args_dict.update({'num_workers': 1, 'pin_memory': True})
    elif use_mps:
        device = torch.device("mps")
    else:
        device = torch.device("cpu")
    
    # pathLoss      = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/{}/'.format(args.path)
    # pathLoss      = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/_evaluateSequential/'
    
    conditions = {
        'nEvents':         -1, # <--- it is the total events to feed KFold_split into (train+valid)+(test).
        'SigmaCut':        1,
        'etaCut':          1.4,
        'caloSampling':    'EM2',#CaloSampling.EMB2,
        'etaSize':         3,
        'phiSize':         3,
        'nSamples':        5,
        'dumpCells':       True,
        'bDumpWindowOnly': True,
        'bNoEneCut':       True,
        'XTalk_ind':       1.1,
        'XTalk_cap':       0.0,
        'XTalkLevel':      'L = 1.1%, C = 0.0%',
        
        # Not implemented yet!
        'etBin':           [[15 , 20] ,[20, 30]  ,[30, 40]  ,[40, 50],[50,100],[100,10000000]],
        'etaBin':          [[0.00, 0.80], [0.80, 1.37], [1.37,1.54],[1.54,2.50],[2.50,3.2]],
        'bPhaseSpaceDiv':  False,
    }

    if not(os.path.isdir(args.path)):
        os.mkdir(args.path)

    startTime=time()
    # Dataset
    data = TChain('ML_tree')
    for ds_index in range(1,30):
        theFile = '/data/atlas/mhufnage/lzt_single_e/elec_singleCell_10kEvts_centroid_e50_DS{}/AOD_Cap{:.1f}_Ind{:.1f}/user.mhufnage.lzt.e50.xtalk.DS{}.win{}{}.nopileup.WithCells_{}.root'.format(ds_index, conditions['XTalk_cap'], conditions['XTalk_ind'], ds_index, conditions['etaSize'], conditions['phiSize'], conditions['caloSampling'])
        data.Add(theFile+'/ML_tree')

    #
    # Configuration
    #
    conditions['nEvents'] = args.nEvents
    lossMethod = 'p'    
    
    
    np.savez(args.path+'{}_conditions.npz'.format(args.path.split('/')[-2]),config=conditions)

    print('Computation method: {}'.format(lossMethod))
    
    # for trainNeuList in trainNeuLists:
    # range_neuInput          = trainNeuList#[45,20,20]
    range_neuInput      = [int(value) for value in args.neuronsList.split('-')] #{'neuLay': [[45,15,15]],} # 2layer (24/05/2024)
    range_neuText       = MLP.neuListToFileText(range_neuInput)
    print(range_neuText)

    experiment_alias        = '''
    learning the samples of a 3x3 window from topocluster at EMB2 layer. 
    The loss for training is the input-output map. 
    The cell organization in the window is eta-oriented.
    # phi [ 3 | 6 | 9 ]
    #     [ 2 | 5 | 8 ]  , eta-orientation cluster indexing. where 5 is the index of the hottest cell
    #     [ 1 | 4 | 7 ]
    #             eta
    '''
    
    totalEvents             = data.GetEntries()#len(data)
    lossComputation         = lossMethod
    train_ratio             = 1 # proportion of data to apply the kFold
    test_ratio              = 0 # proportion of data to apply test on trained model
    eventsArray             = torch.as_tensor(np.arange(0, args.nEvents)) # contiguous indexesfor KFold split

    train_config_dict   = {
            'experiment_alias'      : experiment_alias,
            'conditions'            : conditions,
            'total_events_dataset'  : totalEvents,
            'train_args'            : args_dict, # argument parser dict
            'train_ratio'           : train_ratio,
            'test_ratio'            : test_ratio,
            'lossComputation'       : lossMethod,
            'path_loss'             : args.path,
    }

    # np.savez(pathLoss+'config_train_nn{}_{}_neu{}.npz'.format(lossComputation, optimAlg, range_neuText), config=train_config_dict)
    np.savez(args.path+'config_train_nEvts{}_nn{}_{}_neu{}.npz'.format(args.nEvents, lossMethod, args.optimAlg, range_neuText), config=train_config_dict)

    # X_train, X_test = train_test_split( data[0:nClusters] , test_size=test_ratio)
    datasetTrain = XTalkDataset_LZT_FromROOT(data, conditions, mode='supervised', load_full_data=args.useFullCellData)
    # datasetTrainValid = XTalkDataset_LZT_FromROOT(data, conditions, mode='supervised', load_full_data=bLoadFullCellData)
    
    # kwargs = {}
    #
    # Dataset split
    #
    
    kfold = KFold(n_splits=args.kFolds, shuffle=True, random_state=1)
    
    for nFold, (train_fold_idx, valid_fold_idx) in enumerate(kfold.split(eventsArray)):
        
        train_fold_sampler  = KFoldSampler(train_fold_idx)
        val_fold_sampler    = KFoldSampler(valid_fold_idx)
        
        for it in range(0,args.nIter):
            print('\t\tnEvents={}, Fold {}/{}... iter {}/{}'.format(args.nEvents, nFold+1,args.kFolds, it+1, args.nIter))
            
            train_logging_dict  = {
                    'loss'              : [],
                    'val_loss'          : [],
                    'epoch'             : [],                    
                    'loss_history'      : [],
                    'val_loss_history'  : [],

                    'best_epoch'    : -1,
                    'train_time'    : [],
                }
            
            #
            # Model initialization
            #
            
            if args.mp:
                ## Multiprocess configuration for training ##
                bestModelPath   = args.path+'bestModel_nEvts{}_nn{}_fold{}_iter{}_neu{}_batch{}.pth'.format(args.nEvents,lossComputation,nFold, it, range_neuText, args.batch_size)
                
                # torch.manual_seed(args.seed)                            
                mp.set_start_method('fork', force=True)
                
                model   = MLP.MLP_FF(range_neuInput, mlpType='sampleInSampleOut').to(device)
                model.share_memory()

                loss_history     = torch.zeros(args.num_processes, args.nEpochs)
                val_loss_history = torch.zeros(args.num_processes, args.nEpochs)
                best_epoch       = torch.zeros(args.num_processes, dtype=int)
                loss_history.share_memory_()
                val_loss_history.share_memory_()
                best_epoch.share_memory_()

                processes = []
                
                startTrainTime=time()
                for rank in range(args.num_processes):
                    p = mp.Process(target=trainMLP_ROOTdataset_hogwild, 
                                    args=(rank, args, model, device,
                                            datasetTrain, train_fold_sampler, val_fold_sampler,
                                            loss_history, val_loss_history, best_epoch,
                                            bestModelPath))
                    # We first train the model across `num_processes` processes
                    p.start()
                    processes.append(p)
                for p in processes:
                    p.join()

                endTrainTime=time()
                
                train_logging_dict['loss_history']      = loss_history
                train_logging_dict['val_loss_history']  = val_loss_history
                train_logging_dict['best_epoch']        = best_epoch
                train_logging_dict['train_time'].append(endTrainTime-startTrainTime)
                
                np.savez(args.path+'lossMP_nEvts{}_nn{}_fold{}_iter{}_neu{}_batch{}.npz'.format(conditions['nEvents'], lossComputation, nFold, it, range_neuText, args.batch_size) ,loss=train_logging_dict)
            else:
                model = MLP.MLP_FF(range_neuInput, mlpType='sampleInSampleOut')
                
                if args.optimAlg == 'adam':
                    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weightDecay)
                    
                early_stopper   = MLP.EarlyStopper(patience=args.patience, min_delta=args.min_delta_loss)
                
                # pathLoss      = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/_evaluateSequential/'
                # no multicore strategy
                # train_fold    = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, num_workers=num_workers, pin_memory=False)
                # val_fold      = DataLoader( datasetValid, batch_size=nBatch, shuffle=False, num_workers=num_workers, pin_memory=False)
                # train_fold_sampler  = KFoldSampler(train_fold_idx)
                # val_fold_sampler    = KFoldSampler(valid_fold_idx)
                train_fold    = DataLoader( datasetTrain, batch_size=args.batch_size, shuffle=False, sampler=train_fold_sampler)
                val_fold      = DataLoader( datasetTrain, batch_size=args.batch_size, shuffle=False, sampler=val_fold_sampler)
                
                train_config_dict.update({
                        'train_fold'    : train_fold,
                        'val_fold'      : val_fold,
                        'current_fold'  : nFold,
                        'current_iter'  : it,
                        'path_loss'     : args.path,
                    })
                
                startTrainTime=time()
                
                trainMLP_benchmark(
                        args,
                        model, 
                        optimizer, 
                        early_stopper,
                        datasetTrain,
                        train_logging_dict,
                        train_config_dict)
                
                endTrainTime=time()
                
                train_logging_dict['train_time'].append(endTrainTime-startTrainTime)
                
                np.savez(args.path+'loss_nEvts{}_nn{}_fold{}_iter{}_neu{}_batch{}.npz'.format(conditions['nEvents'], lossComputation, nFold, it, range_neuText, args.batch_size) ,loss=train_logging_dict)
    endTime=time()

    print('Total time: {:.2f} min ({:.2f} s)'.format((endTime-startTime)/60 , endTime-startTime))