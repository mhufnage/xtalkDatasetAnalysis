import sys
import os

## Insert here the path to xtalkDatasetAnalysis git package
sys.path.insert(1, '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/')

import time
import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
import torch.multiprocessing as mp
import ML_lib.MLP as MLP
# torch.multiprocessing.set_sharing_strategy('file_system')

def trainMLP_ROOTdataset_hogwild(rank, args, model, device, dataset, train_sampler, val_sampler, loss_history, val_loss_history, best_epoch, bestModelPath):

    torch.manual_seed(args.seed + rank)
    torch.set_num_threads(int(args.num_threads))
    
    train_dataloader = DataLoader( dataset, batch_size=args.batch_size, shuffle=False, sampler=train_sampler)
    val_dataloader   = DataLoader( dataset, batch_size=args.batch_size, shuffle=False, sampler=val_sampler)
    
    early_stopper    = MLP.EarlyStopper(patience=args.patience, min_delta=args.min_delta_loss)
    
    if args.optimAlg == 'adam':
        optimizer    = torch.optim.Adam( model.parameters(), lr=args.lr, weight_decay=args.weightDecay)
    
    for epoch in range(args.nEpochs):
        # Training
        loss, pid   = trainEpochMLP_ROOTdataset_hogwild(epoch, args, model, rank, device, train_dataloader, optimizer, loss_history)
        
        # Validation
        val_loss, pid_val = crossValidEpochMLP_ROOTdataset_hogwild(epoch, args, model, rank, device, val_dataloader, val_loss_history)

        if epoch % args.log_interval == 0:
            print('{}\tTrain Epoch: {} Loss: {:.6f}, Val_Loss: {:.6f}'.format(pid, epoch, loss.item(), val_loss.item()))
            
        # Early stopping
        bStop = early_stopper.early_stop(val_loss, epoch, model, optimizer, args.path)
        if bStop:
            best_epoch[rank] = early_stopper.bestEpoch
            print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_loss.item(), early_stopper.min_validation_loss, early_stopper.bestEpoch))
            MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, val_loss.item()) # save the model
            break
        else:
            best_epoch[rank] = epoch

    MLP.checkpoint(model, optimizer, bestModelPath, epoch, val_loss.item()) # save the model
    
def crossValidEpochMLP_ROOTdataset_hogwild(epoch, args, model, rank, device, data_loader, val_loss_history):
    pid = os.getpid() # mp
    scalerInputT = torch.tensor(args.scalerInput)
                
    # for data_input, target, batch_idx in data_loader:
    for batch_idx, (data_input, target, batch_array_idx) in enumerate(data_loader):
        
        # pass input through the model and generate the output
        output = model((data_input*scalerInputT).to(device)) #normalized (not yet exactly) input
        val_loss   = model.criterion(output, (target*scalerInputT).to(device)) # loss with normalized data target and model output
    
    val_loss_history[rank, epoch] = val_loss.item()
    
    return val_loss, pid

def trainEpochMLP_ROOTdataset_hogwild(epoch, args, model, rank, device, data_loader, optimizer, loss_history):
    pid = os.getpid() # mp
    scalerInputT = torch.tensor(args.scalerInput)
                
    # for data_input, target, batch_idx in data_loader:
    for batch_idx, (data_input, target, batch_array_idx) in enumerate(data_loader):
        
        # pass input through the model and generate the output
        output  = model((data_input*scalerInputT).to(device)) #normalized (not yet exactly) input
        
        # Loss calculation
        loss    = model.criterion(output, (target*scalerInputT).to(device)) # loss with normalized data target and model output

        # Backpropagation (optimize weights)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
        # if batch_idx % args.log_interval == 0:
        #     print('{}\tTrain Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
        #         pid, epoch, batch_idx * len(data_input), len(data_loader.dataset),
        #         100. * batch_idx / len(data_loader), loss.item()))
        
    # if epoch % args.log_interval == 0:
    #     # print('{}\tTrain Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
    #     #     pid, epoch, batch_idx * len(data_input), len(data_loader.dataset),
    #     #     100. * batch_idx / len(data_loader), loss.item()))
    #     print('{}\tTrain Epoch: {} Loss: {:.6f}'.format(pid, epoch, loss.item()))
            
    
    loss_history[rank, epoch] = loss.item()
    
    return loss, pid
            

def trainMLP_benchmark( args, model, optimizer, early_stopper, datasetTrainValid, train_logging_dict, train_kwargs):
    trainTime       = time.time()
    
    # seed            = 1
    lossComputation = train_kwargs['lossComputation']
    range_neuText   = train_kwargs['neurons_text']
    pathLoss        = train_kwargs['path_loss']
    # pathLossRep     = train_kwargs['path_loss_rep']
    # num_processes   = train_kwargs['num_processes']
    # num_threads     = train_kwargs['num_threads']
    nEpochs         = args.nEpochs#train_kwargs['max_epochs']
    scalerInput     = torch.tensor(args.scalerInput)#train_kwargs['scaler_input']
    scalerOutput    = train_kwargs['scaler_output']
    nFold           = train_kwargs['current_fold']
    it              = train_kwargs['current_iter']
    batch_size      = args.batch_size#train_kwargs['batch_size']
    total_events    = train_kwargs['conditions']['nEvents']
    
    # torch.set_num_threads(int(os.cpu_count()/num_processes))
    # torch.set_num_threads(int(num_threads))
    # torch.manual_seed(seed + rank)
    
    for epoch in range(nEpochs):
        #
        # Training
        #
        pid = os.getpid() # mp
                    
        for data_input, target, batch_idx in train_kwargs['train_fold']:
            
            # pass input through the model and generate the output
            output      = model(data_input*scalerInput) #normalized (not yet exactly) input

            # loss            = model.criterion(output, data_input*scalerInput) # loss with normalized data input and output
            loss            = model.criterion(output, target*scalerInput) # loss with normalized data target and model output

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        
        #
        # End of the current Epoch
        #
        if (epoch%1)==0:                
            # print('\t\t\tPID: {}, Epoch {}/{}...loss={:.5f}, val_loss={:.5f} | tau_loss={:.5f} | ene_loss={:.5f} | total_loss={:.5f}, total_val_loss={:.5f} '.format(pid, epoch,nEpochs,loss.detach().numpy(), val_loss, loss_tau, loss_ene, total_loss, total_val_loss))
            print('\t\t\tPID: {}, Epoch {}/{}...loss={:.5f}'.format(pid, epoch,nEpochs,loss.detach().numpy()))

        train_logging_dict['epoch'].append(epoch)
        train_logging_dict['loss'].append(loss.detach().numpy())
        
    
    thisTrainTime = time.time() - trainTime
    
    # train_logging_dict.update({
    #     'thisTrainTime' : thisTrainTime,
    # })
    
    
    print("PID {}, Fold {}, iter {}: this training time was {} seconds.".format(pid, nFold, it, thisTrainTime))
    
    # train_logging_dict['train_time'].append(endFoldTime-startFoldTime)
    
    # MLP.checkpoint(model, optimizer, bestModelPath, epoch, val_loss) # save model in case Early_Stop didn't happen
    # np.savez(pathLoss+'loss_nEvts{}_nn{}_fold{}_iter{}_neu{}_batch{}.npz'.format(total_events, lossComputation,nFold, it, range_neuText, batch_size) ,loss=train_logging_dict)
#########################################################################################################


def trainMLP( model, optimizer, early_stopper, datasetTrainValid, train_logging_dict, train_kwargs):
    trainTime       = time.time()
    
    # seed            = 1
    lossComputation = train_kwargs['lossComputation']
    range_neuText   = train_kwargs['neurons_text']
    pathLoss        = train_kwargs['path_loss']
    # pathLossRep     = train_kwargs['path_loss_rep']
    # num_processes   = train_kwargs['num_processes']
    # num_threads     = train_kwargs['num_threads']
    nEpochs         = train_kwargs['max_epochs']
    scalerInput     = train_kwargs['scaler_input']
    scalerOutput    = train_kwargs['scaler_output']
    nFold           = train_kwargs['current_fold']
    it              = train_kwargs['current_iter']
    batch_size      = train_kwargs['batch_size']
    total_events    = train_kwargs['conditions']['nEvents']
    
    # torch.set_num_threads(int(os.cpu_count()/num_processes))
    # torch.set_num_threads(int(num_threads))
    # torch.manual_seed(seed + rank)
    
    for epoch in range(nEpochs):
        #
        # Training
        #
        pid = os.getpid() # mp
                    
        for data_input, target, batch_idx in train_kwargs['train_fold']:
            
            # pass input through the model and generate the output
            output      = model(data_input*scalerInput) #normalized (not yet exactly) input
            
            #
            # Loss calculation
            #
            
            # get input and output energy and time
            ene_target, tau_target    = datasetTrainValid.calibrate(batch_idx, target) # unormlized target
            ene_output, tau_output    = datasetTrainValid.calibrate(batch_idx, output*scalerOutput) # denormalized output
            # sigmaTau_out            = torch.std(tau_output)
            # sigmaTau_in             = torch.std(tau_input)
            
            # total window loss
            # loss            = model.criterion(output, data_input*scalerInput) # loss with normalized data input and output
            loss            = model.criterion(output, target*scalerInput) # loss with normalized data target and model output
            loss_tau        = model.criterion(tau_output , tau_target) # time remains in ns
            loss_ene        = model.criterion(ene_output*train_kwargs['typeNormalization']['mevToGev'] , ene_target*train_kwargs['typeNormalization']['mevToGev']) # scale energy from MeV to GeV
            
            #
            # Backpropagation (optimize weights)
            #
            optimizer.zero_grad()
            total_loss      = loss + loss_ene + loss_tau
            pulse_tau_loss  = loss + loss_tau
            
            if lossComputation=='pet':
                total_loss.backward()
                
            elif lossComputation=='p':
                loss.backward()
            elif lossComputation=='t':
                loss_tau.backward()
            elif lossComputation=='pt':
                pulse_tau_loss.backward()    
            else:
                raise AttributeError("loss computation was not set to any implemented options.")
            # loss_tau.backward()
            # model.optimizer.step()
            optimizer.step()
        
        #
        # Validation
        #
        for data_input, target, batch_idx in train_kwargs['val_fold']:
            # pass input through the model and generate the output
            output  = model(data_input*scalerInput)

            #
            # Loss calculation
            #

            # get input and output energy and time
            val_ene_target, val_tau_target  = datasetTrainValid.calibrate(batch_idx, target)# unormlized target
            val_ene_output, val_tau_output  = datasetTrainValid.calibrate(batch_idx, output*scalerOutput)# denormalized output

            val_loss            = model.criterion(output, target*scalerInput) #normalized target
            val_loss_tau        = model.criterion(val_tau_output , val_tau_target)
            val_loss_ene        = model.criterion(val_ene_output*train_kwargs['typeNormalization']['mevToGev'] , val_ene_target*train_kwargs['typeNormalization']['mevToGev'])
            
            total_val_loss = val_loss + val_loss_ene + val_loss_tau
            val_pulse_tau_loss = val_loss + val_loss_tau

        #
        # End of the current Epoch
        #
        if (epoch%5)==0:                
            print('\t\t\tPID: {}, Epoch {}/{}...loss={:.5f}, val_loss={:.5f} | tau_loss={:.5f} | ene_loss={:.5f} | total_loss={:.5f}, total_val_loss={:.5f} '.format(pid, epoch,nEpochs,loss.detach().numpy(), val_loss, loss_tau, loss_ene, total_loss, total_val_loss))

        train_logging_dict['epoch'].append(epoch)
        train_logging_dict['loss'].append(loss.detach().numpy())
        train_logging_dict['ene_loss'].append(loss_ene.detach().numpy())
        train_logging_dict['tau_loss'].append(loss_tau.detach().numpy())
        train_logging_dict['val_loss'].append(val_loss.detach().numpy())
        train_logging_dict['val_ene_loss'].append(val_loss_ene.detach().numpy())
        train_logging_dict['val_tau_loss'].append(val_loss_tau.detach().numpy())
        train_logging_dict['total_loss'].append(total_loss.detach().numpy())
        train_logging_dict['total_val_loss'].append(total_val_loss.detach().numpy())
        
        #
        # Early Stop criteria
        #
        # bestModelPath   = pathLossRep+'bestModel_nn{}_fold{}_iter{}_neu{}.pth'.format(lossComputation,nFold, it, range_neuText)
        bestModelPath   = pathLoss+'bestModel_nn{}_fold{}_iter{}_neu{}.pth'.format(lossComputation,nFold, it, range_neuText)
        
        if lossComputation=='pet':
            bStop           = early_stopper.early_stop(total_val_loss, epoch, model, optimizer, bestModelPath)
        elif lossComputation=='p':
            bStop           = early_stopper.early_stop(val_loss, epoch, model, optimizer, bestModelPath)
        elif lossComputation=='t':
            bStop           = early_stopper.early_stop(val_loss_tau, epoch, model, optimizer, bestModelPath)
        elif lossComputation=='pt':
            bStop           = early_stopper.early_stop(val_pulse_tau_loss, epoch, model, optimizer, bestModelPath)
        else:
            raise AttributeError("select a loss computation method.")
        
        if bStop:
            train_logging_dict['best_epoch'] = early_stopper.bestEpoch
            
            if lossComputation=='pet':
                print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(total_val_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
                MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, total_val_loss) # save the model
                break
            elif lossComputation=='p':
                print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
                MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, val_loss) # save the model
                break
            elif lossComputation=='t':
                print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_loss_tau, early_stopper.min_validation_loss, early_stopper.bestEpoch))
                MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, val_loss_tau) # save the model
                break
            elif lossComputation=='pt':
                print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_pulse_tau_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
                MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, val_pulse_tau_loss) # save the model
                break
            else:
                raise AttributeError("select a loss computation method.")
        else:
            train_logging_dict['best_epoch'] = epoch
    
    thisTrainTime = time.time() - trainTime
    
    # train_logging_dict.update({
    #     'thisTrainTime' : thisTrainTime,
    # })
    
    
    print("PID {}, Fold {}, iter {}: this training time was {} seconds.".format(pid, nFold, it, thisTrainTime))
    
    MLP.checkpoint(model, optimizer, bestModelPath, epoch, val_loss) # save model in case Early_Stop didn't happen
    np.savez(pathLoss+'loss_nEvts{}_nn{}_fold{}_iter{}_neu{}_batch{}.npz'.format(total_events, lossComputation,nFold, it, range_neuText, batch_size) ,loss=train_logging_dict)
#########################################################################################################
########################################################################################################

# def trainMLP_ROOTdataset_multicore(rank, model, optimizer, early_stopper, device, datasetTrainValid, train_logging_dict, train_kwargs):
#     trainTime       = time.time()
    
#     seed            = 1
#     lossComputation = train_kwargs['lossComputation']
#     range_neuText   = train_kwargs['neurons_text']
#     pathLoss        = train_kwargs['path_loss']
#     pathLossRep     = train_kwargs['path_loss_rep']
#     num_processes   = train_kwargs['num_processes']
#     num_threads     = train_kwargs['num_threads']
#     nEpochs         = train_kwargs['max_epochs']
#     scalerInput     = train_kwargs['scaler_input']
#     scalerOutput    = train_kwargs['scaler_output']
#     nFold           = train_kwargs['current_fold']
#     it              = train_kwargs['current_iter']
#     batch_size      = train_kwargs['batch_size']
    
#     # torch.set_num_threads(int(os.cpu_count()/num_processes))
#     torch.set_num_threads(int(num_threads))
#     torch.manual_seed(seed + rank)
    
#     for epoch in range(nEpochs):
#         #
#         # Training
#         #
#         pid = os.getpid() # mp
                    
#         for data_input, target, batch_idx in train_kwargs['train_fold']:
            
#             # pass input through the model and generate the output
#             output      = model((data_input*scalerInput).to(device)) #normalized (not yet exactly) input
            
#             #
#             # Loss calculation
#             #
            
#             # get input and output energy and time
#             ene_target, tau_target    = datasetTrainValid.calibrate(batch_idx, target.to(device)) # unormlized target
#             ene_output, tau_output    = datasetTrainValid.calibrate(batch_idx, (output*scalerOutput).to(device)) # denormalized output
#             # sigmaTau_out            = torch.std(tau_output)
#             # sigmaTau_in             = torch.std(tau_input)
            
#             # total window loss
#             # loss            = model.criterion(output, data_input*scalerInput) # loss with normalized data input and output
#             loss            = model.criterion(output, (target*scalerInput).to(device)) # loss with normalized data target and model output
#             loss_tau        = model.criterion(tau_output , (tau_target).to(device)) # time remains in ns
#             loss_ene        = model.criterion(ene_output*train_kwargs['typeNormalization']['mevToGev'] , (ene_target*train_kwargs['typeNormalization']['mevToGev']).to(device)) # scale energy from MeV to GeV
            
            
#             #
#             # Backpropagation (optimize weights)
#             #
#             optimizer.zero_grad()
#             total_loss      = loss + loss_ene + loss_tau
#             pulse_tau_loss  = loss + loss_tau
            
#             if lossComputation=='pet':
#                 total_loss.backward()
                
#             elif lossComputation=='p':
#                 loss.backward()
#             elif lossComputation=='t':
#                 loss_tau.backward()
#             elif lossComputation=='pt':
#                 pulse_tau_loss.backward()    
#             else:
#                 raise AttributeError("loss computation was not set to any implemented options.")
#             # loss_tau.backward()
#             # model.optimizer.step()
#             optimizer.step()
        
#         #
#         # Validation
#         #
#         for data_input, target, batch_idx in train_kwargs['val_fold']:
#             # pass input through the model and generate the output
#             output  = model((data_input*scalerInput).to(device))

#             #
#             # Loss calculation
#             #

#             # get input and output energy and time
#             val_ene_target, val_tau_target  = datasetTrainValid.calibrate(batch_idx, (target.to(device)))# unormlized target
#             val_ene_output, val_tau_output  = datasetTrainValid.calibrate(batch_idx, (output*scalerOutput).to(device))# denormalized output

#             val_loss            = model.criterion(output, (target*scalerInput).to(device)) #normalized target
#             val_loss_tau        = model.criterion(val_tau_output , (val_tau_target).to(device))
#             val_loss_ene        = model.criterion(val_ene_output*train_kwargs['typeNormalization']['mevToGev'] , val_ene_target*train_kwargs['typeNormalization']['mevToGev'])
            
#             total_val_loss = val_loss + val_loss_ene + val_loss_tau
#             val_pulse_tau_loss = val_loss + val_loss_tau

#         #
#         # End of the current Epoch
#         #
#         if (epoch%5)==0:                
#             print('\t\t\tPID: {}, Epoch {}/{}...loss={:.5f}, val_loss={:.5f} | tau_loss={:.5f} | ene_loss={:.5f} | total_loss={:.5f}, total_val_loss={:.5f} '.format(pid, epoch,nEpochs,loss.detach().numpy(), val_loss, loss_tau, loss_ene, total_loss, total_val_loss))
#         train_logging_dict['loss_history'][rank, epoch] = loss.item()
#         train_logging_dict['epoch'].append(epoch)
#         train_logging_dict['loss'].append(loss.detach().numpy())
#         train_logging_dict['ene_loss'].append(loss_ene.detach().numpy())
#         train_logging_dict['tau_loss'].append(loss_tau.detach().numpy())
#         train_logging_dict['val_loss'].append(val_loss.detach().numpy())
#         train_logging_dict['val_ene_loss'].append(val_loss_ene.detach().numpy())
#         train_logging_dict['val_tau_loss'].append(val_loss_tau.detach().numpy())
#         train_logging_dict['total_loss'].append(total_loss.detach().numpy())
#         train_logging_dict['total_val_loss'].append(total_val_loss.detach().numpy())
        
#         #
#         # Early Stop criteria
#         #
#         bestModelPath   = pathLossRep+'bestModel_nn{}_fold{}_iter{}_neu{}.pth'.format(lossComputation,nFold, it, range_neuText)
        
#         if lossComputation=='pet':
#             bStop           = early_stopper.early_stop(total_val_loss, epoch, model, optimizer, bestModelPath)
#         elif lossComputation=='p':
#             bStop           = early_stopper.early_stop(val_loss, epoch, model, optimizer, bestModelPath)
#         elif lossComputation=='t':
#             bStop           = early_stopper.early_stop(val_loss_tau, epoch, model, optimizer, bestModelPath)
#         elif lossComputation=='pt':
#             bStop           = early_stopper.early_stop(val_pulse_tau_loss, epoch, model, optimizer, bestModelPath)
#         else:
#             raise AttributeError("select a loss computation method.")
        
#         if bStop:
#             train_logging_dict['best_epoch'] = early_stopper.bestEpoch
            
#             if lossComputation=='pet':
#                 print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(total_val_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
#                 MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, total_val_loss) # save the model
#                 break
#             elif lossComputation=='p':
#                 print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
#                 MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, val_loss) # save the model
#                 break
#             elif lossComputation=='t':
#                 print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_loss_tau, early_stopper.min_validation_loss, early_stopper.bestEpoch))
#                 MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, val_loss_tau) # save the model
#                 break
#             elif lossComputation=='pt':
#                 print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_pulse_tau_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
#                 MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, val_pulse_tau_loss) # save the model
#                 break
#             else:
#                 raise AttributeError("select a loss computation method.")
#         else:
#             train_logging_dict['best_epoch'] = epoch
    
#     thisTrainTime = time.time() - trainTime
    
#     train_logging_dict.update({
#         'thisTrainTime' : thisTrainTime,
#     })
    
#     print("PID {}, Fold {}, iter {}: this training time was {} seconds.".format(pid, nFold, it, thisTrainTime))
    
#     MLP.checkpoint(model, optimizer, bestModelPath, epoch, val_loss) # save model in case Early_Stop didn't happen
#     np.savez(pathLossRep+'loss_nn{}_fold{}_iter{}_neu{}_batch{}.npz'.format(lossComputation,nFold, it, range_neuText, batch_size) ,loss=train_logging_dict)
    


# def trainMLP_multicore(rank, model, optimizer, early_stopper, device, datasetTrain, datasetValid, train_logging_dict, train_kwargs):
#     trainTime       = time.time()
    
#     seed            = 1
#     lossComputation = train_kwargs['lossComputation']
#     range_neuText   = train_kwargs['neurons_text']
#     pathLoss        = train_kwargs['path_loss']
#     num_processes   = train_kwargs['num_processes']
#     num_threads     = train_kwargs['num_threads']
#     nEpochs         = train_kwargs['max_epochs']
#     scalerInput     = train_kwargs['scaler_input']
#     scalerOutput    = train_kwargs['scaler_output']
#     nFold           = train_kwargs['current_fold']
#     it              = train_kwargs['current_iter']
#     batch_size      = train_kwargs['batch_size']
    
#     # torch.set_num_threads(int(os.cpu_count()/num_processes))
#     torch.set_num_threads(int(num_threads))
#     torch.manual_seed(seed + rank)
    
#     for epoch in range(nEpochs):
#         #
#         # Training
#         #
#         pid = os.getpid() # mp
                    
#         for data_input, target, batch_idx in train_kwargs['train_fold']:
            
#             # pass input through the model and generate the output
#             output      = model((data_input*scalerInput).to(device)) #normalized (not yet exactly) input
            
#             #
#             # Loss calculation
#             #
            
#             # get input and output energy and time
#             ene_target, tau_target    = datasetTrain.calibrate(batch_idx, target.to(device)) # unormlized target
#             ene_output, tau_output    = datasetTrain.calibrate(batch_idx, (output*scalerOutput).to(device)) # denormalized output
#             # sigmaTau_out            = torch.std(tau_output)
#             # sigmaTau_in             = torch.std(tau_input)
            
#             # total window loss
#             # loss            = model.criterion(output, data_input*scalerInput) # loss with normalized data input and output
#             loss            = model.criterion(output, (target*scalerInput).to(device)) # loss with normalized data target and model output
#             loss_tau        = model.criterion(tau_output , (tau_target).to(device)) # time remains in ns
#             loss_ene        = model.criterion(ene_output*train_kwargs['typeNormalization']['mevToGev'] , (ene_target*train_kwargs['typeNormalization']['mevToGev']).to(device)) # scale energy from MeV to GeV

#             #
#             # Backpropagation (optimize weights)
#             #
#             optimizer.zero_grad()
#             total_loss      = loss + loss_ene + loss_tau
#             pulse_tau_loss  = loss + loss_tau
            
#             if lossComputation=='pet':
#                 total_loss.backward()
                
#             elif lossComputation=='p':
#                 loss.backward()
#             elif lossComputation=='t':
#                 loss_tau.backward()
#             elif lossComputation=='pt':
#                 pulse_tau_loss.backward()    
#             else:
#                 raise AttributeError("loss computation was not set to any implemented options.")
#             # loss_tau.backward()
#             # model.optimizer.step()
#             optimizer.step()
        
#         #
#         # Validation
#         #
#         for data_input, target, batch_idx in train_kwargs['val_fold']:
#             # pass input through the model and generate the output
#             output  = model(data_input*scalerInput)

#             #
#             # Loss calculation
#             #

#             # get input and output energy and time
#             val_ene_target, val_tau_target  = datasetValid.calibrate(batch_idx, (target.to(device)))# unormlized target
#             val_ene_output, val_tau_output  = datasetValid.calibrate(batch_idx, (output*scalerOutput).to(device))# denormalized output

#             val_loss            = model.criterion(output, (target*scalerInput).to(device)) #normalized target
#             val_loss_tau        = model.criterion(val_tau_output , (val_tau_target).to(device))
#             val_loss_ene        = model.criterion(val_ene_output*train_kwargs['typeNormalization']['mevToGev'] , val_ene_target*train_kwargs['typeNormalization']['mevToGev'])
            
#             total_val_loss = val_loss + val_loss_ene + val_loss_tau
#             val_pulse_tau_loss = val_loss + val_loss_tau

#         #
#         # End of the current Epoch
#         #
#         if (epoch%5)==0:                
#             print('\t\t\tPID: {}, Epoch {}/{}...loss={:.5f}, val_loss={:.5f} | tau_loss={:.5f} | ene_loss={:.5f} | total_loss={:.5f}, total_val_loss={:.5f} '.format(pid, epoch,nEpochs,loss.detach().numpy(), val_loss, loss_tau, loss_ene, total_loss, total_val_loss))

#         train_logging_dict['epoch'].append(epoch)
#         train_logging_dict['loss'].append(loss.detach().numpy())
#         train_logging_dict['ene_loss'].append(loss_ene.detach().numpy())
#         train_logging_dict['tau_loss'].append(loss_tau.detach().numpy())
#         train_logging_dict['val_loss'].append(val_loss.detach().numpy())
#         train_logging_dict['val_ene_loss'].append(val_loss_ene.detach().numpy())
#         train_logging_dict['val_tau_loss'].append(val_loss_tau.detach().numpy())
#         train_logging_dict['total_loss'].append(total_loss.detach().numpy())
#         train_logging_dict['total_val_loss'].append(total_val_loss.detach().numpy())
        
#         #
#         # Early Stop criteria
#         #
#         bestModelPath   = pathLoss+'bestModel_nn{}_fold{}_iter{}_neu{}.pth'.format(lossComputation,nFold, it, range_neuText)
        
#         if lossComputation=='pet':
#             bStop           = early_stopper.early_stop(total_val_loss, epoch, model, optimizer, bestModelPath)
#         elif lossComputation=='p':
#             bStop           = early_stopper.early_stop(val_loss, epoch, model, optimizer, bestModelPath)
#         elif lossComputation=='t':
#             bStop           = early_stopper.early_stop(val_loss_tau, epoch, model, optimizer, bestModelPath)
#         elif lossComputation=='pt':
#             bStop           = early_stopper.early_stop(val_pulse_tau_loss, epoch, model, optimizer, bestModelPath)
#         else:
#             raise AttributeError("select a loss computation method.")
        
#         if bStop:
#             train_logging_dict['best_epoch'] = early_stopper.bestEpoch
            
#             if lossComputation=='pet':
#                 print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(total_val_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
#                 MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, total_val_loss) # save the model
#                 break
#             elif lossComputation=='p':
#                 print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
#                 MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, val_loss) # save the model
#                 break
#             elif lossComputation=='t':
#                 print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_loss_tau, early_stopper.min_validation_loss, early_stopper.bestEpoch))
#                 MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, val_loss_tau) # save the model
#                 break
#             elif lossComputation=='pt':
#                 print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_pulse_tau_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
#                 MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, val_pulse_tau_loss) # save the model
#                 break
#             else:
#                 raise AttributeError("select a loss computation method.")
#         else:
#             train_logging_dict['best_epoch'] = epoch
    
#     thisTrainTime = time.time() - trainTime
    
#     train_logging_dict.update({
#         'thisTrainTime' : thisTrainTime,
#     })
    
#     print("PID {}, Fold {}, iter {}: this training time was {} seconds.".format(pid, nFold, it, thisTrainTime))
    
#     MLP.checkpoint(model, optimizer, bestModelPath, epoch, val_loss) # save model in case Early_Stop didn't happen
#     np.savez(pathLoss+'loss_nn{}_fold{}_iter{}_neu{}_batch{}.npz'.format(lossComputation,nFold, it, range_neuText, batch_size) ,loss=train_logging_dict)
    