# Lorenzetti Showers Collaboration 2020 - 2023 - https://doi.org/10.1016/j.cpc.2023.108671
print('Starting script for neuron/layers space search...')

import argparse
parser = argparse.ArgumentParser()

parser.add_argument("--mt", help="Run training on multiprocess", action="store_true")
parser.add_argument("--path", help="Path to store training data.", required=True)

args = parser.parse_args()

import sys
sys.path.insert(0,'/home/mhufnage/lorenzetti/Applications') #lorenzetti utils path

## Insert here the path to xtalkDatasetAnalysis git package
sys.path.insert(1, '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/')

uprootLibrary = 'np'#'np' #'ak'

import json
import os
import time
import shelve

import numpy as np
from glob import glob
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as mcolors
import matplotlib
# matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS

import scipy.stats as scipy
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader, DistributedSampler, Subset
import torch.multiprocessing as mp

torch.multiprocessing.set_sharing_strategy('file_system')

from collections import OrderedDict
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score

import mplhep as hep

plt.style.use([hep.style.ATLAS])

import uproot

# from calibrationFilesHelper import *
# from Autoencoder import *
# from XTalkDatasetClass import XTalkDataset_LZT, Subset
# from auxiliaryFunctions import confInterval, getBins

# Custom lib
from helper_lib.calibrationFilesHelper import *
from helper_lib.auxiliaryFunctions import confInterval, getBins
from helper_lib.functionsHelper import stdVecToArray, stdVecToPythonArray
import ML_lib.MLP as MLP
from ML_lib.ML_custom_auxiliar import KFoldSampler
from ML_lib.Autoencoder import *
from ML_lib.XTalkDatasetClass import XTalkDataset_LZT_FromROOT#, XTalkDataset_LZT, XTalkStreamingDataLoader_LZT, XTalkStreamingDataLoader_LZT_FoldSplit, XTalkStreamingDataLoader_LZT_FoldSplit_Faster
from lorenzetti_reader import *

import ROOT
from ROOT import TFile, gROOT, TChain, TTree

from train_multicore import trainMLP, trainMLP_multicore, trainMLP_ROOTdataset_multicore


# -----------------------------------------------------------------
# -----------------------------------------------------------------
# -----------------------------------------------------------------


if __name__ == '__main__':
    
    # pathLoss      = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/neuLay_1Layer_finer/'
    pathLoss      = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/{}/'.format(args.path)
    # pathLoss      = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/_evaluateSequential/'
    
    conditions = {
        'nEvents':         -1, # <--- it is the total events to feed KFold_split into (train+valid)+(test).
        'SigmaCut':        1,
        'etaCut':          1.4,
        'caloSampling':    'EM2',#CaloSampling.EMB2,
        'etaSize':         3,
        'phiSize':         3,
        'nSamples':        5,
        'dumpCells':       True,
        'bDumpWindowOnly': True,
        'bNoEneCut':       True,
        'XTalk_ind':       1.1,
        'XTalk_cap':       0.0,
        'XTalkLevel':      'L = 1.1%, C = 0.0%',
        
        # Not implemented yet!
        'etBin':           [[15 , 20] ,[20, 30]  ,[30, 40]  ,[40, 50],[50,100],[100,10000000]],
        'etaBin':          [[0.00, 0.80], [0.80, 1.37], [1.37,1.54],[1.54,2.50],[2.50,3.2]],
        'bPhaseSpaceDiv':  False,
    }
    
    # DS_Lim        = 9
    DS_List       = [1, 2, 3, 4]#, 5]#, 6, 7, 8, 9, 10]#, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ]
    DS_test       = [21]
    DS_PathAlias  = '/data/atlas/mhufnage/lzt_single_e/elec_singleCell_10kEvts_centroid_e50_DS{}/AOD/user.mhufnage.lzt.e50.xtalk.DS{}.win3.nopileup.WithCells_EM2.event.npz'

    if not(os.path.isdir(pathLoss)):
        os.mkdir(pathLoss)

    startTime=time()
    # Dataset
    data = TChain('ML_tree')
    for ds_index in range(1,30):
        # print(ds_index, conditions['XTalk_cap'], conditions['XTalk_ind'], conditions['etaSize'], conditions['phiSize'])
        theFile = '/data/atlas/mhufnage/lzt_single_e/elec_singleCell_10kEvts_centroid_e50_DS{}/AOD_Cap{:.1f}_Ind{:.1f}/user.mhufnage.lzt.e50.xtalk.DS{}.win{}{}.nopileup.WithCells_{}.root'.format(ds_index, conditions['XTalk_cap'], conditions['XTalk_ind'], ds_index, conditions['etaSize'], conditions['phiSize'], conditions['caloSampling'])
        data.Add(theFile+'/ML_tree')
    # data            = []
    # print('Loading dataset from list: ',DS_List)
    # for DS_index in DS_List:
    #     data.append (np.load(DS_PathAlias.format(DS_index,DS_index), allow_pickle=True)['dataset'])
    #     print('\tloaded ds {}...'.format(DS_index))
    # data = np.concatenate(data).ravel()
    # print('Dataset loaded with {} events!'.format(len(data)))

    #
    # Configuration
    #

    optimAlg                = 'adam'
    typeNormalization       = {'electronEnergy': 50.0,
                            'mevToGev': 1/1000,
                            'gevToMev': 1000,
                            'none': 1}#'minMax'

    lr                      = 1e-3
    weightDecay_adam        = 1e-5

    optimConfig             = {
        'adam':{
            'learningRate'  : lr,
            'weightDecay'   : weightDecay_adam,
        },
    }

    print(optimConfig[optimAlg])

    # trainNeuLists = {'neuLay': [[45,5,5],[45,15,15],[45,30,30]],
    #                  } #2layers
    # trainNeuLists = {'neuLay': [[45,5,5],[45,30,30],[45,60,60], [45,75,75]],
    #                  } # 1layer
    # trainNeuLists = {'neuLay': [[45,10],[45,15],[45,20], [45,25], [45,35], [45,40], [45,45], [45,50], [45,55], [45,65],[45,70],[45,75]],} # 1layer_finer
    # trainNeuLists = {'neuLay': [[45,75],[45,90],[45,100],[45,120],[45,150],[45,200]],} # 1layer_finer
    # trainNeuLists = {'neuLay': [[45,15]],} # 1layer_finer
    trainNeuLists = {'neuLay': [[45,15,15]],} # 2layer (24/05/2024)

    lossComputationList = ['p']

    # np.savez(pathLoss+'{}_config.npz'.format(pathLoss.split('/')[-2]),config=trainNeuLists)
    np.savez(pathLoss+'{}_conditions.npz'.format(pathLoss.split('/')[-2]),config=conditions)
    
    # nEventsList = [150000, 200000] # loop4
    nEventsList = [50000]
    
    for eventIndex in range(0,len(nEventsList)):
        conditions['nEvents'] = nEventsList[eventIndex]
        
        for lossMethod in lossComputationList:
            print('Computation method: {}'.format(lossMethod))
            
            for trainNeuList in trainNeuLists['neuLay']:
                range_neuInput          = trainNeuList#[45,20,20]
                range_neuText           = MLP.neuListToFileText(range_neuInput)
                print(range_neuText)

                experiment_alias        = '''
                learning the samples of a 3x3 window from topocluster at EMB2 layer. 
                The loss for training is the input-output map. 
                The cell organization in the window is eta-oriented.
                # phi [ 3 | 6 | 9 ]
                #     [ 2 | 5 | 8 ]  , eta-orientation cluster indexing. where 5 is the index of the hottest cell
                #     [ 1 | 4 | 7 ]
                #             eta
                '''
                bParalellData           = args.mt
                num_processes           = 4
                num_threads             = 1
                num_workers             = 0
                
                totalEvents             = data.GetEntries()#len(data)
                
                bLoadFullCellData       = False
                nEpochs                 = 10
                lossComputation         = lossMethod
                patience                = 10
                min_delta_loss          = 0

                k                       = 2 # number of folds in kFold split
                nBatch                  = 8 # batch size in clusters
                nIter                   = 1 # number of random initializations of each model, for a fold.
                nRep                    = 1 # number of fold shuffle repetition, for same fold and iteration
                nClusters               = conditions['nEvents'] # for training and validation

                nSamples                = conditions['nSamples'] # standard for LAr
                nCellsWinEta            = conditions['etaSize'] # dataset selection
                nCellsWinPhi            = conditions['phiSize']
                scalerInput             = torch.tensor((typeNormalization['mevToGev'])) # without normalization, set to 1
                scalerOutput            = torch.tensor(typeNormalization['gevToMev'])

                train_ratio             = 1 # proportion of data to apply the kFold
                test_ratio              = 0 # proportion of data to apply test on trained model
                
                eventsArray             = torch.as_tensor(np.arange(0, nClusters)) # contiguous indexesfor KFold split

                #
                # Dataset split
                #
                total_ene = []
                total_tau = []

                train_config_dict   = {
                        'experiment_alias'      : experiment_alias,
                        'conditions'            : conditions,
                        'patience'              : patience,
                        'min_delta_loss'        : min_delta_loss,
                        'max_epochs'            : nEpochs,
                        'kFolds'                : k,
                        'max_rep'               : nRep,
                        'batch_size'            : nBatch,
                        'max_init'              : nIter,
                        'total_events_dataset'  : totalEvents,
                        'total_clusters'        : nClusters,
                        'train_ratio'           : train_ratio,
                        'test_ratio'            : test_ratio,
                        'cell_win_eta'          : nCellsWinEta,
                        'cell_win_phi'          : nCellsWinPhi,
                        'scaler_input'          : scalerInput,
                        'scaler_output'         : scalerOutput,
                        'neurons_list'          : range_neuInput,
                        'neurons_text'          : range_neuText,
                        'lossComputation'       : lossComputation,
                        'optimConfig'           : optimConfig,
                        'typeNormalization'     : typeNormalization,
                        'path_loss'             : pathLoss,
                        'num_processes'         : num_processes,
                        'num_threads'           : num_threads,
                }

                # if optimAlg=='adam':
                # np.savez(pathLoss+'config_train_nn{}_{}_neu{}.npz'.format(lossComputation, optimAlg, range_neuText), config=train_config_dict)
                np.savez(pathLoss+'config_train_nEvts{}_nn{}_{}_neu{}.npz'.format(conditions['nEvents'], lossComputation, optimAlg, range_neuText), config=train_config_dict)

                # X_train, X_test = train_test_split( data[0:nClusters] , test_size=test_ratio)
                # datasetTrain = XTalkDataset_LZT_FromROOT(data, conditions, mode='supervised', load_full_data=bLoadFullCellData)
                datasetTrainValid = XTalkDataset_LZT_FromROOT(data, conditions, mode='supervised', load_full_data=bLoadFullCellData)

                # datasetTest     =   XTalkDataset_LZT(X_test, nEvents=-1, mode='supervised')

                # kfold = KFold(n_splits=k, shuffle=True, random_state=1)

                for rep in range(0, nRep):
                    pathLossRep = pathLoss+'rep{}/'.format(rep)
                    
                    if not(os.path.isdir(pathLossRep)):
                        os.mkdir(pathLossRep)
            
                    r_seed = 0 # repetition random seed
                    if rep%2 == 0: # if even
                        r_seed = 0+rep**2
                    else: # odd
                        r_seed = 42+rep**2
                    
                    print('\tRepetition {}, seed: {}'.format(rep, r_seed))
                        
                    kfold = KFold(n_splits=k, shuffle=True, random_state=r_seed)
                
                    # for nFold, (train_fold_idx, valid_fold_idx) in enumerate(kfold.split(X_train)):
                    for nFold, (train_fold_idx, valid_fold_idx) in enumerate(kfold.split(eventsArray)):

                        # datasetTrain  = XTalkDataset_LZT(X_train[train_fold_idx], nEvents=-1, mode='supervised')
                        # datasetValid  = XTalkDataset_LZT(X_train[valid_fold_idx], nEvents=-1, mode='supervised')
                        # train_fold_sampler  = KFoldSampler(train_fold_idx)
                        # val_fold_sampler    = KFoldSampler(valid_fold_idx)
                        
                        datasetTrain  = Subset(datasetTrainValid, train_fold_idx)
                        datasetValid  = Subset(datasetTrainValid, valid_fold_idx)
                        
                        # train_fold    = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, sampler=train_fold_sampler)
                        # val_fold      = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, sampler=val_fold_sampler)
                        
                        # train_fold    = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, num_workers=0, pin_memory=False)
                        # val_fold      = DataLoader( datasetValid, batch_size=nBatch, shuffle=False, num_workers=0, pin_memory=False)
                                                
                        for it in range(0,nIter):
                            # print('\t\tFold {}/{}... iter {}/{}'.format(nFold+1,k, it+1, nIter))
                            print('\t\tnEvents={}, Fold {}/{}... iter {}/{}'.format(conditions['nEvents'], nFold+1,k, it+1, nIter))
                            
                            # train_config_dict.update({
                            #     'current_iter'  : it,
                            # })
                            
                            train_logging_dict  = {
                                'loss'              : [],
                                'val_loss'          : [],
                                'tau_loss'          : [],
                                'val_tau_loss'      : [],
                                'ene_loss'          : [],
                                'val_ene_loss'      : [],
                                'total_loss'        : [],
                                'total_val_loss'    : [],
                                'epoch'             : [],
                                
                                'loss_history'      : [],

                                'best_epoch'    : -1,
                                'train_time'    : -1,
                            }
                            
                            #
                            # Model initialization
                            #
                            model = MLP.MLP_FF(range_neuInput, mlpType='sampleInSampleOut')
                            
                            if optimAlg=='adam':
                                optimizer = torch.optim.Adam(
                                    model.parameters(), 
                                    lr=optimConfig[optimAlg]['learningRate'], 
                                    weight_decay=optimConfig[optimAlg]['weightDecay'])
                        
                            early_stopper   = MLP.EarlyStopper(patience=patience, min_delta=min_delta_loss)
                            
                            
                            if bParalellData:
                                ## Multiprocess configuration for training ##
                                device = torch.device("cpu")
                                # torch.set_num_threads(int(num_threads))
                                torch.manual_seed(1)
                                mp.set_start_method('spawn', force=True)
                                
                                loss_history = torch.zeros(train_config_dict['num_processes'], train_config_dict['max_epochs'])
                                loss_history.share_memory_()
                                
                                train_logging_dict['loss_history'] = loss_history
                
                                model = model.to(device)
                                model.share_memory() # gradients are allocated lazily, so they are not shared here
                                
                                processes = []
                                
                                for rank in range(num_processes):
                                    
                                    # train_fold    = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, sampler=train_fold_sampler)
                                    # val_fold      = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, sampler=val_fold_sampler)
                                    
                                    train_fold    = DataLoader(dataset=datasetTrain, 
                                                            sampler=DistributedSampler(dataset=datasetTrain, 
                                                                                        num_replicas=num_processes, 
                                                                                        rank=rank),
                                                            batch_size=nBatch,                                                    
                                                            shuffle=False, 
                                                            num_workers=num_workers, 
                                                            pin_memory=False)
                                    
                                    val_fold      = DataLoader(dataset=datasetValid, 
                                                            sampler=DistributedSampler(dataset=datasetValid, 
                                                                                        num_replicas=num_processes, 
                                                                                        rank=rank),
                                                            batch_size=nBatch,                                                    
                                                            shuffle=False, 
                                                            num_workers=num_workers, 
                                                            pin_memory=False)
                                    
                                    train_config_dict.update({
                                        'train_fold'    : train_fold,
                                        'val_fold'      : val_fold,
                                        'current_fold'  : nFold,
                                        'current_iter'  : it,
                                        'path_loss_rep' : pathLossRep,
                                    })
                                    
                                    
                                    p = mp.Process(target=trainMLP_ROOTdataset_multicore, args=(rank, 
                                                                        model, 
                                                                        optimizer, 
                                                                        early_stopper,
                                                                        device,
                                                                        datasetTrainValid,
                                                                        train_logging_dict,
                                                                        train_config_dict))
                                    # We first train the model across `num_processes` processes
                                    p.start()
                                    processes.append(p)
                                    
                                for p in processes:
                                    p.join()
                                ## **************************************** ##
                            
                            else: 
                                pathLoss      = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/_evaluateSequential/'
                                # no multicore strategy
                                # train_fold    = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, num_workers=num_workers, pin_memory=False)
                                # val_fold      = DataLoader( datasetValid, batch_size=nBatch, shuffle=False, num_workers=num_workers, pin_memory=False)
                                train_fold_sampler  = KFoldSampler(train_fold_idx)
                                val_fold_sampler    = KFoldSampler(valid_fold_idx)
                                train_fold    = DataLoader( datasetTrainValid, batch_size=nBatch, shuffle=False, sampler=train_fold_sampler)
                                val_fold      = DataLoader( datasetTrainValid, batch_size=nBatch, shuffle=False, sampler=val_fold_sampler)
                                
                                train_config_dict.update({
                                        'train_fold'    : train_fold,
                                        'val_fold'      : val_fold,
                                        'current_fold'  : nFold,
                                        'current_iter'  : it,
                                        'path_loss_rep' : pathLossRep,
                                    })
                                
                                trainMLP(model, 
                                        optimizer, 
                                        early_stopper,
                                        datasetTrainValid,
                                        train_logging_dict,
                                        train_config_dict)
                            
                            #
                            # End of training at current fold-iter
                            #



    endTime=time()

    print('Total time: {:.2f} min ({:.2f} s)'.format((endTime-startTime)/60 , endTime-startTime))
