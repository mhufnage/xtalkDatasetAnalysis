# Lorenzetti Showers Collaboration 2020 - 2024 - https://doi.org/10.1016/j.cpc.2023.108671
print('Starting script for neuron/layers space search...')
import sys
sys.path.insert(0,'/home/mhufnage/lorenzetti/Applications') #lorenzetti utils path

## Insert here the path to xtalkDatasetAnalysis git package
sys.path.insert(1, '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/')

uprootLibrary = 'np'#'np' #'ak'

import json
import os
import time
import shelve

import numpy as np
from glob import glob
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as mcolors
import matplotlib
# matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS

import scipy.stats as scipy
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader

from collections import OrderedDict
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score

import mplhep as hep

plt.style.use([hep.style.ATLAS])

# Custom lib
from helper_lib.calibrationFilesHelper import *
from helper_lib.auxiliaryFunctions import confInterval, getBins
from helper_lib.functionsHelper import stdVecToArray, stdVecToPythonArray
import ML_lib.MLP as MLP
from ML_lib.ML_custom_auxiliar import KFoldSampler
from ML_lib.Autoencoder import *
from ML_lib.XTalkDatasetClass import XTalkDataset_LZT_FromROOT#, XTalkDataset_LZT, XTalkStreamingDataLoader_LZT, XTalkStreamingDataLoader_LZT_FoldSplit, XTalkStreamingDataLoader_LZT_FoldSplit_Faster
from lorenzetti_reader import *

import ROOT
from ROOT import TFile, gROOT, TChain, TTree


# --------- Lorenzetti plot style -------
def lorenzettiText(pos='out',subText=' Internal'):
    lorenzetti_title={'fontsize': 21, 'fontfamily': 'sans-serif', 'fontweight':'bold','fontstyle':'italic'}
    lorenzetti_subtitle={'fontsize': 14, 'fontfamily': 'sans-serif', 'fontweight':'normal','fontstyle':'italic'}

    if pos=='out':
        plt.figtext(x=.18   ,y=.945,s='Lorenzetti',fontdict=lorenzetti_title)
        plt.figtext(x=.318  ,y=.945,s=subText,fontdict=lorenzetti_subtitle)
    if pos=='in':
        plt.figtext(x=.18   ,y=.875,s='Lorenzetti',fontdict=lorenzetti_title)
        plt.figtext(x=.318  ,y=.875,s=subText,fontdict=lorenzetti_subtitle)


# --------- Lorenzetti sampling and detector enumeration ---------
class Detector():
  LAR     = 0
  TILE    = 1
  TTEM    = 2
  TTHEC   = 3
  FCALEM  = 5
  FCALHAD = 6

class CaloSampling():
    PSB       = 0
    PSE       = 1
    EMB1      = 2
    EMB2      = 3
    EMB3      = 4
    TileCal1  = 5
    TileCal2  = 6
    TileCal3  = 7
    TileExt1  = 8
    TileExt2  = 9
    TileExt3  = 10
    EMEC1     = 11
    EMEC2     = 12
    EMEC3     = 13
    HEC1      = 14
    HEC2      = 15
    HEC3      = 16


# -----------------------------------------------------------------
# -----------------------------------------------------------------
# -----------------------------------------------------------------
## Path to experiment output

pathLoss      = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/neu_2Layer_33/'

conditions = {
    'nEvents':         30000, # <--- it is the total events to feed KFold_split into (train+valid)+(test).
    'SigmaCut':        1,
    'etaCut':          1.4,
    'caloSampling':    'EM2',#CaloSampling.EMB2,
    'etaSize':         3,
    'phiSize':         3,
    'nSamples':        5,
    'dumpCells':       True,
    'bDumpWindowOnly': True,
    'bNoEneCut':       True,
    'XTalk_ind':       1.1,
    'XTalk_cap':       0.0,
    'XTalkLevel':      'L = 1.1%, C = 0.0%',
    
    # Not implemented yet!
    'etBin':           [[15 , 20] ,[20, 30]  ,[30, 40]  ,[40, 50],[50,100],[100,10000000]],
    'etaBin':          [[0.00, 0.80], [0.80, 1.37], [1.37,1.54],[1.54,2.50],[2.50,3.2]],
    'bPhaseSpaceDiv':  False,
}

if not(os.path.isdir(pathLoss)):
    os.mkdir(pathLoss)

startTime=time()

# Dataset
data = TChain('ML_tree')
for ds_index in range(1,30):
    print(ds_index, conditions['XTalk_cap'], conditions['XTalk_ind'], conditions['etaSize'], conditions['phiSize'])
    theFile = '/data/atlas/mhufnage/lzt_single_e/elec_singleCell_10kEvts_centroid_e50_DS{}/AOD_Cap{:.1f}_Ind{:.1f}/user.mhufnage.lzt.e50.xtalk.DS{}.win{}{}.nopileup.WithCells_{}.root'.format(ds_index, conditions['XTalk_cap'], conditions['XTalk_ind'], ds_index, conditions['etaSize'], conditions['phiSize'], conditions['caloSampling'])
    data.Add(theFile+'/ML_tree')
# data            = []
# print('Loading dataset from list: ',DS_List)
# for DS_index in DS_List:
#     data.append (np.load(DS_PathAlias.format(DS_index,DS_index), allow_pickle=True)['dataset'])
# data = np.concatenate(data).ravel()
# print('Dataset loaded with {} events!'.format(len(data)))

#
# Configuration
#

optimAlg                = 'adam'
typeNormalization       = {'electronEnergy': 50.0,
                           'mevToGev': 1/1000,
                           'gevToMev': 1000,
                           'none': 1}#'minMax'

lr                      = 1e-3
weightDecay_adam        = 1e-5

optimConfig             = {
    'adam':{
        'learningRate'  : lr,
        'weightDecay'   : weightDecay_adam,
    },
}

# print(optimConfig[optimAlg])

# trainNeuLists = {'neuLay': [[45,5,5],[45,15,15],[45,30,30]],
#                  } #2layers

# trainNeuLists = {'neuLay': [[45,10],[45,15],[45,20], [45,30], [45,40], [45,50], [45,65],[45,85],[45,100],[45,125],[45,150],[45,175], [45,200]],} # 1layer (24/05/2024)
trainNeuLists = {'neuLay': [[45,5,5],[45,10,10],[45,15,15], [45,20,20], [45,25,25], [45,30,30], [45,40,40],[45,50,50],[45,60,60],[45,70,70],[45,80,80],[45,90,90], [45,100,100]],} # 2layer (24/05/2024)

lossComputationList = ['p']

np.savez(pathLoss+'{}_config.npz'.format(pathLoss.split('/')[-2]),config=trainNeuLists)

for lossMethod in lossComputationList:
    print('Computation method: {}'.format(lossMethod))
    
    for trainNeuList in trainNeuLists['neuLay']:
        range_neuInput          = trainNeuList#[45,20,20]
        range_neuText           = MLP.neuListToFileText(range_neuInput)
        # range_nLayers           = [0]
        # bn_neurons              = 4
        print(range_neuText)

        experiment_alias        = '''
        learning the samples of a 3x3 window from topocluster at EMB2 layer. 
        The loss for training is the input-output map. 
        The cell organization in the window is eta-oriented.
        # phi [ 3 | 6 | 9 ]
        #     [ 2 | 5 | 8 ]  , eta-orientation cluster indexing. where 5 is the index of the hottest cell
        #     [ 1 | 4 | 7 ]
        #             eta
        '''
        totalEvents             = data.GetEntries()#len(data)

        bLoadFullCellData       = False
        nEpochs                 = 100
        lossComputation         = lossMethod
        patience                = 10
        min_delta_loss          = 0

        k                       = 3 # number of folds in kFold split
        nBatch                  = 512 # batch size in clusters
        nIter                   = 3
        nClusters               = conditions['nEvents'] # for training and validation
        # buffer_batches          = 4  # number of batches to being loaded into TTree dataset reading buffer (buffer_batches * nBatch)

        nSamples                = 5 # standard for LAr
        nCellsWinEta            = 3 # dataset selection
        nCellsWinPhi            = 3
        scalerInput             = torch.tensor((typeNormalization['mevToGev'])) # without normalization, set to 1
        scalerOutput            = torch.tensor(typeNormalization['gevToMev'])

        train_ratio             = 1.#0.90 # proportion of data to apply the kFold
        test_ratio              = 0.#0.10 # proportion of data to apply test on trained model

        eventsArray             = torch.as_tensor(np.arange(0, nClusters)) # contiguous indexesfor KFold split

        # if not(os.path.isdir(pathLoss)):
        #     os.mkdir(pathLoss)

        #
        # Dataset split
        #
        total_ene = []
        total_tau = []

        train_config_dict   = {
            'experiment_alias'      : experiment_alias,
            'conditions'            : conditions,
            'patience'              : patience,
            'min_delta_loss'        : min_delta_loss,
            'max_epochs'            : nEpochs,
            'kFolds'                : k,
            'batch_size'            : nBatch,
            'max_init'              : nIter,
            'total_events_dataset'  : totalEvents,
            'total_clusters'        : nClusters,
            'train_ratio'           : train_ratio,
            'test_ratio'            : test_ratio,
            'cell_win_eta'          : nCellsWinEta,
            'cell_win_phi'          : nCellsWinPhi,
            'scaler_input'          : scalerInput,
            'scaler_output'         : scalerOutput,
            'neurons_list'          : range_neuInput,
            'neurons_text'          : range_neuText,
            'lossComputation'       : lossComputation,
            'optimConfig'           : optimConfig,
            # 'buffer_batches'    : buffer_batches,
        }

        # if optimAlg=='adam':
        np.savez(pathLoss+'config_train_nn{}_{}_neu{}.npz'.format(lossComputation, optimAlg, range_neuText), config=train_config_dict)

        #
        # Dataset split
        #

        # X_train, X_test = train_test_split( data[0:nClusters] , test_size=test_ratio)
        # datasetTest     =   XTalkDataset_LZT(X_test, nEvents=-1, mode='supervised')
        
        datasetTrain = XTalkDataset_LZT_FromROOT(data, conditions, mode='supervised', load_full_data=bLoadFullCellData)

        kfold = KFold(n_splits=k, shuffle=True, random_state=1)

        for nFold, (train_fold_idx, valid_fold_idx) in enumerate(kfold.split(eventsArray)):

            # datasetTrain  = XTalkDataset_LZT(X_train[train_fold_idx], nEvents=-1, mode='supervised')
            # datasetValid  = XTalkDataset_LZT(X_train[valid_fold_idx], nEvents=-1, mode='supervised')            
            # train_fold    = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False)
            # val_fold      = DataLoader( datasetValid, batch_size=nBatch, shuffle=False)
            
            train_fold_sampler  = KFoldSampler(train_fold_idx)
            val_fold_sampler    = KFoldSampler(valid_fold_idx)
            
            train_fold    = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, sampler=train_fold_sampler)
            val_fold      = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, sampler=val_fold_sampler)
            
            for it in range(0,nIter):
                print('\t\tFold {}/{}... iter {}/{}'.format(nFold+1,k, it+1, nIter))
                trainTime = time()
                
                train_logging_dict  = {
                    'loss'              : [],
                    'val_loss'          : [],
                    'tau_loss'          : [],
                    'val_tau_loss'      : [],
                    'ene_loss'          : [],
                    'val_ene_loss'      : [],
                    'total_loss'        : [],
                    'total_val_loss'    : [],

                    'best_epoch'    : -1,
                    'train_time'    : -1,
                }
                
                #
                # Model initialization
                #
                model = MLP.MLP_FF(range_neuInput, mlpType='sampleInSampleOut')
                
                if optimAlg=='adam':
                    optimizer = torch.optim.Adam(model.parameters(), 
                                                lr=optimConfig[optimAlg]['learningRate'], 
                                                weight_decay=optimConfig[optimAlg]['weightDecay'])
                
                early_stopper  = MLP.EarlyStopper(patience=patience, min_delta=min_delta_loss)
                
                for epoch in range(nEpochs):
                    #
                    # Training
                    #
                                
                    for data_input, target, batch_idx in train_fold:
                        
                        # pass input through the model and generate the output
                        output      = model(data_input*scalerInput) #normalized (not yet exactly) input
                        
                        #
                        # Loss calculation
                        #
                        
                        # get input and output energy and time
                        ene_target, tau_target    = datasetTrain.calibrate(batch_idx, target) # unormlized target
                        ene_output, tau_output    = datasetTrain.calibrate(batch_idx, output*scalerOutput) # denormalized output
                        
                        # total window loss
                        loss            = model.criterion(output, target*scalerInput) # loss with normalized data target and model output
                        loss_tau        = model.criterion(tau_output , tau_target) # time remains in ns
                        loss_ene        = model.criterion(ene_output*typeNormalization['mevToGev'] , ene_target*typeNormalization['mevToGev']) # scale energy from MeV to GeV
                        
                        #
                        # Backpropagation (optimize weights)
                        #
                        # model.optimizer.zero_grad()
                        optimizer.zero_grad()
                        total_loss      = loss + loss_ene + loss_tau
                        pulse_tau_loss  = loss + loss_tau
                        
                        if lossComputation=='pet':
                            total_loss.backward()
                            
                        elif lossComputation=='p':
                            loss.backward()
                        elif lossComputation=='t':
                            loss_tau.backward()
                        elif lossComputation=='pt':
                            pulse_tau_loss.backward()    
                        else:
                            raise AttributeError("loss computation was not set to any implemented options.")
                        # loss_tau.backward()
                        # model.optimizer.step()
                        optimizer.step()
                    
                    #
                    # Validation
                    #
                    for data_input, target, batch_idx in val_fold:
                        # pass input through the model and generate the output
                        output  = model(data_input*scalerInput)

                        #
                        # Loss calculation
                        #

                        # get input and output energy and time
                        val_ene_target, val_tau_target  = datasetTrain.calibrate(batch_idx, target)# unormlized target
                        val_ene_output, val_tau_output  = datasetTrain.calibrate(batch_idx, output*scalerOutput)# denormalized output

                        val_loss            = model.criterion(output, target*scalerInput) #normalized target
                        val_loss_tau        = model.criterion(val_tau_output , val_tau_target)
                        val_loss_ene        = model.criterion(val_ene_output*typeNormalization['mevToGev'] , val_ene_target*typeNormalization['mevToGev'])
                        
                        total_val_loss = val_loss + val_loss_ene + val_loss_tau
                        val_pulse_tau_loss = val_loss + val_loss_tau

                    #
                    # End of the current Epoch
                    #
                    if (epoch%1)==0:                
                        print('\t\t\tEpoch {}/{}...loss={:.5f}, val_loss={:.5f} | tau_loss={:.5f} | ene_loss={:.5f} | total_loss={:.5f}, total_val_loss={:.5f} '.format(epoch,nEpochs,loss.detach().numpy(), val_loss, loss_tau, loss_ene, total_loss, total_val_loss))
            
                    train_logging_dict['loss'].append(loss.detach().numpy())
                    train_logging_dict['ene_loss'].append(loss_ene.detach().numpy())
                    train_logging_dict['tau_loss'].append(loss_tau.detach().numpy())
                    train_logging_dict['val_loss'].append(val_loss.detach().numpy())
                    train_logging_dict['val_ene_loss'].append(val_loss_ene.detach().numpy())
                    train_logging_dict['val_tau_loss'].append(val_loss_tau.detach().numpy())
                    train_logging_dict['total_loss'].append(total_loss.detach().numpy())
                    train_logging_dict['total_val_loss'].append(total_val_loss.detach().numpy())
                    
                    #
                    # Early Stop criteria
                    #
                    bestModelPath   = pathLoss+'bestModel_nn{}_fold{}_iter{}_neu{}.pth'.format(lossComputation,nFold, it, range_neuText)
                    
                    if lossComputation=='pet':
                        bStop           = early_stopper.early_stop(total_val_loss, epoch, model, optimizer, bestModelPath)
                    elif lossComputation=='p':
                        bStop           = early_stopper.early_stop(val_loss, epoch, model, optimizer, bestModelPath)
                    elif lossComputation=='t':
                        bStop           = early_stopper.early_stop(val_loss_tau, epoch, model, optimizer, bestModelPath)
                    elif lossComputation=='pt':
                        bStop           = early_stopper.early_stop(val_pulse_tau_loss, epoch, model, optimizer, bestModelPath)
                    else:
                        raise AttributeError("select a loss computation method.")
                    
                    if bStop:
                        train_logging_dict['best_epoch'] = early_stopper.bestEpoch
                        
                        if lossComputation=='pet':
                            print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(total_val_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
                            MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, total_val_loss) # save the model
                            break
                        elif lossComputation=='p':
                            print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
                            MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, val_loss) # save the model
                            break
                        elif lossComputation=='t':
                            print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_loss_tau, early_stopper.min_validation_loss, early_stopper.bestEpoch))
                            MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, val_loss_tau) # save the model
                            break
                        elif lossComputation=='pt':
                            print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_pulse_tau_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
                            MLP.checkpoint(early_stopper.tmpModel, optimizer, bestModelPath, epoch, val_pulse_tau_loss) # save the model
                            break
                        else:
                            raise AttributeError("select a loss computation method.")
                    else:
                        train_logging_dict['best_epoch'] = epoch
                    
                #
                # End of training at current fold-iter
                #
                MLP.checkpoint(model, optimizer, bestModelPath, epoch, val_loss) # save model in case Early_Stop didn't happen
                
                thisTrainTime = time() - trainTime
                train_logging_dict['train_time'] = thisTrainTime
                np.savez(pathLoss+'loss_nn{}_fold{}_iter{}_neu{}_batch{}.npz'.format(lossComputation,nFold, it, range_neuText, nBatch) ,loss=train_logging_dict)


endTime=time()

print('Total time: {:.2f} min ({:.2f} s)'.format((endTime-startTime)/60 , endTime-startTime))







