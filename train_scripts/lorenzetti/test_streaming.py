# Lorenzetti Showers Collaboration 2020 - 2023 - https://doi.org/10.1016/j.cpc.2023.108671
# %load_ext autoreload
# %autoreload 2

import sys
sys.path.insert(0,'/home/mhufnage/lorenzetti/Applications') #lorenzetti utils path

## Insert here the path to xtalkDatasetAnalysis git package
sys.path.insert(1, '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/')

uprootLibrary = 'np'#'np' #'ak'

import json
import os
import time
import shelve

import numpy as np
from glob import glob
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as mcolors
import matplotlib
# matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS
matplotlib.use('module://ipykernel.pylab.backend_inline')  # show plots

import scipy.stats as scipy
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader

from collections import OrderedDict
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score

import mplhep as hep

plt.style.use([hep.style.ATLAS])

import uproot

# Custom lib
from helper_lib.calibrationFilesHelper import *
from helper_lib.auxiliaryFunctions import confInterval, getBins
from helper_lib.functionsHelper import stdVecToArray
from ML_lib.Autoencoder import *
import ML_lib.MLP as MLP
from ML_lib.XTalkDatasetClass import XTalkDataset_LZT, Subset

# %load_ext autoreload
# %autoreload 2

import ROOT
from ROOT import TFile, gROOT, TChain, TTree

from ML_lib.XTalkDatasetClass import XTalkStreamingDataLoader_LZT, XTalkStreamingDataLoader_LZT_FoldSplit, XTalkStreamingDataLoader_LZT_FoldSplit_Faster
from lorenzetti_reader import *

import awkward as ak
from array import array


# Enable Multi-threaded mode
ROOT.EnableImplicitMT()

ds_index = 1
theFile = '/data/atlas/mhufnage/lzt_single_e/elec_singleCell_10kEvts_centroid_e50_DS{}/AOD_Cap{:.1f}_Ind{:.1f}/user.mhufnage.lzt.e50.xtalk.DS{}.win{}{}.nopileup.WithCells_EM2.root'.format(ds_index, 4.2, 2.3, ds_index, 3, 3)
print(theFile)

df = ROOT.RDataFrame('ML_tree', theFile)

print(df)

# from calibrationFilesHelper import *
# from Autoencoder import *
# from XTalkDatasetClass import XTalkDataset_LZT, Subset
# from auxiliaryFunctions import confInterval, getBins

# # --------- Lorenzetti plot style -------
# def lorenzettiText(pos='out',subText=' Internal'):
#     lorenzetti_title={'fontsize': 21, 'fontfamily': 'sans-serif', 'fontweight':'bold','fontstyle':'italic'}
#     lorenzetti_subtitle={'fontsize': 14, 'fontfamily': 'sans-serif', 'fontweight':'normal','fontstyle':'italic'}

#     if pos=='out':
#         plt.figtext(x=.18   ,y=.945,s='  Lorenzetti',fontdict=lorenzetti_title)
#         plt.figtext(x=.318  ,y=.945,s=subText,fontdict=lorenzetti_subtitle)
#     if pos=='in':
#         plt.figtext(x=.18   ,y=.875,s='  Lorenzetti',fontdict=lorenzetti_title)
#         plt.figtext(x=.318  ,y=.875,s=subText,fontdict=lorenzetti_subtitle)


# # --------- Lorenzetti sampling and detector enumeration ---------
# class Detector():
#   LAR     = 0
#   TILE    = 1
#   TTEM    = 2
#   TTHEC   = 3
#   FCALEM  = 5
#   FCALHAD = 6

# class CaloSampling():
#     PSB       = 0
#     PSE       = 1
#     EMB1      = 2
#     EMB2      = 3
#     EMB3      = 4
#     TileCal1  = 5
#     TileCal2  = 6
#     TileCal3  = 7
#     TileExt1  = 8
#     TileExt2  = 9
#     TileExt3  = 10
#     EMEC1     = 11
#     EMEC2     = 12
#     EMEC3     = 13
#     HEC1      = 14
#     HEC2      = 15
#     HEC3      = 16

# #***********************************************************************

# def test_nparray():
#     new_tree = TChain('ML_tree')
#     theFile = '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/newFile.root'
#     new_tree.Add(theFile+'/ML_tree')

#     buff_pulse      =[]
#     buff_xtpulse    =[]
#     buff_e          =[]
#     buff_xte        =[]
#     buff_t          =[]
#     buff_xtt        =[]
#     buff_edep       =[]
#     buff_tof        =[]
#     buff_eta        =[]
#     buff_phi        =[]
#     buff_hash       =[]

#     normE = 1.0

#     for n in range(1):
#         new_tree.GetEntry(n)

#         buff_pulse      .append( torch.tensor(new_tree.cell_pulse, dtype=torch.float, requires_grad=True)*normE     )
#         buff_xtpulse    .append( torch.tensor(new_tree.xtcell_pulse, dtype=torch.float, requires_grad=True)*normE   )
#         buff_e          .append( np.array(new_tree.cell_e)*normE         )
#         buff_xte        .append( np.array(new_tree.xtcell_e)*normE       )
#         buff_t          .append( np.array(new_tree.cell_tau)       )
#         buff_xtt        .append( np.array(new_tree.xtcell_tau)     )
#         buff_edep       .append( np.array(new_tree.cell_edep)      )
#         buff_tof        .append( np.array(new_tree.cell_tof)       )
#         buff_eta        .append( np.array(new_tree.cell_eta)       )
#         buff_phi        .append( np.array(new_tree.cell_phi)       )
#         buff_hash       .append( np.array(new_tree.cell_hash)      )

# def test_stdVec():
#     new_tree = TChain('ML_tree')
#     theFile = '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/newFile.root'
#     new_tree.Add(theFile+'/ML_tree')
    
#     buff_pulse      =[]
#     buff_xtpulse    =[]
#     buff_e          =[]
#     buff_xte        =[]
#     buff_t          =[]
#     buff_xtt        =[]
#     buff_edep       =[]
#     buff_tof        =[]
#     buff_eta        =[]
#     buff_phi        =[]
#     buff_hash       =[]

#     normE = 1.0

#     for n in range(1):
#         new_tree.GetEntry(n)

#         buff_pulse      .append( torch.tensor(new_tree.cell_pulse, dtype=torch.float, requires_grad=True)*normE     )
#         buff_xtpulse    .append( torch.tensor(new_tree.xtcell_pulse, dtype=torch.float, requires_grad=True)*normE   )
#         buff_e          .append( stdVecToArray(new_tree.cell_e)*normE         )
#         buff_xte        .append( stdVecToArray(new_tree.xtcell_e)*normE       )
#         buff_t          .append( stdVecToArray(new_tree.cell_tau)       )
#         buff_xtt        .append( stdVecToArray(new_tree.xtcell_tau)     )
#         buff_edep       .append( stdVecToArray(new_tree.cell_edep)      )
#         buff_tof        .append( stdVecToArray(new_tree.cell_tof)       )
#         buff_eta        .append( stdVecToArray(new_tree.cell_eta)       )
#         buff_phi        .append( stdVecToArray(new_tree.cell_phi)       )
#         buff_hash       .append( stdVecToArray(new_tree.cell_hash)      )

# %timeit -n10 test_nparray()
# %timeit -n10 test_stdVec()



# #**************************************************************************************


# # import sys
# # sys.path.insert(0,'/home/mhufnage/lorenzetti/Applications') #lorenzetti utils path

# # ## Insert here the path to xtalkDatasetAnalysis git package
# # sys.path.insert(1, '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/')

# # import numpy as np
# # from glob import glob

# # import torch
# # import torch.nn as nn
# # from torch.utils.data import Dataset, DataLoader

# # from sklearn.model_selection import KFold
# # from sklearn.model_selection import train_test_split

# # from ROOT import TFile, gROOT, TChain

# # from ML_lib.XTalkDatasetClass import XTalkStreamingDataLoader_LZT_FoldSplit
# # from lorenzetti_reader import *

# # # ******************************

# # datasetPath     = '/data/atlas/mhufnage/lzt_single_e/'
# # datasets        = glob(datasetPath+'elec_singleCell_10kEvts_centroid_e50*/AOD/electron.AOD.root')
# # pathLoss        = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/sampleInSampleOut/'

# # conditions = {
# #     'nEvents':         40, # <--- it is ignored in case of using KFold split.
# #     'SigmaCut':        1,
# #     'etaCut':          1.4,
# #     'caloSampling':    CaloSampling.EMB2,
# #     'etaSize':         5,
# #     'phiSize':         5,
# #     'nSamples':        5,
# #     'dumpCells':       True,
# #     'bDumpWindowOnly': True,
# #     'bNoEneCut':       True,
# #     'XTalkLevel':      'L = 1.1%, C = 0.0%',
    
# #     # Not implemented yet!
# #     'etBin':           [[15 , 20] ,[20, 30]  ,[30, 40]  ,[40, 50],[50,100],[100,10000000]],
# #     'etaBin':          [[0.00, 0.80], [0.80, 1.37], [1.37,1.54],[1.54,2.50],[2.50,3.2]],
# #     'bPhaseSpaceDiv':  False,
# # }

# # print(len(datasets))

# # sTree   = TChain("physics",'')
# # for file in datasets:
# #     sTree.Add(file+"/physics") # main dumped data tree (event by event)

# # print(sTree.GetEntries())


# # batch_size      = 256
# # buffer_batches  = 10 # number of batches to buffer
# # max_epochs      = 1
# # test_ratio      = 0.1
# # k_folds         = 3


# # # myDatasetStream = XTalkStreamingDataLoader_LZT_test(
# # #     sTree,
# # #     conditions,
# # #     batch_size,
# # #     buffer_batches)


# # # X_train, X_test = train_test_split( np.arange(sTree.GetEntries()) ,test_size=test_ratio)
# # X_train, X_test = train_test_split( np.arange(100) ,test_size=test_ratio)

# # kfold = KFold(n_splits=k_folds, shuffle=True, random_state=1)

# # for nFold, (train_fold_idx, valid_fold_idx) in enumerate(kfold.split(X_train)):
# #     print('Fold {}'.format(nFold))
    
# #     # print('train fold', train_fold_idx)
# #     # print('valid fold', valid_fold_idx)
    
# #     print('Loading first dataset buffers...')

# #     myDatasetStream_train = XTalkStreamingDataLoader_LZT_FoldSplit(
# #         sTree,
# #         conditions,
# #         train_fold_idx,
# #         batch_size,
# #         buffer_batches)
    
# #     print('Train data first buffer loaded!')
    
# #     myDatasetStream_valid = XTalkStreamingDataLoader_LZT_FoldSplit(
# #         sTree,
# #         conditions,
# #         valid_fold_idx,
# #         batch_size,
# #         buffer_batches)
    
# #     print('Validation data first buffer loaded!')


# #     for epoch in range(max_epochs):
# #         print('epoch {}'.format(str(epoch)))
        
# #         print('TRAINING')        
# #         for x, y, batch_idx in myDatasetStream_train:
# #             print("batch idx: {}".format(batch_idx))
# #     #         for ind, (x_i, y_i) in enumerate(zip(x,y)):
# #     #             print('x_{}: '.format(ind), len(x_i))
# #     #             print('y_{}: '.format(ind), len(y_i))
                
# #     #         ene, tau = myDatasetStream_train.calibrate(batch_idx, y)
# #     #         print('ene: ', myDatasetStream_train.e)
# #     #         print('Reco_ene: ', ene)
# #     #         print('time: ', myDatasetStream_train.t)
# #     #         print('Reco_time: ', tau)
            

# #         print(' VALIDATION')
        
# #         for x, y, batch_idx in myDatasetStream_valid:
# #             print("batch idx: {}".format(batch_idx))
# #     #         for ind, (x_i, y_i) in enumerate(zip(x,y)):
# #     #             print('x_{}: '.format(ind), len(x_i))
# #     #             print('y_{}: '.format(ind), len(y_i))
                
# #     #         ene, tau = myDatasetStream_valid.calibrate(batch_idx, y)
# #     #         print('ene: ', myDatasetStream_valid.e)
# #     #         print('Reco_ene: ', ene)
# #     #         print('time: ', myDatasetStream_valid.t)
# #     #         print('Reco_time: ', tau)
        