#!/bin/bash

shopt -s expand_aliases
source /home/mhufnage/.bashrc

folderName="/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/loop_model_2layer_33_10folds/"
nIter=10
nFolds=10
nEpochs=500
nEvents=50000

# array of neuros as strings (1 layer)
declare -a neu=("45-10-10" "45-20-20" "45-45-45" "45-60-60" "45-75-75" "45-100-100" "45-120-120" "45-150-150" "45-200-200" "45-250-250")


for modelSize in "${neu[@]}"
do

    echo ""
    savingPath=$folderName"$modelSize-45/"
    
    # if [ -z "$STY" ]; then exec screen -dm -S $modelSize /bin/bash "pyenv;setupROOT;python3 run_multicore_hogwild.py --mp --nIter $nIter --nEpochs $nEpochs --nEvents $nEvents --num-processes 1 --kFolds $nFolds --path $savingPath --neuronsList \"$modelSize\""; fi
    # pyenv;setupROOT;python3 run_multicore_hogwild.py --mp --nIter $nIter --nEpochs $nEpochs --nEvents $nEvents --num-processes 1 --kFolds $nFolds --path $savingPath --neuronsList \"$modelSize\"
    # screen -s "/bin/bash" -dmS $modelSize; screen -S $modelSize -X ;pyenv;setupROOT;python3 run_multicore_hogwild.py --mp --nIter $nIter --nEpochs $nEpochs --nEvents $nEvents --num-processes 1 --kFolds $nFolds --path $savingPath --neuronsList \"$modelSize\"

    # screen -t "pwd" bash -c source /home/mhufnage/.bashrc;pyenv;setupROOT;python3 run_multicore_hogwild.py --mp --nIter $nIter --nEpochs $nEpochs --nEvents $nEvents --num-processes 1 --kFolds $nFolds --path $savingPath --neuronsList $modelSize; exec bash

    echo "pyenv;setupROOT;python3 run_multicore_hogwild.py --mp --nIter $nIter --nEpochs $nEpochs --nEvents $nEvents --num-processes 1 --kFolds $nFolds --path $savingPath --neuronsList $modelSize"
 
done

# screen -list
 