# Lorenzetti Showers Collaboration 2020 - 2023 - https://doi.org/10.1016/j.cpc.2023.108671
print('Starting script for neuron/layers space search...')
import sys
sys.path.insert(0,'/home/mhufnage/lorenzetti/Applications') #lorenzetti utils path

## Insert here the path to xtalkDatasetAnalysis git package
sys.path.insert(1, '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/')

uprootLibrary = 'np'#'np' #'ak'

import json
import os
import time
import shelve

import numpy as np
from glob import glob
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as mcolors
import matplotlib
# matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS

import scipy.stats as scipy
import torch
import torch.nn as nn
import MLP
from torch.utils.data import Dataset, DataLoader, DistributedSampler
import torch.multiprocessing as mp

torch.multiprocessing.set_sharing_strategy('file_system')

from collections import OrderedDict
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score

import mplhep as hep

plt.style.use([hep.style.ATLAS])

import uproot

from calibrationFilesHelper import *
from Autoencoder import *
from XTalkDatasetClass import XTalkDataset_LZT, Subset
from auxiliaryFunctions import confInterval, getBins

from train_multicore import trainMLP, trainMLP_multicore

# --------- Lorenzetti plot style -------
def lorenzettiText(pos='out',subText=' Internal'):
    lorenzetti_title={'fontsize': 21, 'fontfamily': 'sans-serif', 'fontweight':'bold','fontstyle':'italic'}
    lorenzetti_subtitle={'fontsize': 14, 'fontfamily': 'sans-serif', 'fontweight':'normal','fontstyle':'italic'}

    if pos=='out':
        plt.figtext(x=.18   ,y=.945,s='Lorenzetti',fontdict=lorenzetti_title)
        plt.figtext(x=.318  ,y=.945,s=subText,fontdict=lorenzetti_subtitle)
    if pos=='in':
        plt.figtext(x=.18   ,y=.875,s='Lorenzetti',fontdict=lorenzetti_title)
        plt.figtext(x=.318  ,y=.875,s=subText,fontdict=lorenzetti_subtitle)


# --------- Lorenzetti sampling and detector enumeration ---------
class Detector():
  LAR     = 0
  TILE    = 1
  TTEM    = 2
  TTHEC   = 3
  FCALEM  = 5
  FCALHAD = 6

class CaloSampling():
    PSB       = 0
    PSE       = 1
    EMB1      = 2
    EMB2      = 3
    EMB3      = 4
    TileCal1  = 5
    TileCal2  = 6
    TileCal3  = 7
    TileExt1  = 8
    TileExt2  = 9
    TileExt3  = 10
    EMEC1     = 11
    EMEC2     = 12
    EMEC3     = 13
    HEC1      = 14
    HEC2      = 15
    HEC3      = 16
    
# -----------------------------------------------------------------
# -----------------------------------------------------------------
# -----------------------------------------------------------------
## Path to experiment output

if __name__ == '__main__':
    
    # pathLoss      = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/neuLay_1Layer_finer/'
    pathLoss      = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/testMulticore/'
    # DS_Lim        = 9
    DS_List       = [1, 2, 3, 4]#, 5]#, 6, 7, 8, 9, 10]#, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ]
    DS_test       = [21]
    DS_PathAlias  = '/data/atlas/mhufnage/lzt_single_e/elec_singleCell_10kEvts_centroid_e50_DS{}/AOD/user.mhufnage.lzt.e50.xtalk.DS{}.win3.nopileup.WithCells_EM2.event.npz'

    if not(os.path.isdir(pathLoss)):
        os.mkdir(pathLoss)

    startTime=time.time()
    # Dataset

    data            = []
    print('Loading dataset from list: ',DS_List)
    for DS_index in DS_List:
        data.append (np.load(DS_PathAlias.format(DS_index,DS_index), allow_pickle=True)['dataset'])
        print('\tloaded ds {}...'.format(DS_index))
    data = np.concatenate(data).ravel()
    print('Dataset loaded with {} events!'.format(len(data)))

    optimAlg                = 'adam'
    typeNormalization       = {'electronEnergy': 50.0,
                            'mevToGev': 1/1000,
                            'gevToMev': 1000,
                            'none': 1}#'minMax'

    lr                      = 1e-3
    weightDecay_adam        = 1e-5

    optimConfig             = {
        'adam':{
            'learningRate'  : lr,
            'weightDecay'   : weightDecay_adam,
        },
    }

    print(optimConfig[optimAlg])

    # trainNeuLists = {'neuLay': [[45,5,5],[45,15,15],[45,30,30]],
    #                  } #2layers
    # trainNeuLists = {'neuLay': [[45,5,5],[45,30,30],[45,60,60], [45,75,75]],
    #                  } # 1layer
    # trainNeuLists = {'neuLay': [[45,10],[45,15],[45,20], [45,25], [45,35], [45,40], [45,45], [45,50], [45,55], [45,65],[45,70],[45,75]],} # 1layer_finer
    # trainNeuLists = {'neuLay': [[45,75],[45,90],[45,100],[45,120],[45,150],[45,200]],} # 1layer_finer
    trainNeuLists = {'neuLay': [[45,15]],} # 1layer_finer

    lossComputationList = ['p']

    np.savez(pathLoss+'{}_config.npz'.format(pathLoss.split('/')[-2]),config=trainNeuLists)

    for lossMethod in lossComputationList:
        print('Computation method: {}'.format(lossMethod))
        
        for trainNeuList in trainNeuLists['neuLay']:
            range_neuInput          = trainNeuList#[45,20,20]
            range_neuText           = MLP.neuListToFileText(range_neuInput)
            print(range_neuText)

            experiment_alias        = '''
            learning the samples of a 3x3 window from topocluster at EMB2 layer. 
            The loss for training is the input-output map. 
            The cell organization in the window is eta-oriented.
            # phi [ 3 | 6 | 9 ]
            #     [ 2 | 5 | 8 ]  , eta-orientation cluster indexing. where 5 is the index of the hottest cell
            #     [ 1 | 4 | 7 ]
            #             eta
            '''
            bParalellData           = False
            num_processes           = 4
            num_threads             = 2
            num_workers             = 0
            
            totalEvents             = len(data)

            nEpochs                 = 50
            lossComputation         = lossMethod ## can be 'p'=pulse loss, 'pet'=pulse+energy+time losses
            patience                = 15
            min_delta_loss          = 0

            k                       = 3 # number of folds in kFold split
            nBatch                  = 128 # batch size in clusters
            nIter                   = 1
            nClusters               = -1 # for training, validation and test

            nSamples                = 5 # standard for LAr
            nCellsWinEta            = 3 # dataset selection
            nCellsWinPhi            = 3
            scalerInput             = torch.tensor((typeNormalization['mevToGev'])) # without normalization, set to 1
            scalerOutput            = torch.tensor(typeNormalization['gevToMev'])

            train_ratio             = 0.90 # proportion of data to apply the kFold
            test_ratio              = 0.10 # proportion of data to apply test on trained model

            #
            # Dataset split
            #
            total_ene = []
            total_tau = []

            train_config_dict   = {
                'experiment_alias'  : experiment_alias,
                'patience'          : patience,
                'min_delta_loss'    : min_delta_loss,
                'max_epochs'        : nEpochs,
                'kFolds'            : k,
                'batch_size'        : nBatch,
                'max_init'          : nIter,
                'total_clusters'    : totalEvents,
                'train_ratio'       : train_ratio,
                'test_ratio'        : test_ratio,
                'cell_win_eta'      : nCellsWinEta,
                'cell_win_phi'      : nCellsWinPhi,
                'scaler_input'      : scalerInput,
                'scaler_output'     : scalerOutput,
                'neurons_list'      : range_neuInput,
                'neurons_text'      : range_neuText,
                'lossComputation'   : lossComputation,
                'nSamples'          : nSamples,
                'optimConfig'       : optimConfig,
                'typeNormalization' : typeNormalization,
                'path_loss'         : pathLoss,
                'num_processes'     : num_processes,
                'num_threads'       : num_threads,
            }

            # if optimAlg=='adam':
            np.savez(pathLoss+'config_train_nn{}_{}_neu{}.npz'.format(lossComputation, optimAlg, range_neuText), config=train_config_dict)

            X_train, X_test = train_test_split( data[0:nClusters] , test_size=test_ratio)

            datasetTest     =   XTalkDataset_LZT(X_test, nEvents=-1, mode='supervised')

            kfold = KFold(n_splits=k, shuffle=True, random_state=1)

            for nFold, (train_fold_idx, valid_fold_idx) in enumerate(kfold.split(X_train)):

                datasetTrain  = XTalkDataset_LZT(X_train[train_fold_idx], nEvents=-1, mode='supervised')
                datasetValid  = XTalkDataset_LZT(X_train[valid_fold_idx], nEvents=-1, mode='supervised')
                
                # train_fold    = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, num_workers=0, pin_memory=False)
                # val_fold      = DataLoader( datasetValid, batch_size=nBatch, shuffle=False, num_workers=0, pin_memory=False)
                
                # train_config_dict.update({
                #     'train_fold'    : train_fold,
                #     'val_fold'      : val_fold,
                #     'current_fold'  : nFold,
                # })
                
                for it in range(0,nIter):
                    print('\t\tFold {}/{}... iter {}/{}'.format(nFold+1,k, it+1, nIter))
                    
                    # train_config_dict.update({
                    #     'current_iter'  : it,
                    # })
                    
                    train_logging_dict  = {
                        'loss'              : [],
                        'val_loss'          : [],
                        'tau_loss'          : [],
                        'val_tau_loss'      : [],
                        'ene_loss'          : [],
                        'val_ene_loss'      : [],
                        'total_loss'        : [],
                        'total_val_loss'    : [],
                        'epoch'             : [],

                        'best_epoch'    : -1,
                    }
                    
                    #
                    # Model initialization
                    #
                    model = MLP.MLP_FF(range_neuInput, mlpType='sampleInSampleOut')
                    
                    if optimAlg=='adam':
                        optimizer = torch.optim.Adam(
                            model.parameters(), 
                            lr=optimConfig[optimAlg]['learningRate'], 
                            weight_decay=optimConfig[optimAlg]['weightDecay'])
                
                    early_stopper   = MLP.EarlyStopper(patience=patience, min_delta=min_delta_loss)
                    
                    
                    if bParalellData:
                        ## Multiprocess configuration for training ##
                        device = torch.device("cpu")
                        # torch.set_num_threads(int(num_threads))
                        torch.manual_seed(1)
                        mp.set_start_method('spawn', force=True)
        
                        model = model.to(device)
                        model.share_memory() # gradients are allocated lazily, so they are not shared here
                        
                        processes = []
                        
                        for rank in range(num_processes):
                            
                            train_fold    = DataLoader(dataset=datasetTrain, 
                                                    sampler=DistributedSampler(dataset=datasetTrain, 
                                                                                num_replicas=num_processes, 
                                                                                rank=rank),
                                                    batch_size=nBatch,                                                    
                                                    shuffle=False, 
                                                    num_workers=num_workers, 
                                                    pin_memory=False)
                            val_fold      = DataLoader(dataset=datasetValid, 
                                                    sampler=DistributedSampler(dataset=datasetValid, 
                                                                                num_replicas=num_processes, 
                                                                                rank=rank),
                                                    batch_size=nBatch,                                                    
                                                    shuffle=False, 
                                                    num_workers=num_workers, 
                                                    pin_memory=False)
                            
                            train_config_dict.update({
                                'train_fold'    : train_fold,
                                'val_fold'      : val_fold,
                                'current_fold'  : nFold,
                                'current_iter'  : it,
                            })
                            
                            p = mp.Process(target=trainMLP_multicore, args=(rank, 
                                                                model, 
                                                                optimizer, 
                                                                early_stopper,
                                                                device,
                                                                datasetTrain,
                                                                datasetValid,
                                                                train_logging_dict,
                                                                train_config_dict))
                            # We first train the model across `num_processes` processes
                            p.start()
                            processes.append(p)
                            
                        for p in processes:
                            p.join()
                        ## **************************************** ##
                    
                    else: # no multicore strategy
                        train_fold    = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, num_workers=num_workers, pin_memory=False)
                        val_fold      = DataLoader( datasetValid, batch_size=nBatch, shuffle=False, num_workers=num_workers, pin_memory=False)
                        train_config_dict.update({
                                'train_fold'    : train_fold,
                                'val_fold'      : val_fold,
                                'current_fold'  : nFold,
                                'current_iter'  : it,
                            })
                        
                        trainMLP(model, 
                                optimizer, 
                                early_stopper,
                                datasetTrain,
                                datasetValid,
                                train_logging_dict,
                                train_config_dict)
                    
                    #
                    # End of training at current fold-iter
                    #



    endTime=time.time()

    print('Total time: {:.2f} min ({:.2f} s)'.format((endTime-startTime)/60 , endTime-startTime))
