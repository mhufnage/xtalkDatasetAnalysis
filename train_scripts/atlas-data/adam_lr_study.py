# try:
#     !pip install fxpmath
# except:
#     pass
# !source /eos/user/m/mhufnage/.venv/bin/activate
# from IPython.display import clear_output
#
# Notebook runs withou the need of root --notebook server initialization.
# It only needs a python env. with the required packages.

# import ROOT

import json
import os,sys
import time
import shelve

## Insert here the path to EventReader/share
eventReaderSharePath = '/home/mhufnage/ATLAS_QT_XTALK/offline-ringer-dev/crosstalk/EventReader/share/'
sys.path.insert(1, eventReaderSharePath) 

## Insert here the path to xtalkDatasetAnalysis git package
sys.path.insert(1, '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/')


import numpy as np
import awkward as ak
import uproot
import mplhep as hep

from glob import glob
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as mcolors
import matplotlib
plt.style.use([hep.style.ATLAS])
# from fxpmath import Fxp

# import mpl_scatter_density
import scipy.stats as scipy
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader

from collections import OrderedDict
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split

# import matplotlib.rcsetup as rcsetup
# print(rcsetup.all_backends)
# print(matplotlib.get_backend())
# matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS

## Custom Packages
from auxiliaryFunctions import *
from calibrationFilesHelper import *
# from getXTDataAsPythonDict import *
from Autoencoder import *
from XTalkDatasetClass import *
# from calibrationFilesHelperROOT import *

## Load custom Help libraries
fDict       = open(eventReaderSharePath+'dictCaloByLayer.json')
caloDict    = json.load(fDict)







lr_adam                 = [1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6]
weightDecay_adam        = 1e-5

optimAlg                = 'adam'

for lr in lr_adam:
    optimConfig             = {
        'adam':{
            'learningRate'  : lr,
            'weightDecay'   : weightDecay_adam,
        },
    }

    print(optimConfig[optimAlg])

    range_neuInput          = [24]
    range_nLayers           = [1]
    bn_neurons              = 8

    experiment_alias        = 'learning the samples of a 3x3 window from topocluster at EMB2 layer. The loss for training is the input-output map'
    pathLoss                = '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/ml_training/autoencoder/learningRate/'+optimAlg+'_fullDataset/'
    datasetPath             = '/data/atlas/mhufnage/xtalk/dataset/'
    ptCut                   = 0 #GeV

    nEpochs                 = 100
    patience                = 15
    min_delta_loss          = 1e-7

    k                       = 3 # number of folds in kFold split
    nBatch                  = 64 # batch size in clusters
    nIter                   = 1
    nClusters               = -1 # for training, validation and test

    bTrainWithPed           = False
    nSamples                = 4 # standard for LAr
    nCellsWinEta            = 3
    nCellsWinPhi            = 3
    scalerInput             = 1#1/(2**12-1)
    scalerOutput            = 1#2**12-1

    for nLay in range_nLayers:
        print('nLay: {}'.format(nLay))
        for neuIn in range_neuInput:
            print('\tneuInput: {}'.format(neuIn))

            inputSize               = nSamples * nCellsWinEta * nCellsWinPhi
            nNeuronsInput           = neuIn#256
            nLayers                 = nLay#4
            neuronsEnc, neuronsDec  = getAutoencoderNeuronsList(inputSize, nNeuronsInput, nLayers)

            train_ratio         = 0.90 # proportion of data to apply the kFold
            test_ratio          = 0.10 # proportion of data to apply test on trained model
            # validation_ratio    = 0.10

            train_config_dict   = {
                        'experiment_alias'  : experiment_alias,
                        'patience'          : patience,
                        'min_delta_loss'    : min_delta_loss,
                        'max_epochs'        : nEpochs,
                        'kFolds'            : k,
                        'batch_size'        : nBatch,
                        'max_init'          : nIter,
                        'total_clusters'    : nClusters,
                        'train_ratio'       : train_ratio,
                        'test_ratio'        : test_ratio,
                        'cell_win_eta'      : nCellsWinEta,
                        'cell_win_phi'      : nCellsWinPhi,
                        'scaler_input'      : scalerInput,
                        'scaler_output'     : scalerOutput,
                        'neurons_encoder'   : neuronsEnc,
                        'neurons_decoder'   : neuronsDec,
                        'nNeuronsInput'     : nNeuronsInput,
                        'bn_neurons'        : bn_neurons,
                        'nLayers'           : nLayers,
                        'optimConfig'       : optimConfig[optimAlg],
                    }
            if optimAlg=='adam':
                np.savez(pathLoss+'config_train_{}_lr{}_wd{}.npz'.format(optimAlg, optimConfig[optimAlg]['learningRate'], optimConfig[optimAlg]['weightDecay']) ,config=train_config_dict)

            #
            # Data load
            #
            datasetDict = np.load(glob(datasetPath+'dataset_singlElec_ptCut{}_topocluster_sampWin{}-{}_nClus*.npz'.format(ptCut, nCellsWinEta, nCellsWinPhi))[0],allow_pickle=True)['dataset'][0:nClusters]
            confDict    = np.load(glob(datasetPath+'datasetInfo_singlElec_ptCut{}_topocluster_sampWin{}-{}_nClus*.npz'.format(ptCut, nCellsWinEta, nCellsWinPhi))[0],allow_pickle=True)['datasetInfo'].tolist()

            #
            # Dataset split
            #
            X_train, X_test = train_test_split(datasetDict, test_size=test_ratio)
            # X_train, X_valid = train_test_split(X_train, test_size=validation_ratio/(train_ratio+test_ratio))

            datasetTrain    = XTalkDataset(X_train, confDict, nEvents=-1, bRemovePed=not(bTrainWithPed))
            datasetTest     = XTalkDataset(X_test, confDict,  nEvents=-1, bRemovePed=not(bTrainWithPed))

            # Must add the kFold splitting
            kfold = KFold(n_splits=k, shuffle=True, random_state=1)

            nFold = 0

            print('\tnClusters={}'.format(len(datasetDict)))
            for train_fold_idx, valid_fold_idx in kfold.split(datasetTrain): 
                #
                # Mount DataLoader with fold indices
                #
                train_fold    = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, sampler=torch.utils.data.SubsetRandomSampler(indices=train_fold_idx))
                val_fold      = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, sampler=torch.utils.data.SubsetRandomSampler(indices=valid_fold_idx))

                for it in range(0,nIter):
                    print('\t\tFold {}/{}... iter {}/{}'.format(nFold+1,k, it+1, nIter))

                    #
                    # Model initialization
                    #
                    # model               = Autoencoder(nCellsEta=nCellsWinEta, nCellsPhi=nCellsWinPhi, neu1=nNeuronsInput)
                    model               = Autoencoder_Loop(encoderList=neuronsEnc, bottleNeck=bn_neurons,decoderList=neuronsDec, inScaler=scalerInput, outScaler=scalerOutput)
                    if optimAlg=='adam':
                        optimizer = torch.optim.Adam(model.parameters(), lr=optimConfig[optimAlg]['learningRate'], weight_decay=optimConfig[optimAlg]['weightDecay'])

                    train_logging_dict  = {
                        'loss'              : [],
                        'val_loss'          : [],
                        'tau_loss'          : [],
                        'wTauEneLoss'       : [],
                        'val_tau_loss'      : [],
                        'ene_loss'          : [],
                        'val_ene_loss'      : [],
                        'val_wTauEneLoss'   : [],
                        'total_loss'        : [],

                        'sig_tau_in'        : [],
                        'sig_tau_out'       : [],
                        'val_sig_tau_in'    : [],
                        'val_sig_tau_out'   : [],
                        'best_epoch'    : -1,
                    }
                    early_stopper           = EarlyStopper(patience=patience, min_delta=min_delta_loss)
                    weightedTauByEneLoss    = TauLossWeightedByEnergy()
                    
                    # print('train: {}, valid: {}, test: {}'.format(train_data,valid_data, len(X_test)))
                    for epoch in range(nEpochs):
                        #
                        # Training
                        #
                        for data_input, target, batch_idx in train_fold:

                            # pass input through the model and generate the output
                            output      = model(data_input)

                            #
                            # Loss calculation
                            #
                            loss            = model.criterion(output, data_input)        

                            # get input and output energy and time
                            ene_input, tau_input    = datasetTrain.calibrate(batch_idx, data_input)
                            ene_output, tau_output  = datasetTrain.calibrate(batch_idx, output)
                            sigmaTau_out    = torch.std(tau_output)
                            sigmaTau_in     = torch.std(tau_input)
                                                    
                            loss            = model.criterion(output, data_input)                                                
                            loss_tau        = model.criterion(tau_output , tau_input)                                                
                            loss_ene        = model.criterion(ene_output , ene_input)
                            wTauEne_loss    = weightedTauByEneLoss(tau_input, tau_output, ene_input)

                            #
                            # Backpropagation (optimize weights)
                            #
                            # model.optimizer.zero_grad()
                            optimizer.zero_grad()
                            total_loss = loss + loss_ene
                            loss.backward()
                            # loss_tau.backward()
                            # total_loss.backward()
                            # model.optimizer.step()
                            optimizer.step()
                        
                        #
                        # Validation
                        #
                        for data_input, target, batch_idx in val_fold:
                            # pass input through the model and generate the output
                            output  = model(data_input)

                            #
                            # Loss calculation
                            #

                            # get input and output energy and time
                            val_ene_input, val_tau_input    = datasetTrain.calibrate(batch_idx, data_input)
                            val_ene_output, val_tau_output  = datasetTrain.calibrate(batch_idx, output)
                            val_sigmaTau_out    = torch.std(val_tau_output)
                            val_sigmaTau_in     = torch.std(val_tau_input)
                                                    
                            val_loss            = model.criterion(output, data_input)                                                
                            val_loss_tau        = model.criterion(val_tau_output , val_tau_input)                                                
                            val_loss_ene        = model.criterion(val_ene_output , val_ene_input)
                            val_wTauEne_loss    = weightedTauByEneLoss(val_tau_input, val_tau_output, val_ene_input)

                        #
                        # End of the current Epoch
                        #
                        if (epoch%1)==0:
                            
                            print('\t\t\tEpoch {}/{}...loss={} | val_loss={}'.format(epoch,nEpochs,loss.detach().numpy(), val_loss))
                            # print('\t\t\t\t\t loss_tau={0:.2E} | val_loss_tau={0:.2E}'.format( loss_tau, val_loss_tau))
                            print('\t\t\t\t\t sigmaTauIn={} | sigmaTauOut={}'.format( sigmaTau_in.detach().numpy(), sigmaTau_out.detach().numpy()))
                        
                        train_logging_dict['loss'].append(loss.detach().numpy())
                        train_logging_dict['ene_loss'].append(loss_ene.detach().numpy())
                        train_logging_dict['tau_loss'].append(loss_tau.detach().numpy())
                        train_logging_dict['wTauEneLoss'].append(wTauEne_loss.detach().numpy())
                        train_logging_dict['val_loss'].append(val_loss.detach().numpy())
                        train_logging_dict['val_ene_loss'].append(val_loss_ene.detach().numpy())
                        train_logging_dict['val_tau_loss'].append(val_loss_tau.detach().numpy())
                        train_logging_dict['val_wTauEneLoss'].append(val_wTauEne_loss.detach().numpy())
                        train_logging_dict['total_loss'].append(total_loss.detach().numpy())
                        train_logging_dict['sig_tau_in'].append(sigmaTau_in.detach().numpy())
                        train_logging_dict['sig_tau_out'].append(sigmaTau_out.detach().numpy())
                        train_logging_dict['val_sig_tau_in'].append(val_sigmaTau_in.detach().numpy())
                        train_logging_dict['val_sig_tau_out'].append(val_sigmaTau_out.detach().numpy())
                        

                        #
                        # Early Stop criteria
                        #
                        bestModelPath=pathLoss+'bestModel_fold{}_iter{}.pth'.format(nFold, it)
                        # bStop = early_stopper.early_stop(val_loss, epoch, model, bestModelPath)
                        # bStop = early_stopper.early_stop(val_loss_tau, epoch, model, bestModelPath)
                        bStop = False

                        if bStop:
                            train_logging_dict['best_epoch'] = early_stopper.bestEpoch
                            print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
                            break
                        else:
                            train_logging_dict['best_epoch'] = epoch
                    #
                    # End of training at current fold-iter
                    #
                    if optimAlg=='adam':
                        np.savez(pathLoss+'loss_{}_lr{}_wd{}_fold{}_iter{}_neuIn{}_nLay{}_bn{}_batch{}.npz'.format(optimAlg, optimConfig[optimAlg]['learningRate'], optimConfig[optimAlg]['weightDecay'],nFold, it, nNeuronsInput, nLayers, bn_neurons, nBatch) ,loss=train_logging_dict)

                nFold+=1


