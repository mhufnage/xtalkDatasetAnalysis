# try:
#     !pip install fxpmath
# except:
#     pass
# !source /eos/user/m/mhufnage/.venv/bin/activate
from IPython.display import clear_output

import json
import os,sys
import time
import shelve

## Insert here the path to EventReader/share
eventReaderSharePath = '/eos/user/m/mhufnage/ALP_project/offline-ringer-dev/crosstalk/EventReader/share/'
sys.path.insert(1, eventReaderSharePath) 
# sys.path.insert(1, '/eos/user/m/mhufnage/SWAN_projects/XTalk/')

import ROOT
import numpy as np
import awkward as ak
import uproot
import mplhep as hep

from glob import glob
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as mcolors
import matplotlib
plt.style.use([hep.style.ATLAS])
# from fxpmath import Fxp

# import mpl_scatter_density
import scipy.stats as scipy
import torch
import torch.nn as nn
from collections import OrderedDict
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split

# import matplotlib.rcsetup as rcsetup
# print(rcsetup.all_backends)
# print(matplotlib.get_backend())
# matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS

## Custom Packages
from auxiliaryFunctions import *
from calibrationFilesHelper import *
from getXTDataAsPythonDict import *
from Autoencoder import *
# from calibrationFilesHelperROOT import *

# import tensorflow

## Load custom Help libraries
fDict       = open(eventReaderSharePath+'dictCaloByLayer.json')
caloDict    = json.load(fDict)


# !pip install scikit-learn
# import sklearn


nEpochs             = 300
patience            = 15
k                   = 10 #number of folds in kFold split
nBatch              = 32
nClusters           = -1 # for training, validation and test
nSamples            = 4 # standard for LAr
nCellsWinEta        = 3
nCellsWinPhi        = 3

nNeuronsInput       = 100
nLayers             = 2

train_ratio         = 0.80 # proportion of data to apply the kFold
test_ratio          = 0.10 # proportion of data to apply test on trained model
# validation_ratio    = 0.10

#
# Data load
#
dataset = np.load('data/dataset_singlElec_ptCut10_topocluster_sampWin{}-{}_nClus15687.npz'.format(nCellsWinEta, nCellsWinPhi),allow_pickle=True)['dataset'][0:nClusters]
confDict = np.load('data/datasetInfo_singlElec_ptCut10_topocluster_sampWin{}-{}_nClus15687.npz'.format(nCellsWinEta, nCellsWinPhi),allow_pickle=True)['datasetInfo'].tolist()

#
# Dataset split
#
X_train, X_test = train_test_split(dataset, test_size=test_ratio)
# X_train, X_valid = train_test_split(X_train, test_size=validation_ratio/(train_ratio+test_ratio))
# Must add the kFold splitting
kfold = KFold(n_splits=k, shuffle=True, random_state=1)

nFold = 0
print('nClusters={}'.format(len(dataset)))
for train_idx, valid_idx in kfold.split(X_train): 
    ## for each fold... perform a complete train.
    print('Fold {}/{}...'.format(nFold+1,k))

    #
    # Model initialization
    #
    model               = Autoencoder(nCellsEta=nCellsWinEta, nCellsPhi=nCellsWinPhi, neu1=nNeuronsInput)
    train_logging_dict  = {
        'loss'          : [],
        'val_loss'      : [],
        'loss_tau'      : [],
        'val_loss_tau'  : [],
        'loss_ene'      : [],
        'val_loss_ene'  : [],
        'sig_tau'       : [],
    }
    early_stopper   = EarlyStopper(patience=patience, min_delta=10)
    # print('train: {}, valid: {}, test: {}'.format(train_data,valid_data, len(X_test)))
    for epoch in range(nEpochs):
        #
        # Training
        #
        for clus in X_train[train_idx]:

            #
            # Data preparation
            #
            windowOrdered = clus['windowIndexesCh'][clus['orderedIndexes']].tolist()
            Awin, tauDSPWinInput, EDSPWinInput, _ = DSPCalibSamplesInCluster(
                        clus['channelId'][windowOrdered],
                        clus['digits'][windowOrdered], 
                        clus['channelGain'][windowOrdered], 
                        clus['channelLArPed'][windowOrdered], 
                        clus['channelOFCa'][windowOrdered], 
                        clus['channelOFCb'][windowOrdered], 
                        clus['channelRamps0'][windowOrdered], 
                        clus['channelRamps1'][windowOrdered], 
                        clus['channelDSPThr'][windowOrdered], 
                        clus['channelTimeOffset'][windowOrdered], 
                        thresholdMode=confDict['typeThreshold'], 
                        bDoAbsE1=confDict['bAbsDSPE1'])
            
            data_input = torch.tensor(np.hstack(clus['digits'][windowOrdered]).flatten().astype(float)).type(torch.float)

            # pass input through the model and generate the output
            output      = model(data_input)
            output_npy  = output.detach().numpy().reshape(len(clus['channelId'][windowOrdered]),nSamples) # convert from tensor to numpy

            Awin, tauDSPWinOutput, EDSPWinOutput, _ = DSPCalibSamplesInCluster(
                        clus['channelId'][windowOrdered],
                        output_npy,#clus['digits'][windowOrdered], 
                        clus['channelGain'][windowOrdered], 
                        clus['channelLArPed'][windowOrdered], 
                        clus['channelOFCa'][windowOrdered], 
                        clus['channelOFCb'][windowOrdered], 
                        clus['channelRamps0'][windowOrdered], 
                        clus['channelRamps1'][windowOrdered], 
                        clus['channelDSPThr'][windowOrdered], 
                        clus['channelTimeOffset'][windowOrdered], 
                        thresholdMode=confDict['typeThreshold'], 
                        bDoAbsE1=confDict['bAbsDSPE1'])

            #
            # Loss calculation
            #
            loss        = model.criterion(output, data_input)
            loss_tau    = model.criterion(torch.tensor(tauDSPWinOutput).type(torch.float),torch.tensor(tauDSPWinInput).type(torch.float))
            loss_ene    = model.criterion(torch.tensor(EDSPWinOutput).type(torch.float),torch.tensor(EDSPWinInput).type(torch.float))

            #
            # Backpropagation (optimize weights)
            #
            model.optimizer.zero_grad()
            loss.backward()
            model.optimizer.step()
        
        #
        # Validation
        #
        for clus in X_train[valid_idx]:
            #
            # Data preparation
            #
            windowOrdered = clus['windowIndexesCh'][clus['orderedIndexes']].tolist()
            Awin, tauDSPWinInput, EDSPWinInput, _ = DSPCalibSamplesInCluster(
                        clus['channelId'][windowOrdered],
                        clus['digits'][windowOrdered], 
                        clus['channelGain'][windowOrdered], 
                        clus['channelLArPed'][windowOrdered], 
                        clus['channelOFCa'][windowOrdered], 
                        clus['channelOFCb'][windowOrdered], 
                        clus['channelRamps0'][windowOrdered], 
                        clus['channelRamps1'][windowOrdered], 
                        clus['channelDSPThr'][windowOrdered], 
                        clus['channelTimeOffset'][windowOrdered], 
                        thresholdMode=confDict['typeThreshold'], 
                        bDoAbsE1=confDict['bAbsDSPE1'])
            
            data_input = torch.tensor(np.hstack(clus['digits'][windowOrdered]).flatten().astype(float)).type(torch.float)

            # pass input through the model and generate the output
            output      = model(data_input)
            output_npy  = output.detach().numpy().reshape(len(clus['channelId'][windowOrdered]),nSamples) # convert from tensor to numpy

            Awin, tauDSPWinOutput, EDSPWinOutput, _ = DSPCalibSamplesInCluster(
                        clus['channelId'][windowOrdered],
                        output_npy,#clus['digits'][windowOrdered], 
                        clus['channelGain'][windowOrdered], 
                        clus['channelLArPed'][windowOrdered], 
                        clus['channelOFCa'][windowOrdered], 
                        clus['channelOFCb'][windowOrdered], 
                        clus['channelRamps0'][windowOrdered], 
                        clus['channelRamps1'][windowOrdered], 
                        clus['channelDSPThr'][windowOrdered], 
                        clus['channelTimeOffset'][windowOrdered], 
                        thresholdMode=confDict['typeThreshold'], 
                        bDoAbsE1=confDict['bAbsDSPE1'])

            #
            # Loss calculation
            #
            val_loss        = model.criterion(output, data_input)
            val_loss_tau    = model.criterion(torch.tensor(tauDSPWinOutput).type(torch.float),torch.tensor(tauDSPWinInput).type(torch.float))
            val_loss_ene    = model.criterion(torch.tensor(EDSPWinOutput).type(torch.float),torch.tensor(EDSPWinInput).type(torch.float))

        #
        # End of the current Epoch
        #
        if (epoch%1)==0:
            print('\tEpoch {}/{}...loss={}'.format(epoch,nEpochs,loss.detach().numpy()))
        
        train_logging_dict['loss'].append(loss.detach().numpy())
        train_logging_dict['loss_ene'].append(loss_ene.detach().numpy())
        train_logging_dict['val_loss'].append(val_loss.detach().numpy())
        train_logging_dict['val_loss_ene'].append(val_loss_ene.detach().numpy())
        train_logging_dict['loss_tau'].append(loss_tau.detach().numpy())
        train_logging_dict['val_loss_tau'].append(val_loss_tau.detach().numpy())

        if early_stopper.early_stop(val_loss):
            print('--- EarlyStopper = True. val_loss={}'.format(val_loss))
            break
    #
    # End of training at current fold
    #
    np.savez('loss_fold{}_neuInputAE{}'.format(nFold, nNeuronsInput) ,loss=train_logging_dict)

    nFold+=1


# loss_fn = nn.MSELoss()
# optimizer = torch.optim.Adam(model.parameters(), lr=0.001)


# for epoch in range(nEpochs):
    # for tr in X_train:
#         #
#         #
#     for val in X_valid:
#         #
#         #
# for test in X_test:
    #
    #

# y = X

# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_ratio)


# X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=validation_ratio/(train_ratio+test_ratio))



# kf = KFold()
# dataset = np.load('data/dataset_singlElec_ptCut10_topocluster_sampWin3-3_nClus15687.npz',allow_pickle=True)

