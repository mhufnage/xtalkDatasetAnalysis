#################################################################################
# Example without fold division to observe 
# the limits of the training with the amount of data we got.
#################################################################################
#################################################################################

import json
import os,sys
import time
import shelve

## Insert here the path to EventReader/share
eventReaderSharePath = '/home/mhufnage/ATLAS_QT_XTALK/offline-ringer-dev/crosstalk/EventReader/share/'
sys.path.insert(1, eventReaderSharePath) 

## Insert here the path to xtalkDatasetAnalysis git package
sys.path.insert(1, '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/')


import numpy as np
import awkward as ak
import uproot
import mplhep as hep

from glob import glob
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as mcolors
import matplotlib
plt.style.use([hep.style.ATLAS])
# from fxpmath import Fxp

# import mpl_scatter_density
import scipy.stats as scipy
import torch
import torch.nn as nn
from torch.utils.data import DataLoader as torchDataLoader

from collections import OrderedDict
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split

# import matplotlib.rcsetup as rcsetup
# print(rcsetup.all_backends)
# print(matplotlib.get_backend())
# matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS

## Custom Packages
from auxiliaryFunctions import *
from calibrationFilesHelper import *
from getXTDataAsPythonDict import *
from Autoencoder import *
# from calibrationFilesHelperROOT import *

# import tensorflow

## Load custom Help libraries
fDict       = open(eventReaderSharePath+'dictCaloByLayer.json')
caloDict    = json.load(fDict)

# -----------------------------------------------------------------
# -----------------------------------------------------------------
# -----------------------------------------------------------------


# range_neuInput          = [8, 16, 32, 64, 128, 256]
# range_nLayers           = [0, 1, 2, 3]

range_neuInput  = [24]
range_nLayers   = [1]
bn_neurons      = 36

for nLay in range_nLayers:
    print('nLay: {}'.format(nLay))
    for neuIn in range_neuInput:
        print('\tneuInput: {}'.format(neuIn))

        experiment_alias        = 'learning the samples of a 3x3 window from topocluster at EMB2 layer. The loss for training is the input-output map'
        pathLoss                = '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/ml_training/autoencoder/simplestAC_ptCut0/'
        datasetPath             = '/data/atlas/mhufnage/xtalk/dataset/'
        ptCut                   = 0 #GeV

        nEpochs                 = 5000
        patience                = 25
        min_delta_loss          = 0

        k                       = 2 # number of folds in kFold split
        nBatch                  = 32 # xxx not implemented yet!!!!
        nIter                   = 1
        nClusters               = -1 # for training, validation and test

        bTrainWithPed           = False
        nSamples                = 4 # standard for LAr
        nCellsWinEta            = 3
        nCellsWinPhi            = 3
        scalerInput             = 1#1/(2**12-1)
        scalerOutput            = 1#2**12-1

        inputSize               = nSamples * nCellsWinEta * nCellsWinPhi
        nNeuronsInput           = neuIn#256
        nLayers                 = nLay#4
        neuronsEnc, neuronsDec  = getAutoencoderNeuronsList(inputSize, nNeuronsInput, nLayers)



        train_ratio         = 0.90 # proportion of data to apply the kFold
        test_ratio          = 0.10 # proportion of data to apply test on trained model
        # validation_ratio    = 0.10

        train_config_dict   = {
                    'experiment_alias'  : experiment_alias,
                    'ptCut'             : ptCut,
                    'patience'          : patience,
                    'min_delta_loss'    : min_delta_loss,
                    'max_epochs'        : nEpochs,
                    'kFolds'            : k,
                    'max_init'          : nIter,
                    'total_clusters'    : nClusters,
                    'train_ratio'       : train_ratio,
                    'test_ratio'        : test_ratio,
                    'cell_win_eta'      : nCellsWinEta,
                    'cell_win_phi'      : nCellsWinPhi,
                    'scaler_input'      : scalerInput,
                    'scaler_output'     : scalerOutput,
                    'neurons_encoder'   : neuronsEnc,
                    'neurons_decoder'   : neuronsDec,
                    'nNeuronsInput'     : nNeuronsInput,
                    'nLayers'           : nLayers,
                    'bn_neurons'        : bn_neurons,
                }
        np.savez(pathLoss+'config_train_neuIn{}_nLay{}.npz'.format( nNeuronsInput, nLayers) ,config=train_config_dict)

        #
        # Data load
        #
        dataset = np.load(glob(datasetPath+'dataset_singlElec_ptCut{}_topocluster_sampWin{}-{}_nClus*.npz'.format(ptCut, nCellsWinEta, nCellsWinPhi))[0],allow_pickle=True)['dataset'][0:nClusters]
        confDict = np.load(glob(datasetPath+'datasetInfo_singlElec_ptCut{}_topocluster_sampWin{}-{}_nClus*.npz'.format(ptCut, nCellsWinEta, nCellsWinPhi))[0],allow_pickle=True)['datasetInfo'].tolist()

        #
        # Dataset split
        #
        X_train, X_test = train_test_split(dataset, test_size=test_ratio)
        # X_train, X_valid = train_test_split(X_train, test_size=validation_ratio/(train_ratio+test_ratio))
        # Must add the kFold splitting
        kfold = KFold(n_splits=k, shuffle=True, random_state=1)

        nFold = 0

        print('\tnClusters={}'.format(len(dataset)))
        # for train_idx, valid_idx in kfold.split(X_train): 
            ## for each fold... perform a complete train.
        for it in range(0,nIter):
            print('\t\tFold {}/{}... iter {}/{}'.format(nFold+1,k, it+1, nIter))

            #
            # Model initialization
            #
            # model               = Autoencoder(nCellsEta=nCellsWinEta, nCellsPhi=nCellsWinPhi, neu1=nNeuronsInput)
            model               = Autoencoder_Loop(encoderList=neuronsEnc, bottleNeck=bn_neurons,decoderList=neuronsDec, inScaler=scalerInput, outScaler=scalerOutput)
            train_logging_dict  = {
                'loss'              : [],
                'val_loss'          : [],
                'tau_loss'          : [],
                'wTauEneLoss'       : [],
                'val_tau_loss'      : [],
                'ene_loss'          : [],
                'val_ene_loss'      : [],
                'val_wTauEneLoss'   : [],
                'total_loss'        : [],

                'sig_tau'       : [],
                'best_epoch'    : -1,
            }
            early_stopper           = EarlyStopper(patience=patience, min_delta=min_delta_loss)
            weightedTauByEneLoss    = TauLossWeightedByEnergy()
            
            # print('train: {}, valid: {}, test: {}'.format(train_data,valid_data, len(X_test)))
            for epoch in range(nEpochs):
                #
                # Training
                #
                for clus in X_train:#[train_idx]:

                    #
                    # Data preparation
                    #
                    windowOrdered = clus['windowIndexesCh'][clus['orderedIndexes']].tolist()

                    # Input Data Estimation (prior to ML) - Input with pedestal (to be removed inside DSPCalibSamplesInCluster)
                    Awin, tauDSPWinInput, EDSPWinInput, _, InSamplesWithoutPed = DSPCalibSamplesInCluster(
                                clus['channelId'][windowOrdered],
                                clus['digits'][windowOrdered], 
                                clus['channelGain'][windowOrdered], 
                                clus['channelLArPed'][windowOrdered], 
                                clus['channelOFCa'][windowOrdered], 
                                clus['channelOFCb'][windowOrdered], 
                                clus['channelRamps0'][windowOrdered], 
                                clus['channelRamps1'][windowOrdered], 
                                clus['channelDSPThr'][windowOrdered], 
                                clus['channelTimeOffset'][windowOrdered], 
                                thresholdMode=confDict['typeThreshold'], 
                                bDoAbsE1=confDict['bAbsDSPE1'],
                                bDigitsWithPed=True) # refer to input signal: True-> has ped. False->has not ped
                    
                    if bTrainWithPed: # if bTrainWithPed=True, use InSamplesWithoutPed as data_input
                        data_input = torch.tensor(np.hstack(clus['digits'][windowOrdered]).flatten().astype(float)).type(torch.float)
                    else:
                        data_input = torch.tensor(np.hstack( InSamplesWithoutPed ).flatten().astype(float)).type(torch.float)

                    # pass input through the model and generate the output
                    output      = model(data_input)
                    output_npy  = output.detach().numpy().reshape(len(clus['channelId'][windowOrdered]),nSamples) # convert from tensor to numpy

                    # Output Data Estimation (after ML)
                    # If bTrainWithPed=False, the calibration mode must know that the input signal (output from ML), 
                    # will come without the baseline pedestal.
                    Awin, tauDSPWinOutput, EDSPWinOutput, _, OutSamplesWithoutPed = DSPCalibSamplesInCluster(
                                clus['channelId'][windowOrdered],
                                output_npy,#clus['digits'][windowOrdered], 
                                clus['channelGain'][windowOrdered], 
                                clus['channelLArPed'][windowOrdered], 
                                clus['channelOFCa'][windowOrdered], 
                                clus['channelOFCb'][windowOrdered], 
                                clus['channelRamps0'][windowOrdered], 
                                clus['channelRamps1'][windowOrdered], 
                                clus['channelDSPThr'][windowOrdered], 
                                clus['channelTimeOffset'][windowOrdered], 
                                thresholdMode=confDict['typeThreshold'], 
                                bDoAbsE1=confDict['bAbsDSPE1'],
                                bDigitsWithPed=bTrainWithPed) # refer to input signal: True-> has ped. False->has not ped

                    #
                    # Loss calculation
                    #
                    tau_output      = torch.tensor(tauDSPWinOutput, dtype=torch.float,requires_grad=True)
                    tau_input       = torch.tensor(tauDSPWinInput, dtype=torch.float,requires_grad=True)
                    ene_output      = torch.tensor(EDSPWinOutput, dtype=torch.float, requires_grad=True)
                    ene_input       = torch.tensor(EDSPWinInput, dtype=torch.float, requires_grad=True)

                    loss            = model.criterion(output, data_input)
                    loss_tau        = model.criterion(tau_output , tau_input)
                    loss_ene        = model.criterion(ene_output , ene_input)

                    wTauEne_loss    = weightedTauByEneLoss(tau_input, tau_output, ene_input)

                    #
                    # Backpropagation (optimize weights)
                    #
                    model.optimizer.zero_grad()
                    total_loss = loss + loss_ene
                    # loss.backward()
                    # loss_tau.backward()
                    total_loss.backward()
                    model.optimizer.step()

                    # print(tau_input.detach().numpy(), tau_output.detach().numpy())
                
                #
                # Validation
                #
                for clus in X_test:#X_train[valid_idx]:
                    #
                    # Data preparation
                    #
                    windowOrdered = clus['windowIndexesCh'][clus['orderedIndexes']].tolist()
                    Awin, tauDSPWinInput, EDSPWinInput, _, InSamplesWithoutPed = DSPCalibSamplesInCluster(
                                clus['channelId'][windowOrdered],
                                clus['digits'][windowOrdered], 
                                clus['channelGain'][windowOrdered], 
                                clus['channelLArPed'][windowOrdered], 
                                clus['channelOFCa'][windowOrdered], 
                                clus['channelOFCb'][windowOrdered], 
                                clus['channelRamps0'][windowOrdered], 
                                clus['channelRamps1'][windowOrdered], 
                                clus['channelDSPThr'][windowOrdered], 
                                clus['channelTimeOffset'][windowOrdered], 
                                thresholdMode=confDict['typeThreshold'], 
                                bDoAbsE1=confDict['bAbsDSPE1'],
                                bDigitsWithPed=True)

                    # pass input through the model and generate the output
                    if bTrainWithPed: # if bTrainWithPed=False, use InSamplesWithoutPed as data_input
                        data_input = torch.tensor(np.hstack(clus['digits'][windowOrdered]).flatten().astype(float)).type(torch.float)
                    else:
                        data_input = torch.tensor(np.hstack( InSamplesWithoutPed ).flatten().astype(float)).type(torch.float)

                    output      = model(data_input)
                    output_npy  = output.detach().numpy().reshape(len(clus['channelId'][windowOrdered]),nSamples) # convert from tensor to numpy

                    Awin, tauDSPWinOutput, EDSPWinOutput, _, OutSamplesWithoutPed = DSPCalibSamplesInCluster(
                                clus['channelId'][windowOrdered],
                                output_npy,#clus['digits'][windowOrdered], 
                                clus['channelGain'][windowOrdered], 
                                clus['channelLArPed'][windowOrdered], 
                                clus['channelOFCa'][windowOrdered], 
                                clus['channelOFCb'][windowOrdered], 
                                clus['channelRamps0'][windowOrdered], 
                                clus['channelRamps1'][windowOrdered], 
                                clus['channelDSPThr'][windowOrdered], 
                                clus['channelTimeOffset'][windowOrdered], 
                                thresholdMode=confDict['typeThreshold'], 
                                bDoAbsE1=confDict['bAbsDSPE1'],
                                bDigitsWithPed=bTrainWithPed)

                    #
                    # Loss calculation
                    #
                    val_tau_input       = torch.tensor(tauDSPWinInput, dtype=torch.float, requires_grad=True)
                    val_tau_output      = torch.tensor(tauDSPWinOutput, dtype=torch.float, requires_grad=True)
                    val_ene_input       = torch.tensor(EDSPWinInput).type(torch.float)
                    val_ene_output      = torch.tensor(EDSPWinOutput).type(torch.float)

                    val_loss            = model.criterion(output, data_input)
                    val_loss_tau        = model.criterion(val_tau_output , val_tau_input)
                    val_loss_ene        = model.criterion( val_ene_output, val_ene_input)

                    val_wTauEne_loss    = weightedTauByEneLoss(val_tau_input, val_tau_output, val_tau_input)

                #
                # End of the current Epoch
                #
                if (epoch%1)==0:
                    
                    print('\t\t\tEpoch {}/{}...loss={} | val_loss={}'.format(epoch,nEpochs,loss.detach().numpy(), val_loss))
                    print('\t\t\t\t\t loss_tau={0:.2E} | val_loss_tau={0:.2E}'.format( loss_tau, val_loss_tau))
                
                train_logging_dict['loss'].append(loss.detach().numpy())
                train_logging_dict['ene_loss'].append(loss_ene.detach().numpy())
                train_logging_dict['tau_loss'].append(loss_tau.detach().numpy())
                train_logging_dict['wTauEneLoss'].append(wTauEne_loss.detach().numpy())
                train_logging_dict['val_loss'].append(val_loss.detach().numpy())
                train_logging_dict['val_ene_loss'].append(val_loss_ene.detach().numpy())
                train_logging_dict['val_tau_loss'].append(val_loss_tau.detach().numpy())
                train_logging_dict['val_wTauEneLoss'].append(val_wTauEne_loss.detach().numpy())
                train_logging_dict['total_loss'].append(total_loss.detach().numpy())
                

                #
                # Early Stop criteria
                #
                bestModelPath=pathLoss+'bestModel_fold{}_iter{}_epoch{}.pth'.format(nFold, it, early_stopper.bestEpoch)
                bStop = early_stopper.early_stop(val_loss, epoch, model, bestModelPath)
                # bStop = early_stopper.early_stop(val_loss_tau, epoch, model, bestModelPath)
                # bStop = False

                if bStop:
                    train_logging_dict['best_epoch'] = early_stopper.bestEpoch
                    print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
                    break
                else:
                    train_logging_dict['best_epoch'] = epoch
            #
            # End of training at current fold-iter
            #
            np.savez(pathLoss+'loss_fold{}_iter{}_neuIn{}_nLay{}_bn{}.npz'.format(nFold, it, nNeuronsInput, nLayers, bn_neurons) ,loss=train_logging_dict)

        nFold+=1
