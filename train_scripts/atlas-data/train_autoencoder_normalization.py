# 28/09/2023 
# Autoencoder to estimate the sample sets
# Applied simple normalization and monitoring single cell loss values.
#

# try:
#     !pip install fxpmath
# except:
#     pass
# !source /eos/user/m/mhufnage/.venv/bin/activate
# from IPython.display import clear_output
#
# Notebook runs withou the need of root --notebook server initialization.
# It only needs a python env. with the required packages.

# import ROOT

import json
import os,sys
import time
import shelve

## Insert here the path to EventReader/share
eventReaderSharePath = '/home/mhufnage/ATLAS_QT_XTALK/offline-ringer-dev/crosstalk/EventReader/share/'
sys.path.insert(1, eventReaderSharePath) 

## Insert here the path to xtalkDatasetAnalysis git package
sys.path.insert(1, '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/')


import numpy as np
import awkward as ak
import uproot
import mplhep as hep

from glob import glob
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as mcolors
import matplotlib
plt.style.use([hep.style.ATLAS])
# from fxpmath import Fxp

# import mpl_scatter_density
import scipy.stats as scipy
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader

from collections import OrderedDict
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split

# import matplotlib.rcsetup as rcsetup
# print(rcsetup.all_backends)
# print(matplotlib.get_backend())

# matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS
# matplotlib.use('module://ipykernel.pylab.backend_inline')  # show plots

## Custom Packages
from auxiliaryFunctions import *
from calibrationFilesHelper import *
# from getXTDataAsPythonDict import *
from Autoencoder import *
from XTalkDatasetClass import *
# from calibrationFilesHelperROOT import *

## Load custom Help libraries
fDict       = open(eventReaderSharePath+'dictCaloByLayer.json')
caloDict    = json.load(fDict)

#
# --------------------------------------------------------------
#

# lr_adam                 = [1, 1e-1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6]

optimAlg                = 'adam'
typeNormalization       = 'adcLimits'#'minMax'

lr                      = 1e-3
weightDecay_adam        = 1e-5

optimConfig             = {
    'adam':{
        'learningRate'  : lr,
        'weightDecay'   : weightDecay_adam,
    },
}

print(optimConfig[optimAlg])

range_neuInput          = [24]
range_nLayers           = [1]
bn_neurons              = 8

experiment_alias        = 'learning the samples of a 3x3 window from topocluster at EMB2 layer. The loss for training is the input-output map. The cell organization in the window is eta-oriented, eta-orientation cluster indexing. where 5 is the index of the hottest cell.'
# phi [ 3 | 6 | 9 ]
#     [ 2 | 5 | 8 ]  , eta-orientation cluster indexing. where 5 is the index of the hottest cell
#     [ 1 | 4 | 7 ]
#             eta
pathLoss                = '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/ml_training/autoencoder/normalization/byMaxAdcValue/'#lowStatistics/'
datasetPath             = '/data/atlas/mhufnage/xtalk/dataset/'
ptCut                   = 0 #GeV

nEpochs                 = 200
patience                = 15
min_delta_loss          = 0

k                       = 3 # number of folds in kFold split
nBatch                  = 256 # batch size in clusters
nIter                   = 1
nClusters               = 20000 # for training, validation and test

bTrainWithPed           = False

nSamples                = 4 # standard for LAr
nCellsWinEta            = 3
nCellsWinPhi            = 3
scalerInput             = torch.tensor(1/(2**12-1)) # without normalization, set to 1
scalerOutput            = torch.tensor(2**12-1)

train_ratio             = 0.90 # proportion of data to apply the kFold
test_ratio              = 0.10 # proportion of data to apply test on trained model
# validation_ratio        = 0.10

if not(os.path.isdir(pathLoss)):
    os.mkdir(pathLoss)

for nLay in range_nLayers:
    print('nLay: {}'.format(nLay))
    for neuIn in range_neuInput:
        print('\tneuInput: {}'.format(neuIn))

        inputSize               = nSamples * nCellsWinEta * nCellsWinPhi
        nNeuronsInput           = neuIn#256
        nLayers                 = nLay#4
        neuronsEnc, neuronsDec  = getAutoencoderNeuronsList(inputSize, nNeuronsInput, nLayers)

        
        #
        # Data load
        #
        datasetDict = np.load(glob(datasetPath+'dataset_singlElec_ptCut{}_topocluster_sampWin{}-{}_nClus*.npz'.format(ptCut, nCellsWinEta, nCellsWinPhi))[0],allow_pickle=True, encoding='latin1')['dataset'][0:nClusters]
        confDict    = np.load(glob(datasetPath+'datasetInfo_singlElec_ptCut{}_topocluster_sampWin{}-{}_nClus*.npz'.format(ptCut, nCellsWinEta, nCellsWinPhi))[0],allow_pickle=True, encoding='latin1')['datasetInfo'].tolist()

        #
        # Dataset split
        #
        X_train, X_test = train_test_split(datasetDict, test_size=test_ratio)
        # X_train, X_valid = train_test_split(X_train, test_size=validation_ratio/(train_ratio+test_ratio))

        datasetTrain    = XTalkDataset(X_train, confDict, nEvents=-1, bRemovePed=not(bTrainWithPed))
        datasetTest     = XTalkDataset(X_test, confDict,  nEvents=-1, bRemovePed=not(bTrainWithPed))

        # Must add the kFold splitting
        kfold = KFold(n_splits=k, shuffle=True, random_state=1)

        nFold = 0

        print('\tnClusters={}'.format(len(datasetDict)))
        train_config_dict   = {
                    'experiment_alias'  : experiment_alias,
                    'patience'          : patience,
                    'min_delta_loss'    : min_delta_loss,
                    'max_epochs'        : nEpochs,
                    'kFolds'            : k,
                    'batch_size'        : nBatch,
                    'max_init'          : nIter,
                    'total_clusters'    : len(datasetDict),
                    'train_ratio'       : train_ratio,
                    'test_ratio'        : test_ratio,
                    'cell_win_eta'      : nCellsWinEta,
                    'cell_win_phi'      : nCellsWinPhi,
                    'scaler_input'      : scalerInput,
                    'scaler_output'     : scalerOutput,
                    'neurons_encoder'   : neuronsEnc,
                    'neurons_decoder'   : neuronsDec,
                    'nNeuronsInput'     : nNeuronsInput,
                    'bn_neurons'        : bn_neurons,
                    'nLayers'           : nLayers,
                }
        # np.savez(pathLoss+'config_train.npz' ,config=train_config_dict)
        if optimAlg=='adam':
            np.savez(pathLoss+'config_train_{}_lr{}_wd{}.npz'.format(optimAlg, optimConfig[optimAlg]['learningRate'], optimConfig[optimAlg]['weightDecay']) ,config=train_config_dict)

        for train_fold_idx, valid_fold_idx in kfold.split(datasetTrain): 
            #
            # Mount DataLoader with fold indices
            #
            train_fold    = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, sampler=torch.utils.data.SubsetRandomSampler(indices=train_fold_idx))
            val_fold      = DataLoader( datasetTrain, batch_size=nBatch, shuffle=False, sampler=torch.utils.data.SubsetRandomSampler(indices=valid_fold_idx))

            for it in range(0,nIter):
                print('\t\tFold {}/{}... iter {}/{}'.format(nFold+1,k, it+1, nIter))

                #
                # Model initialization
                #
                # model               = Autoencoder(nCellsEta=nCellsWinEta, nCellsPhi=nCellsWinPhi, neu1=nNeuronsInput)
                model               = Autoencoder_Loop(encoderList=neuronsEnc, bottleNeck=bn_neurons,decoderList=neuronsDec, inScaler=scalerInput, outScaler=scalerOutput)
                if optimAlg=='adam':
                    optimizer = torch.optim.Adam(model.parameters(), lr=optimConfig[optimAlg]['learningRate'], weight_decay=optimConfig[optimAlg]['weightDecay'])

                early_stopper           = EarlyStopper(patience=patience, min_delta=min_delta_loss)
                weightedTauByEneLoss    = TauLossWeightedByEnergy()

                # individual cells loss
                cell_names   = ['cell{}'.format(cInd+1) for cInd in range(0, nCellsWinEta*nCellsWinPhi)]
                
                train_logging_dict  = {
                    'loss'              : [],
                    'val_loss'          : [],
                    'tau_loss'          : [],
                    'wTauEneLoss'       : [],
                    'val_tau_loss'      : [],
                    'ene_loss'          : [],
                    'val_ene_loss'      : [],
                    'val_wTauEneLoss'   : [],
                    'total_loss'        : [],

                    # individual cells loss
                    'cells_sample_loss'         : {key: [] for key in cell_names},
                    'cells_ene_loss'            : {key: [] for key in cell_names},
                    'cells_tau_loss'            : {key: [] for key in cell_names},
                    'cells_weightedTau_loss'    : {key: [] for key in cell_names},
                    'cells_sample_valLoss'      : {key: [] for key in cell_names},
                    'cells_ene_valLoss'         : {key: [] for key in cell_names},
                    'cells_tau_valLoss'         : {key: [] for key in cell_names},
                    'cells_weightedTau_valLoss' : {key: [] for key in cell_names},

                    'sig_tau_in'        : [],
                    'sig_tau_out'       : [],
                    'val_sig_tau_in'    : [],
                    'val_sig_tau_out'   : [],
                    'best_epoch'    : -1,
                }
                
                
                # print('train: {}, valid: {}, test: {}'.format(train_data,valid_data, len(X_test)))
                for epoch in range(nEpochs):
                    #
                    # Training
                    #
                    individual_cell_loss = {
                    'cells_sample_loss'         : {key: -1 for key in cell_names},
                    'cells_ene_loss'            : {key: -1 for key in cell_names},
                    'cells_tau_loss'            : {key: -1 for key in cell_names},
                    'cells_weightedTau_loss'    : {key: -1 for key in cell_names},
                    'cells_sample_valLoss'      : {key: -1 for key in cell_names},
                    'cells_ene_valLoss'         : {key: -1 for key in cell_names},
                    'cells_tau_valLoss'         : {key: -1 for key in cell_names},
                    'cells_weightedTau_valLoss' : {key: -1 for key in cell_names},
                    }

                    for data_input, target, batch_idx in train_fold:

                        # pass input through the model and generate the output
                        output      = model(data_input*scalerInput) #normalized input

                        #
                        # Loss calculation
                        #

                        # get input and output energy and time
                        ene_input, tau_input    = datasetTrain.calibrate(batch_idx, data_input) # unormlized input
                        ene_output, tau_output  = datasetTrain.calibrate(batch_idx, output*scalerOutput) # denormalized output
                        sigmaTau_out            = torch.std(tau_output)
                        sigmaTau_in             = torch.std(tau_input)

                        # total window loss
                        loss            = model.criterion(output, data_input*scalerInput) # loss with normalized data input and output
                        loss_tau        = model.criterion(tau_output , tau_input)
                        loss_ene        = model.criterion(ene_output , ene_input)
                        wTauEne_loss    = weightedTauByEneLoss(tau_input, tau_output, ene_input)

                        # individual cell loss
                        for cIndex in range(0,nCellsWinEta*nCellsWinPhi):
                            # tmp_eneLoss = model.criterion(ene_output[:,cIndex], ene_input[:,cIndex])
                            # print(tmp_eneLoss)
                            individual_cell_loss['cells_sample_loss']['cell{}'.format(cIndex+1)]       = model.criterion(output[:,cIndex*nSamples:cIndex*nSamples+nSamples], data_input[:,cIndex*nSamples:cIndex*nSamples+nSamples]*scalerInput)
                            individual_cell_loss['cells_ene_loss']['cell{}'.format(cIndex+1)]          = model.criterion(ene_output[:,cIndex], ene_input[:,cIndex])
                            individual_cell_loss['cells_tau_loss']['cell{}'.format(cIndex+1)]          = model.criterion(tau_output[:,cIndex], tau_input[:,cIndex])
                            individual_cell_loss['cells_weightedTau_loss']['cell{}'.format(cIndex+1)]  = weightedTauByEneLoss(tau_input[:,cIndex], tau_output[:,cIndex], ene_input[:,cIndex])
                        

                        #
                        # Backpropagation (optimize weights)
                        #
                        # model.optimizer.zero_grad()
                        optimizer.zero_grad()
                        total_loss = loss + loss_ene
                        loss.backward()
                        # loss_tau.backward()
                        # total_loss.backward()
                        # model.optimizer.step()
                        optimizer.step()
                    
                    #
                    # Validation
                    #
                    for data_input, target, batch_idx in val_fold:
                        # pass input through the model and generate the output
                        output  = model(data_input*scalerInput)

                        #
                        # Loss calculation
                        #

                        # get input and output energy and time
                        val_ene_input, val_tau_input    = datasetTrain.calibrate(batch_idx, data_input)
                        val_ene_output, val_tau_output  = datasetTrain.calibrate(batch_idx, output*scalerOutput)
                        val_sigmaTau_out    = torch.std(val_tau_output)
                        val_sigmaTau_in     = torch.std(val_tau_input)

                        val_loss            = model.criterion(output, data_input*scalerInput)
                        val_loss_tau        = model.criterion(val_tau_output , val_tau_input)
                        val_loss_ene        = model.criterion(val_ene_output , val_ene_input)
                        val_wTauEne_loss    = weightedTauByEneLoss(val_tau_input, val_tau_output, val_ene_input)

                        # individual cell loss
                        for cIndex in range(0,nCellsWinEta*nCellsWinPhi):
                            individual_cell_loss['cells_sample_valLoss']['cell{}'.format(cIndex+1)]       = model.criterion(output[:,cIndex*nSamples:cIndex*nSamples+nSamples], data_input[:,cIndex*nSamples:cIndex*nSamples+nSamples]*scalerInput)
                            individual_cell_loss['cells_ene_valLoss']['cell{}'.format(cIndex+1)]          = model.criterion(val_ene_input[:,cIndex], val_ene_input[:,cIndex])
                            individual_cell_loss['cells_tau_valLoss']['cell{}'.format(cIndex+1)]          = model.criterion(val_tau_output[:,cIndex], val_tau_input[:,cIndex])
                            individual_cell_loss['cells_weightedTau_valLoss']['cell{}'.format(cIndex+1)]  = weightedTauByEneLoss(val_tau_input[:,cIndex], val_tau_output[:,cIndex], val_ene_input[:,cIndex])

                    #
                    # End of the current Epoch
                    #
                    if (epoch%1)==0:
                        
                        print('\t\t\tEpoch {}/{}...loss={} | val_loss={}'.format(epoch,nEpochs,loss.detach().numpy(), val_loss))
                        # print('\t\t\t\t\t loss_tau={0:.2E} | val_loss_tau={0:.2E}'.format( loss_tau, val_loss_tau))
                        print('\t\t\t\t\t sigmaTauIn={} | sigmaTauOut={}'.format( sigmaTau_in.detach().numpy(), sigmaTau_out.detach().numpy()))
                    
                    train_logging_dict['loss'].append(loss.detach().numpy())
                    train_logging_dict['ene_loss'].append(loss_ene.detach().numpy())
                    train_logging_dict['tau_loss'].append(loss_tau.detach().numpy())
                    train_logging_dict['wTauEneLoss'].append(wTauEne_loss.detach().numpy())
                    train_logging_dict['val_loss'].append(val_loss.detach().numpy())
                    train_logging_dict['val_ene_loss'].append(val_loss_ene.detach().numpy())
                    train_logging_dict['val_tau_loss'].append(val_loss_tau.detach().numpy())
                    train_logging_dict['val_wTauEneLoss'].append(val_wTauEne_loss.detach().numpy())
                    train_logging_dict['total_loss'].append(total_loss.detach().numpy())
                    train_logging_dict['sig_tau_in'].append(sigmaTau_in.detach().numpy())
                    train_logging_dict['sig_tau_out'].append(sigmaTau_out.detach().numpy())
                    train_logging_dict['val_sig_tau_in'].append(val_sigmaTau_in.detach().numpy())
                    train_logging_dict['val_sig_tau_out'].append(val_sigmaTau_out.detach().numpy())

                    for cIndex in range(1, nCellsWinEta*nCellsWinPhi+1):
                        train_logging_dict['cells_sample_loss']['cell{}'.format(cIndex)]        .append(individual_cell_loss['cells_sample_loss']['cell{}'.format(cIndex)].detach().numpy())
                        train_logging_dict['cells_ene_loss']['cell{}'.format(cIndex)]           .append(individual_cell_loss['cells_ene_loss']['cell{}'.format(cIndex)].detach().numpy())
                        train_logging_dict['cells_tau_loss']['cell{}'.format(cIndex)]           .append(individual_cell_loss['cells_tau_loss']['cell{}'.format(cIndex)].detach().numpy())
                        train_logging_dict['cells_weightedTau_loss']['cell{}'.format(cIndex)]   .append(individual_cell_loss['cells_weightedTau_loss']['cell{}'.format(cIndex)].detach().numpy())
                        train_logging_dict['cells_sample_valLoss']['cell{}'.format(cIndex)]     .append(individual_cell_loss['cells_sample_valLoss']['cell{}'.format(cIndex)].detach().numpy())
                        train_logging_dict['cells_ene_valLoss']['cell{}'.format(cIndex)]        .append(individual_cell_loss['cells_ene_valLoss']['cell{}'.format(cIndex)].detach().numpy())
                        train_logging_dict['cells_tau_valLoss']['cell{}'.format(cIndex)]        .append(individual_cell_loss['cells_tau_valLoss']['cell{}'.format(cIndex)].detach().numpy())
                        train_logging_dict['cells_weightedTau_valLoss']['cell{}'.format(cIndex)].append(individual_cell_loss['cells_weightedTau_valLoss']['cell{}'.format(cIndex)].detach().numpy())
                    

                    #
                    # Early Stop criteria
                    #
                    bestModelPath=pathLoss+'bestModel_fold{}_iter{}.pth'.format(nFold, it)
                    # bStop = early_stopper.early_stop(val_loss, epoch, model, bestModelPath)
                    # bStop = early_stopper.early_stop(val_loss_tau, epoch, model, bestModelPath)
                    bStop = False

                    if bStop:
                        train_logging_dict['best_epoch'] = early_stopper.bestEpoch
                        print('--- EarlyStopper=True. val_loss={}... bestModel_loss={}, bestEpoch={}'.format(val_loss, early_stopper.min_validation_loss, early_stopper.bestEpoch))
                        break
                    else:
                        train_logging_dict['best_epoch'] = epoch
                #
                # End of training at current fold-iter
                #
                np.savez(pathLoss+'loss_fold{}_iter{}_neuIn{}_nLay{}_bn{}_batch{}.npz'.format(nFold, it, nNeuronsInput, nLayers, bn_neurons, nBatch) ,loss=train_logging_dict)

            nFold+=1


