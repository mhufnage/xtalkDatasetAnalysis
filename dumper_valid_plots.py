from __future__ import absolute_import, division, print_function

import array

import sys,json, os
from glob import glob
from time import time
import numpy as np

import ROOT as root
from ROOT import TCanvas, TPad, TH1F,TH2F,TH3F, TH1I, TFile,TLine,TGraph, TGraph2D, TGraphErrors, gStyle, THStack
from ROOT import TLatex, gPad, TLegend
from ROOT import kRed, kBlue, kBlack,TLine,kBird, kOrange,kGray, kYellow, kViolet, kGreen, kAzure

root.TH1D.AddDirectory(False)
root.TH1F.AddDirectory(False)
root.TH1I.AddDirectory(False)

import atlasplots as astyle

from functionsHelper import stdVecToArray, stdVecOfStdVecToArrayOfList, stdVecOfStdVecToPlainCPPArray
from auxiliaryFunctions import human_format
from root_plots_lib.root_plots_helper import fillTH1AndUpdateRange, createDictOfTH1ForWholeTree, createDictOfTH1ForTreeSelection





if __name__=='__main__':
    #---------------------------------------------
    #----------   Read the ROOT trees   ----------
    #---------------------------------------------
    total_events        = -1 #nClusters to being user to train the model
    getMLDataset        = False
    electronPtCut       = 0    # GeV, for ML dataset
    clusName            = 'topocluster'
    layer               = 'EMB2'


    datasetPath = '/data/atlas/mhufnage/xtalk/dataset/'

    datasets    = [
        datasetPath+'user.mhufnage.xtalk_dumper.data17_13TeV.00329542.physics_MinBias.ntuple.v0/',
        datasetPath+'user.mhufnage.xtalk_dumper.ZeeTP.data17_13TeV.00329542.physics_MinBias.ntuple.v0_XYZ.root.tgz/',
        datasetPath+'user.mhufnage.xtalk_dumper.mc16_13TeV.361106.Zee.ntuple.v0_XYZ.root.tgz/',
        datasetPath+'user.mhufnage.xtalk_dumper.ZeeTP.mc16_13TeV.361106.Zee.ntuple.v0_XYZ.root.tgz/',
    ]
    isMC             = [False, False, True, True]
    isTagAndProbe    = [False, True, False, True]
    branchCategories = ['event', 'clusters', 'cells', 'channels', 'rawChannels', 'particles']

    for branchCategory in branchCategories:#, True]:
        
        datasetAlias    = [
            'data17_singleElec_full_{}'.format(branchCategory),
            'data17_ZeeTP_full_{}'.format(branchCategory),
            'mc16_singleElect_full_{}'.format(branchCategory),
            'mc16_ZeeTP_full_{}'.format(branchCategory)]

        for dsIndex, dsName in enumerate(datasets):
            # totalTimeInit = time()
            print('Dataset alias: {}'.format(datasetAlias[dsIndex]))
            
            fileNames = glob(dsName+'dumper_minBias*.root')

            if isMC[dsIndex]:
                print('The input files are from ATLAS_MC!')
            else:
                print('The inputs are from collision data!')
            
            sChain   = root.TChain("dumpedData",'')
            for file in fileNames:
                sChain.Add(file+"/dumpedData") # main dumped data tree (event by event)
            
            nEvents = sChain.GetEntries()
            print("{} files were added with {} events.".format(len(fileNames),human_format(nEvents)))

            #
            # Loop over events
            #
            # hists = createDictOfTH1ForWholeTree(sChain)
            hists = createDictOfTH1ForTreeSelection(sChain, branchCategory=branchCategory)
            
            initTime = time()
            for eventNumber, event in enumerate(sChain):
                if not (eventNumber % 100000):
                    print('event {}'.format(eventNumber))
                if (eventNumber > total_events) and (total_events != -1):
                    break
                    
                for branchIndex, branchKey in enumerate(sChain.GetListOfBranches()):
                    
                    branchName = branchKey.GetName()
                    
                    if not sChain.GetBranchStatus(branchName):
                        continue
                    
                    branchValue = getattr(event, branchName)

                    if 'event' in branchName:
                        fillTH1AndUpdateRange(hists[branchName], branchValue )
                        
                    elif any(brName in branchName for brName in ['digit', 'OFCa', 'OFCb', 'shape', 'chInfo']):
                        if branchValue.size() !=0:
                            # newValue = np.concatenate(branchValue).astype(np.float) # too slow!
                            newValue = stdVecOfStdVecToPlainCPPArray(branchValue) # way faster!! (but this one dont care by ordering)
                            fillTH1AndUpdateRange(hists[branchName],  newValue)
                    else:
                        if branchValue.size() !=0:
                            newValue = stdVecToArray(branchValue).astype(np.float)
                            fillTH1AndUpdateRange(hists[branchName],  newValue)

            finalTime = time()
            
            print("{} branches and {} events processed. Total time: {:.4}mins ({:.4}s/event).".format(len(hists.keys()), human_format(eventNumber-1),(finalTime-initTime)/60, (finalTime-initTime)/eventNumber))
            
            sChain.Reset()
            # totalTimeFinal = time()
            
            rootFile = root.TFile(datasetPath+'histograms/'+datasetAlias[dsIndex]+'.root', "RECREATE") 
            # newTree  = root.TTree("tree", "Contains the histograms ")
            print('Storing histograms into RootFile...')
            for histBranch in hists.keys():
                # automaticTH1Rebinning(hists[histBranch], 100)
                hists[histBranch]['hist'].Write()
            rootFile.Close()
            hists = None