import ROOT
import numpy as np
import os, sys


## OFC

def readOFCRootFile(file): 
    '''
    ofc_dict = readOFCRootFile(file)
    np.savez('calibration/OFC_dict.npz', OFC=ofc_dict)
    
    Read the OFC NTuple in *.root file, and convert the info to a python dict, saving to .npz.
    '''
#     nLArChannels    = int(547404/3) # 182468 each gain
#     nBarrelChannels = int(328704/3)
#     nEndCapChannels = int(218700/3)
    
    ofc_tree = ROOT.TChain("OFC",'')
    ofc_tree.Add(file+"/OFC")
    
    ofc_dict = {
    'b_ec': [],
    'p_n': [],
    'ft': [],
    'slot': [],
    'ch'   : [],
    'badCh' : [],
    'gain' : [],
    'chId' : [],
    'ofca' : [], # energy
    'ofcb' : []
    }# time
    
    nentries = ofc_tree.GetEntries()
    
    for entry in range(0, nentries):
        if (entry % 50000) == 0:
            print('Entry {} out of {}'.format(entry, nentries))
        
        ofc_tree.GetEntry(entry)

        ofc_dict['b_ec'].append(ofc_tree.barrel_ec)
        ofc_dict['p_n'].append(ofc_tree.pos_neg)
        ofc_dict['ft'].append(ofc_tree.FT)
        ofc_dict['slot'].append(ofc_tree.slot)
        ofc_dict['ch'].append(ofc_tree.channel)
        ofc_dict['gain'].append(ofc_tree.Gain)
        ofc_dict['badCh'].append(ofc_tree.badChan)
        ofc_dict['chId'].append(ofc_tree.channelId)
        ofc_dict['ofca'].append(ofc_tree.OFCa)
        ofc_dict['ofcb'].append(ofc_tree.OFCb)
    
    ofc_dict['b_ec']    = np.array(ofc_dict['b_ec'])
    ofc_dict['p_n']     = np.array(ofc_dict['p_n'])
    ofc_dict['ft']      = np.array(ofc_dict['ft'])
    ofc_dict['slot']    = np.array(ofc_dict['slot'])
    ofc_dict['ch']      = np.array(ofc_dict['ch'])
    ofc_dict['gain']    = np.array(ofc_dict['gain'])
    ofc_dict['badCh']   = np.array(ofc_dict['badCh'])
    ofc_dict['chId']    = np.array(ofc_dict['chId'])
    ofc_dict['ofca']    = np.array(ofc_dict['ofca'])
    ofc_dict['ofcb']    = np.array(ofc_dict['ofcb'])
    
    if not(os.path.isdir('calibration')):
        os.mkdir('calibration')
    
    np.savez('calibration/OFC_dict.npz', OFC=ofc_dict)
        
    return ofc_dict
#    return ofca, ofcb, b_ec, p_n, ft, slot, ch, gain, chId#, gainIdxMin, gainIdxMax

## PEDESTALS

def readPEDRootFile(file): 
    '''
    ped_dict = readPEDRootFile(file, gainValue)
    np.savez('calibration/PED_dict.npz', PED=PED_dict)
    
    Read the PED NTuple in *.root file, and convert the info to a python dict, saving to .npz.
    '''   
    ped_tree = ROOT.TChain("PEDESTALS",'')
    ped_tree.Add(file+"/PEDESTALS")
    
    ped_dict = {
    'b_ec': [],
    'p_n': [],
    'ft': [],
    'slot': [],
    'ch'   : [],
    'badCh' : [],
    'gain' : [],
    'chId' : [],
    'icell' : [], #cell index
    'ped' : [],
    'rms': []
    }
    
    nentries = ped_tree.GetEntries()
    
    for entry in range(0, nentries):
        if (entry % 50000) == 0:
            print('Entry {} out of {}'.format(entry, nentries))
        
        ped_tree.GetEntry(entry)

        ped_dict['b_ec'].append(ped_tree.barrel_ec)
        ped_dict['p_n'].append(ped_tree.pos_neg)
        ped_dict['ft'].append(ped_tree.FT)
        ped_dict['slot'].append(ped_tree.slot)
        ped_dict['ch'].append(ped_tree.channel)
        ped_dict['gain'].append(ped_tree.gain)
        ped_dict['badCh'].append(ped_tree.badChan)
        ped_dict['chId'].append(ped_tree.channelId)
        ped_dict['icell'].append(ped_tree.icell)
        ped_dict['ped'].append(ped_tree.ped)
        ped_dict['rms'].append(ped_tree.rms)
    
    ped_dict['b_ec']    = np.array(ped_dict['b_ec'])
    ped_dict['p_n']     = np.array(ped_dict['p_n'])
    ped_dict['ft']      = np.array(ped_dict['ft'])
    ped_dict['slot']    = np.array(ped_dict['slot'])
    ped_dict['ch']      = np.array(ped_dict['ch'])
    ped_dict['gain']    = np.array(ped_dict['gain'])
    ped_dict['badCh']   = np.array(ped_dict['badCh'])
    ped_dict['chId']    = np.array(ped_dict['chId'])
    ped_dict['icell']    = np.array(ped_dict['icell'])
    ped_dict['ped']    = np.array(ped_dict['ped'])
    ped_dict['rms']    = np.array(ped_dict['rms'])
    
    if not(os.path.isdir('calibration')):
        os.mkdir('calibration')
    
    np.savez('calibration/PED_dict.npz', PED=ped_dict)
        
    return ped_dict

def readMPMCRootFile(file): 
    '''
    mpmc_dict = readMPMCRootFile(file, gainValue)
    np.savez('calibration/MPMC_dict.npz', MPMC=mpmc_dict)
    
    Read the MPMC NTuple in *.root file, and convert the info to a python dict, saving to .npz.
    '''    
    mpmc_tree = ROOT.TChain("MPMC",'')
    mpmc_tree.Add(file+"/MPMC")
    
    mpmc_dict = {
    'b_ec': [],
    'p_n': [],
    'ft': [],
    'slot': [],
    'ch'   : [],
    'badCh' : [],
    'gain' : [],
    'chId' : [],
    'icell' : [], #cell index
    'mphysovermcal' : []
    }
    
    nentries = mpmc_tree.GetEntries()
    
    for entry in range(0, nentries):
        if (entry % 50000) == 0:
            print('Entry {} out of {}'.format(entry, nentries))
        
        mpmc_tree.GetEntry(entry)

        mpmc_dict['b_ec'].append(mpmc_tree.barrel_ec)
        mpmc_dict['p_n'].append(mpmc_tree.pos_neg)
        mpmc_dict['ft'].append(mpmc_tree.FT)
        mpmc_dict['slot'].append(mpmc_tree.slot)
        mpmc_dict['ch'].append(mpmc_tree.channel)
        mpmc_dict['gain'].append(mpmc_tree.gain)
        mpmc_dict['badCh'].append(mpmc_tree.badChan)
        mpmc_dict['chId'].append(mpmc_tree.channelId)
        mpmc_dict['icell'].append(mpmc_tree.icell)
        mpmc_dict['mphysovermcal'].append(mpmc_tree.mphysovermcal)
    
    mpmc_dict['b_ec']    = np.array(mpmc_dict['b_ec'])
    mpmc_dict['p_n']     = np.array(mpmc_dict['p_n'])
    mpmc_dict['ft']      = np.array(mpmc_dict['ft'])
    mpmc_dict['slot']    = np.array(mpmc_dict['slot'])
    mpmc_dict['ch']      = np.array(mpmc_dict['ch'])
    mpmc_dict['gain']    = np.array(mpmc_dict['gain'])
    mpmc_dict['badCh']   = np.array(mpmc_dict['badCh'])
    mpmc_dict['chId']    = np.array(mpmc_dict['chId'])
    mpmc_dict['icell']    = np.array(mpmc_dict['icell'])
    mpmc_dict['mphysovermcal']    = np.array(mpmc_dict['mphysovermcal'])
    
    if not(os.path.isdir('calibration')):
        os.mkdir('calibration')

    np.savez('calibration/MPMC_dict.npz', MPMC=mpmc_dict)

    return mpmc_dict

# ADC to MeV
def readUAMEVRootFile(file): 
    '''
    adcmev_dict = readUAMEVRootFile(file). Same constants for all gains.
    np.savez('calibration/ADCMEV_dict.npz', ADCMEV=adcmev_dict)
    
    Read the ADCMEV NTuple in *.root file, and convert the info to a python dict, saving to .npz.
    '''    
    adcmev_tree = ROOT.TChain("ADCMEV",'')
    adcmev_tree.Add(file+"/ADCMEV")
    
    adcmev_dict = {
    'b_ec': [],
    'p_n': [],
    'ft': [],
    'slot': [],
    'ch'   : [],
    'badCh' : [],
#     'gain' : [],
    'chId' : [],
    'icell' : [], #cell index
#     'DAC2uA' : [],
    'uAMeV' : []
    }
    
    nentries = adcmev_tree.GetEntries()
    
    for entry in range(0, nentries):
        if (entry % 50000) == 0:
            print('Entry {} out of {}'.format(entry, nentries))
        
        adcmev_tree.GetEntry(entry)

        adcmev_dict['b_ec'].append(adcmev_tree.barrel_ec)
        adcmev_dict['p_n'].append(adcmev_tree.pos_neg)
        adcmev_dict['ft'].append(adcmev_tree.FT)
        adcmev_dict['slot'].append(adcmev_tree.slot)
        adcmev_dict['ch'].append(adcmev_tree.channel)
#         adcmev_dict['gain'].append(adcmev_tree.gain)
        adcmev_dict['badCh'].append(adcmev_tree.badChan)
        adcmev_dict['chId'].append(adcmev_tree.channelId)
        adcmev_dict['icell'].append(adcmev_tree.icell)
#         adcmev_dict['DAC2uA'].append(adcmev_tree.DAC2uA)
        adcmev_dict['uAMeV'].append(adcmev_tree.uAMeV)
    
    adcmev_dict['b_ec']    = np.array(adcmev_dict['b_ec'])
    adcmev_dict['p_n']     = np.array(adcmev_dict['p_n'])
    adcmev_dict['ft']      = np.array(adcmev_dict['ft'])
    adcmev_dict['slot']    = np.array(adcmev_dict['slot'])
    adcmev_dict['ch']      = np.array(adcmev_dict['ch'])
#     adcmev_dict['gain']    = np.array(adcmev_dict['gain'])
    adcmev_dict['badCh']   = np.array(adcmev_dict['badCh'])
    adcmev_dict['chId']    = np.array(adcmev_dict['chId'])
    adcmev_dict['icell']   = np.array(adcmev_dict['icell'])
#     adcmev_dict['DAC2uA']  = np.array(adcmev_dict['DAC2uA'])
    adcmev_dict['uAMeV']   = np.array(adcmev_dict['uAMeV'])
    
    if not(os.path.isdir('calibration')):
        os.mkdir('calibration')
    
    plt.hist(adcmev_dict['chId'])
    np.savez('calibration/UAMEV_dict.npz', UAMEV=adcmev_dict)

    return adcmev_dict

# Ramp
def readRAMPSRootFile(file): 
    '''
    ramp_dict = readRAMPSRootFile(file). Same constants for all gains.
    np.savez('calibration/ramp_dict.npz', ADCMEV=ramp_dict)
    
    Read the RAMPS NTuple in *.root file, and convert the info to a python dict, saving to .npz.
    '''    
    ramp_tree = ROOT.TChain("ramps",'')
    ramp_tree.Add(file+"/RAMPS")
    
    ramp_dict = {
    'b_ec': [],
    'p_n': [],
    'ft': [],
    'slot': [],
    'ch'   : [],
    'badCh' : [],
    'gain' : [],
    'chId' : [],
    'cellIndex' : [], #cell index
    'X' : [],
    'Xi' : []
    }
    
    nentries = ramp_tree.GetEntries()
    
    for entry in range(0, nentries):
        if (entry % 50000) == 0:
            print('Entry {} out of {}'.format(entry, nentries))
        
        ramp_tree.GetEntry(entry)
#         if (entry == 200):
#             break

        ramp_dict['b_ec'].append(ramp_tree.barrel_ec)
        ramp_dict['p_n'].append(ramp_tree.pos_neg)
        ramp_dict['ft'].append(ramp_tree.FT)
        ramp_dict['slot'].append(ramp_tree.slot)
        ramp_dict['ch'].append(ramp_tree.channel)
        ramp_dict['gain'].append(ramp_tree.gain)
        ramp_dict['badCh'].append(ramp_tree.badChan)
        ramp_dict['chId'].append(ramp_tree.channelId)
        ramp_dict['cellIndex'].append(ramp_tree.cellIndex)
        XiAux = np.float32(ramp_tree.X).tolist()
        ramp_dict['X'].append(XiAux)
        ramp_dict['Xi'].append(ramp_tree.Xi)
        
    ramp_dict['b_ec']      = np.array(ramp_dict['b_ec'])
    ramp_dict['p_n']       = np.array(ramp_dict['p_n'])
    ramp_dict['ft']        = np.array(ramp_dict['ft'])
    ramp_dict['slot']      = np.array(ramp_dict['slot'])
    ramp_dict['ch']        = np.array(ramp_dict['ch'])
    ramp_dict['gain']      = np.array(ramp_dict['gain'])
    ramp_dict['badCh']     = np.array(ramp_dict['badCh'])
    ramp_dict['chId']      = np.array(ramp_dict['chId'])
    ramp_dict['cellIndex'] = np.array(ramp_dict['cellIndex'])
    ramp_dict['X']         = np.array(ramp_dict['X'])
    ramp_dict['Xi']        = np.array(ramp_dict['Xi'])
    
    if not(os.path.isdir('calibration')):
        os.mkdir('calibration')

    np.savez('calibration/RAMPS_dict.npz', RAMPS=ramp_dict)
    
    return ramp_dict