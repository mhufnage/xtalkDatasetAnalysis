import numpy as np
import math
import os, sys
# from fxpmath import Fxp
import torch

    
# def getLArChannelOFC(channelId,filename='calibration/OFC_dict.npz'):
def getLArChannelOFC(channelId, ofc_dict):
    '''
    channel_dict = getLArChannelOFC(channelId,filename='calibration/OFC_dict.npz')
    
    Read the LAr_OFC from python dict and return the correspondent OFCa and OFCb and detector mapping [b_ec, p_n, ft, slot, ch],
    for channelId (HWID).
    '''
#     ofc_dict = np.load(filename, allow_pickle=True)['OFC'].tolist()
    
    #find index
    channelIdIdx = np.where(ofc_dict['chId'] == channelId)[0]
#     print(np.shape(channelIdIdx))
    
    channel_dict = {
    'b_ec': [ofc_dict['b_ec'][channelIdIdx]],
    'p_n': [ofc_dict['p_n'][channelIdIdx]],
    'ft': [ofc_dict['ft'][channelIdIdx]],
    'slot': [ofc_dict['slot'][channelIdIdx]],
    'ch'   : [ofc_dict['ch'][channelIdIdx]],
    'badCh' : [ofc_dict['badCh'][channelIdIdx]],
    'gain' : [ofc_dict['gain'][channelIdIdx]],
    'chId' : [ofc_dict['chId'][channelIdIdx]],
    'ofca' : [ofc_dict['ofca'][channelIdIdx]], # energy
    'ofcb' : [ofc_dict['ofcb'][channelIdIdx]] # time
    }
        
    return channel_dict
    

# def getLArChannelPED(channelId,filename='calibration/PED_dict.npz'):
def getLArChannelPED(channelId, ped_dict):
    '''
    channel_dict = getLArChannelPED(channelId,filename='calibration/PED_dict.npz')
    
    Read the LAr_PED from python dict and return the correspondent PED and RMS and detector mapping [b_ec, p_n, ft, slot, ch],
    for channelId (HWID).
    '''
#     ped_dict = np.load(filename, allow_pickle=True)['PED'].tolist()
    
    #find index
    channelIdIdx = np.where(ped_dict['chId'] == channelId)[0]
#     print(np.shape(channelIdIdx))
    
    channel_dict = {
    'b_ec': [ped_dict['b_ec'][channelIdIdx]],
    'p_n': [ped_dict['p_n'][channelIdIdx]],
    'ft': [ped_dict['ft'][channelIdIdx]],
    'slot': [ped_dict['slot'][channelIdIdx]],
    'ch'   : [ped_dict['ch'][channelIdIdx]],
    'badCh' : [ped_dict['badCh'][channelIdIdx]],
    'gain' : [ped_dict['gain'][channelIdIdx]],
    'chId' : [ped_dict['chId'][channelIdIdx]],
    'icell' : [ped_dict['icell'][channelIdIdx]], 
    'ped' : [ped_dict['ped'][channelIdIdx]],
    'rms' : [ped_dict['rms'][channelIdIdx]]
    }
        
    return channel_dict

def getLArChannelMPMC(channelId, mpmc_dict):
    '''
    channel_dict = getLArChannelMPMC(channelId,filename='calibration/MPMC_dict.npz')
    
    Read the LAr_MPMC from python dict and return the correspondent MphysOverMCal and detector mapping [b_ec, p_n, ft, slot, ch],
    for channelId (HWID).
    '''
#     ped_dict = np.load(filename, allow_pickle=True)['PED'].tolist()
    
    #find index
    channelIdIdx = np.where(mpmc_dict['chId'] == channelId)[0]
#     print(np.shape(channelIdIdx))
    
    channel_dict = {
    'b_ec': [mpmc_dict['b_ec'][channelIdIdx]],
    'p_n': [mpmc_dict['p_n'][channelIdIdx]],
    'ft': [mpmc_dict['ft'][channelIdIdx]],
    'slot': [mpmc_dict['slot'][channelIdIdx]],
    'ch'   : [mpmc_dict['ch'][channelIdIdx]],
    'badCh' : [mpmc_dict['badCh'][channelIdIdx]],
    'gain' : [mpmc_dict['gain'][channelIdIdx]],
    'chId' : [mpmc_dict['chId'][channelIdIdx]],
    'icell' : [mpmc_dict['icell'][channelIdIdx]], 
    'mphysovermcal' : [mpmc_dict['mphysovermcal'][channelIdIdx]]
    }
        
    return channel_dict



# def getLArChannelUAMEV(channelId, filename='calibration/UAMEV_dict.npz'):
def getLArChannelUAMEV(channelId, uamev_dict, calibType='database', layers=[], etaCell=[]):
    '''
    channel_dict = getLArChannelUAMEV(channelId,filename='calibration/UAMEV_dict.npz')
    calibType='database' or 'typical' from https://cds.cern.ch/record/942528/files/larg-pub-2006-003.pdf
    
    Read the LAr_UAMEV from python dict and return the correspondent uAMeV and detector mapping [b_ec, p_n, ft, slot, ch],
    for channelId (HWID).
    '''
#     uamev_dict = np.load(filename, allow_pickle=True)['UAMEV'].tolist()
    
    #find index
    if calibType=='database' or calibType=='dumper':
        channelIdIdx = np.where(uamev_dict['chId'] == channelId)[0]
        # print('channelIdIdx: ',channelIdIdx)
        
        channel_dict = {
            'b_ec': [uamev_dict['b_ec'][channelIdIdx]],
            'p_n': [uamev_dict['p_n'][channelIdIdx]],
            'ft': [uamev_dict['ft'][channelIdIdx]],
            'slot': [uamev_dict['slot'][channelIdIdx]],
            'ch'   : [uamev_dict['ch'][channelIdIdx]],
            'badCh' : [uamev_dict['badCh'][channelIdIdx]],
        #     'gain' : [uamev_dict['gain'][channelIdIdx]],
            'chId' : [uamev_dict['chId'][channelIdIdx]],
            'icell' : [uamev_dict['icell'][channelIdIdx]],
            'uAMeV' : [uamev_dict['uAMeV'][channelIdIdx]]
        }

    if calibType=='typical':
        channel_dict = {
            'uAMeV' : []
        }
        
        uamev_dict = {
            'PreSamplerB' : 1149,
            'EMB1'        : 375, #320
            'EMB2'        : [375, 320], #[(eta < 0.8),  #(eta > 0.8)]
            'EMB3'        : 375,
            'PreSamplerE' : 1149,
            'EME1'        : 375,
            'EME2'        : [375, 320], #[(eta < 0.8),  #(eta > 0.8)]
            'EME3'        : 375 #320
        }
        
        if (type(layers)!= list ):
            layers=[layers]
        if (type(etaCell)!=list):
            etaCell=[etaCell]
        if (len(layers)!=len(etaCell)):
            print("Layers and etaCell lists have different sizes! return -8888")
            return -8888

        for index, lay in enumerate(layers):
#         print(index,lay)
            if lay=='PreSamplerE':
                channel_dict['uAMeV'].append(uamev_dict['PreSamplerE'])
            if lay=='EME1':
                channel_dict['uAMeV'].append(uamev_dict['EME1'])
            if lay=='EME2' and etaCell[index]<=0.8:
                channel_dict['uAMeV'].append(uamev_dict['EME2'][0])
            if lay=='EME2' and etaCell[index]>0.8:
                channel_dict['uAMeV'].append(uamev_dict['EME2'][1])
            if lay=='EME3':
                channel_dict['uAMeV'].append(uamev_dict['EME3'])
            if lay=='PreSamplerB':
                channel_dict['uAMeV'].append(uamev_dict['PreSamplerB'])
            if lay=='EMB1':
                channel_dict['uAMeV'].append(uamev_dict['EMB1'])
            if lay=='EMB2' and etaCell[index]<=0.8:
                channel_dict['uAMeV'].append(uamev_dict['EMB2'][0])
            if lay=='EMB2' and etaCell[index]>0.8:
                channel_dict['uAMeV'].append(uamev_dict['EMB2'][1])
            if lay=='EMB3':
                channel_dict['uAMeV'].append(uamev_dict['EMB3'])
            if lay not in uamev_dict.keys():
                # print('in getLArChannelUAMEV: layer {} and/or etaCell not found! return -9999.'.format(lay))
                channel_dict['uAMeV'].append( -9999)
        
    # print(channel_dict)
    return channel_dict

def getLArChannelRAMPS(channelId, ramps_dict):
    '''
    channel_dict = getLArChannelRAMPS(channelId,filename='calibration/UAMEV_dict.npz')
    
    Read the LAr_UAMEV from python dict and return the correspondent uAMeV and detector mapping [b_ec, p_n, ft, slot, ch],
    for channelId (HWID).
    '''
    #find index
    channelIdIdx = np.where(ramps_dict['chId'] == channelId)[0]
#     print(np.shape(channelIdIdx))

    
    
    channel_dict = {
    'b_ec': [ramps_dict['b_ec'][channelIdIdx]],
    'p_n': [ramps_dict['p_n'][channelIdIdx]],
    'ft': [ramps_dict['ft'][channelIdIdx]],
    'slot': [ramps_dict['slot'][channelIdIdx]],
    'ch'   : [ramps_dict['ch'][channelIdIdx]],
    'badCh' : [ramps_dict['badCh'][channelIdIdx]],
    'gain' : [ramps_dict['gain'][channelIdIdx]],
    'chId' : [ramps_dict['chId'][channelIdIdx]],
#     'icell' : [ramps_dict['icell'][channelIdIdx]],
    'cellIndex' : [ramps_dict['cellIndex'][channelIdIdx]],
    'X' : [ramps_dict['X'][channelIdIdx]], # [R0, R1] -> R0 + R1.ADC_peak
    'Xi' : [ramps_dict['Xi'][channelIdIdx]]
    }
        
    return channel_dict

def getLArChannelDAC2uA(layers, etaCell):
    '''
    layers and etaCell can be arrays or lists. The return will always be a list with the respective constants
    DAC2uA (https://cds.cern.ch/record/942528)
    '''
    dac2ua_dict = {
        'PreSamplerB' : 0.002284,
        'EMB1'        : 0.025464,
        'EMB2'        : [0.150127, 0.0751078], #[(eta < 0.8),  #(eta > 0.8)]
        'EMB3'        : 0.0751078,
        'PreSamplerE' : 0.002284,
        'EME1'        : 0.025464,
        'EME2'        : [0.150127, 0.0751078], #[(eta < 0.8),  #(eta > 0.8)]
        'EME3'        : 0.0751078
    }
    if (type(layers)!= list ):
        layers=[layers]
    if (type(etaCell)!=list):
        etaCell=[etaCell]
    if (len(layers)!=len(etaCell)):
        print("Layers and etaCell lists have different sizes! return -8888")
        return -8888
        
    dac2ua_channels_dict = []
    
    for index, lay in enumerate(layers):
#         print(index,lay)
        if lay=='PreSamplerE':
            dac2ua_channels_dict.append(dac2ua_dict['PreSamplerE'])
        if lay=='EME1':
            dac2ua_channels_dict.append(dac2ua_dict['EME1'])
        if lay=='EME2' and etaCell[index]<=0.8:
            dac2ua_channels_dict.append(dac2ua_dict['EME2'][0])
        if lay=='EME2' and etaCell[index]>0.8:
            dac2ua_channels_dict.append(dac2ua_dict['EME2'][1])
        if lay=='EME3':
            dac2ua_channels_dict.append(dac2ua_dict['EME3'])
        if lay=='PreSamplerB':
            dac2ua_channels_dict.append(dac2ua_dict['PreSamplerB'])
        if lay=='EMB1':
            dac2ua_channels_dict.append(dac2ua_dict['EMB1'])
        if lay=='EMB2' and etaCell[index]<=0.8:
            dac2ua_channels_dict.append(dac2ua_dict['EMB2'][0])
        if lay=='EMB2' and etaCell[index]>0.8:
            dac2ua_channels_dict.append(dac2ua_dict['EMB2'][1])
        if lay=='EMB3':
            dac2ua_channels_dict.append(dac2ua_dict['EMB3'])
        if lay not in dac2ua_dict.keys():
            # print('in getLArChannelDAC2uA: layer {} and/or etaCell not found! return -9999.'.format(lay))
            dac2ua_channels_dict.append( -9999)

    # print('dac2ua_channels_dict:', dac2ua_channels_dict)
    return dac2ua_channels_dict

################################################

## OLDER FUNCTION ##
def applyOFCandPEDtoLArSamples(samples, OFCa, OFCb, ped): 
    samples_noPed = []
    for samp in samples:
        samples_noPed.append( samp - ped )
    
    A     = np.dot(samples_noPed, OFCa)
    Atau  = np.dot(samples_noPed, OFCb)
    tau   = Atau/A
    
    return np.float32(A), np.float32(tau), Atau

## New OFC functions ##-----------------------------
def applyOFCEnergyAndPEDtoLArSamples(samples, OFCa, ped): # Online Calibration
    samples_noPed = []
    for samp in samples:
        samples_noPed.append( samp - ped )
    
    A     = np.dot(samples_noPed, OFCa)
    # Atau  = np.dot(samples_noPed, np.flip(OFCb,0))
    # tau   = Atau/A
    
    return float(A)#, np.float32(tau), Atau

def applyOFCEnergytoLArSamples(samples, OFCa): # Online Calibration
    A     = np.dot(samples, OFCa)    
    return float(A)#, np.float32(tau), Atau

# Lorenzetti Energy estimation
def applyOFCEnergyToLorenzettiSamples(samples, OFCa, mode='numpy'):
    if mode=='torch':
        torch.tensor(samples, OFCa)
    else:
        return np.dot(samples, OFCa)

# Lorenzetti Time estimation
def applyOFCTimeToLorenzettiSamples(samples, OFCb, energy, samplingNoise=60, mode='numpy'):
    if mode=='torch':
        if energy < 2*samplingNoise:
            return 0.0
        else:    
            EneTau  = torch.tensor(samples, OFCb, dims=1)
            tau     = EneTau/energy
            return tau
    else:
        if energy < 2*samplingNoise:
            return 0.0
        else:    
            EneTau  = np.dot(samples, OFCb)
            tau     = EneTau/energy
            return tau
        

def applyPEDremover(samples, ped):
    samples_noPed = []
    for samp in samples:
        samples_noPed.append( samp - ped )
    return samples_noPed


# def applyOFCTimingToLArSamples(samples, OFCb, ped, A, Ene, thr_ene=100): #thr_ene in energy to fix the time estimation at zero.
#     samples_noPed = []
#     for samp in samples:
#         samples_noPed.append( samp - ped )
    
#     # A     = np.dot(samples_noPed, np.flip(OFCa,0))
#     if Ene < thr_ene:
#         Atau    = 0
#         tau     = 0
#     else:
#         Atau  = np.dot(samples_noPed, OFCb)
#         tau   = Atau/A
    
#     return np.float32(tau), Atau

# ------------------------------------------
def applyDSPOnlineOFCTimingToLArSamples_quantized(samples, beta, Pb, DSPEne, DSPThr, nb, timeOffset=0, thresholdMode='EneThr', bDoAbsE1=True): #thr_ene in energy to fix the time estimation at zero.
    '''
    The DSPEne here is the Online Calibrated energy, or E1, according to the diagram made from LArRawChannelBuilderAlg.cxx.
    That is corrected from ADC to MeV, using the Ramp constants.

    DSPThr should be the DSPThreshold variable (MeV)
    https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/LArCalorimeter/LArROD/src/LArRawChannelBuilderAlg.cxx

    '''
    samples_noPed   = []
    thrDiff         = -999999
    tau             = 0.0
    Etau            = 0.0
    
    if thresholdMode=='EneThr':

        if bDoAbsE1:
            decisionThr = math.fabs(DSPEne) > DSPThr
            thrDiff     = math.fabs(DSPEne) - DSPThr
        else:
            decisionThr = DSPEne > DSPThr
            thrDiff     = DSPEne - DSPThr

        if decisionThr:
            # Atau  = np.dot(samples_noPed, OFCb)
            for k, samp in enumerate(samples):  
                Etau = Etau + beta[k]*samp
            Etau    = Etau - Pb
            tau = Etau/DSPEne
            tau = tau - timeOffset 
            # tau = tau * 1000 # ns to ps
            # tau_quant = Fxp(tau, signed=True, n_word=16, n_frac=nb, rounding='floor')
            tau_quant = tau#int(np.floor(tau*2**nb))/2**nb
            tau_quant = tau_quant * 1000 # ns to ps
            # tau_quant = int(np.floor(tau*2**nb))*2**(-nb)
            # tau_quant = Fxp(tau_quant, signed=True, n_word=16, n_frac=nb, rounding='floor')
        else:
            tau_quant = 0.0

    # if thresholdMode=='NoiseSigma':

    #     ratio = DSPEne / DSPThr # here, the DSPThr is the noiseEffSigma (which is the same from just noise)
        
    #     if (ratio > 3):
    #         Atau  = np.dot(samples_noPed, OFCb)
    #         if math.fabs(A) > 0.1:
    #             tau = Atau/A #ps
    #             # tau = Atau/DSPEne
    #             tau = tau - timeOffset 
    #             tau = tau * 1000 # ns to ps
    #         else:
    #             tau = 0.0        
    # else:
    #     print("The cell DSP timing does not match to the estimation conditions.")
        # print(tau_quant)
    return int(np.floor(tau_quant+0.5)), tau, thrDiff
    # return tau_quant, tau, thrDiff


def applyDSPOnlineOFCTimingToLArSamples(samples, OFCb, ped, A, DSPEne, DSPThr, timeOffset=0, thresholdMode='EneThr', bDoAbsE1=True, bSamplesWithPed=True): #thr_ene in energy to fix the time estimation at zero.
    '''
    The DSPEne here is the Online Calibrated energy, or E1, according to the diagram made from LArRawChannelBuilderAlg.cxx.
    That is corrected from ADC to MeV, using the Ramp constants.

    DSPThr should be the DSPThreshold variable (MeV)
    https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/LArCalorimeter/LArROD/src/LArRawChannelBuilderAlg.cxx

    '''
    samples_noPed   = []
    thrDiff         = -999999
    tau             = 0.0
    # Atau            = 0.0

    if bSamplesWithPed:
        for samp in samples:
            samples_noPed.append( samp - ped )
    else:
        samples_noPed = samples
    
    # A     = np.dot(samples_noPed, np.flip(OFCa,0))
    # if (DSPEne <= DSPThr) or (math.fabs(A) <= 0.1):
    #     # Atau    = 0.0
    #     tau     = 0.0
    #*1000 # ns to ps
    # print("        DSPEne={} | DSPThr={}".format(DSPEne,DSPThr))
    if thresholdMode=='EneThr':

        if bDoAbsE1: ## not working well 
            decisionThr_ene = math.fabs(DSPEne) > DSPThr
            # decisionThr = math.fabs(DSPEne) > DSPThr
            thrDiff         = math.fabs(DSPEne) - DSPThr
            # decisionThr     = decisionThr_ene or ((thrDiff > -1.5) and  (thrDiff < 0))# testing the margin error of aproval for cells closest to noise region.
            decisionThr     = decisionThr_ene# testing the margin error of aproval for cells closest to noise region.
        else: # update the rules: (reverse engineering the DSP code, which i don't have access.) 26/06/2023 - mateus
            # Step 1: reject negative energies (DSPThr always positive)
            decisionThr_1 = DSPEne < 0 # (True: reject)

            # Step 2: case int(DSPEne) <= int(E1), reject
            decisionThr_2 = int(DSPEne) > int(DSPThr)

            # Step 3: evaluate to finally approve or reject
            #        True: approve cell to estimate time, case DSPEne is not negative, 
            #              and difference is bigger than 1.0 MeV.
            decisionThr = not(decisionThr_1) and decisionThr_2 

            thrDiff     = DSPEne - DSPThr

        if decisionThr:
            Atau  = np.dot(samples_noPed, OFCb)
            if math.fabs(A) > 0.1:
                tau = Atau/A #ps
                # tau = Atau/DSPEne
                tau = tau - timeOffset
                # ** round before convert to ps  **
                # tau = tau * 1000 # ns to ps
            else:
                tau = 0.0
        else:
            tau = 0.0

    if thresholdMode=='NoiseSigma':

        ratio = DSPEne / DSPThr # here, the DSPThr is the noiseEffSigma (which is the same from just noise)
        
        if (ratio > 3):
            Atau  = np.dot(samples_noPed, OFCb)
            if math.fabs(A) > 0.1:
                tau = Atau/A #ps
                # tau = Atau/DSPEne
                tau = tau - timeOffset 
                tau = tau * 1000 # ns to ps
            else:
                tau = 0.0
    if thresholdMode=='MC':
        Atau    = np.dot(samples_noPed, OFCb)
        tau     = Atau/A
        tau     = tau - timeOffset

        return [tau, 0]
    # else:
    #     print("The cell DSP timing does not match to the estimation conditions.")
    
    return [int(np.floor(tau+0.5)), thrDiff] ## old rounding
    # return [np.round(tau/10)*10, thrDiff] ## new rounding 26/06/2023 - mateus
##---------------------------------------------------

def applyOFCtoLArSamples(samples, OFCa, OFCb):
    A     = np.dot(samples, OFCa)
    Atau  = np.dot(samples, OFCb)
    
    return A, Atau

def getGainInteger(gainString):
    '''
    convert the gain string to integer for a set o values.
    '''
    gainInteger = []
    for gain in gainString:
        if gain == 'LARHIGHGAIN':
            gainInteger.append(0)
        if gain == 'LARMEDIUMGAIN':
            gainInteger.append(1)
        if gain == 'LARLOWGAIN':
            gainInteger.append(2)
    return gainInteger


def getOFCEstimationInCluster(chIds, samples, gain, ofc_dict, ped_dict):
    '''
    Pass a list of chIds, samples and gain, for respective cells.
    '''
    AClus       = []
    tauClus     = []
    AtauClus    = []
    
    for chId_cell, samp_cell, gain_cell in zip(chIds, samples, gain):
        cell_dict_ofc = getLArChannelOFC(chId_cell, ofc_dict)
        cell_dict_ped = getLArChannelPED(chId_cell, ped_dict)
#         print(gain_cell)

    #     A, Atau = applyOFCtoLArSamples(samp_cell, cell_dict['ofca'][0][gain_cell][0:4], cell_dict['ofcb'][0][gain_cell][0:4])
#         print(cell_dict_ofc['ofca'][0][gain_cell][0:4])
        A, tau, Atau = applyOFCandPEDtoLArSamples(samp_cell, cell_dict_ofc['ofca'][0][gain_cell][0:4], cell_dict_ofc['ofcb'][0][gain_cell][0:4], cell_dict_ped['ped'][0][gain_cell])
        
        AClus.append(A)
        tauClus.append(tau)
        AtauClus.append(Atau)
        
    return AClus, tauClus, AtauClus

# def applyCalibConstantsToAmplitude(A, tau, chIds, gain, layers, etaCell, ramps_dict, uamev_dict):
#     ACorr = []
#     tauCorr = []
    
#     dac2ua_list  = getLArChannelDAC2uA(layers, etaCell)
#     uamev_dict   = getLArChannelUAMEV(chIds  , uamev_dict)
#     ramps_dict   = getLArChannelRAMPS(chIds  , ramps_dict)
    
#     print('ua2mev: ',uamev_dict)
#     print('ramps: ',ramps_dict)
    
#     return 0

def getLArDSPMaxAlphaBeta(ofc_dict, ped_dict, ramps_dict):
    ''' To get a better estimation of the maximum alpha and beta values, 
    needed to calculate the 'n', which will be used to built the fixed-point calibration constants.
    alpha_i is the coefficient for each sample. Same for beta_i.
    So, max(alpha_i) will give the maximum for each sample.
    '''
    gain    = 0 #the gain is HIGH (0)
    
    ofca    = []
    ofcb    = []
    ped     = []
    X       = []

    alpha   = []
    beta    = []
    maxAlpha = []
    maxBeta  = []

    for cell in range(0, int(len(ofc_dict['b_ec'])/3)):        
        b_ec =  ofc_dict['b_ec'][cell]
        # if cell>50:
        #     break
        if b_ec == 0: # if cell belongs to BARREL 
            alpha_i   = []
            beta_i    = []

            ofca  = ofc_dict['ofca'][cell] # energy
            ofcb  = ofc_dict['ofcb'][cell] # time
            ped   = ped_dict['ped'][cell]
            X     = ramps_dict['X'][cell] # [R0, R1] -> R0 + R1.ADC_peak

            for i in range(0, len(ofca)): # multiply each OFC out of 4 samples.
                alpha_i.append( X[0] + X[1]*ofca[i] )
                beta_i .append( X[0] + X[1]*ofcb[i] )
            # print('alpha_i: ',alpha_i)
            # print('beta_i: ',beta_i)
            alpha.append( alpha_i )
            beta.append ( beta_i  )

    alpha   = np.array(alpha)
    beta    = np.array(beta)
    # print('alpha: ',alpha)
    # print('beta: ',beta)
    for i in range(0,4):
        maxAlpha.append(max(alpha[:,i]))
        maxBeta.append(max(beta[:,i]))
    
    return maxAlpha, maxBeta

def getLArDSPnFactor(ofc_dict, ped_dict, ramps_dict, reorderSamples=False):
    ''' Generate the calibration constants that are loaded to the DSP: link: https://cds.cern.ch/record/1211835?ln=pt
    They are created in full precision (32 bits), from variables extracted directly from DB.
    Then, they are converted to the required DSP precision.
    '''
    gain    = 0 #the gain is HIGH (0)
    
    ofca    = []
    ofcb    = []
    ped     = []
    X       = []

    alpha   = []
    beta    = []
    Pa      = []
    Pb      = []

    maxAlpha = []
    maxBeta  = []
    maxPa   = -1
    maxPb   = -1

    if reorderSamples:
        leftShiftBy2 = math.log2(2)
    else:
        leftShiftBy2 = 1

    for cell in range(0, int(len(ofc_dict['b_ec'])/3)): # low gain
        b_ec =  ofc_dict['b_ec'][cell]
        # if cell>50:
        #     break
        if b_ec == 0: # if cell belongs to BARREL 
            alpha_i   = []
            beta_i    = []
            Pa_i    = []
            Pb_i    = []

            ofca  = ofc_dict['ofca'][cell] # energy
            ofcb  = ofc_dict['ofcb'][cell] # time
            ped   = ped_dict['ped'][cell] # pedestal
            X     = ramps_dict['X'][cell] # [R0, R1] -> R0 + R1.ADC_peak

            for i in range(0, len(ofca)): # multiply each OFC out of 4 samples.
                alpha_i.append( X[1]*ofca[i] )
                beta_i .append( X[1]*ofcb[i] )
                Pa_i.append(    X[1]*ofca[i] )#*ped)
                Pb_i.append(    X[1]*ofcb[i] )#*ped)

            alpha.append( alpha_i           )
            beta.append ( beta_i            )
            Pa.append   ( ped*np.sum(Pa_i)  )
            Pb.append   ( ped*np.sum(Pb_i)  )

    alpha = np.array(alpha)
    beta  = np.array(beta)
    Pa    = np.array(Pa)
    Pb    = np.array(Pb)

    for i in range(0,4):
        maxAlpha.append(max(alpha[:,i]))
        maxBeta.append(max(beta[:,i]))
    maxPa = max(Pa)
    maxPb = max(Pb)
  
    #------------------------------
    #   Calculate the 'n'
    #------------------------------
    n_alpha_upper   = []
    n_alpha_lower   = []
    n_pa_upper      = -1
    n_pa_lower      = -1
    n_beta_upper    = []
    n_beta_lower    = []
    n_pb_upper      = -1
    n_pb_lower      = -1

    for i in range(0,4):
        n_alpha_upper.append(int( 15 - math.log2(np.abs(maxAlpha[i])) ))
        n_alpha_lower.append(int( 14 - math.log2(np.abs(maxAlpha[i])) ))
        n_beta_upper.append(int( 15 - math.log2 (np.abs(maxBeta[i])) ))
        n_beta_lower.append(int( 14 - math.log2 (np.abs(maxBeta[i])) ))
    n_pa_upper = int( (32 - math.log2(np.abs(maxPa)) )/leftShiftBy2 )
    n_pa_lower = int( (31 - math.log2(np.abs(maxPa)) )/leftShiftBy2 )
    n_pb_upper = int( (32 - math.log2(np.abs(maxPb)) )/leftShiftBy2 )
    n_pb_lower = int( (31 - math.log2(np.abs(maxPb)) )/leftShiftBy2 )

    n_alpha = []
    n_beta  = []
    n_pa    = -1
    n_pb    = -1
    na      = -1 #final n values
    nb      = -1 

    for i in range(0,4):
        # Alpha
        lower_cond = 2**14 <= np.abs(maxAlpha[i])*2**n_alpha_lower[i]
        upper_cond = 2**15 > np.abs(maxAlpha[i])*2**n_alpha_upper[i]
        if lower_cond:
            n_alpha.append(n_alpha_lower[i])
        elif upper_cond:
            n_alpha.append(n_alpha_upper[i])
        else:
            print('Neither conditions for alpha were accepted!')
        
        # Beta
        lower_cond = 2**14 <= np.abs(maxBeta[i])*2**n_beta_lower[i]
        upper_cond = 2**15 > np.abs(maxBeta[i])*2**n_beta_upper[i]
        if lower_cond:
            n_beta.append(n_beta_lower[i])
        elif upper_cond:
            n_beta.append(n_beta_upper[i])
        else:
            print('Neither conditions for beta were accepted!')

    # Pa
    lower_cond = 2**31 <= np.abs(maxPa)*2**n_pa_lower
    upper_cond = 2**32 > np.abs(maxPa)*2**n_pa_upper
    if lower_cond:
        n_pa = n_pa_lower
    elif upper_cond:
        n_pa = n_pa_upper
    else:
        print('Neither conditions for Pa were accepted!')

    # Pb
    lower_cond = 2**31 <= np.abs(maxPb)*2**n_pb_lower
    upper_cond = 2**32 > np.abs(maxPb)*2**n_pb_upper
    if lower_cond:
        n_pb = n_pb_lower
    elif upper_cond:
        n_pb = n_pb_upper
    else:
        print('Neither conditions for Pb were accepted!')
        
    # Get the 'n' to avoid overflow
    na  = min(min(n_alpha), n_pa)
    nb  = min(min(n_beta), n_pb)
    n   = min(na,nb)

    # print(na,nb)

    return na, nb, n

def getLArDSPLoadedConstants(chids, ofcas, ofcbs, peds, ramp0, ramp1, na, nb, roundType='floor', applyQuantization=False, reorderSamples=False):
    ''' Step 0
    Generate the calibration constants that are loaded to the DSP: link: https://cds.cern.ch/record/1211835?ln=pt
    They are created in full precision (32 bits), from variables extracted directly from DB.
    Then, they are converted to the required DSP precision.
    Step 1: get the n factor.
    Step 2: quantize them.
    Step 3: round the constants.
    '''
    alpha   = []
    beta    = []
    Pa      = []
    Pb      = []
    P_ped   = []

    if reorderSamples:
        leftShiftBy2 = 2**2
    else:
        leftShiftBy2 = 1

    if len(ofcas)==0:
        return alpha, beta, Pa, Pb, P_ped
        
    for k in range(0, len(ofcas)):
        alpha_i = []
        beta_i  = []
        # Pa_i    = []
        # Pb_i    = []
        # cell_dict_ped   = getLArChannelPED  (chids[k], ped_dict  )
        # ped             = cell_dict_ped['ped'][0][0] # low gain

        for i in range(0, len(ofcas[k])): # multiply each OFC out of 4 samples.
            alpha_i.append( ramp1[k]*ofcas[k][i] )
            beta_i .append( ramp1[k]*ofcbs[k][i] )
            # alpha_i.append( ramp0[k] + ramp1[k]*ofcas[k][i] )
            # beta_i .append( ramp0[k] + ramp1[k]*ofcbs[k][i] )
            # Pb_i   .append( ramp0[k] + ramp1[k]*ofcbs[k][i] )
            # Pa_i   .append( ramp0[k] + ramp1[k]*ofcas[k][i] )
        
        alpha.append( alpha_i   )
        beta.append ( beta_i    )
        # Pa.append   ( ped*np.sum(Pa_i)  )
        # Pb.append   ( ped*np.sum(Pb_i)  )
        P_ped.append( peds[k]       )

    # Variables in 32 bits (full precision)
    alpha   = np.array(alpha)
    beta    = np.array(beta)
    Pa      = np.array(np.sum(alpha,1)*P_ped)
    Pb      = np.array(np.sum(beta,1)*P_ped)
    P_ped   = np.array(P_ped)

    if applyQuantization==False:
        return alpha, beta, Pa, Pb, P_ped
    else:    
        #------------------------------------
        #   Quantization (2) + Rounding (3)
        #------------------------------------

        alpha_int   = np.int16(np.floor(alpha * 2**na + 0.5))               *2**(-na)    # rounded[alpha32 * 2^n]|_16bits signed
        beta_int    = np.int16(np.floor(beta  * 2**nb + 0.5))               *2**(-nb)     # rounded[beta32 * 2^n]|_16bits signed
        P_ped_int   = np.int16(np.floor(P_ped * leftShiftBy2 + 0.5))        *2**(-na)
        Pa_int      = np.int32(np.floor( np.sum(alpha_int,1)*P_ped + 0.5))  *2**(-na)  
        Pb_int      = np.int32(np.floor( np.sum(beta_int,1)*P_ped  + 0.5))  *2**(-nb)  
        
        # alpha_quant   = (np.floor((alpha*2**na-1))).astype(int)*2**(-na) 
        # beta_quant    = (np.floor((beta*2**nb-1))).astype(int)*2**(-nb)
        # P_ped_quant   = np.floor(P_ped*2**na-1).astype(int)*2**(-na)
        # Pa_quant      = np.sum(alpha_quant,1)*P_ped
        # Pb_quant      = np.sum(beta_quant,1)*P_ped
        
        # return alpha_quant, beta_quant, Pa_quant, Pb_quant, P_ped_quant

        ## FXP Package
        # alpha_16int         = Fxp(alpha_int, signed=True, n_word=16, n_frac=0, rounding=roundType)
        # alpha_16    = Fxp(alpha, signed=True, n_word=16, n_frac=na, rounding=roundType)
        # beta_16     = Fxp(beta, signed=True, n_word=16, n_frac=nb, rounding=roundType)
        # Pa_32       = Fxp(Pa, signed=True, n_word=32, n_frac=na, rounding=roundType)
        # Pb_32       = Fxp(Pb, signed=True, n_word=32, n_frac=nb, rounding=roundType)
        # P_ped_16    = Fxp(P_ped, signed=True, n_word=16, n_frac=nb, rounding=roundType)
        
        # print(alpha_16)
        # print(alpha)
        #------------------------------
        #  Special Rounding (3)
        #------------------------------
        # skip for now

        # alpha_16b = Fxp(alpha, signed=True, n_word=16, n_frac=3)

        # return alpha, beta, Pa, Pb
        # return alpha_16, beta_16, Pa_32, Pb_32, P_ped_16
        if reorderSamples:
            return reorderDSPSamples(alpha_int), reorderDSPSamples(beta_int), P_ped_int, Pa_int, Pb_int
        else:
            return alpha_int, beta_int, P_ped_int, Pa_int, Pb_int

# def getIntegerValueForLArDSPLoadedConstants():
#     ''' Step 1: find the scale to transform alpha and beta in 16-bit integers.
#     link: https://cds.cern.ch/record/1211835?ln=pt
    
#     '''

def reorderDSPSamples(samples):
    '''
    Standard order: s0,s1,s2,s3
    DSP Reordered : s2,s0,s1,s3
    '''
    reorderedSamples = np.full_like(samples, 1)
    reorderedSamples[:,0] = samples[:,2]
    reorderedSamples[:,1] = samples[:,0]
    reorderedSamples[:,2] = samples[:,1]
    reorderedSamples[:,3] = samples[:,3]
    
    return reorderedSamples

def DSPCalibSamplesInCluster_quantized( samples, alpha, beta, Pa, Pb, DSPEneThrs, na, nb, timeOffset, bDoAbsE1=True, reorderSamples=False):

    AClus               = []
    tauDSPClus          = []
    EDSPClus            = []
    tauDSPClus_thrDiff  = []

    if reorderSamples:
        samples = reorderDSPSamples(samples)

    for k in range(0, len( samples) ):

        # Ene = Fxp(0, signed=True, n_word=32, n_frac=na, rounding='floor')
        # EnePacked = Fxp(Ene, signed=True, n_word=16, n_frac=na, rounding='floor')
        Ene = 0
        for i in range(0, 4):
            Ene = Ene + samples[k][i] * alpha[k][i] #integers fxp
        Ene = Ene - Pa[k]
        # E_quant = np.float16(Ene*2**(-na))#to floating point  ###Fxp(Ene, signed=True, n_word=16, n_frac=na, rounding='floor')
        E_quant = Ene
        # E_quant = int(np.floor(Ene*2**na))*2**(-na)#Fxp(Ene, signed=True, n_word=16, n_frac=na, rounding='floor')
        # E_quant = Fxp(Ene, signed=True, n_word=16, n_frac=na, rounding='floor')

        tau_dsp_quant, tau_dsp, tau_dsp_thrDiff = applyDSPOnlineOFCTimingToLArSamples_quantized(samples[k], beta[k], Pb[k], E_quant, DSPEneThrs[k], nb, timeOffset=timeOffset[k], thresholdMode='EneThr', bDoAbsE1=bDoAbsE1)

        AClus.append(0)
        EDSPClus.append(int(np.floor(E_quant + 0.5)))
        tauDSPClus.append(tau_dsp_quant)
        tauDSPClus_thrDiff.append(tau_dsp_thrDiff)


    return AClus, tauDSPClus, EDSPClus, tauDSPClus_thrDiff

def getOfflinePileUpOffset(evt_lumiblock, lumiPerBCIDVector, evt_bcid, minBiasAvgs, ofcas, shapes):
    offl_pileupOffset = []
    # equation that uses ofca, shape, minBiasAvg, bcid, luminosityPerBCIDVector

    return offl_pileupOffset

def offlineCalibInCluster(dsp_ene, dsp_tau, offl_HVScale, offl_EneRescaler, offl_pileupOffset, offl_tauOffset):
    ''' Inputs:     DSP energy and Time, Offline HV corrections, Energy Rescale, Pile-up offset, (time scale correction)
        Outputs:    Offline Energy and time.
    '''
    offl_ene = []
    offl_tau = []

    for k in range(0, len(dsp_ene)):
        offl_ene.append(dsp_ene[k] * offl_HVScale[k] * offl_EneRescaler[k] - offl_pileupOffset)
        offl_tau.append(dsp_tau[k] * 1/1000 + offl_tauOffset) # ps to ns

    return offl_ene, offl_tau

def DSPCalibSamplesInCluster(chIds, samples, gain, peds, ofca_dumper, ofcb_dumper, ramp0_dumper, ramp1_dumper, DSPEneThrs, timeOffset, thresholdMode='EneThr', bDoAbsE1=True, bDigitsWithPed=True): # 
    '''
    thresholdMode can be:
        'EneThr', 'fixedValue', 'NoiseSigma' or 'MC' (no threshold)
        bDigitsWithPed: input digits to calibration (OFCa, OFCb, DSP) has pedestal to be removed (True) or not (False)
    '''
    AClus               = []
    tauDSPClus          = []
    EDSPClus            = []
    tauDSPClus_thrDiff  = []
    samples_noPedClus   = []

    for k, chId_cell in enumerate( chIds ):

        # gain_cell = getGainInteger(gain)[k] ## removed 1/12/2023

        # cell_dict_ofc   = getLArChannelOFC  (chId_cell, ofc_dict  )
        # cell_dict_ped   = getLArChannelPED  (chId_cell, ped_dict  )  // uncomment to use dict ped
        # cell_dict_ramp  = getLArChannelRAMPS(chId_cell, ramps_dict)

        # Ramp fit parameters
        R0          = ramp0_dumper[k]
        R1          = ramp1_dumper[k]

        ofca        = ofca_dumper[k]
        ofcb        = ofcb_dumper[k]
        ped         = peds[k]
        # print("(ER) R0: {:.2f}, R1: {:.2f}, OFCa: {}, OFCb: {}".format(R0,R1, ofca, ofcb))
        
        # ped             = cell_dict_ped['ped'][0][gain_cell] // uncomment to use dict ped

        ## updated at 28/08/2023
        samples_noPed       = applyPEDremover(samples[k], ped)
        
        if bDigitsWithPed: # input has pedestal
            A_estimated     = applyOFCEnergyAndPEDtoLArSamples(samples[k], ofca , ped)
        else:           # input do not have pedestal
            A_estimated     = applyOFCEnergytoLArSamples(samples[k], ofca)

        # Ene_dsp         = R1*A_estimated # online without R0 sum 
        Ene_dsp         = R0 + R1*A_estimated # online with R0 sum
        Ene_dsp         = int(np.floor(Ene_dsp + 0.5)) ## update: 27/06/2023 Mateus
        # Ene_dsp         = int(Ene_dsp) ## update: 27/06/2023 Mateus

        if thresholdMode=='fixedValue':
            tau_dsp, tau_dsp_thrDiff    = applyDSPOnlineOFCTimingToLArSamples(samples[k], ofcb , ped , A_estimated, Ene_dsp, DSPEneThrs, timeOffset[k], thresholdMode, bDoAbsE1, bDigitsWithPed)#channelDSPThr[k])
        elif thresholdMode=='EneThr' or thresholdMode=='NoiseSigma':
            tau_dsp, tau_dsp_thrDiff    = applyDSPOnlineOFCTimingToLArSamples(samples[k], ofcb , ped , A_estimated, Ene_dsp, DSPEneThrs[k], timeOffset[k], thresholdMode, bDoAbsE1, bDigitsWithPed)#channelDSPThr[k])
        elif thresholdMode=='MC':
            tau_dsp, tau_dsp_thrDiff    = applyDSPOnlineOFCTimingToLArSamples(samples[k], ofcb , ped , A_estimated, Ene_dsp, -9999         , timeOffset[k], thresholdMode, bDoAbsE1, bDigitsWithPed)#channelDSPThr[k])
        else:
            print('thresholdMode not selected! exiting...')
            sys.exit()

        AClus.append(A_estimated)
        # EDSPClus.append(int(np.floor(Ene_dsp + 0.5)))
        # EDSPClus.append(Ene_dsp) ## update: 23/10/2023 Mateus
        EDSPClus.append(Ene_dsp/1000) ## update: 23/10/2023 Mateus: MeV to GeV cell energy
        tauDSPClus.append(tau_dsp)
        tauDSPClus_thrDiff.append(tau_dsp_thrDiff)
        samples_noPedClus.append(samples_noPed)


    return AClus, tauDSPClus, EDSPClus, tauDSPClus_thrDiff, samples_noPedClus

        # print("(ER) Channel/RawChannel HWID({}/{}): EDSP {:.2f} (A={:.2f})/ {:.2f}\t TAUDSP {:.2f}/ {:.2f}\t".format(chId_cell,rawChId[mirrorIndexes][k], Ene_dsp, A_estimated, rawChEnergyDSP[mirrorIndexes][k], tau_dsp, rawChTimeDSP[mirrorIndexes][k]  ))
        # print("__________________")


        # print("(ER) Channel/RawChannel HWID({}/{}): EDSP {:.2f} (A={:.2f})/ {:.2f}\t TAUDSP {:.2f}/ {:.2f}\t".format(chId_cell,rawChId[mirrorIndexes][k], Ene_dsp, A_estimated, rawChEnergyDSP[mirrorIndexes][k], tau_dsp, rawChTimeDSP[mirrorIndexes][k]  ))
        # print("__________________")

# def getCalibEnergyCellsFromSamplesInCluster(chIds, samples, gain, layers, etaCell, ofc_dict, ped_dict, mpmc_dict, ramps_dict, uamev_dict, ofca_dumper, ofcb_dumper, ramp0_dumper, ramp1_dumper, calibType='database',getAllCalibConst=False, offlineSigmaThr=0, DSPEneThrs=0):
#     '''
#     Pass a list of chIds, samples and gain, for respective cells.
#     calibType='database', 'typical' or 'dumper.

#     if getAllCalibConst:
#         return AClus, tauClus, AtauClus, EClus, R0Clus, R1Clus, MClus, F_ua2mevClus, F_dac2uaClus        
#     else:
#         return AClus, tauClus, EClus
#     '''
#     AClus           = []
#     tauClus         = []
#     tauDSPClus      = []
#     EClus           = []
#     EDSPClus        = []
#     R0Clus          = []
#     R1Clus          = []
#     MClus           = []
#     F_ua2mevClus    = []
#     F_dac2uaClus    = []

#     # if calibType=='typical' or calibType=='database':    
#     F_dac2ua_list  = getLArChannelDAC2uA(layers, etaCell) # Typical values only
#     if calibType=='typical': # returns a list of values        
#         F_ua2mev_list = getLArChannelUAMEV([], [], calibType, layers, etaCell)['uAMeV']
    
#     # for cell_index, chId_cell, samp_cell, gain_cell, dsp_ene_thr in zip(range(0,len(chIds)), chIds, samples, gain, DSPEneThrs):
#     for cell_index, chId_cell, samp_cell, gain_cell, dsp_ene_thr, ofca_cell, ofcb_cell, ramp0_cell, ramp1_cell in zip(range(0,len(chIds)), chIds, samples, gain, DSPEneThrs,ofca_dumper, ofcb_dumper, ramp0_dumper, ramp1_dumper):
        
#         cell_dict_ofc   = getLArChannelOFC  (chId_cell, ofc_dict  )
#         cell_dict_ped   = getLArChannelPED  (chId_cell, ped_dict  )
#         cell_dict_mpmc  = getLArChannelMPMC (chId_cell, mpmc_dict )
#         cell_dict_ramp  = getLArChannelRAMPS(chId_cell, ramps_dict)
#         cell_dict_uamev = getLArChannelUAMEV(chId_cell, uamev_dict, calibType, layers, etaCell)
#         # if calibType=='database':
        

#         # Ramp fit parameters
#         if calibType=='database' or calibType=='typical':
#             R0       = cell_dict_ramp['X'][0][gain_cell][0]
#             R1       = cell_dict_ramp['X'][0][gain_cell][1]
#         if calibType=='dumper':
#             R0       = ramp0_cell
#             R1       = ramp1_cell

#         M        = cell_dict_mpmc['mphysovermcal'][0][gain_cell]
#         F_dac2ua = F_dac2ua_list[cell_index]

#         if calibType=='typical':
#             F_ua2mev = F_ua2mev_list[cell_index] # select a single value from the list of constants
#         if calibType=='database' or calibType=='dumper':
#             F_ua2mev = cell_dict_uamev['uAMeV'][0][0] # 

#         if calibType=='dumper':
#             ofca    = ofca_cell
#             ofcb    = ofcb_cell
        
#         if calibType=='database' or calibType=='typical':
#             ofca        = cell_dict_ofc['ofca'][0][gain_cell][0:4]
#             ofcb        = cell_dict_ofc['ofcb'][0][gain_cell][0:4]
            
#         ped         = cell_dict_ped['ped'][0][gain_cell]

#         A_estimated = applyOFCEnergyAndPEDtoLArSamples(samp_cell, ofca , ped)
        
#         Ene         = F_ua2mev * F_dac2ua * (1/M) * (R0 + R1*A_estimated) # offline (incomplete)
#         Ene_dsp     = R0 + R1*A_estimated # online

#         tau, Atau   = applyOFCTimingToLArSamples         (samp_cell, ofcb , ped , A_estimated, Ene    , offlineSigmaThr)
#         tau_dsp     = applyDSPOnlineOFCTimingToLArSamples(samp_cell, ofcb , ped , A_estimated, Ene_dsp, dsp_ene_thr)
        
#         # Add results to new cluster
#         AClus.append(A_estimated)
#         tauClus.append(tau)
#         EClus.append(Ene)
#         tauDSPClus.append(tau_dsp)
#         EDSPClus.append(Ene_dsp)

#         if getAllCalibConst:
#             R0Clus.append(R0)
#             R1Clus.append(R1)
#             MClus.append(M)
#             F_ua2mevClus.append(F_ua2mev)
#             F_dac2uaClus.append(F_dac2ua)

#     if getAllCalibConst:
#         return AClus, tauClus, EClus, R0Clus, R1Clus, MClus, F_ua2mevClus, F_dac2uaClus        
#     else:
#         return AClus, tauClus, EClus, tauDSPClus, EDSPClus
     #----------------------------------------------------------
     # In construction !!
     # ---------------------------------------------------------       
    # else: # if calibType=='dumper'
    #     for cell_index, chId_cell, samp_cell, gain_cell, dsp_ene_thr, ofca_cell, ofcb_cell, ramp0_cell, ramp1_cell in zip(range(0,len(chIds)), chIds, samples, gain, DSPEneThrs,ofca_dumper, ofcb_dumper, ramp0_dumper, ramp1_dumper):
            
    #         cell_dict_ped   = getLArChannelPED  (chId_cell, ped_dict  )

    #         # Ramp fit parameters
    #         ofca    = ofca_cell
    #         ofcb    = ofcb_cell
    #         R0      = ramp0_cell
    #         R1      = ramp1_cell
    #         ped     = cell_dict_ped['ped'][0][gain_cell] #must dump it!!!

    #         A_estimated = applyOFCEnergyAndPEDtoLArSamples(samp_cell, ofca , ped)
            
    #         # Ene         = F_ua2mev * F_dac2ua * (1/M) * (R0 + R1*A_estimated) # offline (incomplete)
    #         Ene_dsp     = R0 + R1*A_estimated # online

    #         # tau, Atau   = applyOFCTimingToLArSamples         (samp_cell, ofcb , ped , A_estimated, Ene    , offlineSigmaThr)
    #         tau_dsp     = applyDSPOnlineOFCTimingToLArSamples(samp_cell, ofcb , ped , A_estimated, Ene_dsp, dsp_ene_thr)
            
    #         # Add results to new cluster
    #         AClus.append(A_estimated)
    #         # tauClus.append(tau)
    #         # EClus.append(Ene)
    #         tauDSPClus.append(tau_dsp)
    #         EDSPClus.append(Ene_dsp)

    #     return AClus, tauDSPClus, EDSPClus



# ## Online Estimation BACKUP
# def getCalibEnergyCellsFromSamplesInCluster(chIds, samples, gain, layers, etaCell, ofc_dict, ped_dict, mpmc_dict, ramps_dict, uamev_dict, calibType='database',getAllCalibConst=False, offlineSigmaThr=0, DSPEneThrs=0):
#     '''
#     Pass a list of chIds, samples and gain, for respective cells.
#     calibType='database' or 'typical'.

#     if getAllCalibConst:
#         return AClus, tauClus, AtauClus, EClus, R0Clus, R1Clus, MClus, F_ua2mevClus, F_dac2uaClus        
#     else:
#         return AClus, tauClus, EClus
#     '''
#     AClus           = []
#     tauClus         = []
#     tauDSPClus      = []
#     EClus           = []
#     EDSPClus        = []
#     R0Clus          = []
#     R1Clus          = []
#     MClus           = []
#     F_ua2mevClus    = []
#     F_dac2uaClus    = []
    
#     F_dac2ua_list  = getLArChannelDAC2uA(layers, etaCell) # Typical values only
#     if calibType=='typical': # returns a list of values        
#         F_ua2mev_list = getLArChannelUAMEV([], [], calibType, layers, etaCell)['uAMeV']
    
#     for cell_index, chId_cell, samp_cell, gain_cell, dsp_ene_thr in zip(range(0,len(chIds)), chIds, samples, gain, DSPEneThrs):
        
#         cell_dict_ofc   = getLArChannelOFC  (chId_cell, ofc_dict  )
#         cell_dict_ped   = getLArChannelPED  (chId_cell, ped_dict  )
#         cell_dict_mpmc  = getLArChannelMPMC (chId_cell, mpmc_dict )
#         cell_dict_ramp  = getLArChannelRAMPS(chId_cell, ramps_dict)

#         if calibType=='database':
#             cell_dict_uamev = getLArChannelUAMEV(chId_cell, uamev_dict, calibType, layers, etaCell)

#         # Ramp fit parameters
#         R0       = cell_dict_ramp['X'][0][gain_cell][0]
#         R1       = cell_dict_ramp['X'][0][gain_cell][1]
#         M        = cell_dict_mpmc['mphysovermcal'][0][gain_cell]
#         F_dac2ua = F_dac2ua_list[cell_index]

#         if calibType=='typical':
#             F_ua2mev = F_ua2mev_list[cell_index] # select a single value from the list of constants
#         if calibType=='database':
#             F_ua2mev = cell_dict_uamev['uAMeV'][0][0] # 

#         ofca        = cell_dict_ofc['ofca'][0][gain_cell][0:4]
#         ofcb        = cell_dict_ofc['ofcb'][0][gain_cell][0:4]
#         ped         = cell_dict_ped['ped'][0][gain_cell]

#         A_estimated = applyOFCEnergyAndPEDtoLArSamples(samp_cell, ofca , ped)
        
#         Ene         = F_ua2mev * F_dac2ua * (1/M) * (R0 + R1*A_estimated) # offline (incomplete)
#         Ene_dsp     = R0 + R1*A_estimated # online

#         tau, Atau   = applyOFCTimingToLArSamples         (samp_cell, ofcb , ped , A_estimated, Ene    , offlineSigmaThr)
#         tau_dsp     = applyDSPOnlineOFCTimingToLArSamples(samp_cell, ofcb , ped , A_estimated, Ene_dsp, dsp_ene_thr)
        
#         # Add results to new cluster
#         AClus.append(A_estimated)
#         tauClus.append(tau)
#         EClus.append(Ene)
#         tauDSPClus.append(tau_dsp)
#         EDSPClus.append(Ene_dsp)

#         if getAllCalibConst:
#             R0Clus.append(R0)
#             R1Clus.append(R1)
#             MClus.append(M)
#             F_ua2mevClus.append(F_ua2mev)
#             F_dac2uaClus.append(F_dac2ua)

#     if getAllCalibConst:
#         return AClus, tauClus, EClus, R0Clus, R1Clus, MClus, F_ua2mevClus, F_dac2uaClus        
#     else:
#         return AClus, tauClus, EClus, tauDSPClus, EDSPClus
