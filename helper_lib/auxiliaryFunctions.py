## HELPER FUNCTIONS

import numpy as np
import os, sys
import math
import scipy.stats as spstat

def human_format(num): 
    magnitude = 0 
    while abs(num) >= 1000: 
        magnitude += 1 
        num /= 1000.0 # add more suffixes if you need them 
    return '%.2f%s' % (num, ['', 'K', 'M', 'G', 'T', 'P'][magnitude]) 

def getBins(bmin, bmax, bwidth):
    xbins = np.arange(bmin, bmax, bwidth)
    return xbins

def flatten(l):
    '''Flatten a list of lists.'''
    return [item for sublist in l for item in sublist]

def confInterval(alfa,data):
    """
    Return the T-student's confidence interval for alpha. The data shape must be a 1 or 2 dimension,
    with the 'n' samples at the column direction. This function is used for 'n' < 30.
    Besides the confident interval, return the lower and upper interval uncertainty related to the data mean:
    interval = [LowerInterval, UpperInterval, lowErrorBar, upperErrorBar]
    """
    interval = spstat.t.interval(alpha=alfa,df=np.shape(data)[1],loc=np.mean(data,axis=1),scale=spstat.sem(data,axis=1))
    lowErrorBar = np.mean(data,axis=1)-interval[0]
    upperErrorBar = interval[1] - np.mean(data,axis=1)

    return [interval[0], interval[1], lowErrorBar, upperErrorBar]

def listOfListsToArrayOfArrays(theList):
    if len(theList)==0:
        return []
    else:
        length  = max(map(len, theList))
        y       = np.array([xi+[None]*(length-len(xi)) for xi in theList])
        return y

def correctPhi(phi):
    '''
    Return the corrected value of _phi, inside the [-Pi,+Pi] range.
    '''
    phi = round(phi, 4)
    if (phi) < ((-1)*math.pi):
        return round(((phi) + 2*math.pi), 4)
    elif (phi) > math.pi:
        return round(((phi) - 2*math.pi), 4)
    else:
        return round((phi), 4)
    
def getMatchedIndexes(theList, matchValue):
    matched_indexes = []
    i               = 0
    
    while i < len(theList):
        if matchValue == theList[i]:
            matched_indexes.append(i)
        i+=1
    return matched_indexes

# def getRawChValueToChannelBasedOnID(chids, rawchids, rawChValues): # NOT WORKING WELL
#     listOfValuesInChannel = []
#     for chid in chids:
#         indexOfRawChValue   = np.where(rawchids == chid)[0]
#         if len(indexOfRawChValue) == 0:
#             print("index of chid not found in rawchids.")
#         listOfValuesInChannel.append( rawChValues[indexOfRawChValue] )
    
#     if len(chids != len(listOfValuesInChannel)):
#         print('size of chids is != from new list of values in channel !')

#     return listOfValuesInChannel

def getCellsInWindow(cellEneList,  ## OLD!!
                     cellIndexList, 
                     cellEtaList, 
                     cellPhiList, 
                     cellSamplingList, 
                     etaSize, 
                     phiSize, 
                     layer='EMB2',
                     fullCluster=False):
    ''' 
    Create a etaSize x PhiSize cell ROI 
    
    return the indexes of the array of cells. This array is composed of a sub-array of the cluster, with only the cells of the selected layer.
         window        : set of positional index inside sub-array, which belongs to the window
         layerIndexes  : array of indexes of all cells from chosen layer.'''
   
    # get the hot cell pos and value (for the layer)
    # layerIndexes    = np.where(cellSamplingList[0][:] == layer)[0]
    layerIndexes      = getMatchedIndexes(cellSamplingList[0], layer)
    
    if fullCluster == True:
        return layerIndexes
    
    # print(cellSamplingList[0][:], layerIndexes)
    if np.size( layerIndexes ) == 0:
        return [], []
    
    hotCellValue    = np.max(np.array(cellEneList[0])[layerIndexes])
    hotCellPos      = np.argmax(np.array(cellEneList[0])[layerIndexes])
    
    window          = [] # list of indexes of the cells inside the chosen window
    
    # find cells within limits of eta phi window
    # for sampling, ind, eta, phi in zip(cellSamplingList, cellIndexList[0], cellEtaList[0], cellPhiList[0]):
    for ind, eta, phi in zip(layerIndexes, np.array(cellEtaList[0])[layerIndexes], np.array(cellPhiList[0])[layerIndexes]):
        # print(cellEtaList[0][hotCellPos])
        # print (sampling)
        # if sampling == layer:
        etaConditionL    = eta >= (cellEtaList[0][hotCellPos] - etaSize/2)
        etaConditionR    = eta <= (cellEtaList[0][hotCellPos] + etaSize/2)
#         phiConditionU    = phi >= correctPhi( (cellPhiList[0][hotCellPos] - phiSize/2) )
#         phiConditionD    = phi <= correctPhi( (cellPhiList[0][hotCellPos] + phiSize/2) )
        lowerPhiRange    = cellPhiList[0][hotCellPos] - phiSize/2
        upperPhiRange    = cellPhiList[0][hotCellPos] + phiSize/2
        
        bPhiOverFlow = ( (lowerPhiRange < (-1)*math.pi) or (lowerPhiRange > 1*math.pi) or (upperPhiRange < (-1)*math.pi) or (upperPhiRange > 1*math.pi) )
            
            # if overflows, need to include the discontinuity in the middle of the interval.
        if bPhiOverFlow:
            phiCondition = (((phi >= correctPhi(lowerPhiRange)) and (phi <= 1*math.pi)) or ((phi <= correctPhi(upperPhiRange)) and (phi >= (-1)*math.pi)))
        else:
            phiCondition = ((phi >= lowerPhiRange) and (phi <= upperPhiRange))
        
        # print(etaConditionL,etaConditionR,phiConditionU,phiConditionD)
        
        if etaConditionL and etaConditionR and phiCondition:
            window.append( ind ) # add them to a list of index cells
            # print('window: ',window)
        

    return window, layerIndexes

def getSamplingGranularityFromDict(eta,caloDict, sampIndex):
    
    # Number of regions in each sampling layer (eta x phi region)
    regionsPerLayer = []
    for reg in caloDict['granularityEtaLayer']:
        regionsPerLayer.append(len(reg))
        
    for layerRegionIndex, etaMin, etaMax in zip(range(0,regionsPerLayer[sampIndex]), dictCaloLayer['etaLowLim'][sampIndex], dictCaloLayer['etaHighLim'][sampIndex]):    
        if ((abs(eta) > etaMin) and (abs(eta) < etaMax)):
            deta = caloDict['granularityEtaLayerPrecise'][sampIndex][layerRegionIndex]
            dphi = caloDict['granularityPhiLayerPrecise'][sampIndex][layerRegionIndex]
            return deta, dphi 


def getCellsInWindowCustomSize( #DEPRECATED !!!!!!!!!!
                     cellEneList, 
                     cellIndexList, 
                     cellEtaList, 
                     cellPhiList, 
                     cellSamplingList, 
                     nCellsEta, 
                     nCellsPhi, 
                     DEtaList,
                     DPhiList,
                     layer='EMB2',
                     fullCluster=False):
    ''' 
    Create a nCellsEta x nCellsPhi cell ROI.
    
    # Return the indexes of the array of cells. 
    # This array is composed of a sub-array of the cluster, with only the cells of the selected layer.
    # window        : set of positional index inside sub-array, which belongs to the window
    # layerIndexes  : array of indexes of all cells from chosen layer.'''
   
    # get the hot cell pos and value (for the layer)
    layerIndexes      = getMatchedIndexes(cellSamplingList[0], layer)
    
    if fullCluster == True:
        return layerIndexes

    if np.size( layerIndexes ) == 0:
        return [], []
    
    # determine the window size in eta x phi
    deta = np.max(np.array(DEtaList[0]))
    dphi = np.max(np.array(DPhiList[0]))
    
    etaSize = nCellsEta * deta
    phiSize = nCellsPhi * dphi
    
#     print(deta,etaSize,dphi,phiSize)
    
    hotCellValue    = np.max(np.array(cellEneList[0])[layerIndexes])
    hotCellPos      = np.argmax(np.array(cellEneList[0])[layerIndexes])
    
    window          = [] # list of indexes of the cells inside the chosen window
    
    # find cells within limits of eta phi window
    # for sampling, ind, eta, phi in zip(cellSamplingList, cellIndexList[0], cellEtaList[0], cellPhiList[0]):
    for ind, eta, phi in zip(layerIndexes, np.array(cellEtaList[0])[layerIndexes], np.array(cellPhiList[0])[layerIndexes]):
        etaConditionL    = eta >= (cellEtaList[0][hotCellPos] - etaSize/2)
        etaConditionR    = eta <= (cellEtaList[0][hotCellPos] + etaSize/2)
#         phiConditionU    = phi >= correctPhi( (cellPhiList[0][hotCellPos] - phiSize/2) )
#         phiConditionD    = phi <= correctPhi( (cellPhiList[0][hotCellPos] + phiSize/2) )
        
        lowerPhiRange    = cellPhiList[0][hotCellPos] - phiSize/2
        upperPhiRange    = cellPhiList[0][hotCellPos] + phiSize/2
        
        bPhiOverFlow = ( (lowerPhiRange < (-1)*math.pi) or (lowerPhiRange > 1*math.pi) or (upperPhiRange < (-1)*math.pi) or (upperPhiRange > 1*math.pi) )
            
            # if overflows, need to include the discontinuity in the middle of the interval.
        if bPhiOverFlow:
            phiCondition = (((phi >= correctPhi(lowerPhiRange)) and (phi <= 1*math.pi)) or ((phi <= correctPhi(upperPhiRange)) and (phi >= (-1)*math.pi)))
        else:
            phiCondition = ((phi >= lowerPhiRange) and (phi <= upperPhiRange))
        
        if etaConditionL and etaConditionR and phiCondition:
            window.append( ind ) # add them to a list of index cells
            if len(window) >= ( nCellsEta * nCellsPhi ):
                break
                

    return window, layerIndexes

def getCellsInWindowCustomSizeNEW(
                     cellEneList, 
                     cellIndexList, 
                     cellEtaList, 
                     cellPhiList, 
                     cellSamplingList, 
                     nCellsEta, 
                     nCellsPhi, 
                     DEtaList,
                     DPhiList,
                     layer='EMB2',
                     fullCluster=False):
    ''' 
    Create a nCellsEta x nCellsPhi cell ROI.
    
    # Return the indexes of the array of cells. 
    # This array is composed of a sub-array of the cluster, with only the cells of the selected layer.
    # window        : set of positional index inside sub-array, which belongs to the window
    # layerIndexes  : array of indexes of all cells from chosen layer.'''
   
    # get the hot cell pos and value (for the layer)
    layerIndexes      = getMatchedIndexes(cellSamplingList, layer)
    
    if fullCluster == True:
        return layerIndexes

    if np.size( layerIndexes ) == 0:
        return [], []
    
    testEneLen = len(cellEneList)
    if testEneLen != len(cellIndexList):
        print('Energies list has different size from cellIndexList')
        return [],[]
    if testEneLen != len(cellEtaList):
        print('Energies list has different size from cellEtaList')
        return [],[]
    if testEneLen != len(cellPhiList):
        print('Energies list has different size from cellPhiList')
        return [],[]
    if testEneLen != len(cellSamplingList):
        print('Energies list has different size from cellSamplingList')
        return [],[]
    if testEneLen != len(DEtaList):
        print('Energies list has different size from DEtaList')
        return [],[]
    
    # determine the window size in eta x phi
    if layer=='EMB2':
        deta = 0.025#00000037252903
        dphi = 0.025#54369328916073
    else:
        deta = np.min(np.array(DEtaList)[layerIndexes])
        dphi = np.min(np.array(DPhiList)[layerIndexes])

    etaSize = nCellsEta * deta
    phiSize = nCellsPhi * dphi  
    
    # print(deta,etaSize,dphi,phiSize)
    
    hotCellValue    = np.max(np.array(cellEneList)[layerIndexes])
    hotCellPos      = np.argmax(np.array(cellEneList)[layerIndexes])
    hotCellEta      = np.array(cellEtaList)[layerIndexes][hotCellPos]
    hotCellPhi      = np.array(cellPhiList)[layerIndexes][hotCellPos]
    # print('hotcell position: ',hotCellEta,hotCellPhi,', granularity: ',np.array(DEtaList)[layerIndexes][hotCellPos],np.array(DPhiList)[layerIndexes][hotCellPos])
    
    window          = [] # list of indexes of the cells inside the chosen window
    
    # find cells within limits of eta phi window
    # for sampling, ind, eta, phi in zip(cellSamplingList, cellIndexList, cellEtaList, cellPhiList):
    for ind, eta, phi in zip(layerIndexes, np.array(cellEtaList)[layerIndexes], np.array(cellPhiList)[layerIndexes]):
        etaConditionL    = eta >= (np.array(cellEtaList)[layerIndexes][hotCellPos] - etaSize/2)
        etaConditionR    = eta <= (np.array(cellEtaList)[layerIndexes][hotCellPos] + etaSize/2)
#         phiConditionU    = phi >= correctPhi( (cellPhiList[hotCellPos] - phiSize/2) )
#         phiConditionD    = phi <= correctPhi( (cellPhiList[hotCellPos] + phiSize/2) )
        
        lowerPhiRange    = np.array(cellPhiList)[layerIndexes][hotCellPos] - phiSize/2
        upperPhiRange    = np.array(cellPhiList)[layerIndexes][hotCellPos] + phiSize/2
        
        bPhiOverFlow = ( (lowerPhiRange < (-1)*math.pi) or (lowerPhiRange > 1*math.pi) or (upperPhiRange < (-1)*math.pi) or (upperPhiRange > 1*math.pi) )
            
            # if overflows, need to include the discontinuity in the middle of the interval.
        if bPhiOverFlow:
            phiCondition = (((phi >= correctPhi(lowerPhiRange)) and (phi <= 1*math.pi)) or ((phi <= correctPhi(upperPhiRange)) and (phi >= (-1)*math.pi)))
        else:
            phiCondition = ((phi >= lowerPhiRange) and (phi <= upperPhiRange))
        
        if (etaConditionL and etaConditionR) and phiCondition:
            window.append( ind ) # add them to a list of index cells
            if len(window) >= ( nCellsEta * nCellsPhi ):
                break
                

    return np.asarray(window), np.asarray(layerIndexes), hotCellPos


def getNBinsSamplingCluster(eta, phi, sampl, clusterDict):
    '''
    Return the number of bins of TH2F for eta and phi,
    depending of the sampling.
    obs.: its a rough approximation, depending on the size of the cluster window eta-phi.
    '''
    layIdx          = getMatchedIndexes(clusterDict["Layer"], sampl)[0]
    # print(layIdx,np.shape(layIdx))
    lowEtaBounds    = clusterDict["etaLowLim"][layIdx]
    highEtaBounds   = clusterDict["etaHighLim"][layIdx]
    granEta         = clusterDict["granularityEtaLayerPrecise"][layIdx]
    granPhi         = clusterDict["granularityPhiLayerPrecise"][layIdx]
    
    etaGrid         = []
    phiGrid         = []
    
    #positive
    for stepEta, stepPhi, lowBound, highBound in zip(granEta, granPhi, lowEtaBounds, highEtaBounds):
        etaGrid.append( np.arange( lowBound, highBound, stepEta ) )
        phiGrid.append( np.arange( 0, math.pi, stepPhi ) )
    
    #add negative part to Eta/Phi
    fullEtaGrid = np.concatenate( (-1*np.flip(np.concatenate(etaGrid)[1:]), np.concatenate(etaGrid)), axis=0 )
    fullPhiGrid = np.concatenate( (-1*np.flip(np.concatenate(phiGrid)[1:]), np.concatenate(phiGrid)), axis=0 )
    
    #get NBins
    etaBins = 0
    phiBins = 0
    
    etaBins = ((np.min(eta) < fullEtaGrid) & (fullEtaGrid < np.max(eta))).sum() +1
    phiBins = ((np.min(phi) < fullPhiGrid) & (fullPhiGrid < np.max(phi))).sum() +1
    
    return int(etaBins), int(phiBins), fullEtaGrid, fullPhiGrid


# def getSortedClusterIndexes(eta,phi): ## OLD, doesnt keep same order in all clusters as expected.
#     '''Return the indexes of an organized sequence of pairs of eta,phi, following a line from the smallest phi's, then the smallest eta's. '''
#     ind = np.lexsort((eta,phi)) 
#     return ind

def getSortedClusterIndexes(eta,phi,order='eta',neta=3, nphi=3):
    '''The eta and phi indexes will be ordered depending of the axis (order) selected.
        It must have as input, an already selected window.
    '''
    orderedArray = np.zeros([neta,nphi])
    if order=='eta':
        orderedInd   = np.argsort(eta)
    elif order=='phi':
        orderedInd   = np.argsort(phi)
    else:
        print("Error! order should be eta or phi!")
        return -1

    return orderedInd


def unpackCellsFromCluster(dataset_cluster, confDict, ped_dict={}, isMC=False, dumpRawChannels=False):
    # get list indexes that match to current sampling
    channelLayerIndexes     = getMatchedIndexes(dataset_cluster['channels']['sampling'], confDict['sampling']) 
    # rawChannelLayerIndexes  = getMatchedIndexes(ev['electrons'][elec][clusName][clus]['rawChannels']['sampling'][0], layer)

    # is that layer empty for RAWCh or Ch? skip
    if (len(channelLayerIndexes) == 0 ): # or (len(rawChannelLayerIndexes)==0):
        return 0
    
    nCellsWinEta = confDict['nCellsWinEta']
    nCellsWinPhi = confDict['nCellsWinPhi']

    ###################     ###############
    ## Channel + Digits  +  ## RawChannel #
    ###################     ###############
    windowIndexesCh, layerIndexesCh, hotCellPos = getCellsInWindowCustomSizeNEW(
        dataset_cluster['channels']['energy'],
        dataset_cluster['channels']['index'],
        dataset_cluster['channels']['eta'],
        dataset_cluster['channels']['phi'],
        dataset_cluster['channels']['sampling'],
        confDict['nCellsWinEta'], 
        confDict['nCellsWinPhi'], 
        dataset_cluster['channels']['deta'],
        dataset_cluster['channels']['dphi'],
        layer=confDict['sampling'])

    # any of the windows aren't full??
    if confDict['bFullWindows']:
        if (len(windowIndexesCh) != (nCellsWinEta*nCellsWinPhi)):# or (len(windowIndexesRawCh) != (nCellsWinEta*nCellsWinPhi)):
            return 0

    # if they belong to the same calo object and have the same size, they MUST have the same cells, because the dumper rule is the same.
    digits              = np.array(dataset_cluster['channels']['digits'],dtype=object)
    digitsSize          = len (np.array(dataset_cluster['channels']['digits'],dtype=object))
    channelEnergy       = np.array(dataset_cluster['channels']['energy'],dtype=object)
    channelTime         = np.array(dataset_cluster['channels']['time'],dtype=object)
    # channelInfo         = listOfListsToArrayOfArrays(np.array(dataset_cluster['channels']['chInfo'],dtype=object))
    channelInfo         = np.array(dataset_cluster['channels']['chInfo'],dtype=object)
    channelId           = np.array(dataset_cluster['channels']['channelId'],dtype=object)
    channelSampling     = np.array(dataset_cluster['channels']['sampling'],dtype=object)
    channelEta          = np.array(dataset_cluster['channels']['eta'],dtype=object)
    channelPhi          = np.array(dataset_cluster['channels']['phi'],dtype=object)
    channelDEta         = np.array(dataset_cluster['channels']['deta'],dtype=object)
    channelDPhi         = np.array(dataset_cluster['channels']['dphi'],dtype=object)
    channelGain         = np.array(dataset_cluster['channels']['gain'],dtype=object)
    if not(isMC):
        channelDSPThr       = np.array(dataset_cluster['channels']['DSPThreshold'],dtype=object)
    channelOFCa         = np.array(dataset_cluster['channels']['OFCa'],dtype=object)
    channelOFCb         = np.array(dataset_cluster['channels']['OFCb'],dtype=object)
    channelRamps0       = np.array(dataset_cluster['channels']['ADC2MeV0'],dtype=object)
    channelRamps1       = np.array(dataset_cluster['channels']['ADC2MeV1'],dtype=object)
    channelLArPed       = np.array(dataset_cluster['channels']['LArDB_Pedestal'],dtype=object)
    channelTimeOffset   = np.array(dataset_cluster['channels']['OFCTimeOffset'],dtype=object)
    channelEffSigma     = np.array(dataset_cluster['channels']['effSigma'],dtype=object)
    # channelNoise        = np.array(dataset_cluster['channels']['noise'],dtype=object)          [windowIndexesCh] .tolist()
    # channelShape        = np.array(ev['electrons'][elec][clusName][clus]['channels']['Shape'],dtype=object)          [windowIndexesCh] .tolist()

    if dumpRawChannels:
        if not(isMC):
            rawChDSPThr         = np.array(dataset_cluster['rawChannels']['DSPThreshold'],dtype=object) 
        rawChEnergyDSP      = np.array(dataset_cluster['rawChannels']['amplitude'],dtype=object)      # this is not the amplitude, it is the E_dsp
        rawChTimeDSP        = np.array(dataset_cluster['rawChannels']['time'],dtype=object)          
        rawChInfo           = np.array(dataset_cluster['rawChannels']['chInfo'],dtype=object)        
        rawChId             = np.array(dataset_cluster['rawChannels']['channelId'],dtype=object)     
        rawChEta            = np.array(dataset_cluster['rawChannels']['eta'],dtype=object) 
        rawChPhi            = np.array(dataset_cluster['rawChannels']['phi'],dtype=object)
        rawChProv           = np.array(dataset_cluster['rawChannels']['LAr_Provenance'],dtype=object)


    if isMC:
        hitsSampling    = np.array(dataset_cluster['hits']['sampling'], dtype=object)
        hitsEnergy      = np.array(dataset_cluster['hits']['energy'], dtype=object)
        hitsTime        = np.array(dataset_cluster['hits']['time'], dtype=object)
        hitsEta         = np.array(dataset_cluster['hits']['eta'], dtype=object)
        hitsPhi         = np.array(dataset_cluster['hits']['phi'], dtype=object)
    
    # if bDoOfflineCalib:
    #     channelMinBiasAvg   = np.array(dataset_cluster['channels']['MinBiasAvg'],dtype=object)     [windowIndexesCh] .tolist()
    #     channelOfflHV       = np.array(dataset_cluster['channels']['OfflHVScale'],dtype=object)    [windowIndexesCh] .tolist()
    #     channelEneResc      = np.array(dataset_cluster['channels']['OfflEneRescaler'],dtype=object)[windowIndexesCh] .tolist()
    # print('sampling size:',len(channelSampling))
    # print('eta size:', len(channelEta))

    # Selection of Pedestal (DB or Script)
    if not(confDict['bPedFromDB']):
        peds = [] #testing
        gainsInt = getGainInteger(channelGain)
    if confDict['bPedFromDB']:
        peds = channelLArPed

    # The cells that came from Window algorithm are not the same when comparing RawCh and Digits.
    # For that, we will select the same Digits cells set, based on the HWID, inside the Raw Channel cluster cells set. 
    # mirrorIndexes = []
    for k, hwid in enumerate(channelId):
        # position        = np.where(np.array(ev['electrons'][elec][clusKey][clus]['channels']['channelId'],dtype=object)[layerIndexesCh] == hwid)[0][0]
        # mirrorIndexes.append(position) # its the window mirrored indexes for RawCh
        if not(confDict['bPedFromDB']):
            cell_dict_ped   = getLArChannelPED  (hwid, ped_dict  )
            peds.append(cell_dict_ped['ped'][0][gainsInt[k]]) # picking up the right gain weights


    orderedIndexes = getSortedClusterIndexes(channelEta[windowIndexesCh], channelPhi[windowIndexesCh])
    
    ml_dataset = {
        'orderedIndexes'    : orderedIndexes,
        'windowIndexesCh'   : windowIndexesCh,
        'layerIndexesCh'    : layerIndexesCh,
        'hotcellposition'   : hotCellPos,
        'digits'            : digits,
        'digitsSize'        : digitsSize,
        'channelEnergy'     : channelEnergy,
        'channelTime'       : channelTime,
        'channelInfo'       : channelInfo,
        'channelId'         : channelId,
        'channelSampling'   : channelSampling,
        'channelEta'        : channelEta,
        'channelPhi'        : channelPhi,
        'channelDEta'       : channelDEta,
        'channelDPhi'       : channelDPhi,
        'channelGain'       : channelGain,
        'channelOFCa'       : channelOFCa,
        'channelOFCb'       : channelOFCb,
        'channelRamps0'     : channelRamps0,
        'channelRamps1'     : channelRamps1,
        'channelLArPed'     : peds,
        'channelTimeOffset' : channelTimeOffset,
        'channelEffSigma'   : channelEffSigma,
        # 'channelNoise'      : channelNoise,
    }
    if not(isMC):
        ml_dataset.update({'channelDSPThr': channelDSPThr})

    if isMC:
        ml_dataset.update({"hitsSampling": hitsSampling})
        ml_dataset.update({"hitsEnergy": hitsEnergy})
        ml_dataset.update({"hitsTime": hitsTime})
        ml_dataset.update({"hitsEta": hitsEta})
        ml_dataset.update({"hitsPhi": hitsPhi})
    
    if dumpRawChannels:
        if not(isMC):
            ml_dataset.update({"rawChDSPThr": rawChDSPThr})
        ml_dataset.update({"rawChEnergyDSP": rawChEnergyDSP})
        ml_dataset.update({"rawChTimeDSP": rawChTimeDSP})
        ml_dataset.update({"rawChInfo": rawChInfo})
        ml_dataset.update({"rawChId": rawChId})
        ml_dataset.update({"rawChEta": rawChEta})
        ml_dataset.update({"rawChPhi": rawChPhi})
        ml_dataset.update({"rawChProv": rawChProv})
    

    # for key in ml_dataset.keys():
    #     if bReturnOrderedInWindow:
    #         ml_dataset[key] = np.array(ml_dataset[key],dtype=object)
    #     else:
    #         ml_dataset[key] = np.array(ml_dataset[key],dtype=object)
    

    return ml_dataset
