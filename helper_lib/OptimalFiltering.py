import numpy as np

def generateOFCs_LAr(C, g, dg):

    nSamples    = len(g)

    # for amplitude
    lamb    = 1
    ksi     = 0
    gamma   = 0
    # for timing
    mu      = 0
    rho     = -1

    u = nSamples*[0] + [lamb, ksi, gamma]# a = inv(A)*u
    v = nSamples*[0] + [mu, rho]         # b = inv(B)*v

    A = np.zeros((nSamples+3,nSamples+3)) 
    B = np.zeros((nSamples+2,nSamples+2)) 

    A[0:nSamples,0:nSamples]    = C
    A[0:nSamples,nSamples]      = g
    A[nSamples,0:nSamples]      = g
    A[0:nSamples,nSamples+1]    = dg
    A[nSamples+1,0:nSamples]    = dg
    A[0:nSamples,nSamples+2]    = 1
    A[nSamples+2,0:nSamples]    = 1

    B[0:nSamples,0:nSamples]    = C
    B[0:nSamples,nSamples]      = g
    B[nSamples,0:nSamples]      = g
    B[0:nSamples,nSamples+1]    = dg
    B[nSamples+1,0:nSamples]    = dg

    OFCa = np.dot(np.linalg.inv(A),u)
    OFCb = np.dot(np.linalg.inv(B),v)

    return OFCa[0:nSamples],OFCb[0:nSamples]