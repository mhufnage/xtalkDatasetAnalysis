import ROOT
from ROOT import TFile, gROOT, TChain, TTree

from math import log2
import os

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as mcolors
import matplotlib

import numpy as np

from sklearn.metrics import r2_score

from torch.utils.data import Dataset, DataLoader
import torch.multiprocessing as mp
import ML_lib.MLP as MLP

import torch
import torch.nn as nn

from ML_lib.XTalkDatasetClass import XTalkDataset_LZT_FromROOT
from helper_lib.auxiliaryFunctions import confInterval, getBins


###################
##### Metrics #####
###################

def loadTestDataset_LorenzettiSupervised(conditions, batch_size=1, dsIndexesRange=[25,30], dsType='centroid', path='/data/atlas/mhufnage/lzt_single_e/'):
    
    data = TChain('ML_tree')
    for ds_index in range(dsIndexesRange[0],dsIndexesRange[1]+1):
        theFile = path+'elec_singleCell_10kEvts_{}_e50_DS{}/AOD_Cap{:.1f}_Ind{:.1f}/user.mhufnage.lzt.e50.xtalk.DS{}.win{}{}.nopileup.WithCells_{}.root'.format(dsType, ds_index, conditions['XTalk_cap'], conditions['XTalk_ind'], ds_index, conditions['etaSize'], conditions['phiSize'], conditions['caloSampling'])
        data.Add(theFile+'/ML_tree')

    dataset     = XTalkDataset_LZT_FromROOT(data, conditions, mode='supervised', load_full_data=True)
    print('[TestSupervised] Total events loaded [DS{}~DS{}]: {}'.format(dsIndexesRange[0], dsIndexesRange[1], len(dataset)))
    
    theDataloader = DataLoader( dataset, batch_size=batch_size, shuffle=False)
    
    return theDataloader, dataset

# def getTestResutlsDict(dictName, categoryList, max_iter, kFolds, nCellsInCluster=9):
#     testDict = {}
#     testDict[dictName] = {}
    
#     for cell in range(nCellsInCluster):
#         testDict[dictName]['cell_'+str(cell)] = {}
    
#         for kIdx, key in enumerate(categoryList):            
#             testDict[dictName]['cell_'+str(cell)][str(key)] = {}
            
#             for it in range(max_iter):
#                 testDict[dictName]['cell_'+str(cell)][str(key)]['it_'+str(it)] = {}
#                 for fold in range(kFolds):
#                     # testDict[str(key)][str(it)] = str(fold)
#                     testDict[dictName]['cell_'+str(cell)][str(key)]['it_'+str(it)]['fold_'+str(fold)] = []
                                                  
#     return testDict

def getTestResutlsDict(dictName, max_iter, kFolds, categoryList=['category'], nCellsInCluster=9, bSmallerDict=True):
    testDict = {}
    testDict[dictName] = {}
    
    if not(bSmallerDict):
        for kIdx, key in enumerate(categoryList):
            testDict[dictName][str(key)] = {}
            
            for it in range(max_iter):
                testDict[dictName][str(key)]['it_'+str(it)] = {}
                for fold in range(kFolds):
                    # testDict[str(key)][str(it)] = str(fold)
                    testDict[dictName][str(key)]['it_'+str(it)]['fold_'+str(fold)] = {}
                    
                    for cell in range(nCellsInCluster):
                        testDict[dictName][str(key)]['it_'+str(it)]['fold_'+str(fold)]['cell_'+str(cell)] = []
    else:
        for it in range(max_iter):
            testDict[dictName]['it_'+str(it)] = {}
            for fold in range(kFolds):
                # testDict[str(key)][str(it)] = str(fold)
                testDict[dictName]['it_'+str(it)]['fold_'+str(fold)] = {}
                
                for cell in range(nCellsInCluster):
                    testDict[dictName]['it_'+str(it)]['fold_'+str(fold)]['cell_'+str(cell)] = []

    return testDict


def loadTorchModelForTest(neuIn, paramPath, trainArgs, modelType='mlp' ):
    
    if modelType=='mlp':
    
        model       = MLP.MLP_FF(neuronsList=neuIn)
        
        if trainArgs['optimAlg'] == 'adam':
            optimizer   = torch.optim.Adam(model.parameters(), lr=trainArgs['lr'], weight_decay=trainArgs['weightDecay'])
        bestLossVal = MLP.resumeModel(model, optimizer, paramPath)
    
    else:
        raise AttributeError("No model type selected")
    
    return model     


def kl_divergence(p, q):
    return sum(p[i] * log2(p[i]/q[i]) for i in range(len(p)))

def js_divergence(p, q):
    m = 0.5 * (p+q)
    return 0.5 * kl_divergence(p, m) + 0.5 * kl_divergence(q, m)



#################
##### Plots #####
#################

def cellArrayIndexToClusterPositionIndex(cellArrayIndex, bSingleMode=False, etaSize=3, phiSize=3):
    '''
    The cell organization in the window is eta-oriented.
    Indexes in cellArrayIndex are organizes in a sequence, driven by the indexes on the map below:
     phi [ 2 | 5 | 8 ]
         [ 1 | 4 | 7 ]  , eta-orientation cluster indexing. where '4' is the index of the hottest cell
         [ 0 | 3 | 6 ]
    
    This function re-map this in the sequential order of the matrix above, but flattened.
    For example, the input array index 0, corresponds to the 6th sequential position on the matrix above.
    It builts a maks of indexes, applied to the input array, giving back a reordered array.
    
    it can work in single index mode as well, converting cell index to it remapped index position.
    '''
    mask = np.arange(0,etaSize*phiSize).reshape(etaSize,phiSize)
    mask = np.flip(mask, axis=1)
    mask = np.transpose(mask)
    mask = mask.flatten()
    
    if bSingleMode:
        return np.where(mask==cellArrayIndex)[0][0]
    else:
        return cellArrayIndex[mask]
    
    # print(mask, cellArrayIndex, cellArrayIndex[mask])
    

def plot_time_scatter_LZT(dataset, reco_dict, figPath, labelDistX, bShowPlots=False, bZoomAxis=False, bShowXT=False, fontSizeLeg=24, binPlots=[-15,15]):
    '''
    Plot scatter of time for each cell in a cluster, at same figure
    '''
    if bShowPlots:
        matplotlib.use('module://ipykernel.pylab.backend_inline')  # show plots
    else:
        matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS
    
    phiSize     = dataset.conditions['phiSize']
    etaSize     = dataset.conditions['etaSize']
    
    fig = plt.figure(figsize=(36,36))
    
    for cell in range(0,phiSize*etaSize):
        if cell==4:
            bZoomAxis=True#False
        else:
            bZoomAxis=True
            
        cellClusPos = cellArrayIndexToClusterPositionIndex(cell, bSingleMode=True)
        
        tauTargetList = dataset.t.detach().numpy()[:,cell]
        if bShowXT:
            tauOutputList = dataset.xtt.detach().numpy()[:,cell]
        else:
            tauOutputList = np.array(reco_dict['cell_{}'.format(cell)], dtype=object)
        
        max_reco = max(max(tauTargetList), max(tauOutputList))
        min_reco = min(min(tauTargetList), min(tauOutputList))
        
        if cell==4:
            bZoomAxis=True#False
            xbins = getBins(-1,1,0.005)
        else:
            xbins = getBins(binPlots[0],binPlots[1],0.5)
        
        ax0 = plt.subplot(etaSize,phiSize,cellClusPos+1)
        plt.hist2d(tauTargetList, tauOutputList, bins=[xbins,xbins], norm=mcolors.LogNorm(), cmap=plt.cm.jet)
        r2_target_output = r2_score(tauTargetList, tauOutputList)
        plt.plot([binPlots[0], binPlots[1]],[binPlots[0], binPlots[1]], '--r', label=r'$R^2=$'+'{:.4f}'.format(r2_target_output)+'\n')
        plt.legend(loc='lower right', fontsize=fontSizeLeg)
        
        plt.xlabel('Cluster cell time [ns]', labelpad=labelDistX)
        if bShowXT:
            plt.ylabel('Reconstructed XTCluster cell time [ns] 500ps/bin')
            if cell==4:
                plt.ylabel('Reconstructed XTCluster cell time [ns] 5ps/bin')
        else:
            plt.ylabel('Reconstructed cluster cell time [ns] 500ps/bin')
            if cell==4:
                plt.ylabel('Reconstructed cluster cell time [ns] 5ps/bin')
        
        plt.colorbar()
        if bZoomAxis:
            plt.xlim([min_reco, max_reco])
            plt.ylim([min_reco, max_reco])
    
    newPath = '/'.join(figPath.split('/')[:-1])+'/plot_time_scatter_LZT/'
    savePath= newPath+figPath.split('/')[-1]
    if not(os.path.isdir(newPath)):
        os.mkdir(newPath)
        
    plt.savefig(savePath,bbox_inches='tight')
    plt.close(fig)
    # plt.show(fig)
    
    
def plot_energy_scatter_LZT(dataset, reco_ene_dict, scale, figPath, labelDistX, bShowPlots=False, bZoomAxis=False, totalEnergy=50000, bShowXT=False, fontSizeLeg=24):
    '''
    Plot scatter of energy for each cell in a cluster, at same figure
    '''
    if bShowPlots:
        matplotlib.use('module://ipykernel.pylab.backend_inline')  # show plots
    else:
        matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS
    
    ## Selecting scale for plot (mev or gev)
    scaleCnt = 1
    isMevData = np.max(dataset.edep.detach().numpy()[:,4]) >= 500
    
    textScale = ''
    if (scale=='MeV') and not(isMevData):        
        scaleCnt = 1000
    elif (scale=='GeV') and isMevData:
        scaleCnt = 1/1000
    else:
        textScale = 'MeV'
        if not(isMevData):
            textScale='GeV'
            scaleCnt = 1/1000
        scale=textScale
        print('scale not selected. Choosing {}, because isMevData={}. Scaling factor: {}.'.format(textScale, isMevData, scaleCnt))
    
    phiSize     = dataset.conditions['phiSize']
    etaSize     = dataset.conditions['etaSize']
    plotMapSize = phiSize*etaSize
    
    fig = plt.figure(figsize=(36,36))
    
    for cell in range(0,phiSize*etaSize):
        # if cell==4:
        #     bZoomAxis=False
        # else:
        #     bZoomAxis=True
            # continue
        cellClusPos = cellArrayIndexToClusterPositionIndex(cell, bSingleMode=True)
        
        eneTargetList = dataset.e.detach().numpy()[:,cell]*scaleCnt
        if bShowXT:
            eneOutputList = dataset.xte.detach().numpy()[:,cell]*scaleCnt
        else:
            eneOutputList = np.array(reco_ene_dict['cell_{}'.format(cell)], dtype=object)*scaleCnt
        
        xbins = getBins(-2000,totalEnergy,100)
        if scale=='MeV':
            xbins = getBins(-2000,totalEnergy,100)
        if scale=='GeV':
            xbins = getBins(-2,int(totalEnergy*scaleCnt),0.1)
            
        max_reco = max(max(eneTargetList),max(eneOutputList))
        min_reco = min(min(eneTargetList), min(eneOutputList))
        
        ax0 = plt.subplot(etaSize,phiSize,cellClusPos+1)
        plt.hist2d(eneTargetList, eneOutputList, bins=[xbins,xbins], cmin=1,norm=mcolors.LogNorm(), cmap=plt.cm.jet)
        r2_target_output = r2_score(eneTargetList, eneOutputList)
        plt.plot([0, max_reco],[0, max_reco], '--r', label=r'$R^2=$'+'{:.4f}'.format(r2_target_output)+'\n')
        plt.legend(loc='lower right', fontsize=fontSizeLeg)
        
        plt.xlabel('Cluster cell energy [{}]'.format(scale), labelpad=labelDistX)
        if bShowXT:
            plt.ylabel('Reconstructed XTCluster cell energy [{}] 100 MeV/bin'.format(scale))
        else:
            plt.ylabel('Reconstructed cluster cell energy [{}] 100 MeV/bin'.format(scale))
        
        plt.colorbar()
        if bZoomAxis:
            plt.xlim([min_reco, max_reco])
            plt.ylim([min_reco, max_reco])
    
    newPath = '/'.join(figPath.split('/')[:-1])+'/plot_energy_scatter_LZT/'
    savePath= newPath+figPath.split('/')[-1]
    if not(os.path.isdir(newPath)):
        os.mkdir(newPath)
        
    plt.savefig(savePath,bbox_inches='tight')
    plt.close(fig)
    # plt.show(fig)
    
    
    
def plot_train_val_loss_compare_nEvents(theSeries, theRange, pathLossMainFolder, rangeType='Total number of clusters on training+valid dataset'):
    '''
    listParam1 : its the series that will be show in the plot (ML Model)
    listParam2 : the range within the series will be drawn (Events, layers, ...)
    '''
    matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS

    bpShifts        = [0.1,-0.1]

    fig,ax0 = plt.subplots(figsize=(9,6))
    fig1,ax1 = plt.subplots(figsize=(9,6))
    fig2,ax2 = plt.subplots(figsize=(9,6))
        
    for netIdx, trainNeuList in enumerate(theSeries):
        range_neuInput  = trainNeuList#[45,20,20]
        range_neuText   = MLP.neuListToFileText(range_neuInput)

        pathLoss = pathLossMainFolder+'/{}/'.format(range_neuText)

        ci_value        = .90 # confidence interval value alpha

        modelSignalType = 'nnp'
        optimConfig     = 'adam'
        # trainNeuList    = [45,15,15] # 2layer (24/05/2024)
        # range_neuInput  = trainNeuList#[45,20,20]
        range_neuText   = MLP.neuListToFileText(range_neuInput)
        print(range_neuText)

        cellMarkers     = ['x','v','^','<','D','o','>','1','+', '^','*']
        cellColors      = ['blue','red','black','green','yellow','purple','cyan','pink','orange','blue', 'red']

        for idx, total_clusters in enumerate(theRange):
            print('Total events: ',total_clusters)

            # fig,ax0 = plt.subplots(figsize=(14,8))

            # config_train = np.load(pathLoss+'config_train_nEvts{}_{}_{}_neu{}.npz'.format(total_clusters, modelSignalType, optimConfig, range_neuText), allow_pickle=True)['config'].tolist()
            config_file_name = pathLoss+'config_train_nEvts{}_{}_{}_neu{}.npz'.format(total_clusters, modelSignalType, optimConfig, range_neuText)
            with np.load(config_file_name, allow_pickle=True) as config_train_dict:
                config_train = config_train_dict['config'].tolist()
                # config_train_filename = pathLoss+'config_train_nEvts{}_{}_{}_neu{}.npz'.format(total_clusters, modelSignalType, optimConfig, range_neuText)
                
                # with np.load(config_train_filename, allow_pickle=True)['config'].tolist() as config_train:

                trainRatio   = config_train['train_ratio']
                batch_size   = config_train['train_args']['batch_size']
                max_epochs   = config_train['train_args']['nEpochs']
                patience     = config_train['train_args']['patience']
                max_iter     = config_train['train_args']['nIter']
                k            = config_train['train_args']['kFolds']
                num_proc     = config_train['train_args']['num_processes']

                clusters_per_fold_train = round((trainRatio*total_clusters)/k)
                clusters_per_fold_val   = round(((1-trainRatio)*total_clusters)/k)

                # lossAllIterations       = np.zeros((max_epochs, max_iter),dtype=float)
                lossAllIterations       = np.zeros((k, max_iter),dtype=float)
                bestEpochAllIterations  = np.zeros((k, max_iter),dtype=float)
                trainTimehAllIterations = np.zeros((k, max_iter),dtype=float)

                # for rep in range(max_rep):
                for fold in range(k):
                    for it in range(max_iter):
                        train_logging_dict_name = pathLoss+'lossMP_nEvts{}_{}_fold{}_iter{}_neu{}_batch{}.npz'.format(total_clusters, modelSignalType, fold, it, range_neuText, batch_size)
                        with np.load(train_logging_dict_name, allow_pickle=True) as train_logging_file:
                            train_logging_dict = train_logging_file['loss'].tolist()
                            # train_logging_dict  = np.load(pathLoss+'lossMP_nEvts{}_{}_fold{}_iter{}_neu{}_batch{}.npz'.format(total_clusters, modelSignalType, fold, it, range_neuText, batch_size),allow_pickle=True)['loss'].tolist()
                        
                            best_epoch          = train_logging_dict['best_epoch']
                            
                            val_loss_epochs = train_logging_dict['val_loss_history'][num_proc-1].numpy()
                            val_loss = val_loss_epochs[ val_loss_epochs != 0.][int(best_epoch[num_proc-1])]
                            lossAllIterations[fold,it]          = val_loss

                            bestEpochAllIterations[fold,it]     = train_logging_dict['best_epoch']
                            trainTimehAllIterations[fold,it]    = train_logging_dict['train_time'][num_proc-1]/60 # seconds to minutes


                best_val_loss    = lossAllIterations.min(axis=1)
                best_epochs      = bestEpochAllIterations.min(axis=1)
                best_train_time  = trainTimehAllIterations.min(axis=1)
                
                ax0.boxplot(best_val_loss, patch_artist=True,   positions=[idx+1-bpShifts[netIdx]], boxprops=dict(facecolor=cellColors[netIdx], color=cellColors[netIdx]))
                ax1.boxplot(best_epochs, patch_artist=True,     positions=[idx+1-bpShifts[netIdx]], boxprops=dict(facecolor=cellColors[netIdx], color=cellColors[netIdx]))
                ax2.boxplot(best_train_time, patch_artist=True, positions=[idx+1-bpShifts[netIdx]], boxprops=dict(facecolor=cellColors[netIdx], color=cellColors[netIdx]))
            
    # ax0.set_xticks([i+1 for i in range(len(range_clusSize))],[range_clusSize])
    ax0.set_xticks(np.arange(1,len(theRange)+1))
    ax0.set_xticklabels(theRange)
    # ax1.set_xticklabels([f'{label}%' for label in range_clusSize])
    ax0.set_title("Folds: {}, Init: {}, Max. epochs: {}, \nPatience: {}".format(k, max_iter, max_epochs, patience))
    # ax0.set_xlabel("Total {} on training+valid dataset".format(theRange))
    ax0.set_xlabel("{}".format(rangeType))
    ax0.set_ylabel("MSE: validation loss")


    ax1.set_xticks(np.arange(1,len(theRange)+1))
    ax1.set_xticklabels(theRange)
    # ax1.set_xticklabels([f'{label}%' for label in range_clusSize])
    ax1.set_title("Folds: {}, Init: {}, Max. epochs: {}, \nPatience: {}".format(k, max_iter, max_epochs, patience))
    # ax1.set_xlabel("Total number of clusters on training+valid dataset")
    ax1.set_xlabel("{}".format(rangeType))
    ax1.set_ylabel("Best training epoch")
    # ax1.set_ylim(0,105)

    ax2.set_xticks(np.arange(1,len(theRange)+1))
    ax2.set_xticklabels(theRange)
    # ax1.set_xticklabels([f'{label}%' for label in range_clusSize])
    ax2.set_title("Folds: {}, Init: {}, Max. epochs: {}, \nPatience: {}".format(k, max_iter, max_epochs, patience))
    # ax2.set_xlabel("Total number of clusters on training+valid dataset")
    ax2.set_xlabel("{}".format(rangeType))
    ax2.set_ylabel("Training time (min)")


    outputPathAlias = '/'.join(pathLoss.split('/')[:-2])+'/plots_train_loss/'
    if not(os.path.isdir(outputPathAlias)):
        os.mkdir(outputPathAlias)

    fig.savefig(outputPathAlias+'loss_valid_compare.png', bbox_inches='tight')
    fig1.savefig(outputPathAlias+'epochs_valid_compare.png', bbox_inches='tight')
    fig2.savefig(outputPathAlias+'trainTime_valid_compare.png', bbox_inches='tight')

    plt.close(fig)
    plt.close(fig1)
    plt.close(fig2)


def plot_train_val_loss_compare_Neurons(theSeries, theRange, nEventsInTraining, pathLossMainFolder, rangeType='Total number of clusters on training+valid dataset', doublePlotsPerSeriesItem=False):
    '''
    listParam1 : its the series that will be show in the plot (ML Model)
    listParam2 : the range within the series will be drawn (Events, layers, ...)
    '''
    matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS

    if doublePlotsPerSeriesItem:
        bpShifts    = [0.1,-0.1]
    else:
        bpShifts    = 0.0

    fig,ax0 = plt.subplots(figsize=(9,6))
    fig1,ax1 = plt.subplots(figsize=(9,6))
    fig2,ax2 = plt.subplots(figsize=(9,6))
        
    for netIdx, trainNeuList in enumerate(theSeries):
        range_neuInput  = trainNeuList#[45,20,20]
        range_neuText   = MLP.neuListToFileText(range_neuInput)

        pathLoss = pathLossMainFolder+'/{}/'.format(range_neuText)

        ci_value        = .90 # confidence interval value alpha

        modelSignalType = 'nnp'
        optimConfig     = 'adam'
        # trainNeuList    = [45,15,15] # 2layer (24/05/2024)
        # range_neuInput  = trainNeuList#[45,20,20]
        range_neuText   = MLP.neuListToFileText(range_neuInput)
        print(range_neuText)

        cellMarkers     = ['x','v','^','<','D','o','>','1','+', '^','*']
        cellColors      = ['blue','red','black','green','yellow','purple','cyan','pink','orange','blue', 'red']

        print('Total events in training:: ',nEventsInTraining)
        # for idx, total_clusters in enumerate(theRange):
        # for idx, rangeValue in enumerate(theRange):
            

        # fig,ax0 = plt.subplots(figsize=(14,8))

        # config_train = np.load(pathLoss+'config_train_nEvts{}_{}_{}_neu{}.npz'.format(total_clusters, modelSignalType, optimConfig, range_neuText), allow_pickle=True)['config'].tolist()
        config_file_name = pathLoss+'config_train_nEvts{}_{}_{}_neu{}.npz'.format(nEventsInTraining, modelSignalType, optimConfig, range_neuText)
        with np.load(config_file_name, allow_pickle=True) as config_train_dict:
            config_train = config_train_dict['config'].tolist()
            # config_train_filename = pathLoss+'config_train_nEvts{}_{}_{}_neu{}.npz'.format(total_clusters, modelSignalType, optimConfig, range_neuText)
            
            # with np.load(config_train_filename, allow_pickle=True)['config'].tolist() as config_train:

            trainRatio   = config_train['train_ratio']
            batch_size   = config_train['train_args']['batch_size']
            max_epochs   = config_train['train_args']['nEpochs']
            patience     = config_train['train_args']['patience']
            max_iter     = config_train['train_args']['nIter']
            k            = config_train['train_args']['kFolds']
            num_proc     = config_train['train_args']['num_processes']

            # clusters_per_fold_train = round((trainRatio*total_clusters)/k)
            # clusters_per_fold_val   = round(((1-trainRatio)*total_clusters)/k)

            # lossAllIterations       = np.zeros((max_epochs, max_iter),dtype=float)
            lossAllIterations       = np.zeros((k, max_iter),dtype=float)
            bestEpochAllIterations  = np.zeros((k, max_iter),dtype=float)
            trainTimehAllIterations = np.zeros((k, max_iter),dtype=float)

            # for rep in range(max_rep):
            for fold in range(k):
                for it in range(max_iter):
                    train_logging_dict_name = pathLoss+'lossMP_nEvts{}_{}_fold{}_iter{}_neu{}_batch{}.npz'.format(nEventsInTraining, modelSignalType, fold, it, range_neuText, batch_size)
                    with np.load(train_logging_dict_name, allow_pickle=True) as train_logging_file:
                        train_logging_dict = train_logging_file['loss'].tolist()
                        # train_logging_dict  = np.load(pathLoss+'lossMP_nEvts{}_{}_fold{}_iter{}_neu{}_batch{}.npz'.format(total_clusters, modelSignalType, fold, it, range_neuText, batch_size),allow_pickle=True)['loss'].tolist()
                    
                        best_epoch          = train_logging_dict['best_epoch']
                        
                        val_loss_epochs = train_logging_dict['val_loss_history'][num_proc-1].numpy()
                        val_loss = val_loss_epochs[ val_loss_epochs != 0.][int(best_epoch[num_proc-1])]
                        lossAllIterations[fold,it]          = val_loss

                        bestEpochAllIterations[fold,it]     = train_logging_dict['best_epoch']
                        trainTimehAllIterations[fold,it]    = train_logging_dict['train_time'][num_proc-1]/60 # seconds to minutes


            best_val_loss    = lossAllIterations.min(axis=1)
            best_epochs      = bestEpochAllIterations.min(axis=1)
            best_train_time  = trainTimehAllIterations.min(axis=1)
            
            if doublePlotsPerSeriesItem:
                ax0.boxplot(best_val_loss, patch_artist=True,   positions=[netIdx+1-bpShifts[netIdx]], boxprops=dict(facecolor=cellColors[netIdx], color=cellColors[netIdx]))
                ax1.boxplot(best_epochs, patch_artist=True,     positions=[netIdx+1-bpShifts[netIdx]], boxprops=dict(facecolor=cellColors[netIdx], color=cellColors[netIdx]))
                ax2.boxplot(best_train_time, patch_artist=True, positions=[netIdx+1-bpShifts[netIdx]], boxprops=dict(facecolor=cellColors[netIdx], color=cellColors[netIdx]))
            else:
                ax0.boxplot(best_val_loss, patch_artist=True,   positions=[netIdx+1], boxprops=dict(facecolor=cellColors[netIdx], color=cellColors[netIdx]))
                ax1.boxplot(best_epochs, patch_artist=True,     positions=[netIdx+1], boxprops=dict(facecolor=cellColors[netIdx], color=cellColors[netIdx]))
                ax2.boxplot(best_train_time, patch_artist=True, positions=[netIdx+1], boxprops=dict(facecolor=cellColors[netIdx], color=cellColors[netIdx]))
            
    # ax0.set_xticks([i+1 for i in range(len(range_clusSize))],[range_clusSize])
    ax0.set_xticks(np.arange(1,len(theRange)+1))
    ax0.set_xticklabels(theRange)
    # ax1.set_xticklabels([f'{label}%' for label in range_clusSize])
    ax0.set_title("Folds: {}, Init: {}, Max. epochs: {}, \nPatience: {}".format(k, max_iter, max_epochs, patience))
    # ax0.set_xlabel("Total {} on training+valid dataset".format(theRange))
    ax0.set_xlabel("{}".format(rangeType))
    ax0.set_ylabel("MSE: validation loss")


    ax1.set_xticks(np.arange(1,len(theRange)+1))
    ax1.set_xticklabels(theRange)
    # ax1.set_xticklabels([f'{label}%' for label in range_clusSize])
    ax1.set_title("Folds: {}, Init: {}, Max. epochs: {}, \nPatience: {}".format(k, max_iter, max_epochs, patience))
    # ax1.set_xlabel("Total number of clusters on training+valid dataset")
    ax1.set_xlabel("{}".format(rangeType))
    ax1.set_ylabel("Best training epoch")
    # ax1.set_ylim(0,105)

    ax2.set_xticks(np.arange(1,len(theRange)+1))
    ax2.set_xticklabels(theRange)
    # ax1.set_xticklabels([f'{label}%' for label in range_clusSize])
    ax2.set_title("Folds: {}, Init: {}, Max. epochs: {}, \nPatience: {}".format(k, max_iter, max_epochs, patience))
    # ax2.set_xlabel("Total number of clusters on training+valid dataset")
    ax2.set_xlabel("{}".format(rangeType))
    ax2.set_ylabel("Training time (min)")


    outputPathAlias = '/'.join(pathLoss.split('/')[:-2])+'/plots_train_loss/'
    if not(os.path.isdir(outputPathAlias)):
        os.mkdir(outputPathAlias)

    fig.savefig(outputPathAlias+'loss_valid_compare.png', bbox_inches='tight')
    fig1.savefig(outputPathAlias+'epochs_valid_compare.png', bbox_inches='tight')
    fig2.savefig(outputPathAlias+'trainTime_valid_compare.png', bbox_inches='tight')

    plt.close(fig)
    plt.close(fig1)
    plt.close(fig2)