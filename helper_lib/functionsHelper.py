import ROOT
import numpy as np
import math
import array
import json
import pickle

def listOfListsToArrayOfArrays(theList):
    if len(theList)==0:
        return []
    else:
        length  = max(map(len, theList))
        y       = np.array([xi+[None]*(length-len(xi)) for xi in theList])
        return y
 

def isJetInsideCell(jetEta, jetPhi, cellEta, cellPhi, cellDEta, cellDPhi):
    # verify in eta and phi, around Deta and Dphi
    cellLowerBoundEta   = jetEta >= (cellEta - cellDEta/2)
    cellUpperBoundEta   = jetEta < (cellEta + cellDEta/2)
    cellLowerBoundPhi   = jetPhi >= (cellPhi - cellDPhi/2)
    cellUpperBoundPhi   = jetPhi < (cellPhi + cellDPhi/2)

    if cellLowerBoundEta and cellLowerBoundPhi and cellUpperBoundEta and cellUpperBoundPhi:
        isInsideCell = True
    else:
        isInsideCell = False

    return isInsideCell

def getCentralCellIndexJetROI_insideCell(jetEta, jetPhi, cellEtaArray, cellPhiArray, cellDEtaArray, cellDPhiArray):
    '''
    Get the closest cell to a jet eta-phi, verifying if the jet lies within the cell eta-phi. 
    It verifies if the jet eta-phi is within the cell dimensions, using the granularity of a CaloCell
    object.
    '''
    ##### Get central cell by region of a cell. sometimes it cant find the cell. Can be problematic, can fail sometimes, 
    # not finding any cell that contains the jet properly.

    # isInsideCellArray   = np.zeros(len(cellEtaArray), dtype=bool) #array of bool
    cIndex = 0
    for cellEta, cellPhi, cellDEta, cellDPhi in zip(cellEtaArray, cellPhiArray, cellDEtaArray, cellDPhiArray):
        cellLowerBoundEta   = jetEta >= (cellEta - cellDEta/2)
        cellUpperBoundEta   = jetEta < (cellEta + cellDEta/2)
        cellLowerBoundPhi   = jetPhi >= (cellPhi - cellDPhi/2)
        cellUpperBoundPhi   = jetPhi < (cellPhi + cellDPhi/2)
        # print("cIndex {} ::\nJetEta: {} <= {} < {} -> {}\nJetPhi: {} <= {} < {} -> {}".format(cIndex, cellEta - cellDEta/2, jetEta, cellEta + cellDEta/2, cellLowerBoundEta and cellUpperBoundEta, cellPhi - cellDPhi/2, jetPhi, cellPhi + cellDPhi/2, cellLowerBoundPhi and cellUpperBoundPhi))
        ## OBS.: in FWD region there is a kind of cell superposition, at the same calo layer, which this code here at some cases, 
        #   finds two cells for the same jet ETA,PHI. So, because of that, the search will stop after find the first cell,
        #   and this one will be chosen as the central cell of jetROI
        if cellLowerBoundEta and cellLowerBoundPhi and cellUpperBoundEta and cellUpperBoundPhi:
            # isInsideCellArray[cIndex] = True
            # print("cIndex {} ::\nJetEta: {} <= {} < {} -> {}\nJetPhi: {} <= {} < {} -> {}".format(cIndex, cellEta - cellDEta/2, jetEta, cellEta + cellDEta/2, cellLowerBoundEta and cellUpperBoundEta, cellPhi - cellDPhi/2, jetPhi, cellPhi + cellDPhi/2, cellLowerBoundPhi and cellUpperBoundPhi))
            return cIndex
        else:
            cIndex+=1
    # print(sum(isInsideCellArray), cIndex)
    return -999 # if the jet isnt inside any cell, got error code.

def getCentralCellIndexJetROI_deltaR(jetEta, jetPhi, cellEtaArray, cellPhiArray, cellSamplingArray, getDeltaRList=False):
    '''
    Get the closest cell to a jet eta-phi, using the delta_R criteria. 
    The cell chosen, from the cell collection, is that one which has the
    lowest delta_R in the collection. There is an option to get an array of all Delta_R
    from the cell collection passed.
    Jun16/2022: added a condition to not consider the cells in crack the closest to a Jet.
    '''
    # verify in eta and phi, around Deta and Dphi
    deltaR   = np.zeros(len(cellEtaArray)) #array of deltaR
    cIndex = 0
    for cellEta, cellPhi, cellSampling  in zip(cellEtaArray, cellPhiArray, cellSamplingArray):
        if (isInsideLArCrackRegion(cellEta)) and (cellSampling==1 or cellSampling==2 or cellSampling==3): #LAr EMB and EMEC
            deltaR[cIndex]  = 999
        else:
            deltaEta        = jetEta - cellEta
            deltaPhi        = correctedDeltaPhi(jetPhi, cellPhi)
            deltaR[cIndex]  = calcDeltaR(deltaEta, deltaPhi)        
        cIndex+=1
    # closestCellIndex    = np.where(deltaR == np.min(deltaR))[0][0]
    if getDeltaRList:
        return deltaR
    else:
        closestCellIndex    = np.where(deltaR == np.min(deltaR))[0][0]
        return closestCellIndex

# def getCentralCellIndexJetROI_deltaR(jetEta, jetPhi, cellEtaArray, cellPhiArray, getDeltaRList=False):
#     '''
#     Get the closest cell to a jet eta-phi, using the delta_R criteria. 
#     The cell chosen, from the cell collection, is that one which has the
#     lowest delta_R in the collection. There is an option to get an array of all Delta_R
#     from the cell collection passed.
#     '''
#     # verify in eta and phi, around Deta and Dphi
#     deltaR   = np.zeros(len(cellEtaArray)) #array of deltaR
#     cIndex = 0
#     for cellEta, cellPhi  in zip(cellEtaArray, cellPhiArray):
#         deltaEta        = jetEta - cellEta
#         deltaPhi        = correctedDeltaPhi(jetPhi, cellPhi)
#         deltaR[cIndex]  = calcDeltaR(deltaEta, deltaPhi)
        
#         cIndex+=1
#     # closestCellIndex    = np.where(deltaR == np.min(deltaR))[0][0]
#     if getDeltaRList:
#         return deltaR
#     else:
#         closestCellIndex    = np.where(deltaR == np.min(deltaR))[0][0]
#         return closestCellIndex

def getCellEneTimeFromChannel(rawChannelEneArray, rawChannelTimeArray, rawChannelIndexArray, cellIndex):
    ''''
    Return the energy and time of a calo cell, based on the sum of its readout channel energy estimation.
    '''
    chArrayInt  = [int(x) for x in rawChannelIndexArray] # convert the list of float channel indexes into integer
    cellChPos   = np.where(chArrayInt == cellIndex)[0]    # get the positions of the same cell (if there are a multiple ch cell, like in tile)
    if len(cellChPos > 1):
        cellEne     = sum(rawChannelEneArray[cellChPos]) # sum the energy of each channel of the cell. If the it is a single ch, the ene value remains the same.
        cellTime    = np.mean(rawChannelTimeArray[cellChPos])
        return cellEne, cellTime
    elif len(cellChPos) == 0:
        print("Cell %d was not found in raw channel!"%(cellIndex))
    else:
        return rawChannelEneArray[cellChPos], rawChannelTimeArray[cellChPos]

def getCellEnergyFromChannel(rawChannelEneArray, rawChannelIndexArray, cellIndex):
    ''''
    Return the energy of a calo cell, based on the sum of its readout channel energy estimation.
    '''
    chArrayInt  = [int(x) for x in rawChannelIndexArray] # convert the list of float channel indexes into integer
    cellChPos   = np.where(chArrayInt == cellIndex)[0]    # get the positions of the same cell (if there are a multiple ch cell, like in tile)
    if len(cellChPos > 1):
        cellEne   = sum(rawChannelEneArray[cellChPos]) # sum the energy of each channel of the cell. If the it is a single ch, the ene value remains the same.
        return cellEne
    elif len(cellChPos) == 0:
        print("Cell %d was not found in raw channel!"%(cellIndex))
    else:
        return rawChannelEneArray[cellChPos]

def getCellTimeFromChannel(rawChannelTimeArray, rawChannelIndexArray, cellIndex, method='mean'):
    ''''
    Return the time of a calo cell, based at its readout channel time estimation. 
    It is possible to select the method.
    '''
    chArrayInt  = [int(x) for x in rawChannelIndexArray] # convert the list of float channel indexes into integer
    cellChPos   = np.where(chArrayInt == cellIndex)[0]    # get the positions of the same cell (if there are a multiple ch cell, like in tile)
    # print(rawChannelTimeArray[cellChPos], cellChPos, cellIndex, len(rawChannelTimeArray[cellChPos]))
    if len(cellChPos > 1):
        if method == 'mean':
            cellTime     = np.mean(rawChannelTimeArray[cellChPos]) # sum the energy of each channel of the cell. If the it is a single ch, the ene value remains the same.
            # print("time",cellTime)
            return cellTime
    # if len(cellChPos) == 0:
    #     print("empty time for chosen cell.")
    #     cellTime = float(0.0)
    #     return cellTime
    if len(cellChPos) == 1:
        return rawChannelTimeArray[cellChPos]


def indexConversion(indexListA, indexListB, dataB):
    ''''
    Return a list of values indexed by indexListB, in a different order and/or size, which is also mapped 
    by another list of indexes (indexListA).

    indexListA  : list of indexes which you want to retrieve the data linked to it, but from another level (cell, channel, raw channel).
    indexListB  : the list of indexes of the actual level which the data you want to retrieve, belongs to.
    dataB       : the data organized by the indexListB, that will be converted to the list of indexes A.
    '''
    # print("indexListA: ",type(indexListA), np.shape(indexListA), indexListA)
    # if len(indexListA) != 0:
    # indexListA_array    = np.concatenate(indexListA).ravel()
    # indexListB_array    = np.concatenate(indexListB).ravel()
    # dataB_array         = np.concatenate(dataB).ravel()
    indexListA_array    = np.ravel(indexListA)
    indexListB_array    = np.ravel(indexListB)
    dataB_array         = np.ravel(dataB)
    # else:
    #     indexListA_array    = indexListA
    #     indexListB_array    = indexListB
    #     dataB_array         = dataB
    # print("indexListA_array: ",type(indexListA_array), np.shape(indexListA_array), indexListA_array)
    indexListA_array_int  = [int(x) for x in indexListA_array] # convert the list of float channel indexes into integer
    # print("indexListA_int: ",indexListA_array_int)
    arrayIndexesAB = []
    for indA in indexListA_array_int:
        position_AB = np.where(indexListB_array == indA)[0]
        arrayIndexesAB.append(position_AB)
    # print(type(arrayIndexesAB))
    # print(np.shape(arrayIndexesAB))
    if len(arrayIndexesAB)==0:
        return np.asarray([])
    else:
        new_arrayIndexesAB = np.concatenate(arrayIndexesAB).ravel()
        # print("new_arrayIndexesAB: ",new_arrayIndexesAB)
        dataB_asIndexA = np.asarray(dataB_array[new_arrayIndexesAB])
    # print("dataB_asIndexA: ",dataB_asIndexA)

    return dataB_asIndexA

def getSamplingString(layerArray, layerDict):
    '''
    Convert the layer (sampling) integer of the ntuple to a string, which is
    representative of where the cell belongs.
    '''
    newLayerArray   = []
    for k in range(0, len(layerArray)):
        newLayerArray.append( str(layerDict[layerArray[k]]) )
    return newLayerArray

def getRegionString(regionArray, regionDict):
    '''
    Convert the region integer of the ntuple to a string, which is
    representative of where the cell belongs.
    '''
    newRegionArray   = []
    for k in range(0, len(regionArray)):
        newRegionArray.append( str(regionDict[regionArray[k]]) ) # conversion from unicode to ascii
    return newRegionArray

def getCaloGainString(caloGainArray, caloGainNumDict, caloGainNameDict):
    '''
    Convert the caloGain of the ntuple to a string.
    CaloGain {
        TILELOWLOW      = -16 ,
        TILELOWHIGH     = -15 ,
        TILEHIGHLOW     = -12,
        TILEHIGHHIGH    = -11,
        TILEONELOW      = -4,
        TILEONEHIGH     = -3,
        INVALIDGAIN     = -1, 
        LARHIGHGAIN     = 0, 
        LARMEDIUMGAIN   = 1,  
        LARLOWGAIN      = 2,
        LARNGAIN        = 3,
        UNKNOWNGAIN     = 4
        };
    '''
    newCaloGainArray   = []
    for k in range(0, len(caloGainArray)):
        for y in range(0, len(caloGainNumDict)):
            if (caloGainArray[k] == caloGainNumDict[y]):
                newCaloGainArray.append(str(caloGainNameDict[y]))
            else:
                continue
    return newCaloGainArray

def isInsideLArCrackRegion(eta):
    '''
    Return a boolean if eta is inside LAr crack region.
    '''
    return (((np.abs(eta) >= 1.37) and (np.abs(eta) <= 1.52)) or (np.abs(eta) >= 2.37))

def calcDeltaR(deltaEta, deltaPhi ):
    '''
    Return de Delta_R for a given deltaEta and deltaPhi pair.
    '''
    deltaR = math.sqrt(deltaEta*deltaEta + deltaPhi*deltaPhi)
    return deltaR

def calcDeltaEta(eta1, eta2):
    eta1 = round(eta1, 4)
    eta2 = round(eta2, 4)
    return round((eta1-eta2), 4)

def correctedDeltaPhi(phi1, phi2):
    '''
    Return the corrected value of delta_phi, inside the [-Pi,+Pi] range.
    Jun16/2022: Added round with precision at return.
    '''
    phi1 = round(phi1, 4)
    phi2 = round(phi2, 4)
    if (phi1-phi2) < ((-1)*math.pi):
        return round(((phi1-phi2) + 2*math.pi), 4)
    elif (phi1-phi2) > math.pi:
        return round(((phi1-phi2) - 2*math.pi), 4)
    else:
        return round((phi1-phi2), 4)

# def correctedDeltaPhi(phi1, phi2):
#     '''
#     Return the corrected value of delta_phi, inside the [-Pi,+Pi] range.
#     '''
#     if (phi1-phi2) < ((-1)*math.pi):
#         return ((phi1-phi2) + 2*math.pi)
#     elif (phi1-phi2) > math.pi:
#         return ((phi1-phi2) - 2*math.pi)
#     else:
#         return (phi1-phi2)

def stdVecOfStdVecToPlainCPPArray(vecOfVec, num_type='f'):
    a1 = array.array(num_type,[])
    for vecValues in vecOfVec:
        a2 = array.array('f', vecValues)
        for value in a2:
            a1.insert(0,value)
    return a1

def stdVecToPythonArray(vec, num_type='f'):
    a1 = array.array(num_type,[])
    for vecValues in vec:
        a1.insert(0,vecValues)
    return a1

def stdVecToArray(theList, dataType='float32'):
    '''
    Quick conversion of the list readed-out from TTree, into a numpy array.
    '''
    theNumpyArray = []
    for value in theList:
        theNumpyArray.append(value)
    # theNumpyArray = np.array(theNumpyArray)
    # return np.array(theNumpyArray,dtype=dataType)
    return np.array(theNumpyArray)

# def arrayOfArrayToList(stdVecArrayOfArray):
#     # nCells      = int(stdVecDigits.size())
#     myList  = []
#     for vec in stdVecArrayOfArray:
#         myList.append(list(vec))
    
#     return np.array(myList)

def stdVecOfStdVecToArrayOfList(stdVecArrayOfArray, convertToInt=False):
    # nCells      = int(stdVecDigits.size())
    myList  = []
    myArray = []
    for vec in stdVecArrayOfArray:
        myList = np.ravel(list(vec)).tolist()
        if convertToInt:
            myList = [int(x) for x in myList]
            # for k in range(0,len(myList)):
            #     aux         = int(myList[k])
            #     myList[k]   = aux

        myArray.append(myList)
        # myList.append(list(vec))
    # myNewList = []
    # # for firstList in myList:
    # for values in myList:
    #     myNewList.append(values)
    
    # return np.array(myNewList)
    return np.array(myArray,dtype=object)


def loadJsonFile(filename):
    '''
    Alias to return json file.
    '''
    fDict = open(filename)
    return json.load(fDict) # Get Calo dictionary

def saveAsJsonFile(pyObj, filename):
    '''
    Alias to save object as json file.
    '''
    jsonObj = json.dumps(pyObj)
    f       = open(filename,"w")
    f.write(jsonObj)
    f.close()

def saveDictToNpz(pyDict, filename):
    '''
    Alias to save a python dictionary as NPZ.
    '''

def save_pickle(path, data):
    """Salva estruturas de dados, como dicionarios python
    save_pickle(path, data)
        path : Caminho do arquivo
        data : variavel a ser salva.
    """
    with open(path, "wb") as fileObj:
        pickle.dump(data,fileObj ,pickle.HIGHEST_PROTOCOL)
        
def load_pickle(path):
    """Carrega estruturas de dados, como dicionarios python
    load_pickle(path)
        path : Caminho do arquivo
    """
    data = pickle.load(open(path, "rb"))
    return data