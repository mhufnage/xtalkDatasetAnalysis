#
# Final Dataset for Xtalk ML studies
#

# This script generates a dry dataset made out of electron-cluster cells, with only needed information to ML training/val/testing.

import sys,json
from glob import glob
from ROOT import TFile, gROOT, TChain
import numpy as np
import math

# from auxiliaryFunctions import getSortedClusterIndexes, getMatchedIndexes, getCellsInWindowCustomSizeNEW, unpackCellsFromCluster
# from getXTDataAsPythonDict import getDictForAnalysis

# sys.path.insert(1, '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis')

# from ATLAS_Ntuple_reader import ATLAS_NTuple_Reader
# from getXTDataAsPythonDict import getDictForAnalysis, getXTDataAsPythonDictFasterClean
from ATLAS_Ntuple_reader import ATLAS_NTuple_Reader, ATLAS_NTuple_Event_Reader


fDict       = open('dictCaloByLayer.json')
caloDict    = json.load(fDict)

#---------------------------------------------
#----------   Read the ROOT trees   ----------
#---------------------------------------------
total_events        = -1 #nClusters to being user to train the model
nCellsWinEta        = 3
nCellsWinPhi        = 3
bFullWindows        = True # use only full windows from clusters
electronPtCut       = 0    # GeV, for ML dataset
clusName            = 'topocluster'
layer               = 'EMB2'


datasetPath = '/data/atlas/mhufnage/xtalk/dataset/'

datasets    = [
    datasetPath+'user.mhufnage.xtalk_dumper.data17_13TeV.00329542.physics_MinBias.ntuple.v0/',
    datasetPath+'user.mhufnage.xtalk_dumper.ZeeTP.data17_13TeV.00329542.physics_MinBias.ntuple.v0_XYZ.root.tgz/',
    datasetPath+'user.mhufnage.xtalk_dumper.mc16_13TeV.361106.Zee.ntuple.v0_XYZ.root.tgz/',
    datasetPath+'user.mhufnage.xtalk_dumper.ZeeTP.mc16_13TeV.361106.Zee.ntuple.v0_XYZ.root.tgz/',
]
isMC            = [False, False, True, True]
isTagAndProbe   = [False, True, False, True]

for getMLDataset in [False, True]:
    
    if not getMLDataset:
        datasetAlias    = [
            'data17_singleElec_full_',
            'data17_ZeeTP_full_',
            'mc16_singleElect_full',
            'mc16_ZeeTP_full']
    else:
        datasetAlias    = [
            'data17_singleElec_ML_',
            'data17_ZeeTP_ML_',
            'mc16_singleElect_ML_',
            'mc16_ZeeTP_ML_']

    for dsIndex, dsName in enumerate(datasets):
        print('Dataset alias: {}'.format(datasetAlias[dsIndex]))
        fileNames = glob(dsName+'dumper_minBias*.root')

        confDict = {
            'total_events'          : total_events, # if its on Cluster_mode, num_clusters = total_events
            'isMC'                  : isMC[dsIndex],
            'isTagAndProbe'         : isTagAndProbe[dsIndex],
            'electronPtCut'         : electronPtCut, # GeV,  ML Dataset
            'dumpRawChForMLStudies' : True,
            'sampling'              : layer,   # ML Dataset
            'nCellsWinEta'          : 3,    # ML Dataset
            'nCellsWinPhi'          : 3,    # ML Dataset
            'datasetPath'           : datasetPath,
            'calibType'             : 'dumper',     # name of the type of calibration, to differentiate from offline or database (analysis/debug) options: 'database' or 'typical'
            'clusName'              : clusName,     # 'topocluster' or '7x11'
            'bFullWindows'          : bFullWindows,         # use only full windows from clusters
            'bPedFromDB'            : True,         # Ped from DB (TRUE) or from Script (FALSE)
            'bDoOfflineCalib'       : False,    # ML Dataset
            'typeThreshold'         : 'EneThr', # Energy Threshold type for DSP timing estimation. 'fixedValue', 'EneThr','NoiseSigma'
            'bAbsDSPE1'             : False, # ~X~ use abs(E) to get E1, and compare to E_thr
            'bPerfomQuant'          : False, # ~X~ apply the quantized form of energy and time estimation (like DSP)
            'applyCteQuant'         : False, # ~X~ True # apply quantization to the alpha, beta, Pa, Pb. If False, use likeDSP function, but full precision constants.
            'reorderSamp'           : False, # ~X~ reorder samples like-dsp
            'fileNames'             : fileNames
        }

        if isMC:
            print('The input files are from ATLAS_MC!')
        else:
            print('The inputs are from collision data!')
        
        sTree   = TChain("dumpedData",'')
        for file in fileNames:
            sTree.Add(file+"/dumpedData") # main dumped data tree (event by event)
        
        nEvents = sTree.GetEntries()
        print("{} files were added with {} events.".format(len(fileNames),nEvents))
        
        #
        # Loop over events
        #
        for eventNumber, event in enumerate(sTree):
            if (eventNumber > total_events) and (total_events != -1):
                break
            
            if not (eventNumber % 10000):
                print('event {}...'.format(eventNumber))

            data = ATLAS_NTuple_Event_Reader(event, confDict, getMLDataset=getMLDataset)
            
            if data==0:
                continue
            



# ----------------------------------------------------------
# ----------------------------------------------------------
# ----------------------------------------------------------


# fDict       = open('dictCaloByLayer.json')
# caloDict    = json.load(fDict)

# #---------------------------------------------
# #----------   Read the ROOT trees   ----------
# #---------------------------------------------
# total_events        = -1 #nClusters to being user to train the model
# nCellsWinEta        = 3
# nCellsWinPhi        = 3
# bFullWindows        = True # use only full windows from clusters
# electronPtCut       = 0    # GeV, for ML dataset
# clusName            = 'topocluster'
# layer               = 'EMB2'


# datasetPath = '/data/atlas/mhufnage/xtalk/dataset/'

# datasets    = [
#     datasetPath+'user.mhufnage.xtalk_dumper.data17_13TeV.00329542.physics_MinBias.ntuple.v0/',
#     datasetPath+'user.mhufnage.xtalk_dumper.ZeeTP.data17_13TeV.00329542.physics_MinBias.ntuple.v0_XYZ.root.tgz/',
#     datasetPath+'user.mhufnage.xtalk_dumper.mc16_13TeV.361106.Zee.ntuple.v0_XYZ.root.tgz/',
#     datasetPath+'user.mhufnage.xtalk_dumper.ZeeTP.mc16_13TeV.361106.Zee.ntuple.v0_XYZ.root.tgz/',
# ]
# isMC            = [False, False, True, True]
# isTagAndProbe   = [False, True, False, True]

# for getMLDataset in [False, True]:
    
#     if not getMLDataset:
#         datasetAlias    = [
#             'data17_singleElec_full_',
#             'data17_ZeeTP_full_',
#             'mc16_singleElect_full',
#             'mc16_ZeeTP_full']
#     else:
#         datasetAlias    = [
#             'data17_singleElec_ML_',
#             'data17_ZeeTP_ML_',
#             'mc16_singleElect_ML_',
#             'mc16_ZeeTP_ML_']

#     for dsIndex, dsName in enumerate(datasets):
#         fileNames = glob(dsName+'dumper_minBias*.root')

#         confDict = {
#             'total_events'          : total_events, # if its on Cluster_mode, num_clusters = total_events
#             'isMC'                  : isMC[dsIndex],
#             'isTagAndProbe'         : isTagAndProbe[dsIndex],
#             'electronPtCut'         : electronPtCut, # GeV,  ML Dataset
#             'dumpRawChForMLStudies' : True,
#             'sampling'              : layer,   # ML Dataset
#             'nCellsWinEta'          : 3,    # ML Dataset
#             'nCellsWinPhi'          : 3,    # ML Dataset
#             'datasetPath'           : datasetPath,
#             'calibType'             : 'dumper',     # name of the type of calibration, to differentiate from offline or database (analysis/debug) options: 'database' or 'typical'
#             'clusName'              : clusName,     # 'topocluster' or '7x11'
#             'bFullWindows'          : bFullWindows,         # use only full windows from clusters
#             'bPedFromDB'            : True,         # Ped from DB (TRUE) or from Script (FALSE)
#             'bDoOfflineCalib'       : False,    # ML Dataset
#             'typeThreshold'         : 'EneThr', # Energy Threshold type for DSP timing estimation. 'fixedValue', 'EneThr','NoiseSigma'
#             'bAbsDSPE1'             : False, # ~X~ use abs(E) to get E1, and compare to E_thr
#             'bPerfomQuant'          : False, # ~X~ apply the quantized form of energy and time estimation (like DSP)
#             'applyCteQuant'         : False, # ~X~ True # apply quantization to the alpha, beta, Pa, Pb. If False, use likeDSP function, but full precision constants.
#             'reorderSamp'           : False, # ~X~ reorder samples like-dsp
#             'fileNames'             : fileNames
#         }

#         # fileNames   = glob(datasetPath)

#         dataset = ATLAS_NTuple_Reader(confDict, getMLDataset=getMLDataset)
#         print('Dataset {} size:{}.'.format(datasetAlias[dsIndex], len(dataset)))
        
#         if getMLDataset:
#             datasetName     = 'dataset_{}ptCut{}_{}_sampWin{}-{}_nClus{}.npz'.format(datasetAlias[dsIndex], electronPtCut, clusName, nCellsWinEta, nCellsWinPhi,  len(dataset))
#             confName        = 'config_{}ptCut{}_{}_sampWin{}-{}_nClus{}.npz'.format(datasetAlias[dsIndex], electronPtCut, clusName, nCellsWinEta, nCellsWinPhi,  len(dataset))
#         else:
#             datasetName     = 'dataset_{}_{}_nEvents{}.npz'.format(datasetAlias[dsIndex], clusName, len(dataset))
#             confName        = 'config_{}_{}_nEvents{}.npz'.format(datasetAlias[dsIndex], clusName, len(dataset))
            
#         # datasetName = datasetPath+'test_MC.npz'
#         np.savez(datasetPath+datasetName,dataset=dataset)
#         np.savez(datasetPath+confName,dataset=confDict)











# ----------------------------------------------------------
# ----------------------------------------------------------
# ----------------------------------------------------------
# 
# import json
# import os,sys
# import time
# import shelve

# ## Insert here the path to EventReader/share
# eventReaderSharePath = '/home/mhufnage/ATLAS_QT_XTALK/offline-ringer-dev/crosstalk/EventReader/share/'
# sys.path.insert(1, eventReaderSharePath) 
# # sys.path.insert(1, '/eos/user/m/mhufnage/SWAN_projects/XTalk/')

# import ROOT
# import numpy as np
# # import awkward as ak
# # import uproot
# # import mplhep as hep

# from glob import glob
# import math
# # import matplotlib.pyplot as plt
# # import matplotlib.gridspec as gridspec
# # import matplotlib.colors as mcolors
# # import matplotlib
# # plt.style.use([hep.style.ATLAS])

# # import mpl_scatter_density
# # import scipy.stats as scipy
# # import torch
# # import torch.nn as nn
# # from collections import OrderedDict

# ## Custom Packages
# from auxiliaryFunctions import *
# from calibrationFilesHelper import *
# from getXTDataAsPythonDict import *
# # from calibrationFilesHelperROOT import *


# ## Load custom Help libraries
# fDict       = open(eventReaderSharePath+'dictCaloByLayer.json')
# caloDict    = json.load(fDict)


# #---------------------------------------------
# #----------   Read the ROOT trees   ----------
# #---------------------------------------------
# isNtupleFromMC      = True
# dumpRawCh           = True  # for analysis of calibration purposes
# clus_train          = -1    #nClusters to being user to train the model
# clusName            = 'topocluster' #'7x11'#'topocluster'#,'7x11' #
# datasetPath         ='/data/atlas/mhufnage/xtalk/dataset/MC_Zee_10kevts.root'
# # datasetPath         = '/eos/user/m/mhufnage/ALP_project/root/user.mhufnage.data17_13TeV_dumpedEventReader_v01_XYZ.root.tgz/*XYZ.root'
# datasetAlias        = 'MCZee'#'singlElec'#'singlElec' #Zee
# bFullWindows        = True # use only full windows from clusters
# bDoOfflineCalib     = False
# el_thr              = 0 # GeV
# nCellsWinEta        = 3
# nCellsWinPhi        = 3
# calibType           = 'dumper' # name of the type of calibration, to differentiate from offline or database (analysis/debug) #'database'#'database' #'typical' # or 
# layer               = 'EMB2'
# loopModeType        = 'cluster' # or 'event'

# # #---------------------------------------------

# fileNames       = glob(datasetPath)

# sTree   = ROOT.TChain("dumpedData",'')
# sLBTree = ROOT.TChain("lumiblockData",'')
# for file in fileNames:
#     fileNameInputLog = "/".join((file+" added to the TChain...").split('/')[-2:])
#     # print(fileNameInputLog) 
#     sTree.Add(file+"/dumpedData") # main dumped data tree (event by event)
#     sLBTree.Add(file+"/lumiblockData") # LB tree (lumiblock per lumiblock)

# if clusName == 'topocluster':
#     sTree.SetBranchStatus("cluster711_*",0)
#     clusKey = "clusters"
# if clusName == '7x11':
#     sTree.SetBranchStatus("cluster_*",0)
#     clusKey = '711_roi'


# nEvents         = sTree.GetEntries()
# nLumiBlocks     = sLBTree.GetEntries()

# print("{} files were added with {} events.".format(len(fileNames),nEvents))


# #---------------------------------------------
# #--------- Data Analysis conditions ----------
# #---------------------------------------------
# evt_lim             = nEvents
# bPlotClusters       = False
# bPedFromDB          = True # Ped from DB (TRUE) or from Script (FALSE)

# #---------------------------------------------
# #------------- DSP conditions #------------- < Not getting nice results from quantization. Ignore those *Quant* boolean options. >
# #---------------------------------------------
# if isNtupleFromMC:
#     typeThreshold   = 'MC'
# else:
#     typeThreshold   = 'EneThr'#'fixedValue'#'EneThr'#'NoiseSigma'#
# bAbsDSPE1           = False # ~X~ use abs(E) to get E1, and compare to E_thr
# bPerfomQuant        = False # ~X~ apply the quantized form of energy and time estimation (like DSP)
# applyCteQuant       = False # ~X~ True # apply quantization to the alpha, beta, Pa, Pb. If False, use likeDSP function, but full precision constants.
# reorderSamp         = False # ~X~ reorder samples like-dsp

# confDict = {
#     'el_thr'            : el_thr,
#     'isNtupleFromMC'    : isNtupleFromMC,
#     'dumpRawCh'         : dumpRawCh,
#     'sampling'          : layer,
#     'nCellsWinEta'      : nCellsWinEta,
#     'nCellsWinPhi'      : nCellsWinPhi,
#     'datasetPath'       : datasetPath,
#     'calibType'         : calibType,
#     'bFullWindows'      : bFullWindows,
#     'bPedFromDB'        : bPedFromDB,
#     'bDoOfflineCalib'   : bDoOfflineCalib,
#     'typeThreshold'     : typeThreshold,
#     'bAbsDSPE1'         : bAbsDSPE1,
#     'bPerfomQuant'      : bPerfomQuant,
#     'applyCteQuant'     : applyCteQuant,
#     'reorderSamp'       : reorderSamp,
#     'fileNames'         : fileNames
# }

# #---------------------------------------------
# #------- Online Calibration constants --------
# #---------------------------------------------

# ped_dict = {}
# if calibType=='database':
#     ofc_dict = np.load('calibration/OFC_dict.npz',allow_pickle=True )['OFC'].tolist()
#     ped_dict = np.load('calibration/PED_dict.npz',allow_pickle=True )['PED'].tolist()
#     mpmc_dict = np.load('calibration/MPMC_dict.npz',allow_pickle=True )['MPMC'].tolist()
#     uamev_dict = np.load('calibration/UAMEV_dict.npz',allow_pickle=True )['UAMEV'].tolist()
#     ramps_dict = np.load('calibration/RAMPS_dict.npz',allow_pickle=True )['RAMPS'].tolist()


# #---------------------------------------------
# #--------- Event and Data Reading ------------
# #---------------------------------------------
# print('Calibration type: {}'.format(calibType))
# print('Electron pt cut: {} GeV'.format(el_thr))
# print('Processing cluster: {}'.format(clusName))

# # for epoch in range(epochs):
# #     print('Epoch: {}...'.format(epoch))

# dataset_list    = []

# elec_counter    = 0 # counter of electrons in dataset
# clus_counter    = 0
# bMaxClusters    = False

# #---------------------------------------------
# #------ TTree Loop and data extraction -------
# #---------------------------------------------
# meanTimePerEvent = 0.0
# startTime = time()

# for evIndex in range(0, nEvents): ## Loop events into TTree
#     if bMaxClusters:
#         break
    
#     if loopModeType=='event':
#         if evIndex > evt_lim:
#             break
#         if (evIndex%int(evt_lim/5)) == 0:
#             print("Event {}/{}...".format(evIndex,evt_lim))

#     # dataset = getXTDataAsPythonDict(sTree, evIndex, eventReaderSharePath, dataHasBadCh=True, isMC=False)
#     dataset = getXTDataAsPythonDictFaster(sTree, evIndex, eventReaderSharePath, dataHasBadCh=True, isMC=isNtupleFromMC, clusterType=clusName) #<- apply sTree.GetEntry(evIndex) here!

#     if dataset==0: # Data has no electrons in this event!
#         continue

#     for evtn, ev in enumerate(dataset['dataset']): # for each event...
#     # with dataset['dataset'] as ev:
#         for elec in ev['electrons'].keys(): # for each electron in this event...
#             if bMaxClusters:
#                 break
#             # print(ev['electrons'][elec]['pt'])
#             if ev['electrons'][elec]['pt'][0]/1000 < el_thr:
#                 continue
#             if np.abs(ev['electrons'][elec]['eta']) > 1.4:
#                 continue
#             # datasetML = dataset ## Test

#             clusloop        = ev['electrons'][elec][clusKey]

#             # elecPt          = (ev['electrons'][elec]['pt'])
#             # elecEta         = (ev['electrons'][elec]['eta'])
#             # elecPhi         = (ev['electrons'][elec]['phi'])
#             # elecVtxZ        = (ev['electrons'][elec]['vertexZ'])
#             # elecNClus       = len(ev['electrons'][elec][clusKey])
            
#             for clus in clusloop.keys(): # for each cluster associated to that electron...
#                 if loopModeType=='cluster':
#                     if clus_train != -1:
#                         if clus_counter > clus_train:
#                             bMaxClusters = True
#                             break

                
#                 #---------------------------------------------
#                 #------- Cluster Dataset Processing ----------
#                 #---------------------------------------------
#                 dataset_cluster = ev['electrons'][elec][clusKey][clus]

#                 datasetML       = unpackCellsFromCluster(dataset_cluster, confDict, ped_dict=ped_dict, isMC=isNtupleFromMC, dumpRawChannels=dumpRawCh)
                
#                 if datasetML == 0: # Selected sampling has no cells!
#                     # print(' Selected sampling has no cells!')
#                     continue
                
#                 if clus_train != -1:
#                     if(clus_counter%int(clus_train/5))==0:
#                         print('\t{}/{} clusters were dumped so far...(Event index: {})'.format(clus_counter,clus_train,evIndex))
#                 else:
#                     if(clus_counter%1)==0:
#                         print('\t{} clusters were dumped so far...(Event index: {})'.format(clus_counter,evIndex))
                
#                 dataset_list.append(datasetML)

#                 clus_counter+=1

#                 if bMaxClusters:
#                     break
#             elec_counter+=1

#             if bMaxClusters:
#                 break
#         if bMaxClusters:
#             break
#     if bMaxClusters:
#         break
# #
# # End of the current Epoch
# #
# # dataset_list['loss'].append(loss.detach().numpy())
# # train_logging_dict['loss_tau'].append(loss_tau.detach().numpy())

# confDict['elec_counter'] = elec_counter
# print('Total Electrons: ',elec_counter)
# print('Total clusters: ', clus_counter)
# # print(confDict)
# endTime = time()

# print("Total running time: {:.2f} minutes. ({:.2f} us/event)".format((endTime - startTime)/60, (endTime - startTime)/3600*1000000/evt_lim))

# #
# # Save the results
# #
# datasetName     = 'dataset_{}_ptCut{}_{}_sampWin{}-{}_nClus{}.npz'.format(datasetAlias, el_thr, clusName, nCellsWinEta, nCellsWinPhi, clus_counter)
# confFileName    = 'datasetInfo_{}_ptCut{}_{}_sampWin{}-{}_nClus{}.npz'.format(datasetAlias,el_thr, clusName, nCellsWinEta, nCellsWinPhi, clus_counter)
# np.savez(datasetName,dataset=dataset_list)
# print(datasetName,' saved.')
# np.savez(confFileName, datasetInfo=confDict)
# print(confFileName, 'saved.')

