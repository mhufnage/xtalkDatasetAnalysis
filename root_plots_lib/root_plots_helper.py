import ROOT as root
import atlasplots as astyle


def fillTH1AndUpdateRange(histDict, newEntry):
    
    # if not isArray:
    if hasattr(newEntry, "__len__"):        
        if (min(newEntry) < histDict['min'] ):# and (min(newEntry) < edgMin):
            histDict['min'] = min(newEntry)
        if (max(newEntry) > histDict['max']):# and (max(newEntry) > edgMax):
            histDict['max'] = max(newEntry)
            
        for entry in newEntry:
            histDict['hist'].Fill(entry)
    else:
        histDict['hist'].Fill(newEntry)
        
        if (newEntry < histDict['min']) :#or (newEntry < edgMin):
            histDict['min'] = newEntry
        if (newEntry > histDict['max']) :#or (newEntry > edgMax):
            histDict['max'] = newEntry
    

def createDictOfTH1ForWholeTree(rootTree):
    # astyle.set_atlas_style()
    
    hists = {}
    for branch in rootTree.GetListOfBranches():
        # hist = root.TH1F(branch.GetName(), branch.GetName(), 100, 0, 10)
        hist = root.TH1D(branch.GetName(), branch.GetName(),40,-1,-1)
        # hist = root.TH1D(branch.GetName())
        hist.SetBuffer(100000)
        
        hists.update({"{}".format(str(branch.GetName())): {"hist": hist, "min": 0, "max": 0}})
    return hists

def createDictOfTH1ForTreeSelection(rootTree, branchCategory='clusters', nBins=60):
    
    hists = {}
    fullList = []
    
    # read all branches
    for branch in rootTree.GetListOfBranches():
        fullList.append(branch.GetName())
        
    # do branches sub-selection
    if branchCategory == 'event':
        branchesList = ['event_RunNumber','event_EventNumber','event_BCID','event_Lumiblock','event_avg_mu_inTimePU','event_avg_mu_OOTimePU']
    elif branchCategory == 'clusters':
        branchesList = ['cluster_index','c_electronIndex_clusterLvl','cluster_et','cluster_time','cluster_pt','cluster_eta','cluster_phi']
    elif branchCategory == 'cells':
        branchesList = ['cluster_index_cellLvl','cluster_cell_index','cluster_cell_caloGain','cluster_cell_layer','cluster_cell_region','cluster_cell_energy','cluster_cell_time','cluster_cell_eta','cluster_cell_phi','cluster_cell_deta','cluster_cell_dphi','cluster_cellsDist_dphi','cluster_cellsDist_deta']
        if any('hits' in branch for branch in fullList):
            branchesList.extend(['hits_sampling','hits_clusterIndex_chLvl','hits_clusterChannelIndex','hits_hash','hits_energy','hits_time','hits_sampFrac','hits_energyConv','hits_cellEta','hits_cellPhi'])
    elif branchCategory == 'channels':
        branchesList = ['cluster_index_chLvl','cluster_channel_index','cluster_channel_digits','cluster_channel_energy','cluster_channel_time','cluster_channel_layer','cluster_channel_bad','cluster_channel_chInfo','cluster_channel_hash','cluster_channel_id','cluster_channel_effSigma','cluster_channel_noise','cluster_channel_DSPThreshold','cluster_channel_OFCTimeOffset','cluster_channel_ADC2MeV0','cluster_channel_ADC2MeV1','cluster_channel_pedestal','cluster_channel_OFCa','cluster_channel_OFCb','cluster_channel_MinBiasAvg']
        if any(branch in fullList for branch in ['cluster_channel_OfflineEnergyRescaler','cluster_channel_OfflineHVScale','cluster_channel_shape','cluster_channel_shapeDer']):
            branchesList.extend(['cluster_channel_OfflineEnergyRescaler','cluster_channel_OfflineHVScale','cluster_channel_shape','cluster_channel_shapeDer'])
    elif branchCategory == 'rawChannels':
        branchesList = ['cluster_index_rawChLvl','cluster_rawChannel_index','cluster_rawChannel_id','cluster_rawChannel_amplitude','cluster_rawChannel_time','cluster_rawChannel_layer','cluster_rawChannel_Ped','cluster_rawChannel_Prov','cluster_rawChannel_qual','cluster_rawChannel_chInfo']
        if 'cluster_rawChannel_DSPThreshold' in fullList:
            branchesList.append('cluster_rawChannel_DSPThreshold')
    elif branchCategory == 'particles':
        branchesList = ['vtx_x','vtx_y','vtx_z','vtx_deltaZ0','vtx_delta_z0_sin','vtx_d0sig','el_index','el_Pt','el_et','el_Eta','el_Phi','el_m','el_eoverp','el_f1','el_f3','el_eratio','el_weta1','el_weta2','el_fracs1','el_wtots1','el_e277','el_reta','el_rphi','el_deltae','el_rhad','el_rhad1']
        if any('zee' in branch for branch in fullList):
            branchesList.extend( ['zee_M','zee_E','zee_pt','zee_px','zee_py','zee_pz','zee_T','zee_deltaR'] )
        if any('mc_' in branch for branch in fullList):
            branchesList.extend( ['mc_part_energy','mc_part_pt','mc_part_m','mc_part_eta','mc_part_phi','mc_part_pdgId','mc_part_status','mc_part_barcode','mc_vert_x','mc_vert_y','mc_vert_z','mc_vert_time','mc_vert_perp','mc_vert_eta','mc_vert_phi','mc_vert_barcode','mc_vert_id'] )
    # elif branchCategory == 'hits':
    #         branchesList = ['hits_sampling','hits_clusterIndex_chLvl','hits_clusterChannelIndex','hits_hash','hits_energy','hits_time','hits_sampFrac','hits_energyConv','hits_cellEta','hits_cellPhi']
    elif branchCategory == 'AllBranches':
        branchesList = fullList
    else:
        raise ValueError("branchCategory `{branchCategory}` value not allowed!")
        
    print('Selected branches are from {}, with {} out of total {} branches.'.format(branchCategory, len(branchesList), len(fullList)))
            
    # Deactivate non-selected branches
    # Create Histograms
    rootTree.SetBranchStatus("*", 0)
    for br in branchesList:
        rootTree.SetBranchStatus(br, 1)
        
        hist = root.TH1D(br, br, nBins, -1, -1) # automatic ranges
        hist.SetBuffer(100000)        
        hists.update({"{}".format( br ): {"hist": hist, "min": 0, "max": 0}})

    return hists

def createDictOfTH1ForWholeTree_byHand(rootTree):
    astyle.set_atlas_style()
    
    hists = {}
    for branch in rootTree.GetListOfBranches():
        
        branchName = branch.GetName()
        
        if 'event' in branchName:
            hist = root.TH1I(branch.GetName(), branch.GetName(),60,-1,-1)
            hist.SetBuffer(1000)
        else:
        
            hist = root.TH1D(branch.GetName(), branch.GetName(),60,-1,-1)
            # hist = root.TH1D(branch.GetName())
            hist.SetBuffer(1000000)
        
        hists.update({"{}".format(str(branch.GetName())): {"hist": hist, "min": 0, "max": 0}})
    return hists