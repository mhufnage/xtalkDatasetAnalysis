import sys,json
from glob import glob
from ROOT import TFile, gROOT, TChain
import numpy as np
import math

from getXTDataAsPythonDict import getXTDataAsPythonDictFasterClean
# from auxiliaryFunctions import unpackCellsFromCluster
from helper_lib.auxiliaryFunctions import unpackCellsFromCluster



def ATLAS_NTuple_Reader(confDict, getMLDataset=False ):
    '''
    Read XTalk Studies NTuple.
    
    - The confDict must have the keys unpacked below.
    - The getMLDataset boolean option selects between a full NTuple->npz file conversion, in a dict format,
    and a ML Dataset selection, which get cells from certain sampling, centered on its hottest cell, with an eta/phi
    window of cells around it.
    '''
    
    #
    # Unpack config options
    # 
    total_events            = confDict['total_events']
    isMC                    = confDict['isMC']
    isTagAndProbe           = confDict['isTagAndProbe']
    electronPtCut           = confDict['electronPtCut']
    dumpRawChForMLStudies   = confDict['dumpRawChForMLStudies']
    clusName                = confDict['clusName']
    fileNames               = confDict['fileNames']
    
    if isMC:
        print('The input files are from ATLAS_MC!')

    #
    # ROOT Tree reading
    #
    sTree   = TChain("dumpedData",'')
    sLBTree = TChain("lumiblockData",'')
    
    for file in fileNames:
        sTree.Add(file+"/dumpedData") # main dumped data tree (event by event)
        sLBTree.Add(file+"/lumiblockData") # LB tree (lumiblock per lumiblock)

    if clusName == 'topocluster':
        sTree.SetBranchStatus("cluster711_*",0)
        clusKey = "clusters"
    if clusName == '7x11':
        sTree.SetBranchStatus("cluster_*",0)
        clusKey = '711_roi'

    nEvents         = sTree.GetEntries()
    nLumiBlocks     = sLBTree.GetEntries()
    
    print("{} files were added with {} events.".format(len(fileNames),nEvents))
    
    #
    # Loop over events
    #
    dataset = []
    
    for eventNumber, event in enumerate(sTree):
        if (eventNumber > total_events) and (total_events != -1):
            break
        
        if not (eventNumber % 1000):
            print('event {}...'.format(eventNumber))

        #---------------------------------------------
        #----- Get full dataset as python Dict--------
        #---------------------------------------------
        d = getXTDataAsPythonDictFasterClean(
                                        event,
                                        isMC=isMC, 
                                        verbose=0, 
                                        clusterType=clusName, 
                                        isTagAndProbe=isTagAndProbe)
        if d==0:
            continue
        if not getMLDataset:
            dataset.append(d)
        #---------------------------------------------
        #------- Cluster Dataset Processing ----------
        #---------------------------------------------
        if getMLDataset:
            
            ev              = d['dataset']
            clus_counter    = len(dataset)
            
            for elec in ev['electrons'].keys(): # for each electron in this event...
                if ev['electrons'][elec]['pt'][0]/1000 < electronPtCut:
                    continue
                if np.abs(ev['electrons'][elec]['eta']) > 1.4:
                    continue

                elecClusters    = ev['electrons'][elec][clusKey]
                
                for clus in elecClusters.keys(): # for each cluster associated to that electron...
                    dataset_cluster = ev['electrons'][elec][clusKey][clus]
                    
                    datasetML = unpackCellsFromCluster(dataset_cluster, confDict, isMC=isMC, dumpRawChannels=dumpRawChForMLStudies)
                    
                    if datasetML == 0: # Selected sampling has no cells, or incomplete eta/phi window!
                        continue
                    
                    dataset.append(datasetML)
                    
                    if(clus_counter%500)==0:
                        print('\t{} clusters were dumped so far...(Event index: {})'.format(clus_counter,eventNumber))
                        
            if clus_counter >= total_events:
                return dataset

        
    return dataset

def ATLAS_NTuple_Event_Reader(theEvent, confDict, getMLDataset=False ):
    '''
    Read XTalk Studies NTuple.
    
    - The confDict must have the keys unpacked below.
    - The getMLDataset boolean option selects between a full NTuple->npz file conversion, in a dict format,
    and a ML Dataset selection, which get cells from certain sampling, centered on its hottest cell, with an eta/phi
    window of cells around it.
    '''
    
    #
    # Unpack config options
    # 
    isMC                    = confDict['isMC']
    isTagAndProbe           = confDict['isTagAndProbe']
    electronPtCut           = confDict['electronPtCut']
    dumpRawChForMLStudies   = confDict['dumpRawChForMLStudies']
    clusName                = confDict['clusName']
    
    # if isMC:
    #     print('The input files are from ATLAS_MC!')

    #
    # ROOT Tree reading
    #

    if clusName == 'topocluster':
        clusKey = "clusters"
    if clusName == '7x11':
        clusKey = '711_roi'

    #---------------------------------------------
    #----- Get full dataset as python Dict--------
    #---------------------------------------------
    d = getXTDataAsPythonDictFasterClean(
                                        theEvent,
                                        isMC=isMC, 
                                        verbose=0, 
                                        clusterType=clusName, 
                                        isTagAndProbe=isTagAndProbe)
    if (not getMLDataset) or (d==0):
        return d
    #---------------------------------------------
    #------- Cluster Dataset Processing ----------
    #---------------------------------------------
    else:
        ev              = d['dataset']
        # clus_counter    = len(dataset)
        
        for elec in ev['electrons'].keys(): # for each electron in this event...
            if ev['electrons'][elec]['pt'][0]/1000 < electronPtCut:
                continue
            if np.abs(ev['electrons'][elec]['eta']) > 1.4:
                continue

            elecClusters    = ev['electrons'][elec][clusKey]
            
            for clus in elecClusters.keys(): # for each cluster associated to that electron...
                dataset_cluster = ev['electrons'][elec][clusKey][clus]
                
                datasetML = unpackCellsFromCluster(dataset_cluster, confDict, isMC=isMC, dumpRawChannels=dumpRawChForMLStudies)
                
                return datasetML
