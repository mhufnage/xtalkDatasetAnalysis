# import ROOT
import time

start = time.time()

import numpy as np
from glob import glob
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib

matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS

import json
import os,sys

from auxiliaryFunctions import *
from calibrationFilesHelper import *
# from calibrationFilesHelperROOT import *

# import tensorflow

sys.path.insert(1, '/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/EventReader/share')
# sys.path.insert(1, '/eos/user/m/mhufnage/SWAN_projects/XTalk/')

path        = '/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/EventReader/share/'
fDict       = open(path+'dictCaloByLayer.json')
caloDict    = json.load(fDict)

ofc_dict = np.load('calibration/OFC_dict.npz',allow_pickle=True )['OFC'].tolist()
ped_dict = np.load('calibration/PED_dict.npz',allow_pickle=True )['PED'].tolist()
mpmc_dict = np.load('calibration/MPMC_dict.npz',allow_pickle=True )['MPMC'].tolist()
uamev_dict = np.load('calibration/UAMEV_dict.npz',allow_pickle=True )['UAMEV'].tolist()
ramps_dict = np.load('calibration/RAMPS_dict.npz',allow_pickle=True )['RAMPS'].tolist()

### 

# chid = dataset['dataset'][1]['electrons']['el_0']['clusters']['cl_0'][cellInfoName]['channelId'][0]
import time

start = time.time()
evt_lim = 15

resultFileAlias   = 'timeThres'
el_thrs         = [5,10,15,20] # GeV
nCellsChEta     = 5
nCellsChPhi     = 5
nCellsRawChEta  = 5
nCellsRawChPhi  = 5
clusNames       = ['711_roi', 'clusters'] #'clusters'
calibTypes      = ['database', 'typical'] #'database'#'database' #'typical' # or 
eventsBlockList = [-99999999]
layerTimingEneThr   = [100, 100, 200, 100]

# datasetPath ='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/run/dataMinBias/dataMinBias_part*.npz'
# datasetPath ='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/run/dataMinBias/dataMinBiasHwid_part*.npz'
# datasetPath     =['/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/run/dataMinBias/dataMinBiasHwid_part24.npz']
datasetPath ='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/run/dataMinBias_janFix/dataMinBias_janFix_part*.npz'
fileNames       = glob(datasetPath)#datasetPath

for calibType in calibTypes:
    print('Calibration type: {}'.format(calibType))
    for el_thr in el_thrs:
        print('Electron pt cut: {} GeV'.format(el_thr))
        for clusName in clusNames:
            print('Processing cluster: {}'.format(clusName))
            
            analysis   = ['digitsCluster', 
                        'digitsWindow',
                        'digitsClusterSize', 
                        'digitsWindowSize',
                        'channelEnergyCluster',
                        'channelEnergyWindow',
                        'channelTimeCluster',
                        'channelTimeWindow', 
                        'channelInfoCluster',
                        'channelInfoWindow',
                        'channelIdCluster',
                        'channelIdWindow',
                        'channelGainCluster',
                        'channelGainWindow',
                        'Acluster',
                        'tauCluster',
                        'Awindow',
                        'tauWindow',
                        'ECalibCluster',
                        'ECalibWindow',

                        'amplitudeCluster',
                        'amplitudeWindow',
                        'rawChTimeCluster',
                        'rawChTimeWindow',                  
                        'rawChInfoCluster',
                        'rawChInfoWindow',
                        'rawChIdCluster',
                        'rawChIdWindow',
                        'rawChGainCluster',
                        'rawChGainWindow'
                        ]

            results              = {key:{key1:[] for key1 in analysis} for key in caloDict["Layer"]}
            results[clusName]    = {'pt':[],'eta':[],'phi':[]} # Cluster and ROI data

            for file in fileNames:
                print(file)
                dataset     = np.load(file, allow_pickle=True)

                for evtn, ev in enumerate(dataset['dataset']): # for each event...
                    # print("Event number: {}".format(ev['eventNumber']))
                    if (ev['eventNumber'][0] in eventsBlockList):
                        print('Ignoring blocked event {}.'.format(ev['eventNumber']))
                        continue
                    if (evtn%100) == 0:
                        print("Event {}...".format(evtn))
                    # if evtn > evt_lim:
                    #     break

                    for elec in ev['electrons'].keys(): # for each electron in this event...
                #         print(ev['electrons'][elec]['pt'])
                        if ev['electrons'][elec]['pt'][0]/1000 < el_thr:
                            continue
                        if np.abs(ev['electrons'][elec]['eta']) > 1.4:
                            continue
                        
                        clusloop = ev['electrons'][elec][clusName]

                        for clus in clusloop.keys(): # for each cluster associated to that electron...
                            
                            results[clusName]['pt'].append(ev['electrons'][elec][clusName][clus]['pt'])
                            results[clusName]['eta'].append(ev['electrons'][elec][clusName][clus]['eta'])
                            results[clusName]['phi'].append(ev['electrons'][elec][clusName][clus]['phi'])

                            for layern, layer in enumerate(caloDict["Layer"]):
                            ###################
                            ## Channel + Digits
                            ###################
                                layerIndexes    = getMatchedIndexes(ev['electrons'][elec][clusName][clus]['channels']['sampling'][0], layer) # get list indexes that match to current
                                if layern < 4:
                                    layerEneThr     = layerTimingEneThr[layern]
                                else:
                                    layerEneThr     = 0

                                if len(layerIndexes) != 0:

                                    windowIndexesCh, layerIndexesCh = getCellsInWindowCustomSize(
                                        ev['electrons'][elec][clusName][clus]['channels']['energy'],
                                        ev['electrons'][elec][clusName][clus]['channels']['index'],
                                        ev['electrons'][elec][clusName][clus]['channels']['eta'],
                                        ev['electrons'][elec][clusName][clus]['channels']['phi'],
                                        ev['electrons'][elec][clusName][clus]['channels']['sampling'],
                                        nCellsChEta, 
                                        nCellsChPhi, 
                                        ev['electrons'][elec][clusName][clus]['channels']['deta'],
                                        ev['electrons'][elec][clusName][clus]['channels']['dphi'],
                                        layer=layer)

                #                     print(windowIndexesCh , len(layerIndexesCh))

                                    results[layer]['digitsWindow'].append( np.array(ev['electrons'][elec][clusName][clus]['channels']['digits'][0],dtype=object)[windowIndexesCh] )
                                    results[layer]['digitsCluster'].append( np.array(ev['electrons'][elec][clusName][clus]['channels']['digits'][0],dtype=object)[layerIndexesCh] )
                                    results[layer]['digitsWindowSize'].append(len (np.array(ev['electrons'][elec][clusName][clus]['channels']['digits'][0],dtype=object)[windowIndexesCh]) )
                                    results[layer]['digitsClusterSize'].append(len (np.array(ev['electrons'][elec][clusName][clus]['channels']['digits'][0],dtype=object)[layerIndexesCh]) )
                                    results[layer]['channelEnergyCluster'].append (np.array(ev['electrons'][elec][clusName][clus]['channels']['energy'][0],dtype=object)[layerIndexesCh])
                                    results[layer]['channelEnergyWindow'].append (np.array(ev['electrons'][elec][clusName][clus]['channels']['energy'][0],dtype=object)[windowIndexesCh])
                                    results[layer]['channelTimeCluster'].append (np.array(ev['electrons'][elec][clusName][clus]['channels']['time'][0],dtype=object)[layerIndexesCh])
                                    results[layer]['channelTimeWindow'].append (np.array(ev['electrons'][elec][clusName][clus]['channels']['time'][0],dtype=object)[windowIndexesCh])
                                    results[layer]['channelInfoWindow'].append  (np.array(ev['electrons'][elec][clusName][clus]['channels']['chInfo'][0],dtype=object)[windowIndexesCh])
                                    results[layer]['channelInfoCluster'].append (np.array(ev['electrons'][elec][clusName][clus]['channels']['chInfo'][0],dtype=object)[layerIndexesCh])
                                    results[layer]['channelIdWindow'].append  (np.array(ev['electrons'][elec][clusName][clus]['channels']['channelId'][0],dtype=object)[windowIndexesCh])
                                    results[layer]['channelIdCluster'].append ( np.array(ev['electrons'][elec][clusName][clus]['channels']['channelId'][0],dtype=object)[layerIndexesCh])
                                    results[layer]['channelGainWindow'].append( np.array(ev['electrons'][elec][clusName][clus]['channels']['gain'][0],dtype=object)[windowIndexesCh])
                                    results[layer]['channelGainCluster'].append( np.array(ev['electrons'][elec][clusName][clus]['channels']['gain'][0],dtype=object)[layerIndexesCh])


                                    chid        = np.array(ev['electrons'][elec][clusName][clus]['channels']['channelId'][0],dtype=object)[layerIndexesCh]
                                    digits      = np.array(ev['electrons'][elec][clusName][clus]['channels']['digits'][0],dtype=object)[layerIndexesCh]
                                    gainInt     = getGainInteger(np.array(ev['electrons'][elec][clusName][clus]['channels']['gain'][0],dtype=object)[layerIndexesCh])
                                    sampling    = np.array(ev['electrons'][elec][clusName][clus]['channels']['sampling'][0],dtype=object)[layerIndexesCh].tolist()
                                    etaCell     = np.array(ev['electrons'][elec][clusName][clus]['channels']['eta'][0],dtype=object)[layerIndexesCh].tolist()

                                    Aclus, tauClus, ECalClus = getCalibEnergyCellsFromSamplesInCluster(
                                                                chid,
                                                                digits,
                                                                gainInt,
                                                                sampling,
                                                                etaCell ,
                                                                ofc_dict,
                                                                ped_dict,
                                                                mpmc_dict,
                                                                ramps_dict,
                                                                uamev_dict,
                                                                calibType,
                                                                getAllCalibConst=False,
                                                                timingEneThr=layerEneThr)
                                    # AClus, tauClus, AtauClus, ECalClus, R0Clus, R1Clus, MClus, F_ua2mevClus, F_dac2uaClus = getCalibEnergyCellsFromSamplesInCluster(
                                    #                                                                                             chid,
                                    #                                                                                             digits,
                                    #                                                                                             gainInt,
                                    #                                                                                             sampling,
                                    #                                                                                             etaCell ,
                                    #                                                                                             ofc_dict,
                                    #                                                                                             ped_dict,
                                    #                                                                                             mpmc_dict,
                                    #                                                                                             ramps_dict,
                                    #                                                                                             uamev_dict,
                                    #                                                                                             calibType,
                                    #                                                                                             getAllCalibConst=True)
                                    

                                    #### debug #####

                                    # print('chid: ',chid,
                                    # 'digits: ',digits,
                                    # 'gainInt: ',gainInt,
                                    # 'sampling: ',sampling,
                                    # 'etaCell: ' ,etaCell )
                                    # cell_dict_uamev = getLArChannelUAMEV(chid, uamev_dict, calibType, sampling, etaCell)
                                    # print(cell_dict_uamev)

                                    ################
                                    chid        = np.array(ev['electrons'][elec][clusName][clus]['channels']['channelId'][0],dtype=object)[windowIndexesCh]
                                    digits      = np.array(ev['electrons'][elec][clusName][clus]['channels']['digits'][0],dtype=object)[windowIndexesCh]
                                    gainInt     = getGainInteger(np.array(ev['electrons'][elec][clusName][clus]['channels']['gain'][0],dtype=object)[windowIndexesCh])
                                    sampling    = np.array(ev['electrons'][elec][clusName][clus]['channels']['sampling'][0],dtype=object)[windowIndexesCh].tolist()
                                    etaCell     = np.array(ev['electrons'][elec][clusName][clus]['channels']['eta'][0],dtype=object)[windowIndexesCh].tolist()


                                    Awin, tauWin, ECalWin = getCalibEnergyCellsFromSamplesInCluster(
                                                            chid,
                                                            digits,
                                                            gainInt,
                                                            sampling,
                                                            etaCell,
                                                            ofc_dict,
                                                            ped_dict,
                                                            mpmc_dict,
                                                            ramps_dict,
                                                            uamev_dict,
                                                            calibType,
                                                            getAllCalibConst=False,
                                                            timingEneThr=layerEneThr)

                                    results[layer]['Awindow'].append(Awin)
                                    results[layer]['Acluster'].append(Aclus)
                                    results[layer]['tauWindow'].append(tauWin)
                                    results[layer]['tauCluster'].append(tauClus)
                                    results[layer]['ECalibCluster'].append(ECalClus)
                                    results[layer]['ECalibWindow'].append(ECalWin)

                                ###############
                                ## RawChannel #
                                ###############
                                layerIndexes = getMatchedIndexes(ev['electrons'][elec][clusName][clus]['rawChannels']['sampling'][0], layer)

                                if len(layerIndexes) != 0:

                                    windowIndexesRawCh, layerIndexesRawCh = getCellsInWindowCustomSize(
                                        ev['electrons'][elec][clusName][clus]['rawChannels']['amplitude'],
                                        ev['electrons'][elec][clusName][clus]['rawChannels']['index'],
                                        ev['electrons'][elec][clusName][clus]['rawChannels']['eta'],
                                        ev['electrons'][elec][clusName][clus]['rawChannels']['phi'],
                                        ev['electrons'][elec][clusName][clus]['rawChannels']['sampling'],
                                        nCellsRawChEta, 
                                        nCellsRawChPhi, 
                                        ev['electrons'][elec][clusName][clus]['rawChannels']['deta'],
                                        ev['electrons'][elec][clusName][clus]['rawChannels']['dphi'],
                                        layer=layer)

                #                     print(len(windowIndexesRawCh), len(layerIndexesRawCh))

            #                             rawChClusterSize = len(np.array(ev['electrons'][elec][clusName][clus]['rawChannels']['amplitude'][0],dtype=object)[layerIndexesRawCh])
            #                             rawChWindowSize = len(np.array(ev['electrons'][elec][clusName][clus]['rawChannels']['amplitude'][0],dtype=object)[windowIndexesRawCh])
                                    results[layer]['amplitudeCluster'].append( np.array(ev['electrons'][elec][clusName][clus]['rawChannels']['amplitude'][0],dtype=object)[layerIndexesRawCh])
                                    results[layer]['amplitudeWindow'].append(np.array(ev['electrons'][elec][clusName][clus]['rawChannels']['amplitude'][0],dtype=object)[windowIndexesRawCh])
                                    results[layer]['rawChTimeCluster'].append(np.array(ev['electrons'][elec][clusName][clus]['rawChannels']['time'][0],dtype=object)[layerIndexesRawCh])
                                    results[layer]['rawChTimeWindow'].append(np.array(ev['electrons'][elec][clusName][clus]['rawChannels']['amplitude'][0],dtype=object)[windowIndexesRawCh])
                                    results[layer]['rawChInfoWindow'].append(np.array(ev['electrons'][elec][clusName][clus]['rawChannels']['chInfo'][0],dtype=object)[windowIndexesRawCh] )#chInfo
                                    results[layer]['rawChInfoCluster'].append(np.array(ev['electrons'][elec][clusName][clus]['rawChannels']['chInfo'][0],dtype=object)[layerIndexesRawCh]) #chInfo
                                    results[layer]['rawChIdWindow'].append  (np.array(ev['electrons'][elec][clusName][clus]['rawChannels']['channelId'][0],dtype=object)[windowIndexesRawCh])
                                    results[layer]['rawChIdCluster'].append ( np.array(ev['electrons'][elec][clusName][clus]['rawChannels']['channelId'][0],dtype=object)[layerIndexesRawCh])
        #                             results[layer]['rawChGainWindow'].append( np.array(ev['electrons'][elec][clusName][clus]['rawChannels']['gain'][0],dtype=object)[windowIndexesRawCh])
        #                             results[layer]['rawChGainCluster'].append( np.array(ev['electrons'][elec][clusName][clus]['rawChannels']['gain'][0],dtype=object)[layerIndexesRawCh])


            confDict = {
                'el_thr': el_thr,
                'nCellsChEta': nCellsChEta,
                'nCellsChPhi': nCellsChPhi,
                'nCellsRawChEta': nCellsRawChEta,
                'nCellsRawChPhi': nCellsRawChPhi,
                'datasetPath': datasetPath,
                'calibType': calibType
            }
            
            print(results.keys())
            
        #     np.savez('channelTiming.npz',**timing)
            np.savez('calibration/results_analysisOFC{}_{}_{}_etCut{}.npz'.format(resultFileAlias, clusName,calibType, el_thr),**results)
            np.savez('calibration/conf_analysisOFC{}_{}_{}_etCut{}.npz'.format(resultFileAlias, clusName,calibType, el_thr),conf=confDict)
end = time.time()

print("Total running time: {:.2f} minutes.".format((end - start)/60))