# Lorenzetti Showers Collaboration 2020 - 2023 - https://doi.org/10.1016/j.cpc.2023.108671
print('Starting script for plot: Test best models...')
import sys
sys.path.insert(0,'/home/mhufnage/lorenzetti/Applications') #lorenzetti utils path

## Insert here the path to xtalkDatasetAnalysis git package
sys.path.insert(1, '/home/mhufnage/ATLAS_QT_XTALK/xtalkDatasetAnalysis/')

uprootLibrary = 'np'#'np' #'ak'

import json
import os
import time
import shelve

import numpy as np
from glob import glob
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.colors as mcolors
import matplotlib
# matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS

import scipy.stats as scipy
import torch
import torch.nn as nn
import MLP
from torch.utils.data import Dataset, DataLoader

from collections import OrderedDict
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score

import mplhep as hep

plt.style.use([hep.style.ATLAS])

import uproot

from calibrationFilesHelper import *
from Autoencoder import *
from XTalkDatasetClass import XTalkDataset_LZT, Subset
from auxiliaryFunctions import confInterval, getBins

# --------- Lorenzetti plot style -------
def lorenzettiText(pos='out',subText='     Internal'):
    lorenzetti_title={'fontsize': 21, 'fontfamily': 'sans-serif', 'fontweight':'bold','fontstyle':'italic'}
    lorenzetti_subtitle={'fontsize': 14, 'fontfamily': 'sans-serif', 'fontweight':'normal','fontstyle':'italic'}

    if pos=='out':
        plt.figtext(x=.18   ,y=.945,s='Lorenzetti',fontdict=lorenzetti_title)
        plt.figtext(x=.318  ,y=.945,s=subText,fontdict=lorenzetti_subtitle)
    if pos=='in':
        plt.figtext(x=.18   ,y=.875,s='Lorenzetti',fontdict=lorenzetti_title)
        plt.figtext(x=.318  ,y=.875,s=subText,fontdict=lorenzetti_subtitle)


# --------- Lorenzetti sampling and detector enumeration ---------
class Detector():
  LAR     = 0
  TILE    = 1
  TTEM    = 2
  TTHEC   = 3
  FCALEM  = 5
  FCALHAD = 6

class CaloSampling():
    PSB       = 0
    PSE       = 1
    EMB1      = 2
    EMB2      = 3
    EMB3      = 4
    TileCal1  = 5
    TileCal2  = 6
    TileCal3  = 7
    TileExt1  = 8
    TileExt2  = 9
    TileExt3  = 10
    EMEC1     = 11
    EMEC2     = 12
    EMEC3     = 13
    HEC1      = 14
    HEC2      = 15
    HEC3      = 16
    
# -----------------------------------------------------------------
# -----------------------------------------------------------------
# -----------------------------------------------------------------
startTime=time.time()

## Path to experiment output
experiment  = 'neuLay_1Layer'#'neuLay'
netType     = 'p'

# pathLoss     = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/sampleInSampleOut/'
pathLoss        = '/data/atlas/mhufnage/xtalk/ml_training/supervised_lzt/{}/'.format(experiment)

DS_PathAlias        = '/data/atlas/mhufnage/lzt_single_e/elec_singleCell_10kEvts_centroid_e50_DS{}/AOD/user.mhufnage.lzt.e50.xtalk.DS{}.win3.nopileup.WithCells_EM2.event.npz'
DSnoNoise_PathAlias = '/data/atlas/mhufnage/lzt_single_e/elec_singleCell_10kEvts_centroid_e50_DS{}/AOD_noNoise/user.mhufnage.lzt.e50.xtalk.DS{}.win3.nopileup.WithCells_EM2.event.noNoise.npz'
DS_List       = [21,22,23,24,25,26,27,28,29,30]
data          = []
dataNoNoise   = []
for DS_index in DS_List:
    data.append (np.load(DS_PathAlias.format(DS_index,DS_index), allow_pickle=True)['dataset'])
    dataNoNoise.append (np.load(DSnoNoise_PathAlias.format(DS_index,DS_index), allow_pickle=True)['dataset'])
data = np.concatenate(data).ravel()
dataNoNoise = np.concatenate(dataNoNoise).ravel()
print('Dataset loaded with {} events!'.format(len(data)))


matplotlib.use('Agg') #CHANGES THE MATPLOTLIB BACKEND FOR LOOP PLOTS
# matplotlib.use('module://ipykernel.pylab.backend_inline')  # show plots
print('experiment: {}\npath: {}'.format(experiment, pathLoss))
#
# Dataset split
#
# X_train, X_test = train_test_split(data, test_size=testRatio)
datasetTest             =   XTalkDataset_LZT(data, nEvents=-1, mode='supervised')
datasetTestNoNoise      =   XTalkDataset_LZT(dataNoNoise, nEvents=-1, mode='supervised')
testDataLoader          =   DataLoader( datasetTest, batch_size=1, shuffle=False )
testDataLoaderNoNoise   =   DataLoader( datasetTestNoNoise, batch_size=1, shuffle=False )

nEvts = datasetTest.len

loop_dict    = np.load(pathLoss+'{}_config.npz'.format(experiment), allow_pickle=True)['config'].tolist()

it              = 0
xbins           = 100
k               = 3

epochsStart     = 1
epochsLimit     = 250

plotATLAS_loc   = 2
plotFormat      = 'pdf'#'png'
plotYear        = 2017
labelDistX      = 17

for neuLayTopo in loop_dict['neuLay']:
    
    neuLayText = MLP.neuListToFileText(neuLayTopo)
    print('Topo: {}'.format(neuLayText))
    
    config_train = np.load(pathLoss+'config_train_nn{}_adam_neu{}.npz'.format(netType,neuLayText), allow_pickle=True)['config'].tolist()

    k               = config_train['kFolds']
    neuIn           = config_train['neurons_list']
    neuInText       = config_train['neurons_text']
    max_iter        = config_train['max_init']
    trainRatio      = config_train['train_ratio']
    testRatio       = config_train['test_ratio']
    batch_size      = config_train['batch_size']
    total_clusters  = config_train['total_clusters']
    nCellsWinEta    = config_train['cell_win_eta']
    nCellsWinPhi    = config_train['cell_win_phi']
    optimConfig     = config_train['optimConfig']
    scalerInput     = config_train['scaler_input']
    scalerOutput    = config_train['scaler_output']
    
    nSamples = 5

    for fold in range(k):
        print('\tFold {}'.format(fold))
        filename    = pathLoss+'bestModel_nn{}_fold{}_iter{}_neu{}.pth'.format(netType,fold, it, neuInText)
        
        model       = MLP.MLP_FF(neuronsList=neuIn)
        
        if 'adam' in optimConfig.keys():
            optimAlg = 'adam'
            optimizer   = torch.optim.Adam(model.parameters(), lr=optimConfig[optimAlg]['learningRate'], weight_decay=optimConfig[optimAlg]['weightDecay'])
        bestLossVal = MLP.resumeModel(model, optimizer, filename)

        inputNoNoiseList=[]
        targetNoNoiseList=[]
        targetList   = []
        outputList  = []
        inputList = []
        eneTargetList    = []
        eneOutputList   = []
        tauTargetList    = []
        tauOutputList   = []
        
        for data_input, target, batch_idx in testDataLoaderNoNoise:
            # pass input through the model and generate the output
            # output                  = model(data_input*scalerInput) #normalized input
            # outputScaled            = output*scalerOutput
            # ene_target, tau_target  = datasetTest.calibrate(batch_idx, target) # unormlized input
            # ene_output, tau_output  = datasetTest.calibrate(batch_idx, output*scalerOutput) # denormalized output

            targetNoNoiseList.append( target.detach().numpy()*1/1000 )
            # outputList      .append( outputScaled.detach().numpy()*1/1000 )
            inputNoNoiseList.append( data_input.detach().numpy()*1/1000 )
            # eneTargetList    .append( ene_target.detach().numpy()*1/1000 )
            # eneOutputList   .append( ene_output.detach().numpy()*1/1000 )
            # tauTargetList    .append( tau_target.detach().numpy() )
            # tauOutputList   .append( tau_output.detach().numpy() )

        # ML input values (no noise)
        inputNoNoiseList  = (np.concatenate(inputNoNoiseList).ravel()).tolist()
        targetNoNoiseList = np.concatenate(targetNoNoiseList).ravel().tolist()

        for data_input, target, batch_idx in testDataLoader:
            # pass input through the model and generate the output
            output                  = model(data_input*scalerInput) #normalized input
            outputScaled            = output*scalerOutput
            ene_target, tau_target  = datasetTest.calibrate(batch_idx, target) # unormlized input
            ene_output, tau_output  = datasetTest.calibrate(batch_idx, output*scalerOutput) # denormalized output

            targetList       .append( target.detach().numpy()*1/1000 )
            outputList      .append( outputScaled.detach().numpy()*1/1000 )
            inputList       .append( data_input.detach().numpy()*1/1000 )
            eneTargetList    .append( ene_target.detach().numpy()*1/1000 )
            eneOutputList   .append( ene_output.detach().numpy()*1/1000 )
            tauTargetList    .append( tau_target.detach().numpy() )
            tauOutputList   .append( tau_output.detach().numpy() )

        # ML input values (with XT)
        inputList = (np.concatenate(inputList).ravel()).tolist()
        inputXTTau = (np.concatenate(testDataLoader.dataset.xtt)).ravel().tolist()
        inputXTEne = (np.concatenate(testDataLoader.dataset.xte).ravel()*1/1000).tolist()
         # truth
        truth_ene       = (np.concatenate(testDataLoader.dataset.edep).ravel()*1/1000).tolist()
        truth_tau       = (np.concatenate(testDataLoader.dataset.tof).ravel()).tolist()
        # signal samples
        outputList      = np.concatenate(outputList).ravel().tolist() # flatten the list
        targetList      = np.concatenate(targetList).ravel().tolist()
        errorList       = (np.array(targetList) - np.array(outputList)).tolist()
        # energy
        eneTargetList   = np.concatenate(eneTargetList).ravel().tolist()
        eneOutputList   = np.concatenate(eneOutputList).ravel().tolist()
        eneErrorList    = (np.array(eneTargetList) - np.array(eneOutputList))
        eneErrorTarget  = np.array(truth_ene) - np.array(eneTargetList)
        eneErrorOutput  = np.array(truth_ene) - np.array(eneOutputList)
        # time
        tauTargetList   = np.concatenate(tauTargetList).ravel().tolist()
        tauOutputList   = np.concatenate(tauOutputList).ravel().tolist()
        tauErrorList    = ( np.array(tauTargetList) - np.array(tauOutputList))
        tauErrorTarget  = np.array(truth_tau) - np.array(tauTargetList)
        tauErrorOutput  = np.array(truth_tau) - np.array(tauOutputList)
       
        # reshaped samples
        targetNoNoiseListReshaped = np.array(targetNoNoiseList).reshape(nEvts,nCellsWinEta*nCellsWinPhi,nSamples)
        inputNoNoiseListReshaped = np.array(inputNoNoiseList).reshape(nEvts,nCellsWinEta*nCellsWinPhi,nSamples)
        targetListReshaped = np.array(targetList).reshape(nEvts, nCellsWinEta*nCellsWinPhi, nSamples)
        outputListReshaped = np.array(outputList).reshape(nEvts, nCellsWinEta*nCellsWinPhi, nSamples)
        inputListReshaped = np.array(inputList).reshape(nEvts, nCellsWinEta*nCellsWinPhi, nSamples)
        
        #
        # signal samples (single cell analysis: compare sample by sample)
        #
        for cell in range(0,nCellsWinEta*nCellsWinPhi):
            for samp in range(0, nSamples):
        
                fig = plt.figure(figsize=(30,9))
                
                ax0 = plt.subplot(1,3,1)
                xbins = getBins(-2,50,0.05)
                plt.hist2d(targetListReshaped[:,cell,samp], outputListReshaped[:,cell,samp],bins=[xbins,xbins], cmin=1,norm=mcolors.SymLogNorm(linthresh=0.5, linscale=1, base=10), cmap=plt.cm.jet)
                r2_target_output = r2_score(targetListReshaped[:,cell,samp], outputListReshaped[:,cell,samp])
                # r2_target_truth = r2_score(truth_ene, outputList)
                plt.plot([0, max(max(targetListReshaped[:,cell,samp]), max(outputListReshaped[:,cell,samp]))],[0, max(max(targetListReshaped[:,cell,samp]), max(outputListReshaped[:,cell,samp]))], '--r', label='diagonal\n'+r'  $R^2_{target}=$'+'{:.5f}'.format(r2_target_output))
                plt.legend(loc='lower right',fontsize=16)
                plt.xlabel('Cluster cell[{}] sample[{}] [GeV]'.format(cell+1, samp+1))
                plt.ylabel('Reconstructed cluster cell samples [GeV] 50 MeV/bin')
                plt.colorbar()
                lorenzettiText()
                # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)

                ax0 = plt.subplot(1,3,2)
                xbins = getBins(-2,2,0.05)
                stat_error = 'Target - ML output'+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(errorList),np.std(errorList))
                plt.hist(errorList, bins=xbins, histtype='stepfilled',alpha=0.5, ec='black',fc='yellow', label=stat_error)
                plt.xlabel('Sample reconstruction error [GeV] 50 MeV/bin')
                plt.ylabel('Counts')
                plt.yscale('log')
                lorenzettiText()
                plt.legend()
                # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)
                
                ax0 = plt.subplot(1,3,3)
                xbins = getBins(-2,50,0.5)
                stat_error1 = 'Input XT samples'#+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(inputXTEne),np.std(inputXTEne))
                stat_error2 = 'Target'#+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(eneTargetList),np.std(eneTargetList))
                stat_error3 = 'ML output'#+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(eneOutputList),np.std(eneOutputList))
                # stat_error4 = 'Truth'#+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(truth_ene),np.std(truth_ene))
                plt.hist(inputList, bins=xbins, histtype='stepfilled',alpha=0.3, ec='black',fc='orange', label=stat_error1)
                plt.hist(targetList, bins=xbins, histtype='stepfilled',alpha=0.3, ec='black',fc='red', label=stat_error2)
                plt.hist(targetNoNoiseList, bins=xbins, histtype='step', ec='green', label='Target (No Noise)')
                plt.hist(outputList, bins=xbins, histtype='step', ec='blue', label=stat_error3)
                # plt.hist(truth_ene, bins=xbins, histtype='step', ec='green', label=stat_error4)
                plt.xlabel('All samples from each cell [GeV] 500 MeV/bin')
                plt.ylabel('Counts')
                plt.yscale('log')
                plt.legend()

                plt.savefig(pathLoss+'plots/singleCell_cell{}_sample{}_nn{}_fold{}_iter{}_neu{}_batch{}.png'.format(netType,fold, it, neuInText, batch_size))
                plt.close(fig)
                # plt.show()
        
        
        #
        # signal samples
        #
        fig = plt.figure(figsize=(30,9))
        
        ax0 = plt.subplot(1,3,1)
        xbins = getBins(-2,50,0.05)
        plt.hist2d(targetList, outputList,bins=[xbins,xbins], cmin=1,norm=mcolors.SymLogNorm(linthresh=0.5, linscale=1, base=10), cmap=plt.cm.jet)
        r2_target_output = r2_score(targetList, outputList)
        # r2_target_truth = r2_score(truth_ene, outputList)
        plt.plot([0, max(max(targetList), max(outputList))],[0, max(max(targetList), max(outputList))], '--r', label='diagonal\n'+r'  $R^2_{target}=$'+'{:.5f}'.format(r2_target_output))
        plt.legend(loc='lower right',fontsize=16)
        plt.xlabel('Cluster cell samples [GeV]')
        plt.ylabel('Reconstructed cluster cell samples [GeV] 50 MeV/bin')
        plt.colorbar()
        lorenzettiText()
        # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)

        ax0 = plt.subplot(1,3,2)
        xbins = getBins(-2,2,0.05)
        stat_error = 'Target - ML output'+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(errorList),np.std(errorList))
        plt.hist(errorList, bins=xbins, histtype='stepfilled',alpha=0.5, ec='black',fc='yellow', label=stat_error)
        plt.xlabel('Sample reconstruction error [GeV] 50 MeV/bin')
        plt.ylabel('Counts')
        plt.yscale('log')
        lorenzettiText()
        plt.legend()
        # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)
        
        ax0 = plt.subplot(1,3,3)
        xbins = getBins(-2,50,0.5)
        stat_error1 = 'Input XT samples'#+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(inputXTEne),np.std(inputXTEne))
        stat_error2 = 'Target'#+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(eneTargetList),np.std(eneTargetList))
        stat_error3 = 'ML output'#+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(eneOutputList),np.std(eneOutputList))
        # stat_error4 = 'Truth'#+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(truth_ene),np.std(truth_ene))
        plt.hist(inputList, bins=xbins, histtype='stepfilled',alpha=0.3, ec='black',fc='orange', label=stat_error1)
        plt.hist(targetList, bins=xbins, histtype='stepfilled',alpha=0.3, ec='black',fc='red', label=stat_error2)
        plt.hist(targetNoNoiseList, bins=xbins, histtype='step', ec='green', label='Target (No Noise)')
        plt.hist(outputList, bins=xbins, histtype='step', ec='blue', label=stat_error3)
        # plt.hist(truth_ene, bins=xbins, histtype='step', ec='green', label=stat_error4)
        plt.xlabel('All samples from each cell [GeV] 500 MeV/bin')
        plt.ylabel('Counts')
        plt.yscale('log')
        plt.legend()
        # ax0 = plt.subplot(1,3,3)
        # xbins = getBins(-5,5,0.05)
        # plt.hist2d(errorList, targetList ,bins=[xbins,xbins], cmin=1,norm=mcolors.SymLogNorm(linthresh=0.5, linscale=1, base=10), cmap=plt.cm.jet)
        # plt.xlabel('Cluster cell samples [GeV]')
        # plt.ylabel('Sample reconstruction error [GeV]')
        # plt.colorbar()
        # lorenzettiText()
        # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)       

        # hep.save_variations(fig, pathLoss+'scatterBestModel_samples_fold{}_iter{}_neuIn{}_nLay{}_bn{}_batch{}.{}'.format(nFold, it, neuIn, nLay, bn, batch_size, plotFormat),text_list=['Preliminary', 'Work in progress'])
        plt.savefig(pathLoss+'plots/scatterBestModel_samples_nn{}_fold{}_iter{}_neu{}_batch{}.png'.format(netType,fold, it, neuInText, batch_size))
        plt.close(fig)
        # plt.show()

        #
        # Energy
        #
        fig = plt.figure(figsize=(30,9))
        xbins = getBins(-2,50,0.1)
        ax0 = plt.subplot(1,3,1)
        plt.hist2d(eneTargetList, eneOutputList,bins=[xbins,xbins], cmin=1,norm=mcolors.SymLogNorm(linthresh=0.5, linscale=1, base=10), cmap=plt.cm.jet)
        r2_target_output = r2_score(eneTargetList, eneOutputList)
        r2_target_truth = r2_score(truth_ene, eneOutputList)
        plt.plot([0, max(max(eneTargetList), max(eneOutputList))],[0, max(max(eneTargetList), max(eneOutputList))], '--r', label='diagonal\n'+r'  $R^2_{target}=$'+'{:.5f}'.format(r2_target_output)+'\n'+r'  $R^2_{target}=$'+'{:.5f}'.format(r2_target_output) )
        plt.legend(loc='lower right')
        plt.xlabel('Cluster cell energy [GeV]', labelpad=labelDistX)
        plt.ylabel('Reconstructed cluster cell energy [GeV] 50 MeV/bin')
        plt.colorbar()
        # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)
        lorenzettiText()

        ax0 = plt.subplot(1,3,2)
        xbins = getBins(-1.0,1.0,0.025)
        stat_error1 = 'Target - ML output'+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(eneErrorList),np.std(eneErrorList))
        stat_error2 = 'Truth - ML output'+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(eneErrorOutput),np.std(eneErrorOutput))
        stat_error3 = 'Truth - Target'+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(eneErrorTarget),np.std(eneErrorTarget))
        plt.hist(eneErrorList, bins=xbins, histtype='step',ec='black', label=stat_error1)
        plt.hist(eneErrorOutput, bins=xbins, histtype='step', ec='blue', label=stat_error2)
        plt.hist(eneErrorTarget, bins=xbins, histtype='step', ec='green', label=stat_error3)
        plt.xlabel('Energy reconstruction error [GeV] 50MeV/bin', labelpad=labelDistX)
        plt.ylabel('Counts')
        plt.yscale('log')
        plt.legend()
        # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)
        lorenzettiText()
        
        ax0 = plt.subplot(1,3,3)
        xbins = getBins(-2,50,0.5)
        stat_error1 = 'Input XT energy'#+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(inputXTEne),np.std(inputXTEne))
        stat_error2 = 'Target'#+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(eneTargetList),np.std(eneTargetList))
        stat_error3 = 'ML output'#+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(eneOutputList),np.std(eneOutputList))
        stat_error4 = 'Truth'#+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(truth_ene),np.std(truth_ene))
        plt.hist(inputXTEne, bins=xbins, histtype='stepfilled',alpha=0.3, ec='black',fc='orange', label=stat_error1)
        plt.hist(eneTargetList, bins=xbins, histtype='stepfilled',alpha=0.3, ec='black',fc='red', label=stat_error2)
        plt.hist(eneOutputList, bins=xbins, histtype='step', ec='blue', label=stat_error3)
        plt.hist(truth_ene, bins=xbins, histtype='step', ec='green', label=stat_error4)
        plt.xlabel('Reconstructed Energy [GeV] 100MeV/bin', labelpad=labelDistX)
        plt.ylabel('Counts')
        plt.yscale('log')
        plt.legend()
        # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)
        lorenzettiText()

        # ax0 = plt.subplot(1,3,3)
        # plt.hist2d(eneTargetList, eneErrorList,bins=[xbins,xbins], cmin=1,norm=mcolors.SymLogNorm(linthresh=0.5, linscale=1, base=10), cmap=plt.cm.jet)
        # plt.xlabel('Cluster cell energy [GeV]', labelpad=labelDistX)
        # plt.ylabel('Energy reconstruction error [GeV]')
        # plt.grid()
        # plt.colorbar()
        # # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)
        # lorenzettiText()

        # ax0 = plt.subplot(1,4,4)
        # plt.hist2d(eneOutputList, eneErrorList,bins=[xbins,xbins], cmin=1,norm=mcolors.SymLogNorm(linthresh=0.5, linscale=1, base=10), cmap=plt.cm.jet)
        # plt.xlabel('Reconstructed cluster cell energy [GeV]', labelpad=labelDistX)
        # plt.ylabel('Energy reconstruction error [GeV]')
        # # plt.ylabel('Output - Input samples [ADC]')
        # plt.grid()
        # plt.colorbar()
        # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)
        
        plt.savefig(pathLoss+'plots/scatterBestModel_energy_nn{}_fold{}_iter{}_neu{}_batch{}.png'.format(netType,fold, it, neuInText, batch_size))
        # hep.save_variations(fig, pathLoss+'scatterBestModel_energy_fold{}_iter{}_neuIn{}_nLay{}_bn{}_batch{}.{}'.format(nFold, it, neuIn, nLay, bn, batch_size, plotFormat),text_list=['Preliminary', 'Work in progress'])
        plt.close(fig)
        # plt.show()

        #
        # Time
        #
        fig = plt.figure(figsize=(30,9))
        
        xbins = getBins(-15,15,0.5)
        ax0 = plt.subplot(1,3,1)
        plt.hist2d(tauTargetList, tauOutputList,bins=[xbins,xbins], cmin=1,norm=mcolors.SymLogNorm(linthresh=0.5, linscale=1, base=10), cmap=plt.cm.jet)
        plt.plot([0, max(max(tauTargetList), max(tauOutputList))],[0, max(max(tauTargetList), max(tauOutputList))], '--r', label='diagonal')
        plt.legend(loc='lower right')
        plt.xlabel('Cluster cell time [ns]', labelpad=labelDistX)
        plt.ylabel('Reconstructed cluster cell time [ns] 500ps/bin')
        plt.colorbar()
        # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)

        ax0 = plt.subplot(1,3,2)
        xbins = getBins(-15,15,0.5)
        stat_error1 = 'Target - ML output'+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(tauErrorList),np.std(tauErrorList))
        stat_error2 = 'Truth - ML output'+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(tauErrorOutput),np.std(tauErrorOutput))
        stat_error3 = 'Truth - Target'+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(tauErrorTarget),np.std(tauErrorTarget))
        plt.hist(tauErrorList, bins=xbins, histtype='stepfilled',alpha=0.5, ec='black',fc='red', label=stat_error1)
        plt.hist(tauErrorOutput, bins=xbins, histtype='step', ec='blue', label=stat_error2)
        plt.hist(tauErrorTarget, bins=xbins, histtype='step', ec='green', label=stat_error3)
        plt.xlabel('Time reconstruction error [ns] 500ps/bin', labelpad=labelDistX)
        plt.ylabel('Counts')
        plt.yscale('log')
        plt.legend()
        # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)

        ax0 = plt.subplot(1,3,3)
        xbins = getBins(-15,15,0.5)
        stat_error1 = 'Input XT Time'+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(inputXTTau),np.std(inputXTTau))
        stat_error2 = 'Target'+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(tauTargetList),np.std(tauTargetList))
        stat_error3 = 'ML output'+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(tauOutputList),np.std(tauOutputList))
        stat_error4 = 'Truth'+'\n'+r'$\mu={:.3}, \sigma={:.3}$'.format(np.mean(truth_tau),np.std(truth_tau))
        plt.hist(inputXTTau, bins=xbins, histtype='stepfilled',alpha=0.3,fc='blue', label=stat_error1)
        plt.hist(tauTargetList, bins=xbins, histtype='stepfilled',alpha=0.3, ec='black',fc='red', label=stat_error2)
        plt.hist(tauOutputList, bins=xbins, histtype='step', ec='blue', label=stat_error3)
        plt.hist(truth_tau, bins=xbins, histtype='step', ec='green', label=stat_error4)
        # plt.hist2d(tauTargetList, tauErrorList,bins=[xbins,xbins], cmin=1,norm=mcolors.SymLogNorm(linthresh=0.5, linscale=1, base=10), cmap=plt.cm.jet)
        plt.xlabel('Cluster cell time [ns] 500ps/bin', labelpad=labelDistX)
        plt.ylabel('Counts')
        plt.yscale('log')
        plt.grid()
        plt.legend()
        # plt.colorbar()
        # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)

        # ax0 = plt.subplot(1,4,4)
        # plt.hist2d(tauOutputList, tauErrorList,bins=[xbins,xbins], cmin=1,norm=mcolors.SymLogNorm(linthresh=0.5, linscale=1, base=10), cmap=plt.cm.jet)
        # plt.xlabel('Reconstructed cluster cell time [ns]', labelpad=labelDistX)
        # plt.ylabel('Time reconstruction error [ns]')
        # # plt.ylabel('Output - Input samples [ADC]')
        # plt.grid()
        # plt.colorbar()
        # hep.atlas.label(data=True,paper=False,year=plotYear, loc=plotATLAS_loc)
        plt.savefig(pathLoss+'plots/scatterBestModel_time_nn{}_fold{}_iter{}_neu{}_batch{}.png'.format(netType,fold, it, neuInText, batch_size))
        # hep.save_variations(fig, pathLoss+'scatterBestModel_time_fold{}_iter{}_neuIn{}_nLay{}_bn{}_batch{}.{}'.format(nFold, it, neuIn, nLay, bn, batch_size, plotFormat),text_list=['Preliminary', 'Work in progress'])
        plt.close(fig)
        # plt.show()

print('Script finished.')
endTime=time.time()

print('Total time: {:.2f} min ({:.2f} s)'.format((endTime-startTime)/60 , endTime-startTime))